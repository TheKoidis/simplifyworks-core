# What is SimplifyWorks?

Simplifyworks is an open source platform providing a bunch of ready made components with main focus on making your document workflow really simple. You can read more about it on the [project website](http://www.simplifyworks.org).

# How to start using SimplifyWorks framework

## Clone all needed repositories
* **SimplifyWorks parent** - maven parent of all following submodules with common dependencies
    * Clone `git@bitbucket.org:simplifyworks/simplifyworks-parent.git`
* **SimplifyWorks core** - jar module with base framework functionality (backend java and react UI components) 
    * Clone `git@bitbucket.org:simplifyworks/simplifyworks-core.git`
* **SimplifyWorks test** - for running junit tests
    * Clone `git@bitbucket.org:simplifyworks/simplifyworks-test.git`
* **SimplifyWorks module example** - example of jar module with some business functionality   
    * Clone `git@bitbucket.org:simplifyworks/simplifyworks-module-example.git`
* **SimplifyWorks app example** - war module that links core framework and business module(s) together - final war application
    * Clone `git@bitbucket.org:simplifyworks/simplifyworks-app-example.git`

## Install Tomcat 8 and JDK 8
* Configure jndi datasource in tomcat (conf/context.xml):
```xml    
	<Resource name="jdbc/simplify"
            auth="Container"
            type="javax.sql.DataSource"
            username="username"
            password="password"
            driverClassName="com.mysql.jdbc.Driver"
            url="jdbc:mysql://url.to.db/dbname?useUnicode=true&amp;characterEncoding=UTF-8&amp;autoReconnect=true"
            maxActive="8"
            maxIdle="4"/>    
```
* Add jdbc connector to tomcat

## Install NodeJs

Download and install NodeJS from [nodejs.org](https://nodejs.org/)

## Development using IDE (Netbeans or Eclipse)
1. open all 5 projects in netbeans (this manual describes version 8.1)
2. check maven version (at least 3.1 is required)
3. switch maven profile for _SimplifyWorks App Example_ to "devel"
3. configure application url and port of tomcat server in _application.properties_ (in _SimplifyWorks App Example/src/main/resources/_)
	* in case you want to run application with clean db schema you have to set flyway.core.baselineOnMigrate=true, flyway.appexample.baselineOnMigrate=true, flyway.moduleexample.baselineOnMigrate=true (then application will create all db tables on start, for next run you can set this properties back to false)
4. prepare application example project and server
    * Netbeans
        * in _run_ tab - select installed tomcat 8 
        * in _actions_ tab - for actions **Clean**, **Build**, **Clean and build** and **Build with dependencies** add maven property **maven.devel=true** (disables some maven tasks that are run when building production war, build is then faster)
    * Eclipse
        * create server (and server runtime) using installed tomcat 8
        * in server settings set _custom location_
5. build (in this order):
    * parent
    * core
    * test
    * module example
    * application example (with selected profile = _default_)
6. run `npm install` in 
    * _SimplifyWorks Core/src/main/resources/_
    * _SimplifyWorks Module Example/src/main/resources/_
    * _SimplifyWorks App Example/src/main/webapp/_
7. configure _link-target.bat_ (or _link-target.sh_) in root folder of SimplifyWorks App Example - edit symlink path to bussiness module and core
    * Eclipse - also change the location in section scripts in _local/package.json_ to something like _"start": "watchify -o ../../deploy/wtpwebapps/simplifyworks-app-example/js/bundle.js -v -d main.jsx"_
8. run this batch (or sh) file and let it running (it creates symlinks and creates one bundle.js with all needed jsx packed)
9. after running batch file you can run (or debug) web application (project _Simplifyworks Application Example_)
10. bundle.js is now automatically packed in case of change in any jsx file - only refresh of page in browser is needed
11. you have to run this batch file before running web application othervise tomcat cannot see this file
