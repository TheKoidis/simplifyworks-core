insert into core_organization (id, abbreviation, code, name, version) values (1, 'demo organization', '99999999999999', 'demo organization', 0);
update core_user_role set core_organization_id=1 where core_organization_id is null;
alter table core_user_role modify core_organization_id bigint(18) unsigned not null;
