CREATE TABLE `core_flat_role` (
  `id` bigint(18) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `creator` varchar(50) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) DEFAULT NULL,
  `ownee_role_id` bigint(20) NOT NULL,
  `owner_role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_OWNEE_ROLE` (`ownee_role_id`),
  KEY `FK_OWNER_ROLE` (`owner_role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;