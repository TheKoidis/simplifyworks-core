-- SIFY-323: mandatory created and creator attributes
update core_app_setting set created=coalesce(modified,sysdate()) where created is null;
update core_app_setting set creator='SIFY-323' where creator is null;
alter table core_app_setting modify created datetime not null;
alter table core_app_setting modify creator varchar(50) not null;

update core_attachment set created=coalesce(modified,sysdate()) where created is null;
update core_attachment set creator='SIFY-323' where creator is null;
alter table core_attachment modify created datetime not null;
alter table core_attachment modify creator varchar(50) not null;

update core_print_module set created=coalesce(modified,sysdate()) where created is null;
update core_print_module set creator='SIFY-323' where creator is null;
alter table core_print_module modify created datetime not null;
alter table core_print_module modify creator varchar(50) not null;

update core_email_record set created=coalesce(modified,sysdate()) where created is null;
update core_email_record set creator='SIFY-323' where creator is null;
alter table core_email_record modify created datetime not null;
alter table core_email_record modify creator varchar(50) not null;

update core_template set created=coalesce(modified,sysdate()) where created is null;
update core_template set creator='SIFY-323' where creator is null;
alter table core_template modify created datetime not null;
alter table core_template modify creator varchar(50) not null;

update core_organization_ext set created=coalesce(modified,sysdate()) where created is null;
update core_organization_ext set creator='SIFY-323' where creator is null;
alter table core_organization_ext modify created datetime not null;
alter table core_organization_ext modify creator varchar(50) not null;

update core_person_ext set created=coalesce(modified,sysdate()) where created is null;
update core_person_ext set creator='SIFY-323' where creator is null;
alter table core_person_ext modify created datetime not null;
alter table core_person_ext modify creator varchar(50) not null;

update core_person_organization_ext set created=coalesce(modified,sysdate()) where created is null;
update core_person_organization_ext set creator='SIFY-323' where creator is null;
alter table core_person_organization_ext modify created datetime not null;
alter table core_person_organization_ext modify creator varchar(50) not null;

update core_organization set created=coalesce(modified,sysdate()) where created is null;
update core_organization set creator='SIFY-323' where creator is null;
alter table core_organization modify created datetime not null;
alter table core_organization modify creator varchar(50) not null;

update core_permission set created=coalesce(modified,sysdate()) where created is null;
update core_permission set creator='SIFY-323' where creator is null;
alter table core_permission modify created datetime not null;
alter table core_permission modify creator varchar(50) not null;

update core_person set created=coalesce(modified,sysdate()) where created is null;
update core_person set creator='SIFY-323' where creator is null;
alter table core_person modify created datetime not null;
alter table core_person modify creator varchar(50) not null;

update core_person_organization set created=coalesce(modified,sysdate()) where created is null;
update core_person_organization set creator='SIFY-323' where creator is null;
alter table core_person_organization modify created datetime not null;
alter table core_person_organization modify creator varchar(50) not null;

update core_role set created=coalesce(modified,sysdate()) where created is null;
update core_role set creator='SIFY-323' where creator is null;
alter table core_role modify created datetime not null;
alter table core_role modify creator varchar(50) not null;

update core_role_composition set created=coalesce(modified,sysdate()) where created is null;
update core_role_composition set creator='SIFY-323' where creator is null;
alter table core_role_composition modify created datetime not null;
alter table core_role_composition modify creator varchar(50) not null;

update core_user set created=coalesce(modified,sysdate()) where created is null;
update core_user set creator='SIFY-323' where creator is null;
alter table core_user modify created datetime not null;
alter table core_user modify creator varchar(50) not null;

update core_user_role set created=coalesce(modified,sysdate()) where created is null;
update core_user_role set creator='SIFY-323' where creator is null;
alter table core_user_role modify created datetime not null;
alter table core_user_role modify creator varchar(50) not null;
commit;
