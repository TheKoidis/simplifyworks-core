-- Default roles
insert into core_role (created, creator, version, name, description)
  values (curdate(), 'system', '0', 'admin', 'administrator');
insert into core_role (created, creator, version, name, description)
  values (curdate(), 'system', '0', 'admin_scheduler', 'scheduling administrator');

-- Admin user with prson, roles (password is "defaultpassword")
insert into core_person (created, creator, version, surname)
  values(curdate(), 'system', '0', 'admin');
insert into core_user (created, creator, version, password, username, core_person_id)
  values (curdate(), 'system', '0', '$2a$10$1Jy.xKaSDe.d5eGN0AUzz.p0ASZmxLo/mvR/h0SY.T5miMc9kZg/m', 'admin', (select id from core_person where surname = 'admin'));

insert into core_user_role (created, creator, version, core_user_id, core_role_id)
  values (curdate(), 'system', '0', (select id from core_user where username = 'admin'), (select id from core_role where name = 'admin'));
insert into core_user_role (created, creator, version, core_user_id, core_role_id)
  values (curdate(), 'system', '0', (select id from core_user where username = 'admin'), (select id from core_role where name = 'admin_scheduler'));

commit;
