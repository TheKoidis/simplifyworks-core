CREATE TABLE `core_flat_organization` (
  `id` bigint(18) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `creator` varchar(50) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) DEFAULT NULL,
  `ownee_org_id` bigint(20) NOT NULL,
  `owner_org_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_OWNEE_ORG` (`ownee_org_id`),
  KEY `FK_OWNER_ORG` (`owner_org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;