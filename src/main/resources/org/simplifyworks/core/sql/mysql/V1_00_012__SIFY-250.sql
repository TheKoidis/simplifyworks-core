DROP TABLE `core_attachment`;

CREATE TABLE `core_attachment` (
  `id` bigint(18) unsigned NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `creator` varchar(50) NOT NULL,
  `modified` datetime DEFAULT NULL,
  `modifier` varchar(50) DEFAULT NULL,
  `mimetype` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `filesize` bigint(18) unsigned NOT NULL,
  `guid` varchar(36) NOT NULL,
  `uploaded` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) engine=innodb default charset=utf8 collate utf8_bin;

