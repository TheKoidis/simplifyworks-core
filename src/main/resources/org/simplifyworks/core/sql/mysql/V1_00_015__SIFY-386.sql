ALTER TABLE core_person_organization COMMENT 'Pracovne pravni vztah';
ALTER TABLE core_person_organization CHANGE category category varchar(50) COMMENT 'Typ prace';
ALTER TABLE core_person_organization CHANGE obligation obligation decimal(3,2) unsigned COMMENT 'Typ uvazku';
ALTER TABLE core_person_organization CHANGE core_organization_id core_organization_id bigint(18) unsigned not null COMMENT 'ID organizace'; 
ALTER TABLE core_person_organization CHANGE core_person_id core_person_id bigint(18) unsigned not null COMMENT 'ID osoby';
ALTER TABLE core_person COMMENT 'Osoba';
ALTER TABLE core_person CHANGE note note varchar(200) COMMENT 'Poznamka';
ALTER TABLE core_person CHANGE active active tinyint(1) unsigned not null default 1 COMMENT 'Je aktivni';
ALTER TABLE core_person CHANGE unit unit bigint(18) COMMENT 'Ekonomicka jednotka (EKJ)';
ALTER TABLE core_user COMMENT 'Uzivatel';
ALTER TABLE core_user CHANGE core_person_id core_person_id bigint(18) unsigned COMMENT 'ID osoby';
ALTER TABLE core_organization COMMENT 'Organizace';
ALTER TABLE core_organization CHANGE abbreviation abbreviation varchar(50) COMMENT 'Zkratka organizace';
ALTER TABLE core_organization CHANGE code code varchar(50) COMMENT 'Kod organizace';
ALTER TABLE core_organization CHANGE parent_id parent_id bigint(18) unsigned COMMENT 'ID rodicovske organizace';
ALTER TABLE core_organization CHANGE unit unit bigint(18) unsigned COMMENT 'Ekonomicka jednotka (EKJ)';
ALTER TABLE core_role CHANGE description description varchar(255) COMMENT 'Popis role';
ALTER TABLE core_user_role COMMENT 'Vztah role, uzivatele a organizace';
ALTER TABLE core_user_role CHANGE core_organization_id core_organization_id bigint(18) unsigned not null COMMENT 'ID organizace';
ALTER TABLE core_user_role CHANGE core_role_id core_role_id bigint(18) unsigned not null COMMENT 'ID role';
ALTER TABLE core_user_role CHANGE core_user_id core_user_id bigint(18) unsigned not null COMMENT 'ID uzivatele';
ALTER TABLE core_attachment CHANGE guid guid varchar(36) not null COMMENT 'Jednoznacny identifikator prilohy';
ALTER TABLE core_attachment CHANGE mimetype mimetype varchar(255) not null COMMENT 'Typ prilohy';
ALTER TABLE core_flat_organization CHANGE owner_org_id owner_org_id bigint(20) NOT NULL COMMENT 'ID vlastnika';
ALTER TABLE core_flat_organization CHANGE ownee_org_id ownee_org_id bigint(20) NOT NULL COMMENT 'ID vlastnence (muze byt shodny s vlastnikem)';
ALTER TABLE core_flat_role CHANGE owner_role_id owner_role_id bigint(20) NOT NULL COMMENT 'ID nadrazene role';
ALTER TABLE core_flat_role CHANGE ownee_role_id ownee_role_id bigint(20) NOT NULL COMMENT 'ID podrazene role (muze bys shodna s nadrazenou)';
ALTER TABLE core_organization_ext CHANGE core_organization_id core_organization_id bigint(18) unsigned COMMENT 'ID organizace'; 
ALTER TABLE core_person_organization_ext CHANGE core_person_organization_id core_person_organization_id bigint(18) unsigned COMMENT 'ID tabulky core_person_organization';
ALTER TABLE core_print_module CHANGE object_type object_type varchar(128) not null COMMENT 'Typ dto'; 
ALTER TABLE core_print_module CHANGE wf_state wf_state varchar(255) COMMENT 'Stav workflow'; 
ALTER TABLE core_role_composition CHANGE sub_role_id sub_role_id bigint(18) unsigned not null COMMENT 'Id role potomka'; 
ALTER TABLE core_role_composition CHANGE superior_role_id superior_role_id bigint(18) unsigned not null COMMENT 'Id rodicovske role'; 

ALTER TABLE core_person DROP COLUMN addressing_phrase;


