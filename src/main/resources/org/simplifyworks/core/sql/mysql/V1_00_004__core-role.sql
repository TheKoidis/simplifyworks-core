insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_organization_write', 'edit organizations');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_organization_read', 'read organizations');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_organization_delete', 'delete organizations');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_organization_write'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_organization_read'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_organization_delete'));

insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_person_write', 'edit people');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_person_read', 'read people');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_person_delete', 'delete people');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_write'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_read'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_delete'));

insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_person_organization_write', 'edit person-organization relations');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_person_organization_read', 'read person-organization relations');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_person_organization_delete', 'delete person-organization relations');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_organization_write'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_organization_read'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_organization_delete'));

insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_role_write', 'edit roles');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_role_read', 'read roles');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_role_delete', 'delete roles');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_write'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_read'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_delete'));

insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_role_composition_write', 'edit role compositions');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_role_composition_read', 'read role compositions');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_role_composition_delete', 'delete role compositions');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_composition_write'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_composition_read'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_composition_delete'));

insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_user_write', 'edit users');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_user_read', 'read users');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_user_delete', 'delete users');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_write'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_read'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_delete'));

insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_user_role_write', 'edit user-role relations');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_user_role_read', 'read user-role relations');
insert into core_role (created, creator, version, name, description) values (curdate(), 'system', '0', 'core_user_role_delete', 'delete user-role relations');

insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_role_write'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_role_read'));
insert into core_role_composition (created, creator, version, superior_role_id, sub_role_id) values (curdate(), 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_role_delete'));

