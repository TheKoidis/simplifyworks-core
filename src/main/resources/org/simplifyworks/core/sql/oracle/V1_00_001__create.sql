--WHENEVER SQLERROR EXIT FAILURE;
--------------------------------------------------------
--  DDL for Sequence HIBERNATE_SEQUENCE
--------------------------------------------------------

   CREATE SEQUENCE  "HIBERNATE_SEQUENCE"  MINVALUE 1 MAXVALUE 9999999999999999999999999999 INCREMENT BY 1 START WITH 1 CACHE 20 NOORDER  NOCYCLE ;

--------------------------------------------------------
--  DDL for Table CORE_APP_SETTING
--------------------------------------------------------

  CREATE TABLE "CORE_APP_SETTING"
   (	"ID" NUMBER(18,0),
	"SETTING_KEY" VARCHAR2(255 CHAR),
	"VALUE" VARCHAR2(2048 CHAR),
	"SYSTEM" NUMBER(1,0) DEFAULT 0,
	"MODIFIED" DATE,
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );

   COMMENT ON COLUMN "CORE_APP_SETTING"."SYSTEM" IS ' 0 - can be changed; 1 - system record, can not be changed';
--------------------------------------------------------
--  DDL for Table CORE_ATTACHMENT
--------------------------------------------------------

  CREATE TABLE "CORE_ATTACHMENT"
   (	"ID" NUMBER(18,0),
	"ATTACHMENT_TYPE" VARCHAR2(50 CHAR),
	"CONTENT_GUID" VARCHAR2(36 CHAR),
	"CONTENT_PATH" VARCHAR2(512 CHAR),
	"DESCRIPTION" VARCHAR2(512 CHAR),
	"ENCODING" VARCHAR2(100 CHAR),
	"FILESIZE" NUMBER(18,0),
	"MIMETYPE" VARCHAR2(255 CHAR),
	"NAME" VARCHAR2(255 CHAR),
	"OBJECT_IDENTIFIER" VARCHAR2(100 CHAR),
	"OBJECT_STATUS" VARCHAR2(50 CHAR),
	"OBJECT_TYPE" VARCHAR2(128 CHAR),
	"VERSION_LABEL" VARCHAR2(10 CHAR),
	"VERSION_NUMBER" NUMBER(10,0),
	"NEXT_VERSION_ID" NUMBER(18,0),
	"PARENT_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_EMAIL_RECORD
--------------------------------------------------------

  CREATE TABLE "CORE_EMAIL_RECORD"
   (	"ID" NUMBER(18,0),
	"BCC" VARCHAR2(512 CHAR),
	"CC" VARCHAR2(512 CHAR),
	"EMAIL_FROM" VARCHAR2(255 CHAR),
	"ENCODING" VARCHAR2(25 CHAR),
	"MESSAGE" CLOB,
	"RECIPIENTS" VARCHAR2(512 CHAR),
	"SENT" DATE,
	"SENT_LOG" VARCHAR2(2000 CHAR),
	"SUBJECT" VARCHAR2(255 CHAR),
	"PARENT_ID" NUMBER(18,0),
	"PREVIOUS_VERSION" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_ORGANIZATION
--------------------------------------------------------

  CREATE TABLE "CORE_ORGANIZATION"
   (	"ID" NUMBER(18,0),
	"ABBREVIATION" VARCHAR2(50 CHAR),
	"CODE" VARCHAR2(50 CHAR),
	"NAME" VARCHAR2(100 CHAR),
	"PARENT_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_PERMISSION
--------------------------------------------------------

  CREATE TABLE "CORE_PERMISSION"
   (	"ID" NUMBER(18,0),
	"DISCRIMINATOR" VARCHAR2(31 CHAR),
	"ACTIVE" NUMBER(1,0),
	"CAN_DELETE" NUMBER(1,0),
	"CAN_READ" NUMBER(1,0),
	"CAN_WRITE" NUMBER(1,0),
	"OBJECT_IDENTIFIER" VARCHAR2(100 CHAR),
	"OBJECT_TYPE" VARCHAR2(150 CHAR),
	"PERMISSION_FOR" VARCHAR2(255 CHAR),
	"WF_ACTION_TYPE" VARCHAR2(255 CHAR) default 'NONE',
	"WF_BUTTON_KEY" VARCHAR2(255 CHAR),
	"WF_DEFINITION_ID" VARCHAR2(255 CHAR),
	"WF_USER_TASK" VARCHAR2(255 CHAR),
	"CORE_ORGANIZATION_ID" NUMBER(18,0),
	"CORE_ROLE_ID" NUMBER(18,0),
	"CORE_USER_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_PERSON
--------------------------------------------------------

  CREATE TABLE "CORE_PERSON"
   (	"ID" NUMBER(18,0),
	"ADDRESSING_PHRASE" VARCHAR2(100 CHAR),
	"EMAIL" VARCHAR2(255 CHAR),
	"FIRSTNAME" VARCHAR2(100 CHAR),
	"NOTE" VARCHAR2(200 CHAR),
	"PERSONAL_NUMBER" VARCHAR2(255 CHAR),
	"PHONE" VARCHAR2(30 CHAR),
	"SURNAME" VARCHAR2(100 CHAR),
	"TITLE_AFTER" VARCHAR2(100 CHAR),
	"TITLE_BEFORE" VARCHAR2(100 CHAR),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_PERSON_ORGANIZATION
--------------------------------------------------------

  CREATE TABLE "CORE_PERSON_ORGANIZATION"
   (	"ID" NUMBER(18,0),
	"CATEGORY" VARCHAR2(50 CHAR),
	"OBLIGATION" NUMBER(3,2),
	"VALID_FROM" DATE,
	"VALID_TO" DATE,
	"CORE_ORGANIZATION_ID" NUMBER(18,0),
	"CORE_PERSON_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_PRINT_MODULE
--------------------------------------------------------

  CREATE TABLE "CORE_PRINT_MODULE"
   (	"ID" NUMBER(18,0),
	"NAME" VARCHAR2(255 CHAR),
	"TEMPLATE" VARCHAR2(255 CHAR),
	"TYPE" VARCHAR2(255 CHAR),
	"OBJECT_TYPE" VARCHAR2(128 CHAR),
	"WF_STATE" VARCHAR2(255 CHAR),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_ROLE
--------------------------------------------------------

  CREATE TABLE "CORE_ROLE"
   (	"ID" NUMBER(18,0),
	"NAME" VARCHAR2(64 CHAR),
	"DESCRIPTION" VARCHAR2(255 CHAR),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_ROLE_COMPOSITION
--------------------------------------------------------

  CREATE TABLE "CORE_ROLE_COMPOSITION"
   (	"ID" NUMBER(18,0),
	"SUB_ROLE_ID" NUMBER(18,0),
	"SUPERIOR_ROLE_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_TEMPLATE
--------------------------------------------------------

  CREATE TABLE "CORE_TEMPLATE"
   (	"ID" NUMBER(18,0),
	"BODY" CLOB,
	"CODE" VARCHAR2(50 CHAR),
	"DESCRIPTION" VARCHAR2(2000 CHAR),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_USER
--------------------------------------------------------

  CREATE TABLE "CORE_USER"
   (	"ID" NUMBER(18,0),
	"LAST_LOGIN" DATE,
	"PASSWORD" VARCHAR2(255 CHAR),
	"USERNAME" VARCHAR2(50 CHAR),
	"CORE_PERSON_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_USER_ROLE
--------------------------------------------------------

  CREATE TABLE "CORE_USER_ROLE"
   (	"ID" NUMBER(18,0),
	"VALID_FROM" DATE,
	"VALID_TILL" DATE,
	"CORE_ORGANIZATION_ID" NUMBER(18,0),
	"CORE_ROLE_ID" NUMBER(18,0),
	"CORE_USER_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Table CORE_WORKFLOW_ENTITY
--------------------------------------------------------

  CREATE TABLE "CORE_WORKFLOW_ENTITY"
   (	"ID" NUMBER(18,0),
	"OBJECT_IDENTIFIER" VARCHAR2(100 CHAR),
	"OBJECT_TYPE" VARCHAR2(128 CHAR),
	"WF_PROCESS_INSTANCE_ID" VARCHAR2(255 CHAR),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Index SYS_C00117460
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00117460" ON "CORE_APP_SETTING" ("ID");
--------------------------------------------------------
--  DDL for Index CORE_APP_SETTING_UQ
--------------------------------------------------------

  CREATE UNIQUE INDEX "CORE_APP_SETTING_UQ" ON "CORE_APP_SETTING" ("SETTING_KEY");
--------------------------------------------------------
--  DDL for Index SYS_C00118810
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00118810" ON "CORE_ATTACHMENT" ("ID");
--------------------------------------------------------
--  DDL for Index FK_FI0NO2WD5U0IGXVDGUNF99CSI
--------------------------------------------------------

  CREATE INDEX "FK_FI0NO2WD5U0IGXVDGUNF99CSI" ON "CORE_ATTACHMENT" ("NEXT_VERSION_ID");
--------------------------------------------------------
--  DDL for Index FK_5J8KHL29HN2HNGEJ333WYM7UI
--------------------------------------------------------

  CREATE INDEX "FK_5J8KHL29HN2HNGEJ333WYM7UI" ON "CORE_ATTACHMENT" ("PARENT_ID");

--------------------------------------------------------
--  DDL for Index SYS_C00195072
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00195072" ON "CORE_EMAIL_RECORD" ("ID");
--------------------------------------------------------
--  DDL for Index FK_41HYRMFBEV23726TQJH55T9B0
--------------------------------------------------------

  CREATE INDEX "FK_41HYRMFBEV23726TQJH55T9B0" ON "CORE_EMAIL_RECORD" ("PARENT_ID");
--------------------------------------------------------
--  DDL for Index FK_677MOL32R5ET9NBIP9QL3XSJL
--------------------------------------------------------

  CREATE INDEX "FK_677MOL32R5ET9NBIP9QL3XSJL" ON "CORE_EMAIL_RECORD" ("PREVIOUS_VERSION");
--------------------------------------------------------
--  DDL for Index SYS_C00112550
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00112550" ON "CORE_ORGANIZATION" ("ID");
--------------------------------------------------------
--  DDL for Index CORE_PERMISSI_CORE_USER_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_PERMISSI_CORE_USER_ID_IX" ON "CORE_PERMISSION" ("CORE_USER_ID");
--------------------------------------------------------
--  DDL for Index CORE_PERMISSI_CORE_ROLE_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_PERMISSI_CORE_ROLE_ID_IX" ON "CORE_PERMISSION" ("CORE_ROLE_ID");
--------------------------------------------------------
--  DDL for Index CORE_PERM_CORE_ORGANIZATIO_IX
--------------------------------------------------------

  CREATE INDEX "CORE_PERM_CORE_ORGANIZATIO_IX" ON "CORE_PERMISSION" ("CORE_ORGANIZATION_ID");
--------------------------------------------------------
--  DDL for Index CORE_PERMISSION_TICORE
--------------------------------------------------------

  CREATE UNIQUE INDEX "CORE_PERMISSION_TICORE" ON "CORE_PERMISSION" ("OBJECT_TYPE", "OBJECT_IDENTIFIER", "CORE_ORGANIZATION_ID", "CORE_ROLE_ID", "CORE_USER_ID");
--------------------------------------------------------
--  DDL for Index CORE_PERMISSION_TI
--------------------------------------------------------

  CREATE INDEX "CORE_PERMISSION_TI" ON "CORE_PERMISSION" ("OBJECT_TYPE", "OBJECT_IDENTIFIER");
--------------------------------------------------------
--  DDL for Index SYS_C00154749
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00154749" ON "CORE_PERMISSION" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C00112555
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00112555" ON "CORE_PERSON" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C00112560
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00112560" ON "CORE_PERSON_ORGANIZATION" ("ID");
--------------------------------------------------------
--  DDL for Index CORE_PERSON_CORE_PERSON_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_PERSON_CORE_PERSON_ID_IX" ON "CORE_PERSON_ORGANIZATION" ("CORE_PERSON_ID");
--------------------------------------------------------
--  DDL for Index CORE_PERSO_CORE_ORGANIZATIO_IX
--------------------------------------------------------

  CREATE INDEX "CORE_PERSO_CORE_ORGANIZATIO_IX" ON "CORE_PERSON_ORGANIZATION" ("CORE_ORGANIZATION_ID");
--------------------------------------------------------
--  DDL for Index CORE_PRINT_MODULE_IX
--------------------------------------------------------

  CREATE UNIQUE INDEX "CORE_PRINT_MODULE_IX" ON "CORE_PRINT_MODULE" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C00112563
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00112563" ON "CORE_ROLE" ("ID");
--------------------------------------------------------
--  DDL for Index UK_6E0QH51VORMUSRGDCETO0N97N
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_6E0QH51VORMUSRGDCETO0N97N" ON "CORE_ROLE" ("NAME");
--------------------------------------------------------
--  DDL for Index CORE_ROLE_COMPO_SUB_ROLE_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_ROLE_COMPO_SUB_ROLE_ID_IX" ON "CORE_ROLE_COMPOSITION" ("SUB_ROLE_ID");
--------------------------------------------------------
--  DDL for Index SYS_C00112567
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00112567" ON "CORE_ROLE_COMPOSITION" ("ID");
--------------------------------------------------------
--  DDL for Index CORE_ROLE_COMP_SUPER_RO_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_ROLE_COMP_SUPER_RO_ID_IX" ON "CORE_ROLE_COMPOSITION" ("SUPERIOR_ROLE_ID");
--------------------------------------------------------
--  DDL for Index SYS_C00195718
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00195718" ON "CORE_TEMPLATE" ("ID");
--------------------------------------------------------
--  DDL for Index SYS_C00112572
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00112572" ON "CORE_USER" ("ID");
--------------------------------------------------------
--  DDL for Index UK_3UH3XMYN5O3Y6F0L4WPW70SXI
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_3UH3XMYN5O3Y6F0L4WPW70SXI" ON "CORE_USER" ("USERNAME");
--------------------------------------------------------
--  DDL for Index CORE_USER_CORE_PERSON_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_USER_CORE_PERSON_ID_IX" ON "CORE_USER" ("CORE_PERSON_ID");
--------------------------------------------------------
--  DDL for Index SYS_C00112576
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00112576" ON "CORE_USER_ROLE" ("ID");
--------------------------------------------------------
--  DDL for Index CORE_USER_ROLE_CORE_USER_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_USER_ROLE_CORE_USER_ID_IX" ON "CORE_USER_ROLE" ("CORE_USER_ID");
--------------------------------------------------------
--  DDL for Index CORE_USER_ROLE_CORE_ROLE_ID_IX
--------------------------------------------------------

  CREATE INDEX "CORE_USER_ROLE_CORE_ROLE_ID_IX" ON "CORE_USER_ROLE" ("CORE_ROLE_ID");
--------------------------------------------------------
--  DDL for Index CORE_USER_CORE_ORGANIZATIO_IX
--------------------------------------------------------

  CREATE INDEX "CORE_USER_CORE_ORGANIZATIO_IX" ON "CORE_USER_ROLE" ("CORE_ORGANIZATION_ID");
--------------------------------------------------------
--  DDL for Index SYS_C00126393
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C00126393" ON "CORE_WORKFLOW_ENTITY" ("ID");
--------------------------------------------------------
--  DDL for Index UK_88GYIG712LYAKLADBYAGPU3XV
--------------------------------------------------------

  CREATE UNIQUE INDEX "UK_88GYIG712LYAKLADBYAGPU3XV" ON "CORE_WORKFLOW_ENTITY" ("OBJECT_IDENTIFIER", "OBJECT_TYPE");
--------------------------------------------------------
--  Constraints for Table CORE_APP_SETTING
--------------------------------------------------------

  ALTER TABLE "CORE_APP_SETTING" MODIFY ("SETTING_KEY" NOT NULL ENABLE);
  ALTER TABLE "CORE_APP_SETTING" MODIFY ("SYSTEM" NOT NULL ENABLE);
  ALTER TABLE "CORE_APP_SETTING" ADD CONSTRAINT "SYSTEM_CORE_APP_SETTING_C" CHECK (SYSTEM in (0,1)) ENABLE;
  ALTER TABLE "CORE_APP_SETTING" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_APP_SETTING" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_ATTACHMENT
--------------------------------------------------------

  ALTER TABLE "CORE_ATTACHMENT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("VERSION_NUMBER" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("VERSION_LABEL" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("OBJECT_TYPE" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("OBJECT_IDENTIFIER" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("MIMETYPE" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("FILESIZE" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("ENCODING" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("CONTENT_GUID" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_ATTACHMENT" MODIFY ("VERSION" NOT NULL ENABLE);

--------------------------------------------------------
--  Constraints for Table CORE_EMAIL_RECORD
--------------------------------------------------------

  ALTER TABLE "CORE_EMAIL_RECORD" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "CORE_EMAIL_RECORD" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_EMAIL_RECORD" MODIFY ("ENCODING" NOT NULL ENABLE);
  ALTER TABLE "CORE_EMAIL_RECORD" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_ORGANIZATION
--------------------------------------------------------

  ALTER TABLE "CORE_ORGANIZATION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_ORGANIZATION" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "CORE_ORGANIZATION" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_ORGANIZATION" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_PERMISSION
--------------------------------------------------------

  ALTER TABLE "CORE_PERMISSION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_PERMISSION" MODIFY ("PERMISSION_FOR" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("OBJECT_TYPE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("CAN_WRITE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("CAN_READ" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("CAN_DELETE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("ACTIVE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("DISCRIMINATOR" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "CORE_PERMISSI_WF_ACTION_TYPE_C" CHECK (WF_ACTION_TYPE IN ('FORWARD','BACKWARDS','NONE')) ENABLE;
  ALTER TABLE "CORE_PERMISSION" MODIFY ("WF_ACTION_TYPE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "CORE_PERMISSION_CAN_DELETE_C" CHECK (CAN_DELETE IN (0,1)) ENABLE;
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "CORE_PERMISSION_CAN_WRITE_C" CHECK (CAN_WRITE IN (0,1)) ENABLE;
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "CORE_PERMISSION_CAN_READ_C" CHECK (CAN_READ IN (0,1)) ENABLE;
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "CORE_PERMISSI_PERMISSION_FOR_C" CHECK (PERMISSION_FOR IN ('ENTITY','SECTION','SCREEN')) ENABLE;
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "CORE_PERMISSION_ACTIVE_C" CHECK (ACTIVE IN (0,1)) ENABLE;
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "CORE_PERMISSION_TRIO_CHK" CHECK (core_user_id is not null
        or core_role_id is not null
        or (core_organization_id is not null and core_role_id is not null)) ENABLE;
--------------------------------------------------------
--  Constraints for Table CORE_PERSON
--------------------------------------------------------

  ALTER TABLE "CORE_PERSON" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_PERSON" MODIFY ("SURNAME" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERSON" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERSON" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_PERSON_ORGANIZATION
--------------------------------------------------------

  ALTER TABLE "CORE_PERSON_ORGANIZATION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_PERSON_ORGANIZATION" ADD CHECK (obligation>=0 AND obligation<=1) ENABLE;
  ALTER TABLE "CORE_PERSON_ORGANIZATION" MODIFY ("CORE_PERSON_ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERSON_ORGANIZATION" MODIFY ("CORE_ORGANIZATION_ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERSON_ORGANIZATION" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERSON_ORGANIZATION" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_PRINT_MODULE
--------------------------------------------------------

  ALTER TABLE "CORE_PRINT_MODULE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_PRINT_MODULE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "CORE_PRINT_MODULE" MODIFY ("TEMPLATE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PRINT_MODULE" MODIFY ("TYPE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PRINT_MODULE" MODIFY ("OBJECT_TYPE" NOT NULL ENABLE);
  ALTER TABLE "CORE_PRINT_MODULE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_PRINT_MODULE" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE CORE_PRINT_MODULE ADD CONSTRAINT CORE_PRINT_MODULE_TYPE_C CHECK (TYPE IN ('PDF','HTML','XML'));

--------------------------------------------------------
--  Constraints for Table CORE_ROLE
--------------------------------------------------------

  ALTER TABLE "CORE_ROLE" ADD CONSTRAINT "UK_6E0QH51VORMUSRGDCETO0N97N" UNIQUE ("NAME") ENABLE;
  ALTER TABLE "CORE_ROLE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_ROLE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "CORE_ROLE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_ROLE" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_ROLE_COMPOSITION
--------------------------------------------------------

  ALTER TABLE "CORE_ROLE_COMPOSITION" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_ROLE_COMPOSITION" MODIFY ("SUPERIOR_ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_ROLE_COMPOSITION" MODIFY ("SUB_ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_ROLE_COMPOSITION" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_ROLE_COMPOSITION" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_TEMPLATE
--------------------------------------------------------

  ALTER TABLE "CORE_TEMPLATE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_TEMPLATE" MODIFY ("CODE" NOT NULL ENABLE);
  ALTER TABLE "CORE_TEMPLATE" MODIFY ("BODY" NOT NULL ENABLE);
  ALTER TABLE "CORE_TEMPLATE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_TEMPLATE" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_USER
--------------------------------------------------------

  ALTER TABLE "CORE_USER" ADD CONSTRAINT "UK_3UH3XMYN5O3Y6F0L4WPW70SXI" UNIQUE ("USERNAME") ENABLE;
  ALTER TABLE "CORE_USER" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_USER" MODIFY ("USERNAME" NOT NULL ENABLE);
  ALTER TABLE "CORE_USER" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_USER" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_USER_ROLE
--------------------------------------------------------

  ALTER TABLE "CORE_USER_ROLE" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_USER_ROLE" MODIFY ("CORE_USER_ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_USER_ROLE" MODIFY ("CORE_ROLE_ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_USER_ROLE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "CORE_USER_ROLE" MODIFY ("VERSION" NOT NULL ENABLE);
--------------------------------------------------------
--  Constraints for Table CORE_WORKFLOW_ENTITY
--------------------------------------------------------

  ALTER TABLE "CORE_WORKFLOW_ENTITY" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "CORE_WORKFLOW_ENTITY" ADD CONSTRAINT "UK_88GYIG712LYAKLADBYAGPU3XV" UNIQUE ("OBJECT_IDENTIFIER", "OBJECT_TYPE") ENABLE;
  ALTER TABLE "CORE_WORKFLOW_ENTITY" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_WORKFLOW_ENTITY" MODIFY ("OBJECT_TYPE" NOT NULL ENABLE);
  ALTER TABLE "CORE_WORKFLOW_ENTITY" MODIFY ("OBJECT_IDENTIFIER" NOT NULL ENABLE);
  ALTER TABLE "CORE_WORKFLOW_ENTITY" MODIFY ("ID" NOT NULL ENABLE);
--------------------------------------------------------
--  Ref Constraints for Table CORE_ATTACHMENT
--------------------------------------------------------

  ALTER TABLE "CORE_ATTACHMENT" ADD CONSTRAINT "FK_CORE_ATTACHMENT_PARENT_ID" FOREIGN KEY ("PARENT_ID")
	  REFERENCES "CORE_ATTACHMENT" ("ID") ENABLE;
  ALTER TABLE "CORE_ATTACHMENT" ADD CONSTRAINT "FK_CORE_ATTACH_NEXT_VERSION_ID" FOREIGN KEY ("NEXT_VERSION_ID")
	  REFERENCES "CORE_ATTACHMENT" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CORE_EMAIL_RECORD
--------------------------------------------------------

  ALTER TABLE "CORE_EMAIL_RECORD" ADD CONSTRAINT "FK_CORE_EMAIL_RECORD_PARENT_ID" FOREIGN KEY ("PARENT_ID")
	  REFERENCES "CORE_EMAIL_RECORD" ("ID") ENABLE;
  ALTER TABLE "CORE_EMAIL_RECORD" ADD CONSTRAINT "FK_CORE_EMAI_PREVIOUS_VERSION" FOREIGN KEY ("PREVIOUS_VERSION")
	  REFERENCES "CORE_EMAIL_RECORD" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CORE_ORGANIZATION
--------------------------------------------------------

  ALTER TABLE "CORE_ORGANIZATION" ADD CONSTRAINT "FK_CORE_ORGANIZATION" FOREIGN KEY ("PARENT_ID")
	  REFERENCES "CORE_ORGANIZATION" ("ID") deferrable initially deferred ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CORE_PERMISSION
--------------------------------------------------------

  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "FK_CORE_PERMISSI_CORE_ROLE_ID" FOREIGN KEY ("CORE_ROLE_ID")
	  REFERENCES "CORE_ROLE" ("ID") ENABLE;
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "FK_CORE_PERMISSI_CORE_USER_ID" FOREIGN KEY ("CORE_USER_ID")
	  REFERENCES "CORE_USER" ("ID") ENABLE;
  ALTER TABLE "CORE_PERMISSION" ADD CONSTRAINT "FK_CORE_PERM_CORE_ORGANIZATIO" FOREIGN KEY ("CORE_ORGANIZATION_ID")
	  REFERENCES "CORE_ORGANIZATION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CORE_PERSON_ORGANIZATION
--------------------------------------------------------

  ALTER TABLE "CORE_PERSON_ORGANIZATION" ADD CONSTRAINT "FK_CORE_PERSON_CORE_PERSON_ID" FOREIGN KEY ("CORE_PERSON_ID")
	  REFERENCES "CORE_PERSON" ("ID") ENABLE;
  ALTER TABLE "CORE_PERSON_ORGANIZATION" ADD CONSTRAINT "FK_CORE_PERSO_CORE_ORGANIZATIO" FOREIGN KEY ("CORE_ORGANIZATION_ID")
	  REFERENCES "CORE_ORGANIZATION" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CORE_ROLE_COMPOSITION
--------------------------------------------------------

  ALTER TABLE "CORE_ROLE_COMPOSITION" ADD CONSTRAINT "FK_CORE_ROLE_COMPO_SUB_ROLE_ID" FOREIGN KEY ("SUB_ROLE_ID")
	  REFERENCES "CORE_ROLE" ("ID") ENABLE;
  ALTER TABLE "CORE_ROLE_COMPOSITION" ADD CONSTRAINT "FK_CORE_ROLE_COMP_SUPER_RO_ID" FOREIGN KEY ("SUPERIOR_ROLE_ID")
	  REFERENCES "CORE_ROLE" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CORE_USER
--------------------------------------------------------

  ALTER TABLE "CORE_USER" ADD CONSTRAINT "FK_CORE_USER_CORE_PERSON_ID" FOREIGN KEY ("CORE_PERSON_ID")
	  REFERENCES "CORE_PERSON" ("ID") ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table CORE_USER_ROLE
--------------------------------------------------------

  ALTER TABLE "CORE_USER_ROLE" ADD CONSTRAINT "FK_CORE_USER_CORE_ORGANIZATIO" FOREIGN KEY ("CORE_ORGANIZATION_ID")
	  REFERENCES "CORE_ORGANIZATION" ("ID") ENABLE;
  ALTER TABLE "CORE_USER_ROLE" ADD CONSTRAINT "FK_CORE_USER_ROLE_CORE_ROLE_ID" FOREIGN KEY ("CORE_ROLE_ID")
	  REFERENCES "CORE_ROLE" ("ID") ENABLE;
  ALTER TABLE "CORE_USER_ROLE" ADD CONSTRAINT "FK_CORE_USER_ROLE_CORE_USER_ID" FOREIGN KEY ("CORE_USER_ID")
	  REFERENCES "CORE_USER" ("ID") ENABLE;

--------------------------------------------------------
--  ACTIVITI
--------------------------------------------------------

-- schema generovano / aktualizovano primo Activiti frameworkem !

--------------------------------------------------------------------------------
-- Quartz
--------------------------------------------------------------------------------


CREATE TABLE qrtz_job_details
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    JOB_CLASS_NAME   VARCHAR2(250) NOT NULL,
    IS_DURABLE VARCHAR2(1) NOT NULL,
    IS_NONCONCURRENT VARCHAR2(1) NOT NULL,
    IS_UPDATE_DATA VARCHAR2(1) NOT NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NOT NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT QRTZ_JOB_DETAILS_PK PRIMARY KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
);
CREATE TABLE qrtz_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    JOB_NAME  VARCHAR2(200) NOT NULL,
    JOB_GROUP VARCHAR2(200) NOT NULL,
    DESCRIPTION VARCHAR2(250) NULL,
    NEXT_FIRE_TIME NUMBER(13) NULL,
    PREV_FIRE_TIME NUMBER(13) NULL,
    PRIORITY NUMBER(13) NULL,
    TRIGGER_STATE VARCHAR2(16) NOT NULL,
    TRIGGER_TYPE VARCHAR2(8) NOT NULL,
    START_TIME NUMBER(13) NOT NULL,
    END_TIME NUMBER(13) NULL,
    CALENDAR_NAME VARCHAR2(200) NULL,
    MISFIRE_INSTR NUMBER(2) NULL,
    JOB_DATA BLOB NULL,
    CONSTRAINT QRTZ_TRIGGERS_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_TRIGGER_TO_JOBS_FK FOREIGN KEY (SCHED_NAME,JOB_NAME,JOB_GROUP)
      REFERENCES QRTZ_JOB_DETAILS(SCHED_NAME,JOB_NAME,JOB_GROUP)
);
CREATE TABLE qrtz_simple_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    REPEAT_COUNT NUMBER(7) NOT NULL,
    REPEAT_INTERVAL NUMBER(12) NOT NULL,
    TIMES_TRIGGERED NUMBER(10) NOT NULL,
    CONSTRAINT QRTZ_SIMPLE_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_SIMPLE_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
	REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_cron_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    CRON_EXPRESSION VARCHAR2(120) NOT NULL,
    TIME_ZONE_ID VARCHAR2(80),
    CONSTRAINT QRTZ_CRON_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_CRON_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
      REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_simprop_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    STR_PROP_1 VARCHAR2(512) NULL,
    STR_PROP_2 VARCHAR2(512) NULL,
    STR_PROP_3 VARCHAR2(512) NULL,
    INT_PROP_1 NUMBER(10) NULL,
    INT_PROP_2 NUMBER(10) NULL,
    LONG_PROP_1 NUMBER(13) NULL,
    LONG_PROP_2 NUMBER(13) NULL,
    DEC_PROP_1 NUMERIC(13,4) NULL,
    DEC_PROP_2 NUMERIC(13,4) NULL,
    BOOL_PROP_1 VARCHAR2(1) NULL,
    BOOL_PROP_2 VARCHAR2(1) NULL,
    CONSTRAINT QRTZ_SIMPROP_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_SIMPROP_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
      REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_blob_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    BLOB_DATA BLOB NULL,
    CONSTRAINT QRTZ_BLOB_TRIG_PK PRIMARY KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP),
    CONSTRAINT QRTZ_BLOB_TRIG_TO_TRIG_FK FOREIGN KEY (SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
        REFERENCES QRTZ_TRIGGERS(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_calendars
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    CALENDAR_NAME  VARCHAR2(200) NOT NULL,
    CALENDAR BLOB NOT NULL,
    CONSTRAINT QRTZ_CALENDARS_PK PRIMARY KEY (SCHED_NAME,CALENDAR_NAME)
);
CREATE TABLE qrtz_paused_trigger_grps
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    TRIGGER_GROUP  VARCHAR2(200) NOT NULL,
    CONSTRAINT QRTZ_PAUSED_TRIG_GRPS_PK PRIMARY KEY (SCHED_NAME,TRIGGER_GROUP)
);
CREATE TABLE qrtz_fired_triggers
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    ENTRY_ID VARCHAR2(95) NOT NULL,
    TRIGGER_NAME VARCHAR2(200) NOT NULL,
    TRIGGER_GROUP VARCHAR2(200) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    FIRED_TIME NUMBER(13) NOT NULL,
    SCHED_TIME NUMBER(13) NOT NULL,
    PRIORITY NUMBER(13) NOT NULL,
    STATE VARCHAR2(16) NOT NULL,
    JOB_NAME VARCHAR2(200) NULL,
    JOB_GROUP VARCHAR2(200) NULL,
    IS_NONCONCURRENT VARCHAR2(1) NULL,
    REQUESTS_RECOVERY VARCHAR2(1) NULL,
    CONSTRAINT QRTZ_FIRED_TRIGGER_PK PRIMARY KEY (SCHED_NAME,ENTRY_ID)
);
CREATE TABLE qrtz_scheduler_state
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    INSTANCE_NAME VARCHAR2(200) NOT NULL,
    LAST_CHECKIN_TIME NUMBER(13) NOT NULL,
    CHECKIN_INTERVAL NUMBER(13) NOT NULL,
    CONSTRAINT QRTZ_SCHEDULER_STATE_PK PRIMARY KEY (SCHED_NAME,INSTANCE_NAME)
);
CREATE TABLE qrtz_locks
  (
    SCHED_NAME VARCHAR2(120) NOT NULL,
    LOCK_NAME  VARCHAR2(40) NOT NULL,
    CONSTRAINT QRTZ_LOCKS_PK PRIMARY KEY (SCHED_NAME,LOCK_NAME)
);

create index idx_qrtz_j_req_recovery on qrtz_job_details(SCHED_NAME,REQUESTS_RECOVERY);
create index idx_qrtz_j_grp on qrtz_job_details(SCHED_NAME,JOB_GROUP);

create index idx_qrtz_t_j on qrtz_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP);
create index idx_qrtz_t_jg on qrtz_triggers(SCHED_NAME,JOB_GROUP);
create index idx_qrtz_t_c on qrtz_triggers(SCHED_NAME,CALENDAR_NAME);
create index idx_qrtz_t_g on qrtz_triggers(SCHED_NAME,TRIGGER_GROUP);
create index idx_qrtz_t_state on qrtz_triggers(SCHED_NAME,TRIGGER_STATE);
create index idx_qrtz_t_n_state on qrtz_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP,TRIGGER_STATE);
create index idx_qrtz_t_n_g_state on qrtz_triggers(SCHED_NAME,TRIGGER_GROUP,TRIGGER_STATE);
create index idx_qrtz_t_next_fire_time on qrtz_triggers(SCHED_NAME,NEXT_FIRE_TIME);
create index idx_qrtz_t_nft_st on qrtz_triggers(SCHED_NAME,TRIGGER_STATE,NEXT_FIRE_TIME);
create index idx_qrtz_t_nft_misfire on qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME);
create index idx_qrtz_t_nft_st_misfire on qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_STATE);
create index idx_qrtz_t_nft_st_misfire_grp on qrtz_triggers(SCHED_NAME,MISFIRE_INSTR,NEXT_FIRE_TIME,TRIGGER_GROUP,TRIGGER_STATE);

create index idx_qrtz_ft_trig_inst_name on qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME);
create index idx_qrtz_ft_inst_job_req_rcvry on qrtz_fired_triggers(SCHED_NAME,INSTANCE_NAME,REQUESTS_RECOVERY);
create index idx_qrtz_ft_j_g on qrtz_fired_triggers(SCHED_NAME,JOB_NAME,JOB_GROUP);
create index idx_qrtz_ft_jg on qrtz_fired_triggers(SCHED_NAME,JOB_GROUP);
create index idx_qrtz_ft_t_g on qrtz_fired_triggers(SCHED_NAME,TRIGGER_NAME,TRIGGER_GROUP);
create index idx_qrtz_ft_tg on qrtz_fired_triggers(SCHED_NAME,TRIGGER_GROUP);

ALTER TABLE CORE_ORGANIZATION ADD (VALID_FROM DATE );

ALTER TABLE CORE_ORGANIZATION ADD (VALID_TO DATE );

ALTER TABLE CORE_ORGANIZATION ADD (ACTIVE NUMBER(1, 0) DEFAULT 1 );

ALTER TABLE CORE_ORGANIZATION ADD (UNIT NUMBER(18, 0) );

ALTER TABLE CORE_ORGANIZATION MODIFY (ACTIVE NOT NULL);

--------------------------------------------------------
--  DDL for Table CORE_ORGANIZATION_EXT
--------------------------------------------------------

  CREATE TABLE "CORE_ORGANIZATION_EXT"
   (	"ID" NUMBER(18,0),
	"CORE_ORGANIZATION_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
  create index core_orga_core_organizatio_ix on core_organization_ext (core_organization_id);
--------------------------------------------------------
--  DDL for Index SYS_C0019603
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0019603" ON "CORE_ORGANIZATION_EXT" ("ID");
--------------------------------------------------------
--  Constraints for Table CORE_ORGANIZATION_EXT
--------------------------------------------------------

  ALTER TABLE "CORE_ORGANIZATION_EXT" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "CORE_ORGANIZATION_EXT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_ORGANIZATION_EXT" MODIFY ("ID" NOT NULL ENABLE);

--------------------------------------------------------
--  Ref Constraints for Table CORE_ORGANIZATION_EXT
--------------------------------------------------------

  ALTER TABLE "CORE_ORGANIZATION_EXT" ADD CONSTRAINT "FK_CORE_ORG_ID" FOREIGN KEY ("CORE_ORGANIZATION_ID")
	  REFERENCES "CORE_ORGANIZATION" ("ID") ENABLE;

--------------------------------------------------------
--  DDL for Table CORE_PERSON_ORGANIZATION_EXT
--------------------------------------------------------

  CREATE TABLE "CORE_PERSON_ORGANIZATION_EXT"
   (	"ID" NUMBER(18,0),
	"CORE_PERSON_ORGANIZATION_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Index SYS_C0019610
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0019610" ON "CORE_PERSON_ORGANIZATION_EXT" ("ID");
--------------------------------------------------------
--  Constraints for Table CORE_PERSON_ORGANIZATION_EXT
--------------------------------------------------------

  ALTER TABLE "CORE_PERSON_ORGANIZATION_EXT" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERSON_ORGANIZATION_EXT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_PERSON_ORGANIZATION_EXT" MODIFY ("ID" NOT NULL ENABLE);
  create index core_perso_core_person_org_ix on core_person_organization_ext (core_person_organization_id);

--------------------------------------------------------
--  Ref Constraints for Table CORE_PERSON_ORGANIZATION_EXT
--------------------------------------------------------

  ALTER TABLE "CORE_PERSON_ORGANIZATION_EXT" ADD CONSTRAINT "FK_CORE_PER_ORG_ID" FOREIGN KEY ("CORE_PERSON_ORGANIZATION_ID")
	  REFERENCES "CORE_PERSON_ORGANIZATION" ("ID") ENABLE;

--------------------------------------------------------
--  DDL for Table CORE_PERSON_EXT
--------------------------------------------------------

  CREATE TABLE "CORE_PERSON_EXT"
   (	"ID" NUMBER(18,0),
	"CORE_PERSON_ID" NUMBER(18,0),
	"CREATED" DATE,
	"CREATOR" VARCHAR2(50 CHAR),
	"MODIFIED" DATE,
	"MODIFIER" VARCHAR2(50 CHAR),
	"VERSION" NUMBER(10,0) DEFAULT 0
   );
--------------------------------------------------------
--  DDL for Index SYS_C0019611
--------------------------------------------------------

  CREATE UNIQUE INDEX "SYS_C0019611" ON "CORE_PERSON_EXT" ("ID");
--------------------------------------------------------
--  Constraints for Table CORE_PERSON_EXT
--------------------------------------------------------

  ALTER TABLE "CORE_PERSON_EXT" MODIFY ("VERSION" NOT NULL ENABLE);
  ALTER TABLE "CORE_PERSON_EXT" ADD PRIMARY KEY ("ID") ENABLE;
  ALTER TABLE "CORE_PERSON_EXT" MODIFY ("ID" NOT NULL ENABLE);

--------------------------------------------------------
--  Ref Constraints for Table CORE_PERSON_EXT
--------------------------------------------------------

  ALTER TABLE "CORE_PERSON_EXT" ADD CONSTRAINT "FK_CORE_PERSON_ID" FOREIGN KEY ("CORE_PERSON_ID")
	  REFERENCES "CORE_PERSON" ("ID") ENABLE;

ALTER TABLE CORE_PERSON ADD (VALID_FROM DATE );

ALTER TABLE CORE_PERSON ADD (VALID_TO DATE );

ALTER TABLE CORE_PERSON ADD (ACTIVE NUMBER(1, 0) DEFAULT 1 );

ALTER TABLE CORE_PERSON ADD (UNIT NUMBER(18, 0) );

ALTER TABLE CORE_PERSON MODIFY (ACTIVE NOT NULL);

ALTER TABLE CORE_PERSON ADD (DATE_OF_BIRTH DATE );

ALTER TABLE CORE_PERSON ADD (SEX VARCHAR2(1 CHAR) );

UPDATE CORE_PERSON SET SEX='F';

ALTER TABLE CORE_PERSON MODIFY (SEX DEFAULT 'F' NOT NULL);

ALTER TABLE CORE_PERSON ADD CONSTRAINT CORE_PERSON_SEX CHECK (SEX IN ('F','M')) ENABLE;
alter table core_person add constraint core_person_active_c check (active in (0,1));

--------------------------------------------------------
--  Constraints for Table CORE_PERMISSION
--------------------------------------------------------

ALTER TABLE CORE_PERMISSION ADD (REL_TYPE VARCHAR2(150 CHAR));

ALTER TABLE CORE_PERMISSION ADD (REL_ID NUMBER(18,0));

ALTER TABLE CORE_PERMISSION ADD (VALID_FROM DATE);

ALTER TABLE CORE_PERMISSION ADD (VALID_TILL DATE);

DROP INDEX "CORE_PERMISSION_TICORE";
CREATE UNIQUE INDEX "CORE_PERMISSION_TICORE" ON "CORE_PERMISSION" ("OBJECT_TYPE", "OBJECT_IDENTIFIER", "CORE_ORGANIZATION_ID", "CORE_ROLE_ID", "CORE_USER_ID","REL_TYPE","REL_ID");

alter table core_organization add constraint core_organization_active_c check (active in (0,1));

CREATE TABLE CORE_WF_PROC_DEF_ENTITY
(
  ID NUMBER(18, 0) NOT NULL
, PROCESS_DEFINITION_KEY VARCHAR2(255 CHAR) NOT NULL
, ENTITY_CLASS_NAME VARCHAR2(255 CHAR) NOT NULL
, CREATED DATE
, CREATOR VARCHAR2(50 CHAR)
, MODIFIED DATE
, MODIFIER VARCHAR2(50 CHAR)
, VERSION NUMBER(10, 0) DEFAULT 0 NOT NULL
, CONSTRAINT CORE_WF_PROC_DEF_ENTITY_PK PRIMARY KEY (ID) ENABLE
);

drop table CORE_WORKFLOW_ENTITY;

CREATE INDEX CORE_PER_EXT_CORE_PERSON_ID_IX ON CORE_PERSON_EXT (CORE_PERSON_ID);

INSERT INTO CORE_APP_SETTING (ID,SETTING_KEY, VALUE, SYSTEM, CREATED) VALUES (HIBERNATE_SEQUENCE.NEXTVAL, 'CORE_VERSION', '0.3-SNAPSHOT (row 0)', 1, sysdate);
COMMIT;
