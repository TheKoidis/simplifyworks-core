CREATE TABLE core_flat_organization (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  ownee_org_id number(20) NOT NULL,
  owner_org_id number(20) NOT NULL,
  PRIMARY KEY (id) 
);

alter table core_flat_organization
  add constraint fk_flat_organization_ownee
    foreign key (ownee_org_id) references core_organization (id);
    
alter table core_flat_organization
  add constraint fk_flat_organization_owner
    foreign key (owner_org_id) references core_organization (id);