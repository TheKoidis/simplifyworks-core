insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_organization_write', 'Edit organizations');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_organization_read', 'Read organizations');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_organization_delete', 'Delete organizations');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_organization_write'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_organization_read'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_organization_delete'));

insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_person_write', 'Edit people');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_person_read', 'Read people');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_person_delete', 'Delete people');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_write'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_read'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_delete'));

insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_person_organization_write', 'Edit person-organization relations');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_person_organization_read', 'Read person-organization relations');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_person_organization_delete', 'Delete person-organization relations');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_organization_write'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_organization_read'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_person_organization_delete'));

insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_role_write', 'Edit roles');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_role_read', 'Read roles');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_role_delete', 'Delete roles');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_write'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_read'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_delete'));

insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_role_composition_write', 'Edit role compositions');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_role_composition_read', 'Read role compositions');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_role_composition_delete', 'Delete role compositions');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_composition_write'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_composition_read'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_role_composition_delete'));

insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_user_write', 'Edit users');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_user_read', 'Read users');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_user_delete', 'Delete users');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_write'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_read'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_delete'));

insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_user_role_write', 'Edit user-role relations');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_user_role_read', 'Read user-role relations');
insert into core_role (ID, CREATED, CREATOR, VERSION, NAME, DESCRIPTION) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', 'core_user_role_delete', 'Delete user-role relations');

INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_role_write'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_role_read'));
INSERT INTO CORE_ROLE_COMPOSITION (ID, CREATED, CREATOR, VERSION, SUPERIOR_ROLE_ID, SUB_ROLE_ID) VALUES (HIBERNATE_SEQUENCE.nextVal, sysdate, 'system', '0', (select id from core_role where name='admin'), (select id from core_role where name='core_user_role_delete'));
