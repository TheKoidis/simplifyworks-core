COMMENT ON TABLE CORE_PERSON_ORGANIZATION IS 'Pracovne pravni vztah';
COMMENT ON COLUMN CORE_PERSON_ORGANIZATION.CATEGORY IS 'Typ prace';
COMMENT ON COLUMN CORE_PERSON_ORGANIZATION.OBLIGATION IS 'Typ uvazku';
COMMENT ON COLUMN CORE_PERSON_ORGANIZATION.CORE_ORGANIZATION_ID IS 'ID organizace'; 
COMMENT ON COLUMN CORE_PERSON_ORGANIZATION.CORE_PERSON_ID IS 'ID osoby';
COMMENT ON TABLE CORE_PERSON IS 'Osoba';
COMMENT ON COLUMN CORE_PERSON.NOTE IS 'Poznamka';
COMMENT ON COLUMN CORE_PERSON.ACTIVE IS 'Je aktivni';
COMMENT ON COLUMN CORE_PERSON.UNIT IS 'Ekonomicka jednotka (EKJ)';
COMMENT ON TABLE CORE_USER IS 'Uzivatel';
COMMENT ON COLUMN CORE_USER.CORE_PERSON_ID IS 'ID osoby';
COMMENT ON TABLE CORE_ORGANIZATION IS 'Organizace';
COMMENT ON COLUMN CORE_ORGANIZATION.ABBREVIATION IS 'Zkratka organizace';
COMMENT ON COLUMN CORE_ORGANIZATION.CODE IS 'Kod organizace';
COMMENT ON COLUMN CORE_ORGANIZATION.PARENT_ID IS 'ID rodicovske organizace';
COMMENT ON COLUMN CORE_ORGANIZATION.UNIT IS 'Ekonomicka jednotka (EKJ)';
COMMENT ON COLUMN CORE_ROLE.DESCRIPTION IS 'Popis role';
COMMENT ON TABLE CORE_USER_ROLE IS 'Vztah role, uzivatele a organizace';
COMMENT ON COLUMN CORE_USER_ROLE.CORE_ORGANIZATION_ID IS 'ID organizace';
COMMENT ON COLUMN CORE_USER_ROLE.CORE_ROLE_ID IS 'ID role';
COMMENT ON COLUMN CORE_USER_ROLE.CORE_USER_ID IS 'ID uzivatele';
COMMENT ON COLUMN CORE_ATTACHMENT.GUID IS 'Jednoznacny identifikator prilohy';
COMMENT ON COLUMN CORE_ATTACHMENT.MIMETYPE IS 'Typ prilohy';
COMMENT ON COLUMN CORE_FLAT_ORGANIZATION.OWNER_ORG_ID IS 'ID vlastnika';
COMMENT ON COLUMN CORE_FLAT_ORGANIZATION.OWNEE_ORG_ID IS 'ID vlastnence (muze byt shodny s vlastnikem)';
COMMENT ON COLUMN CORE_FLAT_ROLE.OWNER_ROLE_ID IS 'ID nadrazene role';
COMMENT ON COLUMN CORE_FLAT_ROLE.OWNEE_ROLE_ID IS 'ID podrazene role (muze bys shodna s nadrazenou)';
COMMENT ON COLUMN CORE_ORGANIZATION_EXT.CORE_ORGANIZATION_ID IS 'ID organizace'; 
COMMENT ON COLUMN CORE_PERSON_ORGANIZATION_EXT.CORE_PERSON_ORGANIZATION_ID IS 'ID tabulky core_person_organization';
COMMENT ON COLUMN CORE_PRINT_MODULE.OBJECT_TYPE IS 'Typ dto'; 
COMMENT ON COLUMN CORE_PRINT_MODULE.WF_STATE IS 'Stav workflow'; 
COMMENT ON COLUMN CORE_ROLE_COMPOSITION.SUB_ROLE_ID IS 'Id role potomka'; 
COMMENT ON COLUMN CORE_ROLE_COMPOSITION.SUPERIOR_ROLE_ID IS 'Id rodicovske role'; 

ALTER TABLE CORE_PERSON DROP COLUMN ADDRESSING_PHRASE;


