CREATE TABLE core_flat_role (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  ownee_role_id number(20) NOT NULL,
  owner_role_id number(20) NOT NULL,
  PRIMARY KEY (id) 
);

alter table core_flat_role
  add constraint fk_flat_role_ownee
    foreign key (ownee_role_id) references core_role (id);
    
alter table core_flat_role
  add constraint fk_flat_role_owner
    foreign key (owner_role_id) references core_role (id);