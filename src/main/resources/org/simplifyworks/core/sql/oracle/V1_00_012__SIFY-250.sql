drop table CORE_ATTACHMENT cascade constraints;

CREATE TABLE core_attachment (
  id number(20) NOT NULL,
  created date DEFAULT NULL,
  creator varchar(50)  DEFAULT NULL,
  modified date DEFAULT NULL,
  modifier varchar(50)  DEFAULT NULL,
  mimetype varchar(255) NOT NULL,
  name varchar(255) NOT NULL,
  filesize number(20) NOT NULL,
  guid varchar(36) NOT NULL,
  uploaded date DEFAULT NULL,
  PRIMARY KEY (id)
);

