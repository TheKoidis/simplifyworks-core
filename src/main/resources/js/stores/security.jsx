var constants = require('./../constants/security.jsx');
var formConstants = require('./../constants/form.jsx');
var listConstants = require('./../constants/list.jsx');

// TODO get this constants from API
var AUTO_LOG_OUT_WARNING_TIMEOUT = 40 * 60 * 1000;
var AUTO_LOG_OUT_TIMEOUT = 60 * 60 * 1000;

var SecurityStore = Fluxxor.createStore({
	
	initialize: function() {
		var info = localStorage.getItem('security');
		
		if(!info || new Date().getTime() - new Date(JSON.parse(info).lastInvocation).getTime() > AUTO_LOG_OUT_TIMEOUT) {
			localStorage.setItem('security', JSON.stringify({status: 'notLoggedIn'}));
		}
		
		this.bindActions(
			constants.SECURITY_LOG_IN, this.onLogIn,
			constants.SECURITY_LOG_IN_SUCCESS, this.onLogInSuccess,
			constants.SECURITY_LOG_IN_FAILURE, this.onLogInFailure,
			
			constants.SECURITY_LOG_OUT, this.onLogOut,
			constants.SECURITY_SSO_LOG_OUT, this.onSsoLogOut,
			
			formConstants.FORM_LOAD_SUCCESS, this.onRestInvocation,
			formConstants.DETAIL_LOCK_SUCCESS, this.onRestInvocation,
			formConstants.DETAIL_UNLOCK_SUCCESS, this.onRestInvocation,
			formConstants.DETAIL_CREATE_SUCCESS, this.onRestInvocation,
			formConstants.DETAIL_SAVE_SUCCESS, this.onRestInvocation,
			
			listConstants.LIST_RELOAD_SUCCESS, this.onRestInvocation
		);
	},
	
	getInfo: function() {
		return JSON.parse(localStorage.getItem('security'));
	},
	
	getAuthToken: function() {
		return JSON.parse(localStorage.getItem('security')).authToken;
	},
	
	getCurrentUsername: function() {
		return JSON.parse(localStorage.getItem('security-token-info')).currentUsername;
	},
	
	getOriginalUsername: function() {
		return JSON.parse(localStorage.getItem('security-token-info')).originalUsername;
	},
	
	getPerson: function() {
		return JSON.parse(localStorage.getItem('security-token-info')).person;
	},
	
	getRoles: function() {
		return JSON.parse(localStorage.getItem('security')).allRoles;
	},
	
	hasRole: function(role) {
		return JSON.parse(localStorage.getItem('security')).allRoles.indexOf(role) != -1;
	},
	
	onLogIn: function(payload) {
		var info = JSON.parse(localStorage.getItem('security'));
		
		info.status = 'loggingIn';
		
		localStorage.setItem('security', JSON.stringify(info));
		
		this.emit('change');
	},
	
	onLogInSuccess: function(payload) {
		var info = JSON.parse(localStorage.getItem('security'));
		
		info.status = 'loggedIn';
		
		// TODO not elegant
		global['auth-token'] = payload.token;
		info.authToken = payload.token;
		info.allRoles = payload.allRoles;
		info.rolesInOrganizations = payload.rolesInOrganizations;
		
		info.lastInvocation = new Date().toJSON();
		this.refreshAutoLogOutWarningTimer(info);
		this.refreshAutoLogOutTimer(info);
		
		localStorage.setItem('security', JSON.stringify(info));
		this.parseAndSaveTokenInfo(payload.token);
		this.emit('change');
	},
	
	parseAndSaveTokenInfo: function(token) {
		var pattTokenInfo = /[^.]*\.([^.]*)\..*/;
		var tokenSubstr = pattTokenInfo.exec(token);
		var fullTokenInfoB64 = new Buffer(tokenSubstr[1], 'base64');
		var fullTokenInfo = fullTokenInfoB64.toString('utf8');
		localStorage.setItem('security-token-info', fullTokenInfo);
	},
	
	onLogInFailure: function(payload) {
		var info = JSON.parse(localStorage.getItem('security'));
		
		info.status = 'notLoggedIn';
		info.error = payload.error;
		
		localStorage.setItem('security', JSON.stringify(info));
		
		this.emit('change');
	},
	
	onLogOut: function(payload) {
		this.onSsoLogOut(payload);
		
		this.emit('change');
	},
	
	onSsoLogOut: function(payload) {
		var info = JSON.parse(localStorage.getItem('security'));
		
		info.status = 'notLoggedIn';
		info.username = null;
		info.authToken = null;
		
		global['auth-token'] = null;
		
		this.clearAutoLogOutWarningTimer(info);
		this.clearAutoLogOutTimer(info);
		
		localStorage.setItem('security', JSON.stringify(info));
	},
	
	onRestInvocation: function(payload) {
		var info = JSON.parse(localStorage.getItem('security'));
		
		info.lastInvocation = new Date().toJSON();
		this.refreshAutoLogOutWarningTimer(info);
		this.refreshAutoLogOutTimer(info);
		
		localStorage.setItem('security', JSON.stringify(info));
		
		this.emit('change');
	},
	
	clearAutoLogOutWarningTimer: function(info) {
		if(info.autoLogOutWarningTimerId) {
			clearTimeout(info.autoLogOutWarningTimerId);
			
			info.autoLogOutWarningTimerId = null;
		}
	},
	
	refreshAutoLogOutWarningTimer: function(info) {
		this.clearAutoLogOutWarningTimer(info);
		
		info.autoLogOutWarningTimerId = setTimeout(function() {
			notification.showLocalizedMessage('warning', 'auto-log-out-warning-title', 'auto-log-out-warning', {timeInMinutesLeft: Math.floor((AUTO_LOG_OUT_TIMEOUT - AUTO_LOG_OUT_WARNING_TIMEOUT) / 60000)});
		}, AUTO_LOG_OUT_WARNING_TIMEOUT);
	},
	
	clearAutoLogOutTimer: function(info) {
		if(info.autoLogOutTimerId) {
			clearTimeout(info.autoLogOutTimerId);
			
			info.autoLogOutTimerId = null;
		}
	},
	
	refreshAutoLogOutTimer: function(info) {
		this.clearAutoLogOutTimer(info);
		
		info.autoLogOutTimerId = setTimeout(function() {
			var info = JSON.parse(localStorage.getItem('security'));
			
			info.status = 'notLoggedIn';
			
			localStorage.setItem('security', JSON.stringify(info));
			
			notification.showLocalizedMessage('warning', 'auto-log-out-title', 'auto-log-out');
			this.emit('change');
		}.bind(this), AUTO_LOG_OUT_TIMEOUT);
	}
});

module.exports = SecurityStore;
