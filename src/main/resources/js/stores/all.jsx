var ApplicationStore = require('./application.jsx');
var ListStore = require('./list.jsx');
var FormStore = require('./form.jsx');
var TaskStore = require('./task.jsx');
var SecurityStore = require('./security.jsx');
var WorkflowStore = require('./workflow.jsx');
var ReportStore = require('./report.jsx');

module.exports = {
	application: new ApplicationStore(),
	list: new ListStore(),
	form: new FormStore(),
	task: new TaskStore(),
	security: new SecurityStore(),
	workflow: new WorkflowStore(),
	report: new ReportStore()
};
