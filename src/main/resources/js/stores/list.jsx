var constants = require('./../constants/list.jsx');

var ListStore = Fluxxor.createStore({
	
	initialize: function() {
		if(!sessionStorage.getItem('lists')) {
			sessionStorage.setItem('lists', JSON.stringify({}));
		}
		
		this.bindActions(
			constants.LIST_RELOAD, this.onReload,
			constants.LIST_RELOAD_SUCCESS, this.onReloadSuccess,
			constants.LIST_RELOAD_FAILURE, this.onReloadFailure,
			constants.LIST_INIT, this.onInit,
			
			constants.LIST_SELECT_SINGLE, this.onSelectSingle,
			constants.LIST_SELECT_PAGE, this.onSelectPage,
			constants.LIST_DESELECT_SINGLE, this.onDeselectSingle,
			constants.LIST_DESELECT_PAGE, this.onDeselectPage,
			constants.LIST_DESELECT_ALL, this.onDeselectAll,
			
			constants.LIST_TOGGLE_FILTER, this.onToggleFilter,
			constants.LIST_RESET_FILTER, this.onResetFilter,
			constants.LIST_FILTER_CHANGE_VALUE, this.onChangeValue,
			
			constants.LIST_SHOW_MODAL_DETAIL, this.onShowModalDetail,
			
			constants.LIST_REMOVE_FROM_STORE, this.onRemoveFromStore
		);
	},
	
	getList: function(name) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		
		if(!lists[name]) {
			lists[name] = {
				name: name,
				status: 'initialized',
				options: {
					ordering: [],
					quickSearch: {
						properties: [],
						tokens: []
					},
					filters: {
						column: {
							visible: false,
							
							types: [],
							values: []
						}
					},
					showFilter: false,
					filter: {}
				},
				showModalDetailValue: null,
				selection: [],
				data: {}
			}
			
			sessionStorage.setItem('lists', JSON.stringify(lists));
		}
		
		return lists[name];
	},
	
	onInit: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.status = 'initialized';
		list.options = payload.options;
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onReload: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.status = 'reloading';
		list.options = payload.options;
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onReloadSuccess: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.status = 'success'
		list.options = payload.options;
		
		list.data = payload.data;
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onReloadFailure: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.status = 'failure'
		list.options = payload.options;
		
		list.data = {};
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onSelectSingle: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.selection.push(payload.resource);
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onSelectPage: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.selection = list.selection.filter(function(element) {
			return !list.data.some(function(selectedElement) {
				return element[payload.identifier] === selectedElement[payload.identifier];
			});
		});
		
		list.selection = list.selection.concat(list.data);
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onDeselectSingle: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.selection = list.selection.filter(function(element, index, array) {
			return element[payload.identifier] !== payload.resource[payload.identifier];
		});
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onDeselectPage: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.selection = list.selection.filter(function(element) {
			return !list.data.some(function(selectedElement) {
				return element.resource[payload.identifier] === selectedElement.resource[payload.identifier];
			});
		});
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onDeselectAll: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.selection = [];
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onToggleFilter: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.options.showFilter = !list.options.showFilter;
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onResetFilter: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.options.filter = {};
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onChangeValue: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.options.filter[payload.property] = payload.value;
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
	
	onShowModalDetail: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		var list = lists[payload.name];
		
		list.showModalDetailValue = payload.identifier;
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	},
		
	onRemoveFromStore: function(payload) {
		var lists = JSON.parse(sessionStorage.getItem('lists'));
		delete lists[payload.name];
		
		sessionStorage.setItem('lists', JSON.stringify(lists));
		
		this.emit('change');
	}
});

module.exports = ListStore;
