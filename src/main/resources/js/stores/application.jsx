var constants = require('./../constants/application.jsx');

var ApplicationStore = Fluxxor.createStore({
	
	initialize: function() {
		this.status = 'empty';
		this.currentAvailableDescriptors = [];
		this.latestAvailableDescriptors = [];
		
		this.bindActions(
			constants.APPLICATION_LOAD_AVAILABLE_DESCRIPTORS, this.onLoadAvailableDescriptors,
			constants.APPLICATION_LOAD_AVAILABLE_DESCRIPTORS_SUCCESS, this.onLoadAvailableDescriptorsSuccess,
			constants.APPLICATION_LOAD_AVAILABLE_DESCRIPTORS_FAILURE, this.onLoadAvailableDescriptorsFailure,
			
			constants.APPLICATION_RELOAD_AVAILABLE_DESCRIPTORS, this.onReloadAvailableDescriptors,
			constants.APPLICATION_RELOAD_AVAILABLE_DESCRIPTORS_SUCCESS, this.onReloadAvailableDescriptorsSuccess,
			constants.APPLICATION_RELOAD_AVAILABLE_DESCRIPTORS_FAILURE, this.onReloadAvailableDescriptorsFailure
		);
	},
	
	getStatus: function() {
		return this.status;
	},
	
	getCurrentAvailableDescriptors: function() {
		return this.currentAvailableDescriptors;
	},
	
	getLatestAvailableDescriptors: function() {
		return this.latestAvailableDescriptors;
	},
	
	getReloadTimeoutInSeconds: function() {
		return 5 * 60
	},
	
	onLoadAvailableDescriptors: function(payload) {
		this.status = 'loading';
		
		this.emit('change');
	},
	
	onLoadAvailableDescriptorsSuccess: function(payload) {
		this.status = 'done';
		this.currentAvailableDescriptors = payload.descriptors;
		this.latestAvailableDescriptors = payload.descriptors;
		
		this.emit('change');
	},
	
	onLoadAvailableDescriptorsFailure: function(payload) {
		this.status = 'error';
		
		this.emit('change');
	},
	
	onReloadAvailableDescriptors: function(payload) {
		this.status = 'loading';
		
		this.emit('change');
	},
	
	onReloadAvailableDescriptorsSuccess: function(payload) {
		this.status = 'done';
		this.latestAvailableDescriptors = payload.descriptors;
		
		this.emit('change');
	},
	
	onReloadAvailableDescriptorsFailure: function(payload) {
		this.status = 'error';
		
		this.emit('change');
	}
});

module.exports = ApplicationStore;
