var FormStore = Fluxxor.createStore({
	
	initialize: function() {
		if(!sessionStorage.getItem('details')) {
			sessionStorage.setItem('details', JSON.stringify({}));
		}
		
		this.bindActions(
			FormConstants.FORM_CHANGE_VALUE, this.onChangeValue,
			
			FormConstants.FORM_INIT, this.onFormInit,
			FormConstants.DETAIL_INIT, this.onDetailInit,
			
			FormConstants.FORM_LOAD, this.onLoad,
			FormConstants.FORM_LOAD_SUCCESS, this.onLoadSuccess,
			FormConstants.FORM_LOAD_FAILURE, this.onLoadFailure,
			
			FormConstants.DETAIL_LOCK, this.onLock,
			FormConstants.DETAIL_LOCK_SUCCESS, this.onLockSuccess,
			FormConstants.DETAIL_LOCK_FAILURE, this.onLockFailure,
			
			FormConstants.DETAIL_UNLOCK, this.onUnlock,
			FormConstants.DETAIL_UNLOCK_SUCCESS, this.onUnlockSuccess,
			FormConstants.DETAIL_UNLOCK_FAILURE, this.onUnlockFailure,
			FormConstants.CLOSE_MODAL_SUCCESS, this.onCloseModalSuccess,
			
			FormConstants.DETAIL_CREATE, this.onCreate,
			FormConstants.DETAIL_CREATE_SUCCESS, this.onCreateSuccess,
			FormConstants.DETAIL_CREATE_FAILURE, this.onCreateFailure,
			
			FormConstants.DETAIL_SAVE, this.onSave,
			FormConstants.DETAIL_SAVE_SUCCESS, this.onSaveSuccess,
			FormConstants.DETAIL_SAVE_FAILURE, this.onSaveFailure,
			
			FormConstants.DETAIL_VALIDATION_FAILED, this.onValidationFailed,
			FormConstants.DETAIL_CUSTOM_VALIDATION_FAILED, this.onCustomValidationFailed,
			FormConstants.REFRESH_VALIDATION, this.onRefreshValidation
		);
	},
	
	getDetail: function(restPrefix) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		
		if (!details[restPrefix]) {
			details[restPrefix] = {
				modified: false,
				status: 'new',
				forms: {},
				validation: {}
			};
			
			sessionStorage.setItem('details', JSON.stringify(details));
		}
		
		return details[restPrefix];
	},
	
	getForm: function(restPrefix, rest) {
		if (rest == null) {
			rest = 'DEFAULT';
		}
		var details = JSON.parse(sessionStorage.getItem('details'));
		
		if (!details[restPrefix]) {
			details[restPrefix] = {
				modified: false,
				status: 'new',
				forms: {},
				validation: {}
			};
		}
		
		if (!details[restPrefix].forms[rest]) {
			details[restPrefix].forms[rest] = {
				rest: rest,
				modified: false,
				status: 'new',
				data: {}
			};
			
			sessionStorage.setItem('details', JSON.stringify(details));
		}
		
		return details[restPrefix].forms[rest];
	},
	
	onChangeValue: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		var form = detail.forms[payload.rest != null ? payload.rest : 'DEFAULT'];
		
		if (payload.callback) {
			payload.callback(form.data, payload.property, payload.value, this.changeValue.bind(this, details, detail, form, payload));
		} else {
			this.changeValue(details, detail, form, payload);
		}
	},
	
	changeValue: function(details, detail, form, payload) {
		form.modified = true;
		detail.modified = true;
		PropertyUtils.setPropertyValue(form.data, payload.property, payload.value);
		if (!form.validation[payload.property]) {
			form.validation[payload.property] = {};
		}
		
		form.validation[payload.property].result = payload.validationResult;
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onValidationFailed: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		for (var formName in payload.validationFailed) {
			var form = detail.forms[formName];
			
			for (var property in payload.validationFailed[formName].validation) {
				form.validation[property].result = payload.validationFailed[formName].validation[property].validationResult;
			}
		}
		
		ValidationUtils.showValidationResults(payload.validationFailed);
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onCustomValidationFailed: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.validation = {};
		payload.invalidTabs.map(function (tab, index, array) {
			detail.validation[tab] = 'invalid';
		});
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onRefreshValidation: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		var form = detail.forms[payload.rest != null ? payload.rest : 'DEFAULT'];
		
		for (var key in payload.validation) {
			if (!form.validation) {
				form.validation = {};
			}
			if (!form.validation[key]) {
				form.validation[key] = {};
			}
			
			form.validation[key].hidden = payload.validation[key].hidden;
			form.validation[key].validations = payload.validation[key].validations;
			form.validation[key].label = payload.validation[key].label;
			if (payload.validation[key].hidden) {
				form.data[key] = null;
				form.validation[key].result = "";
			}
		}
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onFormInit: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		var form = detail.forms[payload.rest != null ? payload.rest : 'DEFAULT'];
		
		form.status = 'new';
		form.modified = false;
		form.validation = payload.validation;
		form.tabText = payload.tabText;
		form.data = payload.initialValue ? payload.initialValue : {};
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onDetailInit: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'new';
		detail.modified = false;
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onLoad: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		var form = detail.forms[payload.rest != null ? payload.rest : 'DEFAULT'];
		
		form.status = 'processing';
		form.data = {};
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onLoadSuccess: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		var form = detail.forms[payload.rest != null ? payload.rest : 'DEFAULT'];
		
		form.status = 'managed';
		
		form.validation = payload.validation;
		form.tabText = payload.tabText;
		
		form.data = payload.data;
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onLoadFailure: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		var form = detail.forms[payload.rest != null ? payload.rest : 'DEFAULT'];
		
		form.status = 'failed';
		form.data = {};
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onLock: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'processing';
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onLockSuccess: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'locked';
		detail.modified = false;
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onLockFailure: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'managed';
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onUnlock: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'processing';
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onUnlockSuccess: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		delete details[payload.restPrefix];
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onUnlockFailure: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'locked';
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onCloseModalSuccess: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		delete details[payload.restPrefix];
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onCreate: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'processing';
		detail.modified = false;
		detail.validation = {};
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onCreateSuccess: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		delete details[payload.restPrefix];
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onCreateFailure: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'new';
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onSave: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'processing';
		detail.validation = {};
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onSaveSuccess: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'locked';
		detail.modified = false;
		detail.forms = {};
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	onSaveFailure: function(payload) {
		var details = JSON.parse(sessionStorage.getItem('details'));
		var detail = details[payload.restPrefix];
		
		detail.status = 'locked';
		
		sessionStorage.setItem('details', JSON.stringify(details));
		
		this.emit('change');
	},
	
	
	isAnyDetailChanged: function() {
		var details = JSON.parse(sessionStorage.getItem('details'));
		
		for (var key in details) {
			if (details[key].modified) {
				return key;
			}
		}
		
		return null;
	}
});

module.exports = FormStore;
