var constants = require('./../constants/workflow.jsx');

var workflows = {};

var WorkflowStore = Fluxxor.createStore({

    initialize: function() {
        this.bindActions(
			constants.WORKFLOW_SEARCH_TASK_DEFINITIONS, this.onSearchTaskDefinitions,
			constants.WORKFLOW_SEARCH_TASK_DEFINITIONS_SUCCESS, this.onSearchTaskDefinitionsSuccess,
			constants.WORKFLOW_SEARCH_TASK_DEFINITIONS_FAILURE, this.onSearchTaskDefinitionsFailure,
			constants.WORKFLOW_SEARCH_TASKS, this.onSearchTasks,
			constants.WORKFLOW_SEARCH_TASKS_SUCCESS, this.onSearchTasksSuccess,
			constants.WORKFLOW_SEARCH_TASKS_FAILURE, this.onSearchTasksFailure,
			constants.WORKFLOW_COMPLETE_TASKS, this.onCompleteTasks,
			constants.WORKFLOW_COMPLETE_TASKS_SUCCESS, this.onCompleteTasksSuccess,
			constants.WORKFLOW_COMPLETE_TASKS_FAILURE, this.onCompleteTasksFailure
        );
    },

	getWorkflow: function(objectType) {
		if(!workflows[objectType]) {
			workflows[objectType] = {
				status: 'new',
				taskDefinitions: [],
				data: {}
			};
		}

		return workflows[objectType];
	},

	onSearchTaskDefinitions: function(payload) {
		var workflow = workflows[payload.objectType];

		workflow.status = 'searching';

		this.emit('change');
	},

	onSearchTaskDefinitionsSuccess: function(payload) {
		var workflow = workflows[payload.objectType];

		workflow.status = 'success';
		workflow.taskDefinitions = payload.taskDefinitions;

		this.emit('change');
	},

	onSearchTaskDefinitionsFailure: function(payload) {
		var workflow = workflows[payload.objectType];

		workflow.status = 'failure';

		this.emit('change');
	},
	
	onSearchTasks: function(payload) {
		var workflow = workflows[payload.objectType];
		
		workflow.status = 'reloading';
		
		this.emit('change');
	},

	onSearchTasksSuccess: function(payload) {
		var workflow = workflows[payload.objectType];
		
		workflow.status = 'success'
		
		workflow.data = payload.data;
		
		this.emit('change');
	},

	onSearchTasksFailure: function(payload) {
		var workflow = workflows[payload.objectType]
		
		workflow.status = 'failure';
		
		this.emit('change');
	},
	
	onCompleteTasks: function(payload) {
		var workflow = workflows[payload.objectType];
		
		workflow.status = 'reloading';
		
		this.emit('change');
	},
	
	onCompleteTasksSuccess: function(payload) {
		var workflow = workflows[payload.objectType];
		
		workflow.status = 'success';
		
		this.emit('change');
	},
	
	onCompleteTasksFailure: function(payload) {
		var workflow = workflows[payload.objectType]
		
		workflow.status = 'failure';
		
		this.emit('change');
	}
});

module.exports = WorkflowStore;
