var constants = require('./../constants/report.jsx');

var status = 'done';

var ReportStore = Fluxxor.createStore({
	
	initialize: function() {
		this.bindActions(
			constants.GETTING_XML, this.onGettingXml,
			constants.GETTING_XML_SUCCESS, this.onGettingXmlSuccess,
			constants.GETTING_XML_FAILURE, this.onGettingXmlFailure
		);
	},
	
	getStatus: function() {
		return status;
	},
	
	onGettingXml: function(payload) {
		status = 'processing';
		
		this.emit('change');
	},
	
	onGettingXmlSuccess: function(payload) {
		status = 'done';
		
		this.emit('change');
	},
	
	onGettingXmlFailure: function(payload) {
		status = 'done';
		
		this.emit('change');
	}
});

module.exports = ReportStore;
