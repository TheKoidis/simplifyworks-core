var constants = require('./../constants/task.jsx');
var tasks = [];

var TaskStore = Fluxxor.createStore({

    initialize: function() {
        this.bindActions(
            constants.TASK_LOAD_TASKS, this.loadTasks,
            constants.TASK_GET_TASKS, this.getTasks,
            constants.TASK_SAVE_ONE_TIME_TRIGGER, this.saveOneTimeTrigger,
            constants.TASK_SAVE_CRON_TRIGGER, this.saveCronTrigger,
            constants.TASK_RUN_NOW, this.runNow,
            constants.TASK_CHANGE_TRIGGER_STATE, this.changeTriggerState,
            constants.TASK_REMOVE_TRIGGER, this.removeTrigger
        );
    },

    loadTasks: function(payload) {
      tasks = payload;
      this.emit('change');
    },

    getTasks: function() {
        return tasks;
    },

    saveOneTimeTrigger: function() {
    },

    saveCronTrigger: function() {
    },

    runNow: function() {
    },

    changeTriggerState: function() {
    },

    removeTrigger: function() {
    }

});

module.exports = TaskStore;
