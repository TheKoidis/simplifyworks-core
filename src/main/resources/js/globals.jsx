/**
*   Sets common globals (no require is needed for common types, functions etc.)
*/
console.log('Setting globals ...');

/**
*   Sets React
*/
global['React'] = require('react');

/**
*   Sets ReactDOM
*/
global['ReactDOM'] = require('react-dom');

global['OnUnload'] = require("react-window-mixins").OnUnload;

/**
*   Sets ReactRouter
*/
var ReactRouter = require('react-router');
global['ReactRouter'] = ReactRouter;

for(var property in ReactRouter) {
	global[property] = ReactRouter[property];
}

/**
*   Sets ReactBootstrap
*/
var ReactBootstrap = require('react-bootstrap');
global['ReactBootstrap'] = ReactBootstrap;

for(var property in ReactBootstrap) {
	global[property] = ReactBootstrap[property];
}

/**
*   Sets Fluxxor
*/
var Fluxxor = require('fluxxor');
global['Fluxxor'] = Fluxxor;
global['FluxMixin'] = Fluxxor.FluxMixin(React);
global['StoreWatchMixin'] = Fluxxor.StoreWatchMixin;

/**
*   Sets i18next (i18n)
*/
global['i18n'] = require('i18next');

/**
*   Set dateFormat
*/
global['dateFormat'] = require('dateformat');

/**
*	Set moment
*/
global['moment'] = require('moment');

/**
*   Sets ReactNotificationSystem
*/
global['NotificationSystem'] = require('react-notification-system');
global['notification'] = {
	ref: null,
	
	showMessage: function(level, title, message, action, autoDismiss) {
		if(this.ref) {
			this.ref.addNotification({
				level: level,
				title: title,
				message: message,
				position: 'tc',
				autoDismiss: autoDismiss!==undefined ? autoDismiss : 5,
				action: action
			});
		} else {
			console.error('Notification ref is not set');
		}
	},
	
	showLocalizedMessage: function(level, titleKey, messageKey, params, action, autoDismiss) {
		this.showMessage(level, i18n.t(titleKey), i18n.t(messageKey, params), action, autoDismiss);
	}
}

/**
*   Sets request (lib for HTTP communication)
*/
global['request'] = require('request');

/**
*   Sets Simplifyworks utils
*/
global['ComponentUtils'] = require('./components/component-utils.jsx');
global['PropertyUtils'] = require('./components/property-utils.jsx');
global['NumberUtils'] = require('./components/number-utils.jsx');
global['LocalizationUtils'] = require('./components/localization-utils.jsx');
global['ValidationUtils'] = require('./components/validation-utils.jsx');
global['RenderUtils'] = require('./components/render-utils.jsx');
global['WorkflowUtils'] = require('./components/workflow-utils.jsx');
global['Utils'] = require('./components/utils.jsx');
global['FormConstants'] = require('./constants/form.jsx');
global['ListConstants'] = require('./constants/list.jsx');
/**
* Selection in codelists
*/
global['OrganizationSelection'] = require('./views/administration/organization/organization-selection.jsx');
global['PersonSelection'] = require('./views/administration/person/person-selection.jsx');
global['RoleSelection'] = require('./views/administration/role/role-selection.jsx');
global['UserSelection'] = require('./views/administration/user/user-selection.jsx');

/**
*   Sets Simplifyworks components
*/
global['SW'] = {};
require('./components/all.jsx');

// FIXME is this needed? where is en locale?
require('moment/locale/cs');

console.log('Globals set and ready for use');
