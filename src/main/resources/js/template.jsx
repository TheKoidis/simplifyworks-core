var FormAuth = require('./auth/form.jsx');
var SsoAuth = require('./auth/sso.jsx');

/**
* Template represents base template for whole application.
*/
var Template = React.createClass({
	
	/**
	* FluxMixin, StoreWatchMixin('security')
	*/
	mixins: [FluxMixin, StoreWatchMixin('security')],
	
	contextTypes: {
		/**
		* flux
		*/
		flux: React.PropTypes.object.isRequired
	},
	
	componentDidMount: function() {
		global['notification'].ref = this.refs.notificationSystem;
	},
	
	getStateFromFlux: function() {
		return {
			info: this.context.flux.store('security').getInfo()
		};
	},
	
	render: function() {
		return (
			<div>
				{global.modules.map(function(module, index, modules) {
					return (
						<link rel='stylesheet' type='text/css' href={hostName + contextName + '/css/' + module.name + '.css'} key={module.name}/>
					);
				}.bind(this))}
				
				<NotificationSystem ref='notificationSystem' />
				
				{this.state.info.status === 'loggedIn'
					?
					<div>
						<Template.Header title={this.props.title} template={this} />
						<Grid fluid>
							<Row>
								<Col xs={12}>
									{React.cloneElement(this.props.main, {template: this})}
								</Col>
							</Row>
						</Grid>
					</div>
					:
					(
						ssoEnabled
						?
						<SsoAuth template={this} />
						:
						(
							global.customization.auth
							?
							React.createElement(global.customization.auth, {}, <FormAuth template={this} />)
							:
							<FormAuth template={this} />
						)
					)
				}
			</div>
		);
	}
});

/**
* Header contains application menu, logo, context title and user menu.
*/
Template.Header = React.createClass({
	
	propTypes: {
		/**
		* Context title
		*/
		title: React.PropTypes.node.isRequired,
		/**
		* Page template
		*/
		template: React.PropTypes.any.isRequired
	},
	
	render: function() {
		return (
			<Navbar fluid>
				<Navbar.Header>
					<Navbar.Brand>
						<Template.Logo />
					</Navbar.Brand>
				</Navbar.Header>
				
				<Nav>
					<Template.ApplicationMenu />
				</Nav>
				
				<Nav>
					<Template.Home />
				</Nav>
				
				<Navbar.Header>
					<Navbar.Brand>
						<span style={{marginLeft: 35}}/>
						<strong>
							{this.props.title}
						</strong>
					</Navbar.Brand>
				</Navbar.Header>
				
				<Nav pullRight>
					<Template.UserMenu template={this.props.template}/>
				</Nav>
				
				<Nav pullRight>
					<Template.LanguageMenu template={this.props.template}/>
				</Nav>
			</Navbar>
		);
	}
});

/**
* Application menu contains merged menus.
*/
Template.ApplicationMenu = React.createClass({
	
	/**
	* FluxMixin, StoreWatchMixin('security')
	*/
	mixins: [FluxMixin, StoreWatchMixin('security')],
	
	contextTypes: {
		/**
		* router
		*/
		router: React.PropTypes.object.isRequired,
		/**
		* flux
		*/
		flux: React.PropTypes.object.isRequired
	},
	
	getStateFromFlux: function() {
		return {
			roles: this.context.flux.store('security').getRoles()
		};
	},
	
	render: function() {
		var applicationMenuIcon = (
			<div>
				<Glyphicon glyph='menu-hamburger' title={i18n.t('menu')} />
				
				<span className='visible-xs-inline'>{' ' + i18n.t('menu')}</span>
			</div>
		);
		
		return (
			<NavDropdown id='application-menu' noCaret title={applicationMenuIcon}>
				{global.modules.map(function(module, index, modules) {
					return this.createModuleMenu(module, index);
				}.bind(this))}
			</NavDropdown>
		);
	},
	
	createModuleMenu: function(module, index) {
		var menuElements = this.createModuleMenuElements(module.menu);
		
		if(menuElements && menuElements.length > 0) {
			var moduleMenu = [];
			
			moduleMenu.push(
				<MenuItem key={module.name + '-header'} header>{i18n.t(module.key)}</MenuItem>
			);
			
			moduleMenu = moduleMenu.concat(menuElements);
			
			if(index !== modules.length - 1) {
				moduleMenu.push(
					<MenuItem key={module.name + '-divider'} divider />
				);
			}
			
			return moduleMenu;
		} else {
			return null;
		}
	},
	
	createModuleMenuElements: function(menu) {
		var moduleMenu = [];
		
		for(var item in menu) {
			var component = this.giveComponent(menu[item]);
			
			if(component) {
				moduleMenu.push(component);
			}
		}
		
		return moduleMenu;
	},
	
	goTo: function(path) {
		this.context.router.push(path);
	},
	
	giveComponent: function(item){
		if(item.path) {
			if(!item.rolesAllowed || item.rolesAllowed.some(function(roleAllowed) {
				return this.state.roles.indexOf(roleAllowed) != -1;
			}.bind(this))) {
				return (
					<MenuItem key={item.path} onClick={this.goTo.bind(this, item.path)}>
						<Glyphicon glyph={item.icon} />
						{' ' + i18n.t(item.labelKey)}
					</MenuItem>
				)
			} else {
				return null;
			}
		} else {
			var moduleMenu = [];
			var title = (
				<span>
					<Glyphicon glyph={item.icon} />{' ' + i18n.t(item.labelKey)}<Glyphicon glyph='menu-right' />
				</span>
			);
			for(var it in item.items){
				var component = this.giveComponent(item.items[it]);
				
				if(component) {
					moduleMenu.push(component);
				}
			}
			
			if(moduleMenu.length > 0) {
				return (
					<NavDropdown noCaret title={title} id={item.labelKey} className='menu-right'>
						{moduleMenu}
					</NavDropdown>
				);
			} else {
				return null;
			}
		}
	}
});

/**
* Home represents quick link to dashboard.
*/
Template.Home = React.createClass({
	
	contextTypes: {
		/**
		* router
		*/
		router: React.PropTypes.object.isRequired
	},
	
	render: function() {
		return (
			<NavItem onClick={this.goHome} title={i18n.t('dashboard')}>
				<Glyphicon glyph='home' />
				
				<span className='visible-xs-inline'>{' ' + i18n.t('home')}</span>
			</NavItem>
		);
	},
	
	goHome: function() {
		this.context.router.push('/');
	}
});

/**
* Application logo
*/
Template.Logo = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('application')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	componentDidMount: function() {
		this.context.flux.actions.application.loadAvailableDescriptors();
		
		this.setState({
			timer: setInterval(function() {
				this.context.flux.actions.application.reloadAvailableDescriptors();
			}.bind(this), this.context.flux.store('application').getReloadTimeoutInSeconds() * 1000)
		});
	},
	
	componentWillUnmount: function() {
		clearInterval(this.state.timer);
	},
	
	getStateFromFlux: function() {
		return {
			status: this.context.flux.store('application').getStatus(),
			currentAvailableDescriptors: this.context.flux.store('application').getCurrentAvailableDescriptors()
		};
	},
	
	render: function() {
		return (
			<OverlayTrigger trigger='click' rootClose placement='bottom' overlay={this.createApplicationInfo()}>
				<Image src={contextName + '/images/logo.png'} responsive />
			</OverlayTrigger>
		);
	},
	
	createApplicationInfo: function() {
		return (
			<Popover title={i18n.t('application-info')} style={{maxWidth: 1000}}>
				{this.state.status === 'empty' || this.state.status === 'loading'
					?
					<ProgressBar active now={100} />
					:
					null
				}
				
				{this.state.status === 'done'
					?
					<Accordion>
						{this.state.currentAvailableDescriptors.map(function(descriptor) {
							return (
								<Panel key={descriptor.name} bsStyle={descriptor.module ? 'info' : 'success'} header={descriptor.name} eventKey={descriptor.name}>
									<div>{i18n.t('application-info-descriptor-version', {value: descriptor.version})}</div>
									<div>{i18n.t('application-info-descriptor-revision', {value: descriptor.revision})}</div>
									<div>{i18n.t('application-info-descriptor-timestamp', {value: descriptor.timestamp})}</div>
									<div>{i18n.t('application-info-descriptor-number', {value: descriptor.number})}</div>
								</Panel>
							);
						})}
					</Accordion>
					:
					null
				}
				
				{this.state.status === 'error'
					?
					i18n.t('cannot-load-descriptors')
					:
					null
				}
			</Popover>
		);
	}
});

/**
* User menu contains information related to language.
*/
Template.LanguageMenu = React.createClass({
	
	propTypes: {
		/**
		* Template
		*/
		template: React.PropTypes.any.isRequired
	},
	
	render: function() {
		return (
			<NavDropdown id='language-menu' noCaret title={this.createMenuIcon()} className='language'>
				<MenuItem key='language-cs' disabled={i18n.language === 'cs'} onClick={this.changeLanguage.bind(this, 'cs')}>
					<Image src={contextName + '/images/flags/32/cz.png'} responsive />
					{i18n.t('language-cs')}
				</MenuItem>
				
				<MenuItem key='language-en' disabled={i18n.language === 'en'} onClick={this.changeLanguage.bind(this, 'en')}>
					<Image src={contextName + "/images/flags/32/uk.png"} responsive />
					{i18n.t('language-en')}
				</MenuItem>
			</NavDropdown>
		);
	},
	
	createMenuIcon: function() {
		return (
			<Image src={contextName + "/images/flags/16/"+ (i18n.language === 'cs' ? 'cz' : 'uk') +".png"} responsive />
		);
	},
	
	changeLanguage: function(language) {
		i18n.changeLanguage(language, function(err) {
			if(err) {
				console.error(err);
			} else {
				this.props.template.forceUpdate();
			}
		}.bind(this));
	}
});

/**
* User menu contains information related to current user.
*/
Template.UserMenu = React.createClass({
	
	/**
	* FluxMixin, StoreWatchMixin('security')
	*/
	mixins: [FluxMixin, StoreWatchMixin('security')],
	
	contextTypes: {
		/**
		* flux
		*/
		flux: React.PropTypes.object.isRequired,
		/**
		* router
		*/
		router: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		/**
		* Template
		*/
		template: React.PropTypes.any.isRequired
	},
	
	getStateFromFlux: function() {
		return {
			currentUsername: this.context.flux.store('security').getCurrentUsername(),
			originalUsername: this.context.flux.store('security').getOriginalUsername(),
			fullname: PersonSelection.renderText(this.context.flux.store('security').getPerson()),
			hasAdminRole: this.context.flux.store('security').hasRole('admin')
		};
	},
	
	render: function() {
		return (
			<NavDropdown id='user-menu' noCaret title={this.createMenuIcon()}>
				<MenuItem key='user-header' header>{i18n.t('core-user-detail')}</MenuItem>
				<MenuItem key='profile' onClick={this.goTo.bind(this, '/system/profile')}>{i18n.t('user-profile')}</MenuItem>
				<MenuItem key='user-divider' divider />
				
				{this.createSystemMenu()}
				
				<MenuItem key='auth-log-out' onClick={this.logOut}>{i18n.t('auth-log-out')}</MenuItem>
			</NavDropdown>
		);
	},
	
	createMenuIcon: function() {
		return (
			<div><Glyphicon glyph='user'/>{' ' + this.createUsernameTitle()}</div>
		);
	},
	
	createUsernameTitle: function() {
		if(this.state.currentUsername === this.state.originalUsername) {
			return this.state.currentUsername + ' (' + PersonSelection.renderText(this.context.flux.store('security').getPerson()) + ')';
		} else {
			return this.state.currentUsername + ' [' + this.state.originalUsername + ']';
		}
	},
	
	createSystemMenu: function() {
		if(this.state.hasAdminRole) {
			var systemItems = ['users', 'persons', 'roles', 'organizations', 'settings', 'templates', 'print-modules', 'tasks', 'workflow'];
			var systemMenu = [];
			
			systemMenu.push(<MenuItem key='system-header' header>{i18n.t('system')}</MenuItem>);
			
			systemItems.forEach(function(systemItem) {
				systemMenu.push(<MenuItem key={'system-' + systemItem} onClick={this.goTo.bind(this, 'system/' + systemItem)}>{i18n.t('core-' + systemItem)}</MenuItem>);
			}.bind(this))
			
			var notificationsTitle = (
				<span>
					{' ' + i18n.t('core-notifications') + ' '}
					<Glyphicon glyph='menu-right' />
				</span>
			)
			
			systemMenu.push(
				<NavDropdown key='notifications' noCaret title={notificationsTitle} id='notifications' className='menu-left'>
					<MenuItem key='templates' onClick={this.goTo.bind(this, 'system/notification/templates')}>{i18n.t('core-notification-templates')}</MenuItem>
					<MenuItem key='rules' onClick={this.goTo.bind(this, 'system/notification/rules')}>{i18n.t('core-notification-rules')}</MenuItem>
					<MenuItem key='schemes' onClick={this.goTo.bind(this, 'system/notification/schemes')}>{i18n.t('core-notification-schemes')}</MenuItem>
				</NavDropdown>
			);
			
			systemMenu.push(<MenuItem key='system-divider' divider />);
			
			return systemMenu;
		} else {
			return null;
		}
	},
	
	logOut: function() {
		this.context.flux.actions.security.logOut(this.context.router);
	},
	
	goTo: function(path) {
		this.context.router.push(path);
	}
});

module.exports = Template;
