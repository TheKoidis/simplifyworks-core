var PersonList = require('./views/administration/person/list.jsx');
var PersonDetail = require('./views/administration/person/detail.jsx');
var UserList = require('./views/administration/user/list.jsx');
var UserDetail = require('./views/administration/user/detail.jsx');
var RoleList = require('./views/administration/role/list.jsx');
var RoleDetail = require('./views/administration/role/detail.jsx');
var OrganizationList = require('./views/administration/organization/list.jsx');
var OrganizationDetail = require('./views/administration/organization/detail.jsx');
var SettingList = require('./views/administration/setting/list.jsx');
var TemplateList = require('./views/administration/template/list.jsx');
var TemplateDetail = require('./views/administration/template/detail.jsx');
var PrintModuleList = require('./views/administration/print-module/list.jsx');
var PrintModuleDetail = require('./views/administration/print-module/detail.jsx');
var TaskList = require('./views/administration/task/list.jsx');
var ProfileDetail = require('./views/administration/profile/detail.jsx');
var WorkflowDetail = require('./views/administration/workflow/detail.jsx');
var NotificationTemplates = require('./views/administration/notification/templates.jsx');
var NotificationRules = require('./views/administration/notification/rules.jsx');
var NotificationSchemes = require('./views/administration/notification/schemes.jsx');

var SystemRoutes = {
	systemRoute:
	<Route path='system/'>
		<Route path='persons' components={{title: PersonList.title, main: PersonList.main}} />
		<Redirect from="person/:personId" to="person/:personId/default" />
		<Route path='person/:personId/:tabName' components={{title: PersonDetail.title, main: PersonDetail.main}} />
		
		<Route path='users' components={{title: UserList.title, main: UserList.main}} />
		<Redirect from="user/:userId" to="user/:userId/default" />
		<Route path='user/:userId/:tabName' components={{title: UserDetail.title, main: UserDetail.main}} />
		
		<Route path='roles' components={{title: RoleList.title, main: RoleList.main}} />
		<Redirect from="role/:roleId" to="role/:roleId/default" />
		<Route path='role/:roleId/:tabName' components={{title: RoleDetail.title, main: RoleDetail.main}} />
		
		<Route path='organizations' components={{title: OrganizationList.title, main: OrganizationList.main}} />
		<Redirect from="organization/:organizationId" to="organization/:organizationId/default" />
		<Route path='organization/:organizationId/:tabName' components={{title: OrganizationDetail.title, main: OrganizationDetail.main}} />
		
		<Route path='settings' components={{title: SettingList.title, main: SettingList.main}} />
		
		<Route path='templates' components={{title: TemplateList.title, main: TemplateList.main}} />
		<Redirect from="template/:templateId" to="template/:templateId/default" />
		<Route path='template/:templateId/:tabName' components={{title: TemplateDetail.title, main: TemplateDetail.main}} />
		
		<Route path='print-modules' components={{title: PrintModuleList.title, main: PrintModuleList.main}} />
		<Redirect from="print-module/:printModuleId" to="print-module/:printModuleId/default" />
		<Route path='print-module/:printModuleId/:tabName' components={{title: PrintModuleDetail.title, main: PrintModuleDetail.main}} />
		
		<Route path='tasks' components={{title: TaskList.title, main: TaskList.main}} />
		
		<Route path='profile' components={{title: ProfileDetail.title, main: ProfileDetail.main}} />
		
		<Route path='workflow' components={{title: WorkflowDetail.title, main: WorkflowDetail.main}} />
		
		<Route path='notification/templates' components={{title: NotificationTemplates.title, main: NotificationTemplates.main}} />
		<Route path='notification/rules' components={{title: NotificationRules.title, main: NotificationRules.main}} />
		<Route path='notification/schemes' components={{title: NotificationSchemes.title, main: NotificationSchemes.main}} />
	</Route>
};

module.exports = SystemRoutes;
