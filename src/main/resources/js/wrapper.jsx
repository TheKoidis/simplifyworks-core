/**
* Wrapper defines context for rendered children.
*/
var Wrapper = React.createClass({
	
	childContextTypes: {
		/**
		* flux
		*/
		flux: React.PropTypes.object.isRequired,
		/**
		* location
		*/
		location: React.PropTypes.object.isRequired,
		/**
		* route
		*/
		route: React.PropTypes.object.isRequired,
	},
	
	propTypes: {
		template: React.PropTypes.object
	},
	
	getChildContext: function() {
		return {
			flux: this.props.flux,
			location: this.props.location,
			route:  this.props.route,
		};
	},
	
	render: function() {
		return (
			React.cloneElement(this.props.children, {template: this.props.template})
		);
	}
});

module.exports = Wrapper;
