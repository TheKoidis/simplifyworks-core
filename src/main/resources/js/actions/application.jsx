var constants = require('./../constants/application.jsx');
var api = require('./../rest/application.jsx');

var ApplicationActions = {
	
	loadAvailableDescriptors: function() {
		this.dispatch(constants.APPLICATION_LOAD_AVAILABLE_DESCRIPTORS, {});
		
		api.loadAvailableDescriptors(function(descriptors) {
			this.dispatch(constants.APPLICATION_LOAD_AVAILABLE_DESCRIPTORS_SUCCESS, {descriptors: descriptors});
		}.bind(this), function(error) {
			this.dispatch(constants.APPLICATION_LOAD_AVAILABLE_DESCRIPTORS_FAILURE, {error: error});
			
			notification.showLocalizedMessage('error', 'loading-error', 'cannot-load-descriptors');
		}.bind(this));
	},
	
	reloadAvailableDescriptors: function() {
		this.dispatch(constants.APPLICATION_RELOAD_AVAILABLE_DESCRIPTORS, {});
		
		api.loadAvailableDescriptors(function(descriptors) {
			this.dispatch(constants.APPLICATION_RELOAD_AVAILABLE_DESCRIPTORS_SUCCESS, {descriptors: descriptors});
			
			var currentAvailableDescriptors = this.flux.store('application').getCurrentAvailableDescriptors();
			var latestAvailableDescriptors = this.flux.store('application').getLatestAvailableDescriptors();
			
			var currentBuildNumber = currentAvailableDescriptors.filter(function(descriptor) {
				return !descriptor.module;
			})[0].number;
			
			var newBuildNumber = latestAvailableDescriptors.filter(function(descriptor) {
				return !descriptor.module;
			})[0].number;
			
			if(currentBuildNumber !== newBuildNumber) {
				notification.showLocalizedMessage('warning', 'application-update-available', 'application-update-info', {}, {
					label: i18n.t('update-application'),
					callback: function() {
						window.location.reload();
					}.bind(this)
				}, this.flux.store('application').getReloadTimeoutInSeconds());
			}
		}.bind(this), function(error) {
			this.dispatch(constants.APPLICATION_RELOAD_AVAILABLE_DESCRIPTORS_FAILURE, {error: error});
		}.bind(this));
	}
};

module.exports = ApplicationActions;
