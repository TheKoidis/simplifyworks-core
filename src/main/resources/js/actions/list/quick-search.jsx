var ListQuickSearchActions = {

    search: function(name, tokens) {
        var options = this.flux.store('list').getList(name).options;
        options.quickSearch.tokens = tokens;
        options.pageNumber = 1;

        this.flux.actions.list.common.reload(name, options);
    }
};

module.exports = ListQuickSearchActions;
