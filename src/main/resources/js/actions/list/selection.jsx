var constants = require('./../../constants/list.jsx');

var ListSelectionActions = {

	selectSingle: function(name, identifier, resource) {
		this.dispatch(constants.LIST_SELECT_SINGLE, {name: name, identifier: identifier, resource: resource});
	},

	selectPage: function(name, identifier) {
		this.dispatch(constants.LIST_SELECT_PAGE, {name: name, identifier: identifier});
	},

	deselectSingle: function(name, identifier, resource) {
		this.dispatch(constants.LIST_DESELECT_SINGLE, {name: name, identifier: identifier, resource: resource});
	},

	deselectPage: function(name, identifier) {
		this.dispatch(constants.LIST_DESELECT_PAGE, {name: name, identifier: identifier});
	},

	deselectAll: function(name) {
		this.dispatch(constants.LIST_DESELECT_ALL, {name: name});
	},

	showModalDetail: function(name, identifier) {
		this.dispatch(constants.LIST_SHOW_MODAL_DETAIL, {name: name, identifier: identifier})
	}
};

module.exports = ListSelectionActions;
