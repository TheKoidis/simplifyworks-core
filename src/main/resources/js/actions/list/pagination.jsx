var ListPaginationActions = {

    changePageNumber: function(name, pageNumber) {
        var options = this.flux.store('list').getList(name).options;

        options.pageNumber = pageNumber;

        this.flux.actions.list.common.reload(name, options);
    },

    changePageSize: function(name, pageSize) {
        var options = this.flux.store('list').getList(name).options;

		options.pageNumber = 1;
        options.pageSize = pageSize;

        this.flux.actions.list.common.reload(name, options);
    }
};

module.exports = ListPaginationActions;
