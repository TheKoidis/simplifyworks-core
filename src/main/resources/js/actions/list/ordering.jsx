var ListOrderingActions = {

    changeOrder: function(name, property, order, multi) {
        var options = this.flux.store('list').getList(name).options;
        options.ordering = Utils.changeOrder(options.ordering, property, order, multi);
        this.flux.actions.list.common.reload(name, options);
    },

    clearOrdering: function(name) {
        var options = this.flux.store('list').getList(name).options;

        options.ordering = [];

        this.flux.actions.list.common.reload(name, options);
    }
};

module.exports = ListOrderingActions;
