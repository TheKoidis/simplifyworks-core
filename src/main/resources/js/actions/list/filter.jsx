var constants = require('./../../constants/list.jsx');

var ListFilterActions = {

    toggleFilter: function(name) {
		this.dispatch(constants.LIST_TOGGLE_FILTER, {name: name});
    },

	resetFilter: function(name) {
		this.dispatch(constants.LIST_RESET_FILTER, {name: name});

		var options = this.flux.store('list').getList(name).options;		
		this.flux.actions.list.common.reload(name, options);
    },

	changeValue: function(name, property, value) {
		this.dispatch(constants.LIST_FILTER_CHANGE_VALUE, {name: name, property: property, value: value});
    }

};

module.exports = ListFilterActions;
