var api = require('./../../rest/list.jsx');

var ListCommonActions = {
	
	reload: function(name, options) {
		this.dispatch(ListConstants.LIST_RELOAD, {name: name, options: options});
		
		api.reload(name, options, function(name, options, data) {
			this.dispatch(ListConstants.LIST_RELOAD_SUCCESS, {name: name, options: options, data: data});
		}.bind(this), function(name, options, error) {
			notification.showLocalizedMessage('error', 'reloading-error', 'cannot-reload-data');
			
			this.dispatch(ListConstants.LIST_RELOAD_FAILURE, {name: name, options: options, error: error})
		}.bind(this));
	},
	
	init: function(name, options) {
		this.dispatch(ListConstants.LIST_INIT, {name: name, options: options});
	},
	
	delete: function(name, rest, callback, labelKeyPrefix) {
		api.lock(rest);
		
		api.delete(rest, function() {
			if (!callback || !callback(ListConstants.LIST_DELETE, ListConstants.LIST_DELETE_SUCCESS)) {
				notification.showLocalizedMessage('success',
				i18n.exists(labelKeyPrefix+'-deleting-success') ? labelKeyPrefix+'-deleting-success' : 'deleting-success',
				i18n.exists(labelKeyPrefix+'-deleted-successfully') ? labelKeyPrefix+'-deleted-successfully' : 'deleted-successfully');
			}
			
			var options = this.flux.store('list').getList(name).options;
			options.pageNumber = 1;
			
			this.flux.actions.list.common.reload(name, options);
		}.bind(this), function(error, response, body) {
			if (!callback || !callback(ListConstants.LIST_DELETE, ListConstants.LIST_SAVE_FAILURE, error, response, body)) {
				notification.showLocalizedMessage('error',
				i18n.exists(labelKeyPrefix+'-deleting-error') ? labelKeyPrefix+'-deleting-error' : 'deleting-error',
				i18n.exists(labelKeyPrefix+'-cannot-deleted-data') ? labelKeyPrefix+'-cannot-deleted-data' : 'cannot-deleted-data');
			}
		}.bind(this));
	},
	
	save: function(name, rest, data, callback, labelKeyPrefix) {
		api.lock(rest);
		
		api.save(rest, data, function() {
			if (!callback || !callback(ListConstants.LIST_SAVE, ListConstants.LIST_SAVE_SUCCESS)) {
				notification.showLocalizedMessage('success',
				i18n.exists(labelKeyPrefix+'-saving-success') ? labelKeyPrefix+'-saving-success' : 'saving-success',
				i18n.exists(labelKeyPrefix+'-changes-saved-successfully') ? labelKeyPrefix+'-changes-saved-successfully' : 'changes-saved-successfully');
			}
			
			var options = this.flux.store('list').getList(name).options;
			
			this.flux.actions.list.common.reload(name, options);
		}.bind(this), function(error, response, body) {
			if (!callback || !callback(ListConstants.LIST_SAVE, ListConstants.LIST_SAVE_FAILURE, error, response, body)) {
				notification.showLocalizedMessage('error',
				i18n.exists(labelKeyPrefix+'-saving-error') ? labelKeyPrefix+'-saving-error' : 'saving-error',
				i18n.exists(labelKeyPrefix+'-cannot-save-data') ? labelKeyPrefix+'-cannot-save-data' : 'cannot-save-data');
			}
		}.bind(this));
	},
	
	removeFromStore: function(name){
		this.dispatch(ListConstants.LIST_REMOVE_FROM_STORE, {name: name});
	}
};

module.exports = ListCommonActions;
