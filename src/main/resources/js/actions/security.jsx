var constants = require('./../constants/security.jsx');
var rest = require('./../rest/security.jsx');

var SecurityActions = {
	
	changePassword: function(newPassword, callback) {
		rest.changePassword(newPassword, function() {
			notification.showLocalizedMessage('success', 'change-password-success', 'change-password-success-detail');
			
			callback();
		}.bind(this), function(error) {
			notification.showLocalizedMessage('error', 'change-password-error', 'change-password-error-detail');
		}.bind(this));
	},
	
	logInUsingForm: function(username, password) {
		this.dispatch(constants.SECURITY_LOG_IN, {});
		
		var processError = function(error) {
			this.dispatch(constants.SECURITY_LOG_IN_FAILURE, {error: error})
			
			notification.showLocalizedMessage('error', 'log-in-error', error ? error.messageKey : 'log-in-error-detail', (error && error.parameters) ? error.parameters : undefined);
		}.bind(this);
		
		rest.logInUsingForm(username, password, function(token) {
			global['auth-token'] = token;
			
			rest.loadAllRoles(function(allRoles) {
				rest.loadRolesInOrganizations(function(rolesInOrganizations) {
					notification.showLocalizedMessage('success', 'log-in-success', 'greet-after-log-in');
					
					this.dispatch(constants.SECURITY_LOG_IN_SUCCESS, {token: token, allRoles: allRoles, rolesInOrganizations: rolesInOrganizations});
				}.bind(this), processError);
			}.bind(this), processError);
		}.bind(this), processError);
	},
	
	logInUsingSso: function() {
		this.dispatch(constants.SECURITY_LOG_IN, {});
		
		var processError = function(error) {
			this.dispatch(constants.SECURITY_LOG_IN_FAILURE, {error: error});
		}.bind(this);
		
		rest.logInUsingSso(function(token) {
			global['auth-token'] = token;
			
			rest.loadAllRoles(function(allRoles) {
				rest.loadRolesInOrganizations(function(rolesInOrganizations) {
					notification.showLocalizedMessage('success', 'log-in-success', 'greet-after-log-in');
					
					this.dispatch(constants.SECURITY_LOG_IN_SUCCESS, {token: token, allRoles: allRoles, rolesInOrganizations: rolesInOrganizations});
				}.bind(this), processError);
			}.bind(this), processError);
		}.bind(this), processError);
	},
	
	logOut: function(router) {
		var rest = this.flux.store('form').isAnyDetailChanged();
		if (rest) {
			notification.showLocalizedMessage('warning', 'form-changes-found', 'form-save-or-discard-changes', {}, {
				label: i18n.t('form-leave-changes'),
				callback: function() {
					this.flux.actions.form.unlock(rest, function() {
						if(ssoEnabled) {
							this.dispatch(constants.SECURITY_SSO_LOG_OUT);
							
							window.location = ssoLogoutUrl;
						} else {
							this.dispatch(constants.SECURITY_LOG_OUT);
							notification.showLocalizedMessage('success', 'log-out-success', 'bye-after-log-out');
							
							router.push("/dashboard");
						}
					}.bind(this));
					
					
				}.bind(this)
			}, 0);
			return;
		}
		
		if(ssoEnabled) {
			this.dispatch(constants.SECURITY_SSO_LOG_OUT);
			
			window.location = ssoLogoutUrl;
		} else {
			this.dispatch(constants.SECURITY_LOG_OUT);
			notification.showLocalizedMessage('success', 'log-out-success', 'bye-after-log-out');
			
			router.push("/dashboard");
		}
	}
};

module.exports = SecurityActions;
