var constants = require('./../constants/report.jsx');
var api = require('./../rest/report.jsx');

var ReportActions = {
	
	getXml: function(rest, id, success, failure) {
		this.dispatch(constants.GETTING_XML, {});
		
		api.getXml(rest, id, function(body) {
			this.dispatch(constants.GETTING_XML_SUCCESS, {});
			if (success) {
				success(body);
			}
		}.bind(this), function(error) {
			this.dispatch(constants.GETTING_XML_FAILURE, {});
			if (failure) {
				failure(error);
			}
		}.bind(this));
	},
	
	generateReport: function(rest, template, id, type, name) {
		api.generateReport(rest, template, id, type, name);
	},
};

module.exports = ReportActions;
