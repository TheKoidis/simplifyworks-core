var constants = require('./../constants/workflow.jsx');
var api = require('./../rest/workflow.jsx');
var listApi = require('./../rest/list.jsx');

var WorkflowActions = {
	
	searchTaskDefinitions: function(objectType) {
		this.dispatch(constants.WORKFLOW_SEARCH_TASK_DEFINITIONS, {objectType: objectType});
		
		api.searchTaskDefinitions(objectType, function(taskDefinitions) {
			this.dispatch(constants.WORKFLOW_SEARCH_TASK_DEFINITIONS_SUCCESS, {objectType: objectType, taskDefinitions: taskDefinitions});
		}.bind(this), function(error) {
			this.dispatch(constants.WORKFLOW_SEARCH_TASK_DEFINITIONS_FAILURE, {objectType: objectType, error: error});
		}.bind(this));
	},
	
	searchTasks: function(objectType, name, rest, success) {
		var options = {
			rest: rest,
			
			ordering: [],
			pageNumber: 1,
			pageSize: 10,
			
			quickSearch: {},
			filters: {
				column: {
					visible: false,
					
					types: [],
					values: []
				}
			},
			showFilter: false,
			filter: {}
		}
		this.dispatch(constants.WORKFLOW_SEARCH_TASKS, {objectType: objectType, name: name});
		listApi.reload(name, options, function(name, options, data) {
			this.dispatch(constants.WORKFLOW_SEARCH_TASKS_SUCCESS, {objectType: objectType, name: name, data: data});
			if (success) {
				success(data);
			}
		}.bind(this), function(name, options, error) {
			this.dispatch(constants.WORKFLOW_SEARCH_TASKS_FAILURE, {objectType: objectType, name: name, error: error});
		}.bind(this));
	},
	
	completeTasks: function(objectType, taskIds, actionId, success){
		this.dispatch(constants.WORKFLOW_COMPLETE_TASKS, {objectType: objectType, taskIds: taskIds});
		api.completeTasks(taskIds, actionId, function() {
			notification.showLocalizedMessage('success', 'tasks-successful-completion', 'tasks-completed-successfully');
			this.dispatch(constants.WORKFLOW_COMPLETE_TASKS_SUCCESS, {objectType: objectType, taskIds: taskIds});
			if(success){
				success();
			}
		}.bind(this), function() {
			notification.showLocalizedMessage('error', 'tasks-failed-completion', 'tasks-not-completed');
			this.dispatch(constants.WORKFLOW_COMPLETE_TASKS_FAILURE, {objectType: objectType, taskIds: taskIds});
		}.bind(this));
	},
};

module.exports = WorkflowActions;
