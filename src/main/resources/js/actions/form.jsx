var api = require('./../rest/form.jsx');

var FormActions = {
	
	changeValue: function(restPrefix, rest, property, value, callback, validations) {
		var validationResult = null;
		if (validations) {
			validationResult = ValidationUtils.validate(property, value, validations);
		}
		this.dispatch(FormConstants.FORM_CHANGE_VALUE, {restPrefix: restPrefix, rest: rest, property: property, value: value, callback: callback, validations: validations, validationResult: validationResult});
		
	},
	
	refreshValidation: function(restPrefix, rest, validation) {
		this.dispatch(FormConstants.REFRESH_VALIDATION, {restPrefix: restPrefix, rest: rest, validation: validation});
	},
	
	detailInit: function(restPrefix) {
		this.flux.store('form').getDetail(restPrefix);
		this.dispatch(FormConstants.DETAIL_INIT, {restPrefix: restPrefix});
	},
	
	formInit: function(restPrefix, rest, tabText, validation, initialValue) {
		this.flux.store('form').getForm(restPrefix, rest);
		this.dispatch(FormConstants.FORM_INIT, {restPrefix: restPrefix, rest: rest, validation: validation, tabText: tabText, initialValue: initialValue});
	},
	
	loadEmbeddableData: function(name, data, tabText, validation) {
		this.flux.store('form').getForm(name, null);
		this.dispatch(FormConstants.FORM_LOAD_SUCCESS, {restPrefix: name, rest: null, data: data, validation: validation, tabText: tabText});
	},
	
	confirmModalForm: function(name, validCallback, customValidation) {
		var validationFailed = this.flux.actions.form.validate(name);
		if (validationFailed) {
			this.dispatch(FormConstants.DETAIL_VALIDATION_FAILED, {restPrefix: name, validationFailed: validationFailed});
			return;
		}
		
		if (customValidation) {
			var data = this.flux.store('form').getForm(name).data;
			customValidation(data, function(valid) {
				if (valid) {
					validCallback();
				}
			});
			return;
		}
		
		validCallback();
	},
	
	closeModalForm: function(name) {
		this.dispatch(FormConstants.CLOSE_MODAL_SUCCESS, {restPrefix: name});
	},
	
	delete: function(rest, callback, labelKeyPrefix) {
		api.delete(rest, function() {
			if (!callback || !callback(FormConstants.DETAIL_DELETE, FormConstants.DETAIL_DELETE_SUCCESS)) {
				notification.showLocalizedMessage('success',
				i18n.exists(labelKeyPrefix+'-deleting-success') ? labelKeyPrefix+'-deleting-success' : 'deleting-success',
				i18n.exists(labelKeyPrefix+'-deleted-successfully') ? labelKeyPrefix+'-deleted-successfully' : 'deleted-successfully');
			}
		}.bind(this), function(error, response, body) {
			if (!callback || !callback(FormConstants.DETAIL_DELETE, FormConstants.DETAIL_SAVE_FAILURE, error, response, body)) {
				notification.showLocalizedMessage('error',
				i18n.exists(labelKeyPrefix+'-deleting-error') ? labelKeyPrefix+'-deleting-error' : 'deleting-error',
				i18n.exists(labelKeyPrefix+'-cannot-deleted-data') ? labelKeyPrefix+'-cannot-deleted-data' : 'cannot-deleted-data');
			}
		}.bind(this));
	},
	
	load: function(restPrefix, rest, tabText, validation) {
		this.dispatch(FormConstants.FORM_LOAD, {restPrefix: restPrefix, rest: rest});
		
		api.load(restPrefix + (rest != null ? '/' + rest : ''), function(data) {
			this.dispatch(FormConstants.FORM_LOAD_SUCCESS, {restPrefix: restPrefix, rest: rest, data: data, validation: validation, tabText: tabText});
		}.bind(this), function(error) {
			notification.showLocalizedMessage('error', 'loading-error', 'cannot-load-data');
			
			this.dispatch(FormConstants.FORM_LOAD_FAILURE, {restPrefix: restPrefix, rest: rest, error: error})
		}.bind(this));
	},
	
	holdLock: function(restPrefix) {
		api.holdLock(restPrefix, function(data) {
			//TODO
		}.bind(this), function(error) {
			notification.showLocalizedMessage('error', 'locking-error', 'cannot-lock-data');
		}.bind(this));
	},
	
	lock: function(restPrefix, successCallback) {
		this.dispatch(FormConstants.DETAIL_LOCK, {restPrefix: restPrefix});
		
		api.lock(restPrefix, function(data) {
			this.dispatch(FormConstants.DETAIL_LOCK_SUCCESS, {restPrefix: restPrefix, data: data});
			if (successCallback) {
				successCallback();
			}
		}.bind(this), function(error) {
			notification.showLocalizedMessage('error', 'locking-error', 'cannot-lock-data');
			
			this.dispatch(FormConstants.DETAIL_LOCK_FAILURE, {restPrefix: restPrefix, error: error})
		}.bind(this));
	},
	
	unlock: function(restPrefix, successCallback) {
		this.dispatch(FormConstants.DETAIL_UNLOCK, {restPrefix: restPrefix});
		
		api.unlock(restPrefix, function(data) {
			this.dispatch(FormConstants.DETAIL_UNLOCK_SUCCESS, {restPrefix: restPrefix, data: data});
			if (successCallback) {
				successCallback();
			}
		}.bind(this), function(error) {
			notification.showLocalizedMessage('error', 'unlocking-error', 'cannot-unlock-data');
			
			this.dispatch(FormConstants.DETAIL_UNLOCK_FAILURE, {restPrefix: restPrefix, error: error})
		}.bind(this));
	},
	
	composeData: function(restPrefix) {
		var data = {};
		
		var detail = this.flux.store('form').getDetail(restPrefix);
		
		for (var key in detail.forms) {
			var form = detail.forms[key];
			if (form.modified) {
				if (key === '' || key === 'DEFAULT') {
					data = form.data;
				} else {
					data[key] = form.data;
				}
			}
		};
		
		return data;
	},
	
	create: function(restPrefix, callback, labelKeyPrefix, rest, customValidation) {
		var data = this.flux.actions.form.composeData(restPrefix);
		
		var validationFailed = this.flux.actions.form.validate(restPrefix);
		if (validationFailed) {
			this.dispatch(FormConstants.DETAIL_VALIDATION_FAILED, {restPrefix: restPrefix, validationFailed: validationFailed});
			return;
		}
		
		if (customValidation) {
			customValidation(data, this.flux.actions.form.createCallback.bind(this, data, restPrefix, callback, labelKeyPrefix, rest));
			return;
		}
		
		this.flux.actions.form.createCallback(data, restPrefix, callback, labelKeyPrefix, rest, true, []);
	},
	
	createCallback: function(data, restPrefix, callback, labelKeyPrefix, rest, valid, invalidTabs) {
		if (!valid) {
			this.dispatch(FormConstants.DETAIL_CUSTOM_VALIDATION_FAILED, {restPrefix: restPrefix, invalidTabs: invalidTabs});
			return;
		}
		
		this.dispatch(FormConstants.DETAIL_CREATE, {restPrefix: restPrefix, data: data});
		
		api.create(restPrefix, data, function(id) {
			this.dispatch(FormConstants.DETAIL_CREATE_SUCCESS, {restPrefix: restPrefix, id: id});
			
			if (!callback || !callback(FormConstants.DETAIL_CREATE, id)) {
				notification.showLocalizedMessage('success',
				i18n.exists(labelKeyPrefix+'-creation-success') ? labelKeyPrefix+'-creation-success' : 'creation-success',
				i18n.exists(labelKeyPrefix+'-created-successfully') ? labelKeyPrefix+'-created-successfully' : 'created-successfully');
			}
		}.bind(this), function(error, response, body) {
			this.dispatch(FormConstants.DETAIL_CREATE_FAILURE, {restPrefix: restPrefix, error: error})
			
			if (!callback || !callback(FormConstants.DETAIL_CREATE, null, error, response, body)) {
				if(body.messageKey) {
					notification.showLocalizedMessage('error', i18n.exists(labelKeyPrefix+'-create-creation-error') ? labelKeyPrefix+'-create-creation-error' : 'creation-error', body.messageKey, body.parameters, null, 0);
				} else {
					notification.showLocalizedMessage('error',
					i18n.exists(labelKeyPrefix+'-create-creation-error') ? labelKeyPrefix+'-create-creation-error' : 'creation-error',
					i18n.exists(labelKeyPrefix+'-create-cannot-create-data') ? labelKeyPrefix+'-create-cannot-create-data' : 'cannot-create-data');
				}
			}
		}.bind(this));
	},
	
	validateOnly: function(restPrefix, callback, labelKeyPrefix, rest, customValidation, reload) {
		var data = this.flux.actions.form.composeData(restPrefix);
		
		var validationFailed = this.flux.actions.form.validate(restPrefix);
		if (validationFailed) {
			this.dispatch(FormConstants.DETAIL_VALIDATION_FAILED, {restPrefix: restPrefix, validationFailed: validationFailed});
			return;
		}
		
		if (customValidation) {
			customValidation(data, this.flux.actions.form.validateOnlyCallback.bind(this, data, restPrefix, callback, labelKeyPrefix, rest, reload));
			return;
		}
		
		this.flux.actions.form.validateOnlyCallback(data, restPrefix, callback, labelKeyPrefix, rest, reload, true, []);
	},
	
	validateOnlyCallback: function(data, restPrefix, callback, labelKeyPrefix, rest, reload, valid, invalidTabs) {
		if (!valid) {
			this.dispatch(FormConstants.DETAIL_CUSTOM_VALIDATION_FAILED, {restPrefix: restPrefix, invalidTabs: invalidTabs});
			return;
		} else {
			notification.showLocalizedMessage('success', 'validation-ok', 'validation-ok-text');
		}
	},
	
	save: function(restPrefix, callback, labelKeyPrefix, rest, customValidation, reload) {
		var data = this.flux.actions.form.composeData(restPrefix);
		
		var validationFailed = this.flux.actions.form.validate(restPrefix);
		if (validationFailed) {
			this.dispatch(FormConstants.DETAIL_VALIDATION_FAILED, {restPrefix: restPrefix, validationFailed: validationFailed});
			return;
		}
		
		if (customValidation) {
			customValidation(data, this.flux.actions.form.saveCallback.bind(this, data, restPrefix, callback, labelKeyPrefix, rest, reload));
			return;
		}
		
		this.flux.actions.form.saveCallback(data, restPrefix, callback, labelKeyPrefix, rest, reload, true, []);
	},
	
	saveCallback: function(data, restPrefix, callback, labelKeyPrefix, rest, reload, valid, invalidTabs) {
		if (!valid) {
			this.dispatch(FormConstants.DETAIL_CUSTOM_VALIDATION_FAILED, {restPrefix: restPrefix, invalidTabs: invalidTabs});
			return;
		}
		
		this.dispatch(FormConstants.DETAIL_SAVE, {restPrefix: restPrefix, data: data});
		
		api.save(restPrefix, data, function(data) {
			var form = this.flux.store('form').getForm(restPrefix, rest);
			this.dispatch(FormConstants.DETAIL_SAVE_SUCCESS, {restPrefix: restPrefix, data: data});
			if (reload) {
				this.flux.actions.form.load(restPrefix, rest, form.tabText, form.validation);
			}
			
			if (!callback || !callback(FormConstants.DETAIL_SAVE, FormConstants.DETAIL_SAVE_SUCCESS)) {
				notification.showLocalizedMessage('success',
				i18n.exists(labelKeyPrefix+'-saving-success') ? labelKeyPrefix+'-saving-success' : 'saving-success',
				i18n.exists(labelKeyPrefix+'-changes-saved-successfully') ? labelKeyPrefix+'-changes-saved-successfully' : 'changes-saved-successfully');
			}
		}.bind(this), function(error, response, body) {
			this.dispatch(FormConstants.DETAIL_SAVE_FAILURE, {restPrefix: restPrefix, error: error})
			
			if (!callback || !callback(FormConstants.DETAIL_SAVE, FormConstants.DETAIL_SAVE_FAILURE, error, response, body)) {
				notification.showLocalizedMessage('error',
				i18n.exists(labelKeyPrefix+'-saving-error') ? labelKeyPrefix+'-saving-error' : 'saving-error',
				i18n.exists(labelKeyPrefix+'-cannot-save-data') ? labelKeyPrefix+'-cannot-save-data' : 'cannot-save-data');
			}
		}.bind(this));
	},
	
	validate: function(restPrefix) {
		var detail = this.flux.store('form').getDetail(restPrefix);
		var validationFailed;
		
		for (var formName in detail.forms) {
			var form = detail.forms[formName];
			
			if (form.validation) {
				for (var property in form.validation) {
					if (!form.validation[property].hidden) {
						var validationResult = ValidationUtils.validate(property, PropertyUtils.getPropertyValue(form.data, property), form.validation[property].validations);
						if (validationResult) {
							if (!validationFailed) {
								validationFailed = {};
							}
							if (!validationFailed[formName]) {
								validationFailed[formName] = {};
								validationFailed[formName].tabText = form.tabText;
								validationFailed[formName].validation = {};
							}
							validationFailed[formName].validation[property] = {
								label: form.validation[property].label,
								validationResult: validationResult
							};
						}
					}
				};
			}
		};
		
		return validationFailed;
	}
};

module.exports = FormActions;
