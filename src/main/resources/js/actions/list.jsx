var ListCommonActions = require('./list/common.jsx');
var ListPaginationActions = require('./list/pagination.jsx');
var ListOrderingActions = require('./list/ordering.jsx');
var ListQuickSearchActions = require('./list/quick-search.jsx');
var ListSelectionActions = require('./list/selection.jsx');
var ListFilterActions = require('./list/filter.jsx');

var ListActions = {
    common: ListCommonActions,
    pagination: ListPaginationActions,
    ordering: ListOrderingActions,
    quickSearch: ListQuickSearchActions,
    selection: ListSelectionActions,
	filter: ListFilterActions
};

module.exports = ListActions;
