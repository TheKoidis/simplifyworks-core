var constants = require('./../constants/task.jsx');
var rest = require('./../rest/task.jsx');

var TaskActions = {

  getTasks: function() {
    this.dispatch(constants.TASK_GET_TASKS, {});
  },

  loadTasks: function() {
    rest.loadTasks(function(data) {
      this.dispatch(constants.TASK_LOAD_TASKS, data);
    }.bind(this), function(error) {
      notification.showLocalizedMessage('error', 'reloading-error', 'cannot-reload-data');
    }.bind(this));
  },

  saveOneTimeTrigger: function(taskName, description, time, callback) {
    rest.saveOneTimeTrigger(taskName, description, time, function() {
      notification.showLocalizedMessage('success', 'save-success', 'save-trigger');
      this.dispatch(constants.TASK_SAVE_ONE_TIME_TRIGGER, {});
      callback();
    }.bind(this), function(error) {
      notification.showLocalizedMessage('error', 'save-error', 'cannot-save-oneTimeTrigger');
    }.bind(this));
  },

  saveCronTrigger: function(taskName, description, cron, callback) {
    rest.saveCronTrigger(taskName, description, cron, function() {
      notification.showLocalizedMessage('success', 'save-success', 'save-trigger');
      this.dispatch(constants.TASK_SAVE_CRON_TRIGGER, {});
      callback();
    }.bind(this), function(error) {
      notification.showLocalizedMessage('error', 'save-error', 'cannot-save-cronTrigger');
    }.bind(this));
  },

  runNow: function(taskName) {
    rest.runNow(taskName, function() {
      notification.showLocalizedMessage('success', 'run-success', 'run-task');
      this.dispatch(constants.TASK_RUN_NOW, {});
    }.bind(this), function(error) {
      notification.showLocalizedMessage('error', 'run-error', 'cannot-run-task');
    }.bind(this));
  },

  changeTriggerState: function(taskName, triggerName, state, callback) {
    rest.changeTriggerState(taskName, triggerName, state, function() {
      notification.showLocalizedMessage('success', 'save-success', 'change-trigger');
      this.dispatch(constants.TASK_CHANGE_TRIGGER_STATE, {});
      callback();
    }.bind(this), function(error) {
      notification.showLocalizedMessage('error', 'save-error', 'cannot-change-trigger');
    }.bind(this));
  },

  removeTrigger: function(taskName, triggerName, callback) {
    rest.removeTrigger(taskName, triggerName, function() {
      notification.showLocalizedMessage('success', 'remove-success', 'remove-trigger');
      this.dispatch(constants.TASK_REMOVE_TRIGGER, {});
      callback();
    }.bind(this), function(error) {
      notification.showLocalizedMessage('error', 'remove-error', 'cannot-remove-trigger');
    }.bind(this));
  },

};

module.exports = TaskActions;
