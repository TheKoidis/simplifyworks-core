module.exports = {
	application: require('./application.jsx'),
	list: require('./list.jsx'),
	form: require('./form.jsx'),
	task: require('./task.jsx'),
	security: require('./security.jsx'),
	workflow: require('./workflow.jsx'),
	report: require('./report.jsx')
};
