var LanguageSelector = require('./../localization/language-selector.jsx');

/**
* Component for form based authentication (username with password)
*/
var FormAuth = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('security')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	getInitialState: function() {
		return {
			username: '',
			password: '',
		}
	},
	
	getStateFromFlux: function() {
		return {
			info: this.context.flux.store('security').getInfo()
		};
	},
	
	render: function() {
		var header = (
			<h3>{i18n.t('form-auth-header')}</h3>
		);
		
		var footer = (
			<Grid fluid>
				<Row>
					<Col xs={6} className='language' style={{paddingLeft: 0}}>
						<LanguageSelector template={this.props.template} />
					</Col>
					
					<Col xs={6} className='text-right' style={{paddingRight: 0}}>
						<Button type='submit' bsStyle='primary' disabled={this.state.info.status === 'loggingIn'} onClick={this.logIn}>
							{i18n.t('form-auth-log-in')}
						</Button>
					</Col>
				</Row>
			</Grid>
		);
		
		var usernameIcon = (
			<Glyphicon glyph='user' />
		);
		
		var passwordIcon = (
			<Glyphicon glyph='lock' />
		);
		
		return (
			<Grid style={{marginTop: 50}}>
				<Row>
					<Col xs={12} md={8} mdPush={2} lg={6} lgPush={3}>
						<form>
							<Panel header={header} footer={footer}>
								<Input
									type='text'
									addonBefore={usernameIcon}
									placeholder={i18n.t('form-auth-username')}
									value={this.state.username}
									onChange={this.changeUsername}
									disabled={this.state.info.status === 'loggingIn'} />
								
								<Input
									type='password'
									addonBefore={passwordIcon}
									placeholder={i18n.t('form-auth-password')}
									value={this.state.password}
									onChange={this.changePassword}
									disabled={this.state.info.status === 'loggingIn'} />
							</Panel>
						</form>
					</Col>
				</Row>
			</Grid>
		);
	},
	
	changeUsername: function(event) {
		this.setState({
			username: event.target.value
		});
	},
	
	changePassword: function(event) {
		this.setState({
			password: event.target.value
		});
	},
	
	logIn: function(event) {
		event.preventDefault();
		this.context.flux.actions.security.logInUsingForm(this.state.username, this.state.password);
	}
});

module.exports = FormAuth;
