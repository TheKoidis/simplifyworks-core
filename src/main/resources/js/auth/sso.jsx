var LanguageSelector = require('./../localization/language-selector.jsx');

var SsoAuth = React.createClass({
	
	/**
	* FluxMixin, StoreWatchMixin('security')
	*/
	mixins: [FluxMixin, StoreWatchMixin('security')],
	
	contextTypes: {
		/**
		* flux
		*/
		flux: React.PropTypes.object.isRequired
	},
	
	getStateFromFlux: function() {
		return {
			info: this.context.flux.store('security').getInfo()
		};
	},
	
	componentDidMount: function() {
		if(this.state.info.status == 'notLoggedIn') {
			this.logInUsingSso();
		}
	},
	
	render: function() {
		var header = (
			<h3>{i18n.t('sso-auth-header')}</h3>
		);
		
		var footer = (
			<Grid fluid>
				<Row>
					<Col xs={6} className='language' style={{paddingLeft: 0}}>
						<LanguageSelector template={this.props.template} />
					</Col>
					
					<Col xs={6} className='text-right' style={{paddingRight: 0}}>
						<ButtonGroup>
							<Button type='submit' bsStyle='danger' disabled={this.state.info.status === 'loggingIn'} onClick={this.logOut}>
								{i18n.t('auth-log-out')}
							</Button>
							<Button type='submit' bsStyle='primary' disabled={this.state.info.status === 'loggingIn'} onClick={this.logInUsingSso}>
								{i18n.t('sso-auth-try-again')}
							</Button>
						</ButtonGroup>
					</Col>
				</Row>
			</Grid>
		);
		
		switch(this.state.info.status) {
			case 'loggingIn':
			return (
				<div id='loading-disabled' onClick={false}>
					<div id='loading-img'>
						<Image src={contextName + '/images/reload.gif'} />
						<p>{i18n.t('table-loading')}</p>
					</div>
				</div>
			);
			case 'notLoggedIn':
			return (
				<Grid style={{marginTop: 50}}>
					<Row>
						<Col xs={12} md={8} mdPush={2} lg={6} lgPush={3}>
							<Panel header={header} footer={footer}>
								{this.state.info.error
									?	i18n.t(this.state.info.error.messageKey, this.state.info.error.parameters ? this.state.info.error.parameters : undefined)
									:	null
								}
							</Panel>
						</Col>
					</Row>
				</Grid>
			);
			default:
			return null;
		}
	},
	
	logInUsingSso: function() {
		this.context.flux.actions.security.logInUsingSso();
	},
	
	logOut: function() {
		this.context.flux.actions.security.logOut();
	}
});

module.exports = SsoAuth;
