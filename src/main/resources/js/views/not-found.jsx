var NotFoundTitle = React.createClass({
    
    render: function() {
        return (
            <span>{i18n.t('not-found')}</span>
        );
    }
});

var NotFound = React.createClass({

    contextTypes: {
        /**
         * router
         */
        router: React.PropTypes.object.isRequired
    },
    
    render: function() {
        return (
            <div>
                <h1>
                    {i18n.t('not-found-header')}
                </h1>

                <p>{i18n.t('not-found-info')}</p>
                <Button bsStyle='primary' onClick={this.goBack}>{i18n.t('not-found-back')}</Button>
            </div>
        );
    },
    goBack: function(){
        this.context.router.goBack();
    }
});

module.exports = {title: NotFoundTitle, main: NotFound};
