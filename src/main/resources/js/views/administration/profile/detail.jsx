var LanguageSelector = require('./../../../localization/language-selector.jsx');

var ProfileTitle = React.createClass ({
	
	render: function(){
		return (
			<span>{i18n.t('user-profile')}</span>
		);
	}
});

var ProfileMain = React.createClass ({
	
	mixins: [FluxMixin, StoreWatchMixin('security')],
	
	contextTypes: {
		router: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		/**
		* Page template
		*/
		template: React.PropTypes.any
	},
	
	getInitialState: function() {
		return {
			changePasswordModalVisible: false,
			newPassword: '',
			newPasswordValidationEnabled: false
		};
	},
	
	getStateFromFlux: function() {
		return {
			username: this.context.flux.store('security').getOriginalUsername(),
			fullname: PersonSelection.renderText(this.context.flux.store('security').getPerson()),
			email: this.context.flux.store('security').getPerson().email,
		};
	},
	
	render: function() {
		return (
			<Grid fluid>
				<Row>
					<Col xs={12} md={6} mdPush={3} lg={4} lgPush={4} className='text-right'>
						<div className='user-profile-header'>
							<Button bsStyle='link' onClick={this.goBack}>
								<Glyphicon glyph='remove' />
								{' ' + i18n.t('back')}
							</Button>
						</div>
					</Col>
				</Row>
				
				<Row>
					<Col xs={12} md={6} mdPush={3} lg={4} lgPush={4}>
						<div className='user-profile-body'>
							<Grid fluid>
								<Row>
									<Col xs={12} md={3}>
										{i18n.t('core-user')}
									</Col>
									
									<Col xs={12} md={9}>
										<strong>
											{this.state.username}
										</strong>
									</Col>
								</Row>
								
								<Row>
									<Col xs={12} md={3}>
										{i18n.t('core-person')}
									</Col>
									
									<Col xs={12} md={9}>
										<strong>
											{this.state.fullname}
										</strong>
									</Col>
								</Row>
								
								<Row>
									<Col xs={12} md={3}>
										{i18n.t('core-person-email')}
									</Col>
									
									<Col xs={12} md={9}>
										<strong>
											{this.state.email}
										</strong>
									</Col>
								</Row>
								
								<Row>
									<Col xs={12} md={3}>
										{i18n.t('core-user-password')}
									</Col>
									
									<Col xs={12} md={9}>
										<Button bsStyle='link' onClick={this.showChangePasswordModal} style={{padding: 0}}>
											{i18n.t('change-password')}
										</Button>
										
										<Modal show={this.state.changePasswordModalVisible} onHide={this.disposeChangePasswordModal}>
											<Modal.Header closeButton>
												<Modal.Title>{i18n.t('change-password')}</Modal.Title>
											</Modal.Header>
											
											<Modal.Body>
												<Input type='password' value={this.state.newPassword} label={i18n.t('core-user-newPassword')} onChange={this.onNewPasswordChange} autoComplete='off'
													bsStyle={this.getNewPasswordValidationStatus()} help={this.getNewPasswordValidationText()}/>
											</Modal.Body>
											
											<Modal.Footer>
												<Button onClick={this.disposeChangePasswordModal} bsStyle='link'>
													<Glyphicon glyph='remove' />
													{' ' + i18n.t('exit')}
												</Button>
												
												<Button bsStyle='primary' onClick={this.changePassword} disabled={!this.state.newPasswordValidationEnabled || this.getValidationResults().length !== 0}>
													<Glyphicon glyph='ok' />
													{' ' + i18n.t('ok')}
												</Button>
											</Modal.Footer>
										</Modal>
									</Col>
								</Row>
								
							</Grid>
						</div>
					</Col>
				</Row>
				
				<Row>
					<Col xs={12} md={6} mdPush={3} lg={4} lgPush={4} className='language'>
						<Grid fluid>
							<Row>
								<Col xs={12} md={6}>{i18n.t('language-bilingual')}</Col>
								<Col xs={12} md={6} className='text-right'><LanguageSelector template={this.props.template} /></Col>
							</Row>
						</Grid>
					</Col>
				</Row>
			</Grid>
		);
	},
	
	goBack: function() {
		this.context.router.goBack();
	},
	
	showChangePasswordModal: function() {
		this.setState({
			changePasswordModalVisible: true
		});
	},
	
	disposeChangePasswordModal: function() {
		this.setState({
			changePasswordModalVisible: false,
			newPassword: '',
			newPasswordValidationEnabled: false
		});
	},
	
	onNewPasswordChange: function(event) {
		this.setState({
			newPassword: event.target.value,
			newPasswordValidationEnabled: true
		});
	},
	
	getNewPasswordValidationStatus: function() {
		return this.getValidationResults().length === 0 ? null : 'warning';
	},
	
	getNewPasswordValidationText: function() {
		return (
			<Label bsStyle='warning'>
				{this.getValidationResults().map(function(element, index, array) {
					return i18n.t(element.name, element.args);
				}).join(' ')}
			</Label>
		);
	},
	
	getValidationResults: function() {
		var validationResults = [];
		
		if(this.state.newPasswordValidationEnabled) {
			var validationResult = ValidationUtils.validateRequired(this.state.newPassword);
			if(validationResult) {
				validationResults.push(validationResult);
			}
			
			validationResult = ValidationUtils.validateLengthRange(this.state.newPassword, [8, 50]);
			if(validationResult) {
				validationResults.push(validationResult);
			}
		}
		
		return validationResults;
	},
	
	changePassword: function() {
		this.context.flux.actions.security.changePassword(this.state.newPassword, this.disposeChangePasswordModal);
	}
});

module.exports = {title: ProfileTitle, main: ProfileMain};
