var SettingListTitle = React.createClass({
	
	render: function() {
		return (
			<span>
				{i18n.t('core-settings')}
			</span>
		);
	}
});

var SettingList = React.createClass({
	renderTitle: function(setting) {
		var label = !setting ? i18n.t('core-setting-new') : i18n.t('core-setting-detail', {name: setting.settingKey}) ;
		
		return (
			<span>
				{label}
			</span>
		);
	},
	
	render: function() {
		return (
			<SW.List name='core-setting' restPrefix='/api/core/settings' rest=''>
				<SW.List.Table>
					<SW.List.Selection property='id' width='2.5%'/>
					<SW.List.String property='settingKey' width='22.5%' pathProperty='id'/>
					<SW.List.String property='value' width='60%'/>
					<SW.List.Boolean property='system'/>
				</SW.List.Table>
				
				<SW.List.ActivitiesGroup>
					<SW.List.LinkModal icon='plus' labelKey='create-new'/>
				</SW.List.ActivitiesGroup>
				
				<SW.List.DetailModal renderTitle={this.renderTitle}>
					<SW.List.FormModal rest='/api/core/settings' labelKeyPrefix='core-setting'>
						<SW.Form.Section>
							<SW.Form.String size={8} property='settingKey' validations="required;length(50)"/>
							<SW.Form.Boolean size={4} property='system' showNull={false} />
							<SW.Form.String size={12} property='value' type='area' validations='required'/>
						</SW.Form.Section>
					</SW.List.FormModal>
				</SW.List.DetailModal>
				
				<SW.List.QuickSearch />
				<SW.List.Pagination />
				<SW.List.Info />
			</SW.List>
		);
	}
});

module.exports = {title: SettingListTitle, main: SettingList};
