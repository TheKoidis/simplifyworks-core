var WorkflowTitle = React.createClass ({
	
	render: function(){
		return (
			<span>{i18n.t('core-workflow')}</span>
		);
	}
});

var WorkflowMain = React.createClass ({
	
	getInitialState: function() {
		return {
			processDefinitionIdForDiagram: null
		};
	},
	
	renderTitle: function(processDefinition) {
		return processDefinition ? processDefinition.name : i18n.t('core-workflow-process-definition-create-new');
	},
	
	renderNameColumn: function(resource, property) {
		return (
			<div>
				<Button bsSize='xs' bsStyle='link' style={{cursor: 'help'}} onClick={this.showDiagram.bind(this, PropertyUtils.getPropertyValue(resource, 'id'))}>
					<strong>{PropertyUtils.getPropertyValue(resource, property)}</strong>
				</Button>
				
				<Modal show={this.state.processDefinitionIdForDiagram === PropertyUtils.getPropertyValue(resource, 'id')} onHide={this.closeDiagram} container={this} className='workflow-process-definition-diagram-modal'>
					<Modal.Header closeButton>
						<Modal.Title id="contained-modal-title">{PropertyUtils.getPropertyValue(resource, 'name')}</Modal.Title>
					</Modal.Header>
					
					<Modal.Body style={{height: window.innerHeight * 0.9}}>
						<div>
							{this.state.processDefinitionIdForDiagram
								?
								<Image src={hostName + contextName + '/api/workflow/process-definitions/diagrams/' + this.state.processDefinitionIdForDiagram + '?auth-token=' + global['auth-token']} responsive />
								:
								null
							}
						</div>
					</Modal.Body>
				</Modal>
			</div>
		);
	},
	
	showDiagram: function(processDefinitionId) {
		this.setState({
			processDefinitionIdForDiagram: processDefinitionId
		});
	},
	
	closeDiagram: function() {
		this.setState({
			processDefinitionIdForDiagram: null
		});
	},
	
	render: function() {
		return (
			<SW.List name='core-workflow-process-definition' restPrefix='/api/workflow/process-definitions' rest=''>
				<SW.List.Table>
					<SW.List.String property='key' width='15%' />
					<SW.List.Custom property='name' width='15%' render={this.renderNameColumn} />
					<SW.List.String property='description' width='40%' orderable={false} />
					<SW.List.String property='category' width='20%' />
					<SW.List.Number property='version' width='5%'/>
				</SW.List.Table>
				
				<SW.List.ActivitiesGroup>
					<SW.List.LinkModal icon='plus' labelKey='core-workflow-process-definition-create-new' />
				</SW.List.ActivitiesGroup>
				
				<SW.List.DetailModal renderTitle={this.renderTitle}>
					<SW.List.FormModal rest='/api/workflow/deployments' labelKeyPrefix='core-workflow-process-definition'>
						<SW.Form.Section>
							<SW.Form.AttachmentsSimple size={12} property='attachments' />
						</SW.Form.Section>
					</SW.List.FormModal>
				</SW.List.DetailModal>
				
				<SW.List.Pagination />
				<SW.List.Info />
			</SW.List>
		);
	}
});

module.exports = {title: WorkflowTitle, main: WorkflowMain};
