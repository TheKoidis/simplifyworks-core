var TemplateDetailTitle = React.createClass({
	
	render: function() {
		return RenderUtils.renderDetailTitle('core-template', this.props.params.templateId);
	}
});

var TemplateDetail = React.createClass({
	
	contextTypes: {
		router: React.PropTypes.object.isRequired,
	},
	
	newCallback: function(type, id) {
		if (type == FormConstants.FORM_CREATE && id) {
			this.context.router.push('/system/template/' + id);
		}
	},
	
	render: function() {
		var isNew = this.props.params.templateId === 'new';
		
		// TODO replace body string with HTML editor
		
		return (
			<SW.Detail rest='/api/core/templates' restId={this.props.params.templateId} exitPath='/system/templates' tabName={this.props.params.tabName}>
				<SW.Form rest='' labelKey='core-template' new={isNew} newCallback={this.newCallback} labelKeyPrefix='core-template' tabName='default'>
					<SW.Form.Section icon='pencil' labelKey='basic-info'>
						<SW.Form.String size={6} property='code' validations='required;lengthRange(1,50)'/>
						<SW.Form.String size={6} property='subject' validations='length(255)'/>
						<SW.Form.String size={12} property='description' type='area' validations='length(2000)'/>
						<SW.Form.String size={12} property='body' type='area' validations='required'/>
					</SW.Form.Section>
				</SW.Form>
			</SW.Detail>
		);
	}
});

module.exports = {title: TemplateDetailTitle, main: TemplateDetail};
