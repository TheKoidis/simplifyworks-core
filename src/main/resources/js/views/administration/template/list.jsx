var TemplateListTitle = React.createClass({
	
	render: function() {
		return (
			<span>
				{i18n.t('core-templates')}
			</span>
		);
	}
});

var TemplateList = React.createClass({
	
	render: function() {
		return (
			<SW.List name='core-template' restPrefix='/api/core/templates' rest=''>
				<SW.List.Table>
					<SW.List.Selection property='id' width='2.5%'/>
					<SW.List.String property='code' width='15%' path='/system/template' pathProperty='id'/>
					<SW.List.String property='description'/>
				</SW.List.Table>
				
				<SW.List.ActivitiesGroup>
					<SW.List.Link icon='plus' labelKey='core-template-create-new' path={'/system/template/new'}/>
				</SW.List.ActivitiesGroup>
				
				<SW.List.QuickSearch properties={['code', 'description']}/>
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List>
		);
	}
});

module.exports = {title: TemplateListTitle, main: TemplateList};
