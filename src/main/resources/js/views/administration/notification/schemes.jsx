var SchemesTitle = React.createClass ({
	
	render: function(){
		return (
			<span>{i18n.t('core-notification-schemes')}</span>
		);
	}
});

var SchemesMain = React.createClass ({
	
	render: function() {
		return null;
	}
});

module.exports = {title: SchemesTitle, main: SchemesMain};
