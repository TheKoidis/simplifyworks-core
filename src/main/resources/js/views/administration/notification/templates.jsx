var TemplatesTitle = React.createClass ({
	
	render: function(){
		return (
			<span>{i18n.t('core-notification-templates')}</span>
		);
	}
});

var TemplatesMain = React.createClass ({
	
	render: function() {
		return null;
	}
});

module.exports = {title: TemplatesTitle, main: TemplatesMain};
