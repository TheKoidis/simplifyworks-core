var DateTime = require('react-datetime');
var moment = require('moment');

var TaskListTitle = React.createClass({
	
	render: function() {
		return (
			<span>
				{i18n.t('core-tasks')}
			</span>
		);
	}
});

var TaskList = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('task')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	getInitialState: function() {
		return {
			taskName: null,
			modal: null,
			description: '',
			descriptionValid: '',
			time: '',
			timeValid: '',
			cron: '',
			cronValidRequired: '',
			cronValidLength: ''
		};
	},
	
	getStateFromFlux: function() {
		return {
			tasks: this.context.flux.store("task").getTasks()
		};
	},
	
	componentDidMount: function() {
		this.context.flux.actions.task.loadTasks();
	},
	
	getItems: function(tasks) {
		var items = [];
		for (var i = 0; i < tasks.length; i++) {
			items.push(this.getTaskBox(tasks[i]));
		}
		return items;
	},
	
	getTaskBox: function(task) {
		var triggers = task.triggers;
		return (
			<Panel key={task.name}>
				<span className='lead'>{i18n.t('core-task-' + task.name)}</span>
				<ButtonGroup bsSize='small' className='pull-right'>
					<Button onClick={this.openModal.bind(this, 'oneTimeTrigger', task.name)}>
						<Glyphicon glyph='time'/>
						{' ' + i18n.t('core-task-setOneTimeTrigger')}
					</Button>
					<Button onClick={this.openModal.bind(this, 'cronTrigger', task.name)}>
						<Glyphicon glyph='time'/>
						{' ' + i18n.t('core-task-setCronTriger')}
					</Button>
					<Button bsStyle='success'
						onClick={this.context.flux.actions.task.runNow.bind(this, task.name)}>
						<Glyphicon glyph='play'/>
						{' ' + i18n.t('core-task-runNow')}
					</Button>
				</ButtonGroup>
				<div>
					<br />
					{triggers.length > 0 ? this.createTriggerTable(task) : i18n.t('core-task-no-triggers')}
				</div>
			</Panel>
		);
	},
	
	createTriggerTable: function(task) {
		return (
			<Table hover className='table table-striped table-bordered table-condensed' cellSpacing='0' width='100%'>
				<thead>
					<tr>
						<th style={{'width': '30%'}}>
							<span>{i18n.t('core-task-description')}</span>
						</th>
						<th style={{'width': '20%'}}>
							<span>{i18n.t('core-task-detail')}</span>
						</th>
						<th style={{'width': '20%'}}>
							<span>{i18n.t('core-task-nextFireTime')}</span>
						</th>
						<th style={{'width': '20%'}}>
							<span>{i18n.t('core-task-previousFireTime')}</span>
						</th>
						<th style={{'width': '4%'}}>
							<span>{i18n.t('core-task-active')}</span>
						</th>
						<th style={{'width': '6%'}}/>
					</tr>
				</thead>
				<tbody>
					{this.createTriggerTableRows(task)}
				</tbody>
			</Table>
		);
	},
	
	createTriggerTableRows: function(task) {
		var rows = [];
		var triggers = task.triggers;
		for (var index = 0; index < triggers.length; index++) {
			var trigger = triggers[index];
			rows.push(
				<tr>
					<td>
						{trigger.description}
					</td>
					<td>
						{i18n.t('core-task-' + (trigger.type === 'OneTimeTaskTrigger' ? 'oneTimeTrigger' : 'cronTrigger'))}
					</td>
					<td>
						{trigger.nextFireTime ? dateFormat(new Date(trigger.nextFireTime), i18n.t('dateTimeLocale') === 'cs' ? 'dd.mm.yyyy HH:MM' : 'mm/dd/yyyy HH:MM') : ''}
					</td>
					<td>
						{trigger.previousFireTime ? dateFormat(new Date(trigger.previousFireTime), i18n.t('dateTimeLocale') === 'cs' ? 'dd.mm.yyyy HH:MM' : 'mm/dd/yyyy HH:MM') : ''}
					</td>
					<td>
						<Glyphicon glyph={trigger.state === 'ACTIVE' ? 'check' : 'unchecked'}/>
					</td>
					<td>
						{this.createTriggerActions(task.name, trigger)}
					</td>
				</tr>
			);
		}
		return rows;
	},
	
	createTriggerActions: function(taskName, trigger) {
		return (
			<ButtonGroup bsSize='small'>
				{this.getTriggerButton(taskName, trigger.name, trigger.state === 'ACTIVE' ? 'pause' : 'resume')}
				<Button bsStyle='danger'
					title={i18n.t('core-task-trigger-remove')}
					onClick={this.context.flux.actions.task.removeTrigger.bind(this, taskName, trigger.name, this.context.flux.actions.task.loadTasks.bind(this))}>
					<Glyphicon glyph='remove'/>
				</Button>
			</ButtonGroup>
		);
	},
	
	getTriggerButton: function(taskName, triggerName, actionType) {
		return (
			<Button bsStyle={actionType === 'resume' ? 'success' : 'warning'}
				title={i18n.t('core-task-trigger-' + actionType)}
				onClick={this.context.flux.actions.task.changeTriggerState.bind(this, taskName, triggerName, actionType, this.context.flux.actions.task.loadTasks.bind(this))}>
				<Glyphicon glyph={actionType === 'resume' ? 'play' : 'pause'}/>
			</Button>
		);
	},
	
	getModalDialog: function(type) {
		return (
			<Modal show={this.state.modal === type ? true : false} onHide={this.closeModal}>
				<Modal.Header closeButton>
					<Modal.Title>{i18n.t('core-task-' + type) + ' - ' + i18n.t('core-task-' + this.state.taskName)}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Input type={'text'}
						value={this.state.description}
						placeholder={i18n.t('core-task-description')}
						label={i18n.t('core-task-description')}
						help={this.getHelpLabel(i18n.t(this.state.descriptionValid))}
						onChange={this.descriptionHandleChange}/>
					{type === 'oneTimeTrigger' ?
						<Input label={i18n.t('core-task-time')}
							help={this.getHelpLabel(i18n.t(this.state.timeValid))}>
							<DateTime value={this.state.time}
								closeOnSelect={true}
								onChange={this.timeHandleChange}
								dateFormat={i18n.t('patternDate')}
								timeFormat={i18n.t('patternTime')}
								locale={i18n.t('dateTimeLocale')}/>
						</Input> :
						<Input type={'text'}
							value={this.state.cron}
							placeholder={i18n.t('core-task-cron')}
							label={i18n.t('core-task-cron')}
							help={this.getHelpLabel(i18n.t(this.state.cronValidLength) + i18n.t(this.state.cronValidRequired))}
							onChange={this.cronHandleChange}/>}
						</Modal.Body>
						<Modal.Footer>
							<Button bsStyle='primary' onClick={type === 'oneTimeTrigger' ? this.saveOneTimeTrigger : this.saveCronTrigger}>{i18n.t('save')}</Button>
							<Button onClick={this.closeModal}>{i18n.t('exit')}</Button>
						</Modal.Footer>
					</Modal>
				);
			},
			
			getHelpLabel: function(info) {
				return (<Label bsStyle='warning'>{info}</Label>);
			},
			
			openModal: function(type, taskName) {
				if (type === 'oneTimeTrigger') {
					this.timeHandleChange(this.state.time);
				} else {
					this.cronHandleChange(null, this.state.cron);
				}
				this.setState({
					taskName: taskName,
					modal: type
				});
			},
			
			closeModal: function() {
				this.setState({
					taskName: null,
					modal: null,
					description: '',
					descriptionValid: '',
					time: '',
					timeValid: '',
					cron: '',
					cronValidRequired: '',
					cronValidLength: ''
				});
			},
			
			descriptionHandleChange: function(event) {
				var value = event.target.value;
				var valid = ValidationUtils.validateLengthRange(value.split(''), [1, 100]);
				this.setState({
					description: value,
					descriptionValid: (valid === null ? '' : 'core-task-description-' + valid.name)
				});
			},
			
			timeHandleChange: function(value) {
				this.setState({
					time: value,
					timeValid: (value && value instanceof moment ? '' : 'core-task-time-validation-required')
				});
			},
			
			cronHandleChange: function(event, value) {
				if (event) value = event.target.value;
				var validRequired = ValidationUtils.validateRequired(value.split(''));
				var validLength = ValidationUtils.validateLengthRange(value.split(''), [1, 100]);
				this.setState({
					cron: value,
					cronValidRequired: (validRequired === null ? '' : 'core-task-cron-' + validRequired.name),
					cronValidLength: (validLength === null ? '' : 'core-task-cron-' + validLength.name)
				});
			},
			
			saveOneTimeTrigger: function() {
				if (this.state.descriptionValid === '' && this.state.timeValid === '') {
					this.context.flux.actions.task.saveOneTimeTrigger(this.state.taskName, this.state.description, this.state.time.toISOString(),
					function() {
						this.context.flux.actions.task.loadTasks();
						this.closeModal();
					}.bind(this)
				);
			}
		},
		
		saveCronTrigger: function() {
			if (this.state.descriptionValid === '' && this.state.cronValidRequired === '' && this.state.cronValidLength === '') {
				this.context.flux.actions.task.saveCronTrigger(this.state.taskName, this.state.description, this.state.cron,
					function() {
						this.context.flux.actions.task.loadTasks();
						this.closeModal();
					}.bind(this)
				);
			}
		},
		
		render: function() {
			var tasks = this.state.tasks;
			return (
				<div>
					{tasks && tasks.length > 0 ? this.getItems(tasks) : i18n.t('core-no-task')}
					{this.getModalDialog('oneTimeTrigger')}
					{this.getModalDialog('cronTrigger')}
				</div>
			);
		}
	});
	
	module.exports = {title: TaskListTitle, main: TaskList};
