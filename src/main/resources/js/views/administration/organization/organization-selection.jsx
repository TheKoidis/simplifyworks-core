var OrganizationSelection = {

	renderSelection: function(customRest) {
		return (
			<SW.List.EntityList name='core-organization' rest={customRest ? customRest : '/api/core/organizations'}>
				<SW.List.Table>
					<SW.List.String property='code' width='20%'/>
					<SW.List.String property='name' width='50%'/>
					<SW.List.String property='abbreviation' width='30%'/>
				</SW.List.Table>
				<SW.List.Filter name='filter-core-organization' labelKeyPrefix='core-organization'>
					<SW.Form.Section>
						<SW.Form.String size={5} property='name' />
						<SW.Form.String size={4} property='abbreviation' />
						<SW.Form.String size={3} property='code' />
					</SW.Form.Section>
				</SW.List.Filter>
				<SW.List.QuickSearch />
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List.EntityList>
		);
	},

	renderText: function(organization) {
		if (!organization) return null;
		return (organization.name);
	},
}

module.exports = OrganizationSelection;
