var UserRoleOrganziationDetail = require('../user-role-organization/detail.jsx');
var PersonObligationDetail = require('../person-obligation/detail.jsx');

var OrganizationDetailTitle = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var isNew = this.props.params.organizationId === 'new';
		
		return isNew ? {} : this.context.flux.store('form').getForm('/api/core/organizations/' + this.props.params.organizationId, '');
	},
	
	render: function() {
		return RenderUtils.renderDetailTitle('core-organization', this.props.params.organizationId, this.state.data ? this.state.data.name : null);
	}
});

var OrganizationDetail = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired,
		router: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var isNew = this.props.params.userId === 'new';
		
		return isNew ? {} : this.context.flux.store('form').getForm('/api/core/organizations/' + this.props.params.organizationId, '');
	},
	
	newCallback: function(type, id) {
		if (type == FormConstants.DETAIL_CREATE && id) {
			this.context.router.push('/system/organization/' + id);
		}
	},
	
	renderUserRoleOrganizationTitle: function() {
		return i18n.t('core-user-role-organization');
	},
	
	renderPersonObligationsTitle: function() {
		return i18n.t('core-obligation');
	},
	
	renderDetailColumn: function(resource) {
		return i18n.t('core-user-role-organization-detail');
	},
	
	renderText: function(resource) {
		var value = resource.person;
		return (
			PersonSelection.renderText(value)
		);
	},
	
	render: function() {
		var isNew = this.props.params.organizationId === 'new';
		
		return (
			<SW.Detail rest='/api/core/organizations' restId={this.props.params.organizationId} newCallback={this.newCallback} exitPath='/system/organizations' tabName={this.props.params.tabName}>
				<SW.Form rest='' labelKey='core-organization' new={isNew} labelKeyPrefix='core-organization' tabName='default'>
					<SW.Form.Section icon='pencil' labelKey='basic-info'>
						<SW.Form.String size={6} property='code' validations='lengthRange(1,50)'/>
						<SW.Form.String size={6} property='abbreviation' validations='lengthRange(0,50)'/>
						<SW.Form.String size={6} property='name' validations='required;lengthRange(1,100)'/>
						<SW.Form.Entity size={6} property='organization' render={OrganizationSelection.renderText}>
							{OrganizationSelection.renderSelection()}
						</SW.Form.Entity>
						<SW.Form.Datetime size={2} property='validFrom'/>
						<SW.Form.Datetime size={2} property='validTo'/>
						<SW.Form.Boolean size={2} property='active'/>
					</SW.Form.Section>
				</SW.Form>
				
				{!isNew
					?
					<SW.List name='core-organization' rest='organizations' labelKey='core-organization-organizations'>
						<SW.List.Table //TODO filter
							>
							<SW.List.Selection property='id' width='2.5%'/>
							<SW.List.String property='code' width='15%'/>
							<SW.List.String property='abbreviation' width='15%'/>
							<SW.List.String property='name' path='/system/organization' pathProperty='id'/>
						</SW.List.Table>
						<SW.List.Info />
						<SW.List.QuickSearch properties={['code', 'name', 'abbreviation']}/>
						<SW.List.Pagination />
					</SW.List>
					:null
				}
				
				{!isNew
					?
					<SW.List name='core-user-role' rest='user-roles' icon='user' labelKey='core-users'>
						<SW.List.Table //TODO filter
							>
							<SW.List.Selection property='id' width='2.5%'/>
							<SW.List.Custom property='id' labelKey='core-user-role-organization' width='15%' pathProperty='id' render={this.renderDetailColumn}/>
							<SW.List.String property='user.username' labelKey='core-user-username' width='15%' path='/system/user' pathProperty='user.id'/>
							<SW.List.String property='role.name' labelKey='core-role-name' width='15%' path='/system/role' pathProperty='role.id'/>
							<SW.List.Datetime property='validFrom' width='15%'/>
							<SW.List.Datetime property='validTill'/>
						</SW.List.Table>
						
						<SW.List.ActivitiesGroup>
							<SW.List.LinkModal icon='plus' labelKey='core-role-organization-create-new'/>
						</SW.List.ActivitiesGroup>
						
						<SW.List.DetailModal renderTitle={this.renderUserRoleOrganizationTitle}>
							<SW.List.FormModal rest={'/api/core/organizations/' + this.props.params.organizationId + '/user-roles'}
								labelKeyPrefix='core-user-role-organization' initialValue={{organization: this.state.data}}>
								{UserRoleOrganziationDetail.renderModalForm('organization')}
							</SW.List.FormModal>
						</SW.List.DetailModal>
						
						<SW.List.Info />
						<SW.List.QuickSearch properties={['user.username', 'role.name']}/>
						<SW.List.Pagination />
					</SW.List>
					:null
				}
				
				{!isNew
					?
					<SW.List name='core-person-organization' rest='persons' icon='euro' labelKey='core-obligations'>
						<SW.List.Table //TODO filter and create new (modal dialog)
							>
							<SW.List.Selection property='id' width='2.5%'/>
							<SW.List.Custom property='id' labelKey='core-obligations' width='15%' pathProperty='id' render={this.renderDetailColumn}/>
							<SW.List.Custom property='person' labelKey='core-person' width='15%' path='/system/person' pathProperty='person.id' render={this.renderText}/>
							<SW.List.Datetime property='validFrom' width='15%'/>
							<SW.List.Datetime property='validTill' width='15%'/>
							<SW.List.Number property='obligation' width='15%' labelKey='core-obligations-obligation'/>
							<SW.List.String property='category' labelKey='core-obligations-category'/>
						</SW.List.Table>
						
						<SW.List.ActivitiesGroup>
							<SW.List.LinkModal icon='plus' labelKey='core-obligations-create-new' />
						</SW.List.ActivitiesGroup>
						
						<SW.List.DetailModal renderTitle={this.renderPersonObligationsTitle}>
							<SW.List.FormModal rest={'/api/core/organizations/' + this.props.params.organizationId + '/persons'}
								labelKeyPrefix='core-obligations' initialValue={{organization: this.state.data}}>
								{PersonObligationDetail.renderModalForm('organization')}
							</SW.List.FormModal>
						</SW.List.DetailModal>
						
						<SW.List.Info />
						<SW.List.QuickSearch properties={['person.firstname', 'person.surname', 'obligation']}/>
						<SW.List.Pagination />
					</SW.List>
					
					:null
				}
			</SW.Detail>
		);
	}
});

module.exports = {title: OrganizationDetailTitle, main: OrganizationDetail};
