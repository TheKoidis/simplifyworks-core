var OrganizationListTitle = React.createClass({
	
	render: function() {
		return (
			<span>
				{i18n.t('core-organizations')}
			</span>
		);
	}
});

var OrganizationList = React.createClass({
	
	render: function() {
		return (
			<SW.List name='core-organization' restPrefix='/api/core/organizations' rest=''>
				<SW.List.Table>
					<SW.List.Selection property='id' width='2.5%'/>
					<SW.List.String property='name' width='45%' path='/system/organization' pathProperty='id'/>
					<SW.List.String property='code' width='15%'/>
					<SW.List.String property='abbreviation'/>
				</SW.List.Table>
				
				<SW.List.ActivitiesGroup>
					<SW.List.Link icon='plus' labelKey='core-organization-create-new' path={'/system/organization/new'}/>
				</SW.List.ActivitiesGroup>
				
				<SW.List.QuickSearch properties={['code', 'name', 'abbreviation']}/>
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List>
		);
	}
});

module.exports = {title: OrganizationListTitle, main: OrganizationList};
