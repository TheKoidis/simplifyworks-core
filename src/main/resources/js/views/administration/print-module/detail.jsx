var PrintModuleDetailTitle = React.createClass({
	
	render: function() {
		return RenderUtils.renderDetailTitle('core-print-module', this.props.params.printModuleId);
	}
});

var PrintModuleDetail = React.createClass({
	
	contextTypes: {
		router: React.PropTypes.object.isRequired,
	},
	
	newCallback: function(type, id) {
		if (type == FormConstants.FORM_CREATE && id) {
			this.context.router.push('/system/print-module/' + id);
		}
	},
	
	render: function() {
		var isNew = this.props.params.printModuleId === 'new';
		
		return (
			<SW.Detail rest='/api/core/print-modules' restId={this.props.params.printModuleId}  exitPath='/system/print-modules' tabName={this.props.params.tabName}>
				<SW.Form rest='' labelKey='core-print-module' new={isNew} newCallback={this.newCallback} labelKeyPrefix='core-print-module' tabName='default'>
					<SW.Form.Section icon='pencil' labelKey='basic-info'>
						<SW.Form.String size={6} property='name' validations='required;lengthRange(1,255)'/>
						<SW.Form.String size={6} property='template' validations='required;lengthRange(1,255)'/>
						<SW.Form.Enum size={2} property='type' options={{enum: 'Type', values: ['PDF', 'HTML', 'XML']}} validations='required'/>
						<SW.Form.String size={5} property='objectType' validations='required;lengthRange(1,128)'/>
					</SW.Form.Section>
				</SW.Form>
			</SW.Detail>
		);
	}
});

module.exports = {title: PrintModuleDetailTitle, main: PrintModuleDetail};
