var PrintModuleListTitle = React.createClass({
	
	render: function() {
		return (
			<span>
				{i18n.t('core-print-modules')}
			</span>
		);
	}
});

var PrintModuleList = React.createClass({
	
	render: function() {
		return (
			<SW.List name='core-print-module' restPrefix='/api/core/print-modules' rest=''>
				<SW.List.Table>
					<SW.List.Selection property='id' width='2.5%'/>
					<SW.List.String property='name' width='15%' path='/system/print-module' pathProperty='id'/>
					<SW.List.String property='template' width='15%'/>
					<SW.List.String property='type' width='15%'/>
					<SW.List.String property='objectType' width='15%'/>
				</SW.List.Table>
				
				<SW.List.ActivitiesGroup>
					<SW.List.Link icon='plus' labelKey='core-print-module-create-new' path={'/system/print-module/new'}/>
				</SW.List.ActivitiesGroup>
				
				<SW.List.QuickSearch properties={['name', 'template']}/>
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List>
		);
	}
});

module.exports = {title: PrintModuleListTitle, main: PrintModuleList};
