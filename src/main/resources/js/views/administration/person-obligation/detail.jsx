var PersonObligationDetail = {

	renderModalForm: function(hide) {
		return (
			<SW.Form.Section>
				{hide=='person' ? null :
					<SW.Form.Entity size={6} property='person' render={PersonSelection.renderText}>
						{PersonSelection.renderSelection()}
					</SW.Form.Entity>
				}
				{hide=='organization' ? null :
					<SW.Form.Entity size={6} property='organization' render={OrganizationSelection.renderText}>
						{OrganizationSelection.renderSelection()}
					</SW.Form.Entity>
				}
				<SW.Form.Number size={6} property='obligation'/>
				<SW.Form.Datetime size={6} property='validFrom'/>
				<SW.Form.Datetime size={6} property='validTo'/>
				<SW.Form.String size={6} property='category'/>
			</SW.Form.Section>
		);
	},
}

module.exports = PersonObligationDetail;
