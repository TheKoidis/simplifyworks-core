var UserRoleOrganziationDetail = require('../user-role-organization/detail.jsx');

var UserDetailTitle = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var isNew = this.props.params.userId === 'new';
		
		return isNew ? {} : this.context.flux.store('form').getForm('/api/core/users/' + this.props.params.userId);
	},
	
	render: function() {
		return RenderUtils.renderDetailTitle('core-user', this.props.params.userId, this.state.data ? this.state.data.name : null);
	}
});

var UserDetail = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired,
		router: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var isNew = this.props.params.userId === 'new';
		
		return isNew ? {} : this.context.flux.store('form').getForm('/api/core/users/' + this.props.params.userId, '');
	},
	
	newCallback: function(type, id) {
		if (type == FormConstants.DETAIL_CREATE && id) {
			this.context.router.push('/system/user/' + id);
		}
	},
	
	renderUserRoleOrganizationTitle: function() {
		return i18n.t('core-user-role-organization');
	},
	
	renderDetailColumn: function(resource) {
		return i18n.t('core-user-role-organization-detail');
	},
	
	render: function() {
		var isNew = this.props.params.userId === 'new';
		
		return (
			<SW.Detail rest='/api/core/users' restId={this.props.params.userId} newCallback={this.newCallback} exitPath='/system/users' tabName={this.props.params.tabName} >
				<SW.Form rest='' labelKey='core-user' new={isNew} labelKeyPrefix='core-user' tabName='default'>
					<SW.Form.Section icon='pencil' labelKey='basic-info'>
						<SW.Form.String size={6} property='username' disabled={this.props.params.userId !== 'new'} validations="required;lengthRange(3,50)"/>
						<SW.Form.String size={6} property='newPassword' type='password' hidden={!isNew} validations="required;lengthRange(3,50)"/>
						<SW.Form.Entity size={6} property='person' render={PersonSelection.renderText}>
							{PersonSelection.renderSelection()}
						</SW.Form.Entity>
					</SW.Form.Section>
				</SW.Form>
				
				{!isNew
					?
					<SW.List name='core-user-role-organization' rest='role-organizations' labelKey='core-roles'>
						<SW.List.Table //TODO filter
							>
							<SW.List.Selection property='id' width='2.5%'/>
							<SW.List.Custom property='id' labelKey='core-user-role-organization' width='15%' pathProperty='id' render={this.renderDetailColumn}/>
							<SW.List.String property='role.name' labelKey='core-role-name' width='15%' path='/system/role' pathProperty='role.id'/>
							<SW.List.String property='organization.name' labelKey='core-organization-name' width='15%' path='/system/organization' pathProperty='organization.id'/>
							<SW.List.Datetime property='validFrom' width='15%'/>
							<SW.List.Datetime property='validTill'/>
						</SW.List.Table>
						
						<SW.List.ActivitiesGroup>
							<SW.List.LinkModal icon='plus' labelKey='core-role-organization-create-new'/>
						</SW.List.ActivitiesGroup>
						
						<SW.List.DetailModal renderTitle={this.renderUserRoleOrganizationTitle}>
							<SW.List.FormModal rest={'/api/core/users/' + this.props.params.userId + '/role-organizations'}
								labelKeyPrefix='core-user-role-organization' initialValue={{user: this.state.data}}>
								{UserRoleOrganziationDetail.renderModalForm('user')}
							</SW.List.FormModal>
						</SW.List.DetailModal>
						
						<SW.List.Info />
						<SW.List.QuickSearch properties={['role.name', 'organization.name']}/>
						<SW.List.Pagination />
					</SW.List>
					:null}
				</SW.Detail>
			);
		}
	});
	
	module.exports = {title: UserDetailTitle, main: UserDetail};
