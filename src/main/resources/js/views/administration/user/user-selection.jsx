var UserSelection = {

	renderSelection: function(customRest) {
		return (
			<SW.List.EntityList name='coreUser' rest={customRest ? customRest : '/api/core/users'}>
				<SW.List.Table>
					<SW.List.String property='username' width='100%'/>
				</SW.List.Table>
				<SW.List.QuickSearch />
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List.EntityList>
		);
	},

	renderText: function(user) {
		if (!user) return null;
		return (user.username);
	},
}

module.exports = UserSelection;
