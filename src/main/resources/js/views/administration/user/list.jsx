var UserListTitle = React.createClass({
	
	render: function() {
		return (
			<span>
				{i18n.t('core-users')}
			</span>
		);
	}
});

var UserList = React.createClass({
	
	renderText: function(resource) {
		var value = resource.person;
		return (
			PersonSelection.renderText(value)
		);
	},
	
	render: function() {
		return (
			<SW.List name='core-user' restPrefix='/api/core/users' rest=''>
				<SW.List.Table>
					<SW.List.Selection property='id' width='2.5%'/>
					<SW.List.String property='username' width='15%' path='/system/user' pathProperty='id'/>
					<SW.List.Custom property='full-name' labelKey='core-person-full-name' width='30%' render={this.renderText} />
					<SW.List.String property='person.email' labelKey='core-person-email'/>
				</SW.List.Table>
				
				<SW.List.ActivitiesGroup>
					<SW.List.Link icon='plus' labelKey='core-user-create-new' path={'/system/user/new'}/>
				</SW.List.ActivitiesGroup>
				
				<SW.List.QuickSearch properties={['username', 'person.surname', 'person.firstname', 'person.email']}/>
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List>
		);
	}
});

module.exports = {title: UserListTitle, main: UserList};
