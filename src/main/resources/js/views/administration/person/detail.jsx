var PersonObligationDetail = require('../person-obligation/detail.jsx');

var PersonDetailTitle = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var isNew = this.props.params.personId === 'new';
		
		return isNew ? {} : this.context.flux.store('form').getForm('/api/core/persons/' + this.props.params.personId);
	},
	
	render: function() {
		return RenderUtils.renderDetailTitle('core-person', this.props.params.personId, this.state.data ? this.state.data.name : null);
	}
});

var PersonDetail = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired,
		router: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var isNew = this.props.params.personId === 'new';
		
		return isNew ? {} : this.context.flux.store('form').getForm('/api/core/persons/' + this.props.params.personId, '');
	},
	
	newCallback: function(type, id) {
		if (type == FormConstants.DETAIL_CREATE && id) {
			this.context.router.push('/system/person/' + id);
		}
	},
	
	renderPersonObligationsTitle: function() {
		return i18n.t('core-obligation');
	},
	
	renderDetailColumn: function(resource) {
		return i18n.t('core-obligation-detail');
	},
	
	render: function() {
		var isNew = this.props.params.personId === 'new';
		return (
			<SW.Detail rest='/api/core/persons' restId={this.props.params.personId} newCallback={this.newCallback} exitPath='/system/persons' tabName={this.props.params.tabName}>
				<SW.Form rest='' labelKey='core-person' new={isNew} labelKeyPrefix='core-person' initialValue={{sex: 'M'}} tabName='default' >
					<SW.Form.Section icon='pencil' labelKey='basic-info'>
						<SW.Form.String size={2} property='titleBefore' />
						<SW.Form.String size={4} property='firstname' validations="lengthRange(0,50)"/>
						<SW.Form.String size={4} property='surname' validations="required;lengthRange(0,50)"/>
						<SW.Form.String size={2} property='titleAfter' />
					</SW.Form.Section>
					
					<SW.Form.Section icon='pencil' labelKey='contact'>
						<SW.Form.String size={4} property='email' validations="email;lengthRange(0,50)"/>
						<SW.Form.String size={4} property='phone' validations="lengthRange(0,50)"/>
						<SW.Form.String size={4} property='personalNumber' validations="lengthRange(0,50)"/>
					</SW.Form.Section>
					
					<SW.Form.Section icon='pencil' labelKey='other'>
						<SW.Form.Datetime size={3} property='dateOfBirth'/>
						<SW.Form.Enum size={3} property='sex' options={{enum: 'Sex', values:['M', 'F']}} validations="required"/>
						<SW.Form.Datetime size={2} property='validFrom'/>
						<SW.Form.Datetime size={2} property='validTo'/>
						<SW.Form.Boolean size={2} property='active'/>
						<SW.Form.String size={12} property='note' type='area'/>
					</SW.Form.Section>
				</SW.Form>
				
				{!isNew
					?
					<SW.List icon='user' labelKey='core-users' name='core-user' rest='users'>
						<SW.List.Table //TODO filter
							>
							<SW.List.Selection property='id' width='2.5%'/>
							<SW.List.String property='username' width='15%' path='/system/user' pathProperty='id'/>
							<SW.List.Datetime property='lastLogin'/>
						</SW.List.Table>
						
						<SW.List.Info />
						<SW.List.QuickSearch properties={['username', 'lastLogin']}/>
						<SW.List.Pagination />
					</SW.List>
					:null}
					
					{!isNew
						?
						<SW.List icon='euro' labelKey='core-obligations' name='core-person-organization' rest='organizations'>
							<SW.List.Table //TODO filter and create new (modal dialog)
								>
								<SW.List.Selection property='id' width='2.5%'/>
								<SW.List.Custom property='id' labelKey='core-obligations' width='15%' pathProperty='id' render={this.renderDetailColumn}/>
								<SW.List.String property='organization.name' labelKey='core-organization-name' width='15%' path='/system/organization' pathProperty='organization.id'/>
								<SW.List.Datetime property='validFrom' width='15%'/>
								<SW.List.Datetime property='validTo' width='15%'/>
								<SW.List.Number property='obligation' width='15%' labelKey='core-obligations-obligation'/>
								<SW.List.String property='category' labelKey='core-obligations-category'/>
							</SW.List.Table>
			
							<SW.List.ActivitiesGroup>
								<SW.List.LinkModal icon='plus' labelKey='core-obligations-create-new' />
							</SW.List.ActivitiesGroup>
			
							<SW.List.DetailModal renderTitle={this.renderPersonObligationsTitle}>
								<SW.List.FormModal rest={'/api/core/persons/' + this.props.params.personId + '/organizations'}
									labelKeyPrefix='core-obligations' initialValue={{person: this.state.data}}>
									{PersonObligationDetail.renderModalForm('person')}
								</SW.List.FormModal>
							</SW.List.DetailModal>
							
							<SW.List.Info />
							<SW.List.QuickSearch properties={['organization.name', 'obligation']}/>
							<SW.List.Pagination />
						</SW.List>
						:null}
					</SW.Detail>
				);
			}
		});
		
		module.exports = {title: PersonDetailTitle, main: PersonDetail};
