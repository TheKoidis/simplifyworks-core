var PersonListTitle = React.createClass({
	
	render: function() {
		return (
			<span>
				{i18n.t('core-persons')}
			</span>
		);
	}
});

var PersonList = React.createClass({
	
	renderColumn: function(resource, property) {
		var value = resource[property];
		return (
			<Glyphicon glyph={value === 'M' ? 'user text-primary' : 'user text-danger'}/>
		);
	},
	
	renderText: function(resource) {
		var value = resource;
		return (
			PersonSelection.renderText(value)
		);
	},
	
	render: function() {
		return (
			<SW.List name='core-person' restPrefix='/api/core/persons' rest=''>
				<SW.List.Table>
					<SW.List.Selection property='id' width='2.5%'/>
					<SW.List.Custom property='full-name' width='15%' render={this.renderText} path='/system/person' pathProperty='id' />
					<SW.List.String property='email' width='15%'/>
					<SW.List.String property='personalNumber' width='15%'/>
					<SW.List.Boolean property='active' width='5%' orderable={false}/>
					<SW.List.Custom property='sex' width='5%' orderable={false} render={this.renderColumn}/>
					<SW.List.Datetime property='dateOfBirth' />
				</SW.List.Table>
				
				<SW.List.ActivitiesGroup>
					<SW.List.Link icon='plus' labelKey='core-person-create-new' path={'/system/person/new'}/>
				</SW.List.ActivitiesGroup>
				
				<SW.List.Filter name='filter-core-person' labelKeyPrefix='core-person'>
					<SW.Form.Section>
						<SW.Form.String size={6} property='firstname' />
						<SW.Form.String size={6} property='surname' />
						<SW.Form.String size={6} property='email' />
						<SW.Form.String size={6} property='personalNumber' />
					</SW.Form.Section>
				</SW.List.Filter>
				
				<SW.List.QuickSearch properties={['surname', 'firstname', 'email', 'personalNumber']}/>
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List>
		);
	}
});

module.exports = {title: PersonListTitle, main: PersonList};
