var PersonSelection = {
	
	renderSelection: function(customRest) {
		return (
			<SW.List.EntityList name='core-person' rest={customRest ? customRest : '/api/core/persons'}>
				<SW.List.Table>
					<SW.List.Custom property='full-name' width='30%' render={this.renderCol} />
					<SW.List.String property='email' width='20%'/>
					<SW.List.String property='personalNumber' width='20%'/>
					<SW.List.Datetime property='dateOfBirth' />
				</SW.List.Table>
				<SW.List.Filter name='filter-core-person' labelKeyPrefix='core-person'>
					<SW.Form.Section>
						<SW.Form.String size={6} property='firstname' />
						<SW.Form.String size={6} property='surname' />
						<SW.Form.String size={6} property='email' />
						<SW.Form.String size={6} property='personalNumber' />
					</SW.Form.Section>
				</SW.List.Filter>
				<SW.List.QuickSearch />
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List.EntityList>
		);
	},
	
	renderText: function(person) {
		if (!person) return null;
		return (person.titleBefore ? person.titleBefore + ' ' : '')
		+ (person.firstname ? person.firstname : '')
		+ (person.surname ? ' ' + person.surname : '')
		+ (person.titleAfter ? ' ' + person.titleAfter : '');
	},
	
	renderCol: function(resource) {
		var value = resource;
		return (
			PersonSelection.renderText(value)
		);
	},
}

module.exports = PersonSelection;
