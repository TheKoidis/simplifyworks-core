var UserRoleOrganziationDetail = {

	renderModalForm: function(hide) {
		return (
			<SW.Form.Section>
				{hide=='user' ? null :
					<SW.Form.Entity size={6} property='user' render={UserSelection.renderText}>
						{UserSelection.renderSelection()}
					</SW.Form.Entity>
				}
				{hide=='role' ? null :
					<SW.Form.Entity size={6} property='role' render={RoleSelection.renderText}>
						{RoleSelection.renderSelection()}
					</SW.Form.Entity>
				}
				{hide=='organization' ? null :
					<SW.Form.Entity size={6} property='organization' render={OrganizationSelection.renderText}>
						{OrganizationSelection.renderSelection()}
					</SW.Form.Entity>
				}
				<SW.Form.Datetime size={6} property='validFrom'/>
				<SW.Form.Datetime size={6} property='validTill'/>
			</SW.Form.Section>
		);
	},
}

module.exports = UserRoleOrganziationDetail;
