var UserRoleOrganziationDetail = require('../user-role-organization/detail.jsx');

var RoleDetailTitle = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var isNew = this.props.params.roleId === 'new';
		
		return isNew ? {} : this.context.flux.store('form').getForm('/api/core/roles/' + this.props.params.roleId, '');
	},
	
	render: function() {
		return RenderUtils.renderDetailTitle('core-role', this.props.params.roleId, this.state.data ? this.state.data.name : null);
	}
});

var RoleDetail = React.createClass({
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		router: React.PropTypes.object.isRequired,
		flux: React.PropTypes.object.isRequired,
	},
	
	getStateFromFlux: function() {
		var currentRole = this.context.flux.store('form').getForm('/api/core/roles/' + this.props.params.roleId, '').data;
		return {currentRole: currentRole};
	},
	
	newCallback: function(type, id) {
		if (type == FormConstants.DETAIL_CREATE && id) {
			this.context.router.push('/system/role/' + id);
		}
	},
	
	renderSuperiorRoleTitle: function(record) {
		var params = {
			subRoleName: this.state.currentRole ? this.state.currentRole.name : '?'
		};
		
		var label = !record ? i18n.t('core-role-composition-subRole-detail-new', params) : i18n.t('core-role-composition-subRole-detail', params);
		
		return (
			<span>
				{label}
			</span>
		);
	},
	
	renderSubRoleTitle: function(record) {
		var params = {
			superiorRoleName: this.state.currentRole ? this.state.currentRole.name : '?',
		};
		
		var label = !record ? i18n.t('core-role-composition-superiorRole-detail-new', params) : i18n.t('core-role-composition-superiorRole-detail', params);
		
		return (
			<span>
				{label}
			</span>
		);
	},
	
	renderUserRoleOrganizationTitle: function() {
		return i18n.t('core-user-role-organization');
	},
	
	renderDetailColumn: function(resource) {
		return i18n.t('core-user-role-organization-detail');
	},
	
	render: function() {
		var isNew = this.props.params.roleId === 'new';
		
		return (
			<SW.Detail rest='/api/core/roles' restId={this.props.params.roleId} newCallback={this.newCallback} exitPath='/system/roles' tabName={this.props.params.tabName}>
				<SW.Form rest='' labelKey='core-role' new={isNew} labelKeyPrefix='core-role' tabName='default'>
					<SW.Form.Section icon='pencil' labelKey='basic-info'>
						<SW.Form.String size={6} property='name' validations='required;lengthRange(3,25)'/>
						<SW.Form.String size={6} property='description' type='area' validations='required;lengthRange(0,255)'/>
					</SW.Form.Section>
				</SW.Form>
				
				{!isNew
					?
					<SW.List name='core-user-role' rest='user-organizations' icon='user' labelKey='core-users'>
						<SW.List.Table //TODO filter a create new (modal dialog)
							>
							<SW.List.Selection property='id' width='2.5%'/>
							<SW.List.Custom property='id' labelKey='core-user-role-organization' width='15%' pathProperty='id' render={this.renderDetailColumn}/>
							<SW.List.String property='user.username' labelKey='core-user-username' width='15%' path='/system/user' pathProperty='user.id'/>
							<SW.List.String property='organization.name' labelKey='core-organization-name' width='15%' path='/system/organization' pathProperty='organization.id'/>
							<SW.List.Datetime property='validFrom' width='15%'/>
							<SW.List.Datetime property='validTill'/>
						</SW.List.Table>
						
						<SW.List.ActivitiesGroup>
							<SW.List.LinkModal icon='plus' labelKey='core-role-organization-create-new'/>
						</SW.List.ActivitiesGroup>
						
						<SW.List.DetailModal renderTitle={this.renderUserRoleOrganizationTitle}>
							<SW.List.FormModal rest={'/api/core/roles/' + this.props.params.roleId + '/user-organizations'}
								labelKeyPrefix='core-user-role-organization' initialValue={{role: this.state.currentRole}}>
								{UserRoleOrganziationDetail.renderModalForm('role')}
							</SW.List.FormModal>
						</SW.List.DetailModal>
						
						<SW.List.QuickSearch properties={['user.username', 'organization.name']}/>
						<SW.List.Info />
						<SW.List.Pagination />
					</SW.List>
					: null
				}
				
				{!isNew
					?
					<SW.List name='core-role-composition-superior' rest='super-roles' labelKey='core-role-compositions-superior'>
						<SW.List.Table //TODO filter
							>
							<SW.List.Selection property='superiorRole.id' width='2.5%'/>
							<SW.List.String property='superiorRole.name' labelKey='core-role-composition-superiorRole' pathProperty='id'/>
						</SW.List.Table>
						
						<SW.List.ActivitiesGroup>
							<SW.List.LinkModal icon='plus' labelKey='core-role-composition-superiorRole-create-new'/>
						</SW.List.ActivitiesGroup>
						
						<SW.List.Info />
						<SW.List.Pagination />
						
						<SW.List.DetailModal renderTitle={this.renderSuperiorRoleTitle}>
							<SW.List.FormModal rest={'/api/core/roles/' + this.props.params.roleId + '/super-roles'} labelKeyPrefix='core-role-composition'>
								<SW.Form.Section>
									<SW.Form.Entity size={12} property='superiorRole' render={RoleSelection.renderText} validations='required'>
										{RoleSelection.renderSelection()}
									</SW.Form.Entity>
								</SW.Form.Section>
							</SW.List.FormModal>
						</SW.List.DetailModal>
					</SW.List>
					: null
				}
				
				{!isNew
					?
					<SW.List name='core-role-composition-sub' rest='sub-roles' labelKey='core-role-compositions-sub'>
						<SW.List.Table //TODO filter
							>
							<SW.List.Selection property='subRole.id' width='2.5%'/>
							<SW.List.String property='subRole.name' labelKey='core-role-composition-subRole' pathProperty='id'/>
						</SW.List.Table>
						
						<SW.List.ActivitiesGroup>
							<SW.List.LinkModal icon='plus' labelKey='core-role-composition-subRole-create-new'/>
						</SW.List.ActivitiesGroup>
						
						<SW.List.Info />
						<SW.List.Pagination />
						
						<SW.List.DetailModal renderTitle={this.renderSubRoleTitle}>
							<SW.List.FormModal rest={'/api/core/roles/' + this.props.params.roleId + '/sub-roles'} labelKeyPrefix='core-role-composition'>
								<SW.Form.Section>
									<SW.Form.Entity size={12} property='subRole' render={RoleSelection.renderText} validations='required'>
										{RoleSelection.renderSelection()}
									</SW.Form.Entity>
								</SW.Form.Section>
							</SW.List.FormModal>
						</SW.List.DetailModal>
					</SW.List>
					: null
				}
			</SW.Detail>
		);
	}
});

module.exports = {title: RoleDetailTitle, main: RoleDetail};
