var RoleSelection = {

	renderSelection: function(customRest) {
		return (
			<SW.List.EntityList name='coreRole' rest={customRest ? customRest : '/api/core/roles'}>
				<SW.List.Table>
					<SW.List.String property='name' width='100%'/>
				</SW.List.Table>
				<SW.List.QuickSearch />
				<SW.List.Info />
				<SW.List.Pagination />
			</SW.List.EntityList>
		);
	},

	renderText: function(role) {
		if (!role) return null;
		return (role.name);
	},
}

module.exports = RoleSelection;
