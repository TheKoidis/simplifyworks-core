var RoleListTitle = React.createClass({

    render: function() {
        return (
            <span>
                {i18n.t('core-roles')}
            </span>
        );
    }
});

var RoleList = React.createClass({

    render: function() {
        return (
	        <SW.List name='core-role' restPrefix='/api/core/roles' rest=''>
	            <SW.List.Table>
                	<SW.List.Selection property='id' width='2.5%'/>
	          		<SW.List.String property='name' width='15%' path='/system/role' pathProperty='id'/>
	                <SW.List.String property='description'/>
	            </SW.List.Table>

				<SW.List.ActivitiesGroup>
                	<SW.List.Link icon='plus' labelKey='core-role-create-new' path={'/system/role/new'}/>
                </SW.List.ActivitiesGroup>

				<SW.List.QuickSearch properties={['name', 'description']}/>
	            <SW.List.Info />
	            <SW.List.Pagination />
	        </SW.List>
        );
    }
});

module.exports = {title: RoleListTitle, main: RoleList};
