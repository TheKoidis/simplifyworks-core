var DashboardTitle = React.createClass({
	
	render: function() {
		return (
			<span>{i18n.t('dashboard')}</span>
		);
	}
});

var Dashboard = React.createClass({
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	render: function() {
		return (
			<Jumbotron>
				<h1>{i18n.t('dashboard-welcome')}</h1>
				<p>{i18n.t('dashboard-info')}</p>
			</Jumbotron>
		);
	}
});

module.exports = {title: DashboardTitle, main: Dashboard};
