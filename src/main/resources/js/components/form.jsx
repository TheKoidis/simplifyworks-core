/**
*	Form represents the smallest lockable and editable part of detail. Form is divided into sections.
*/
SW.Form = React.createClass({
	
	displayName: 'SW.Form',
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		/**
		*	Flux context
		*/
		flux: React.PropTypes.object.isRequired,
		
		/**
		*	Router context
		*/
		router: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		/**
		*	Prefix for label keys (for localization purposes), automatically included in case of tab usage
		*/
		labelKeyPrefix: React.PropTypes.string,
		
		/**
		*	REST path for data loading and saving (also used as key for Flux store and actions)
		*/
		rest: React.PropTypes.string.isRequired,
		
		/**
		* initial value
		*/
		initialValue: React.PropTypes.object,
		
		new: React.PropTypes.bool,
		
		/**
		* The form is read only
		*/
		readOnly: React.PropTypes.bool,
		
		newCallback: React.PropTypes.func,
		
		/**
		* listener for value changes
		*/
		valueChanged: React.PropTypes.func,
		
		/**
		* Glyph for tab icon
		*/
		icon: React.PropTypes.string,
		
		/**
		* Tab label key (for localization purposes, used in case Label is not set)
		*/
		labelKey: React.PropTypes.string,
		
		/**
		* Tab label
		*/
		label: React.PropTypes.string,
		
		/**
		*	Children structure (at least one section is required)
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.Section': 'multiple-required'
		})
	},
	
	componentDidMount: function() {
		if (this.state.status !== 'managed') {
			if (this.props.new) {
				this.init();
			} else if (!this.props.locked || this.state.status === 'new') {
				this.load();
			}
		}
	},
	
	componentDidUpdate: function(prevProps, prevState) {
		if (this.props.rest !== prevProps.rest || this.props.restPrefix !== prevProps.restPrefix) {
			
			if (this.state.status !== 'managed') {
				if (this.props.new) {
					this.init();
				} else if (!this.props.locked || this.state.status === 'new') {
					this.load();
				}
			}
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if(this.props.rest !== nextProps.rest || this.props.restPrefix !== nextProps.restPrefix) {
			this.setState(this.context.flux.store('form').getForm(nextProps.restPrefix, nextProps.rest));
		}
	},
	
	getStateFromFlux: function() {
		return this.context.flux.store('form').getForm(this.props.restPrefix, this.props.rest);
	},
	
	valueChanged: function(property, value, validations) {
		this.context.flux.actions.form.changeValue(this.props.restPrefix, this.props.rest, property, value, this.props.valueChanged, validations);
	},
	
	init: function() {
		this.context.flux.actions.form.formInit(this.props.restPrefix, this.props.rest, LocalizationUtils.getTabText(this.props), ValidationUtils.getValidationFromProps(this.props, this.props.labelKeyPrefix), this.props.initialValue);
	},
	
	load: function() {
		this.context.flux.actions.form.load(this.props.restPrefix, this.props.rest, LocalizationUtils.getTabText(this.props), ValidationUtils.getValidationFromProps(this.props, this.props.labelKeyPrefix));
	},
	
	render: function() {
		return (
			<div>
				{this.state.status === 'new' || this.state.status === 'managed'
					?	ComponentUtils.cloneChildren(this.props.children, 'SW.Form.Section', {
						labelKeyPrefix: this.props.labelKeyPrefix,
						data: this.state.data,
						disabled: !this.props.locked && this.state.status !== 'new',
						valueChanged: this.valueChanged,
						validation: this.state.validation})
						:	<ProgressBar active now={100} />
				}
			</div>
		);
	},
});
