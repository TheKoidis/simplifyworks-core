/**
*	This class contains workflow functions
*/
var WorkflowUtils = {
	
	// returns name with - symbols
	getName: function(objectType, objectGroup, objectId) {
		return objectType + '-' + objectGroup + '-' + objectId;
	},
	
	// returns rest with priority objectGroup > objectId
	getRest: function(objectType, objectGroup, objectId) {
		if(objectGroup) {
			return '/api/workflow/task-instances/' + objectType + '/mine-for-group/' + objectGroup;
		} else if(objectId) {
			return '/api/workflow/task-instances/' + objectType + '/mine-for-id/' + objectId;
		} else {
			return null;
		}
	},
	
	renderAction: function(action, disabled, onClick) {
		var icon = action.icon ? <Glyphicon glyph={action.icon} /> : null;
		var label = (action.key && i18n.exists(action.key + "-label")) ? i18n.t(action.key + "-label") : action.name;
		var tooltip = (action.key && i18n.exists(action.key + "-tooltip")) ? i18n.t(action.key + "-tooltip") : action.name;
		
		return (
			<Button key={action.id} bsStyle={action.style} disabled={disabled} onClick={onClick} title={tooltip}>
				{icon}
				{icon ? (' ' + label) : label}
			</Button>
		);
	}
}
module.exports = WorkflowUtils;
