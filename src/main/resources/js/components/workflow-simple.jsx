
SW.WorkflowSimple = React.createClass({
	
	displayName: 'SW.WorkflowSimple',
	
	mixins: [FluxMixin, StoreWatchMixin('workflow')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		objectType: React.PropTypes.string.isRequired,
		objectGroup: React.PropTypes.string,
		objectId: React.PropTypes.string,
		objectPath: React.PropTypes.string,
		
		/**
		* Function called before workflow action, signature: function(actionId, taskIds, callback)
		* You have to call callback function (e.g. after some validation etc.) if you can run workflow action
		*/
		beforeAction: React.PropTypes.func,
		
		onActionDoneCallback: React.PropTypes.func,
		
		/**
		* Disable workflow buttons (DO NOT SET MANUALLY!)
		*/
		disabled: React.PropTypes.bool,
		
		/**
		* Parent detail component (DO NOT SET MANUALLY!)
		*/
		detail: React.PropTypes.object
	},
	
	componentDidMount: function() {
		this.init(false);
	},
	
	componentDidUpdate: function(prevProps, prevState) {
		if(prevProps.objectType != this.props.objectType || prevProps.objectGroup != this.props.objectGroup) {
			this.init(false);
		}
	},
	
	getStateFromFlux: function() {
		return {
			workflow: this.context.flux.store('workflow').getWorkflow(this.props.objectType),
		};
	},
	
	init: function(callCallback) {
		this.context.flux.actions.workflow.searchTasks(
			this.props.objectType,
			WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId),
			WorkflowUtils.getRest(this.props.objectType, this.props.objectGroup, this.props.objectId),
			function(result) {
				if (callCallback) {
					if (this.props.onActionDoneCallback) {
						this.props.onActionDoneCallback(result);
					}
					this.props.detail.refs.currentTab.load();
				}
			}.bind(this)
		);
	},
	
	render: function() {
		if(this.state.workflow.data.resources){
			switch(this.state.workflow.data.resources.length){
				case 0 : return null;
				case 1 : return (
					<ButtonGroup style={{marginLeft: '5px'}}>
						{this.state.workflow.data.resources[0].definition.actions.map(function(action) {
							return WorkflowUtils.renderAction(action, this.state.workflow.status === 'reloading' || this.props.disabled, this.onActionClick.bind(this, action.id));
						}.bind(this))}
					</ButtonGroup>
				)
				default : return (<p>{i18n.t('workflow-many-tasks')}</p>);
			}
		} else {
			return null;
		}
	},
	
	onActionClick: function(actionId) {
		var taskIds = this.state.workflow.data.resources.map(function(resource) {
			return resource.id;
		});
		
		if (this.props.beforeAction) {
			this.props.beforeAction(actionId, taskIds, function() {
				this.onActionClickCallback(actionId, taskIds);
			}.bind(this));
		} else {
			this.onActionClickCallback(actionId, taskIds);
		}
	},
	
	onActionClickCallback: function(actionId, taskIds) {
		this.context.flux.actions.workflow.completeTasks(this.props.objectType, taskIds, actionId, function() {
			this.init(true);
		}.bind(this));
	}
});
