/**
*	This class contains all component related library functions - i.e. mainly functions
*	for component structure validations and for children cloning.
*/
var ComponentUtils = {
	
	// finds out if component has a child of defined type; returns true if it does
	childExists: function(children, type) {
		var result = this.findChild(children, type);
		return result === undefined ? false : true;
	},
	
	// finds component's child of defined type; throws error if more than 1 child
	findChild: function(children, type) {
		var result = this.findChildren(children, type);
		
		if(result.length > 1) {
			throw new Error('Multiple children found for type \"' + type + '\".');
		}
		
		return result.length === 0 ? undefined : result[0];
	},
	
	// finds component's children of defined type; returns array of children
	findChildren: function(children, type) {
		var types = type instanceof Array ? type : [type];
		var result = [];
		
		React.Children.forEach(children, function(child) {
			if (child && child.type && (types.indexOf(child.type.displayName) > -1 || types.indexOf(child.type) > -1)) {
				result.push(child);
			}
		});
		
		return result;
	},
	
	// finds and clones child of defined type with props
	cloneChild: function(children, type, props) {
		var result = this.findChild(children, type);
		
		return result ? React.cloneElement(result, props) : undefined;
	},
	
	// returns array of children of defined type with props and refPrefix
	cloneChildren: function(children, type, props, refPrefix) {
		var types = type instanceof Array ? type : [type];
		var result = [];
		
		React.Children.forEach(children, function(child) {
			if(child && child.type && (types.indexOf(child.type.displayName) > -1 || types.indexOf(child.type) > -1)) {
				props.key = result.length;
				props.ref = (refPrefix ? refPrefix : '') + result.length;
				
				result.push(React.cloneElement(child, props));
			}
		});
		
		return result;
	},
	
	// checks if component's structure is according to component's propTypes
	checkPropTypesStructure: function(structure) {
		return function(props, propName, componentName) {
			var value = props[propName];
			
			if(structure[value]) {
				var fails = [];
				
				for(var index = 0; index < structure[value].length; index++) {
					if(!props[structure[value][index]]) {
						fails.push('conditionally-required property \"' + structure[value][index] + '\" is missing');
					}
				}
				
				return (fails.length > 0) ? new Error(componentName + '\'s structure is wrong: ' + fails.join(' and ') + '.') : undefined;
			} else {
				return new Error(componentName + '\'s prop \"' + propName + '\" has invalid value \"' + value + '\" (possible values are ' + Object.keys(structure).join(', ') + ')');
			}
		}.bind(this)
	},
	
	// checks if component's children are according to children definition in component
	checkChildrenStructure: function(structure) {
		return function(props, propName, componentName) {
			var allStructures = structure instanceof Array ? structure : [structure];
			var allFails = [];
			
			allStructures.forEach(function(element, index, array) {
				var counter = this.splitAndCount(props[propName], element);
				var fails = this.checkCounter(counter, element);
				
				if(fails.length > 0) {
					allFails.push('[' + fails.join(' and ') + ']')
				}
			}.bind(this));
			
			return (allFails.length === allStructures.length
				? new Error(componentName + '\'s children structure is wrong: ' + allFails.join(' or ') + '.')
				: undefined
			);
		}.bind(this)
	},
	
	// returns count per type of children
	splitAndCount: function(children, structure) {
		var counters = {};
		
		for(var property in structure) {
			counters[property] = 0;
		}
		
		React.Children.forEach(children, function(child) {
			if(child && child.type) {
				var name = child.type.displayName ? child.type.displayName : child.type;
				
				if(counters[name]) {
					counters[name]++;
				} else {
					counters[name] = 1;
				}
			}
		});
		
		return counters;
	},
	
	// checks if children count and requirement is according to the definition
	checkCounter: function(counter, structure) {
		var fails = [];
		
		for(var property in counter) {
			var expectation = structure[property];
			var count = counter[property];
			
			switch(expectation) {
				// 0...1
				case 'single-optional':
				this.checkSingleOptional(property, count, fails);
				break;
				// 1...1
				case 'single-required':
				this.checkSingleRequired(property, count, fails);
				break;
				// 0...*
				case 'multiple-optional':
				this.checkMultipleOptional(property, count, fails);
				break;
				// 1...*
				case 'multiple-required':
				this.checkMultipleRequired(property, count, fails);
				break;
				default:
				this.checkUnknown(property, count, fails);
				break;
			}
		}
		
		return fails;
	},
	
	// checks if property is used at most one time
	checkSingleOptional: function(property, count, fails) {
		if(count > 1) {
			fails.push('single and optional property \"' + property + '\" has ' + count + ' occurences');
		}
	},
	
	// checks if property is used one time
	checkSingleRequired: function(property, count, fails) {
		if(count > 1) {
			fails.push('single and required property \"' + property + '\" has ' + count + ' occurences');
		} else if (count === 0) {
			fails.push('single and required property \"' + property + '\" is missing');
		}
	},
	
	checkMultipleOptional: function(property, count, fails) {
		// do nothing
	},
	
	// checks if property is used at least one time
	checkMultipleRequired: function(property, count, fails) {
		if (count === 0) {
			fails.push('multiple and required property \"' + property + '\" is missing');
		}
	},
	
	// fails everytime, says how many count property has
	checkUnknown: function(property, count, fails) {
		fails.push('unknown property \"' + property + '\" has ' + count + ' occurence(s)');
	}
}

module.exports = ComponentUtils;
