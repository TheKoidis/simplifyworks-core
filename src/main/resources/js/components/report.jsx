var api = require('./../rest/report.jsx');

SW.Report = React.createClass({
	
	displayName: 'SW.Report',
	
	mixins: [ OnUnload, FluxMixin, StoreWatchMixin('report') ],
	
	contextTypes: {
		/**
		*	Flux context
		*/
		flux: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		showXml: React.PropTypes.bool,
		template: React.PropTypes.string.isRequired,
		type: React.PropTypes.string.isRequired,
		rest: React.PropTypes.string.isRequired,
		id: React.PropTypes.string,
		label: React.PropTypes.string,
		name: React.PropTypes.string.isRequired,
		disabled: React.PropTypes.bool
	},
	
	getDafaultProps: function() {
		return {
			showXml: false
		}
	},
	
	getStateFromFlux: function() {
		return {
			reportStatus: this.context.flux.store('report').getStatus()
		}
	},
	
	getInitialState: function() {
		return {xml: null};
	},
	
	render: function() {
		return (
			<span>
				<Button onClick={this.generateReport} style={{marginLeft: '5px'}} title={i18n.t('report-export-tooltip')} disabled={this.state.reportStatus==='processing' || this.props.disabled}>
					<Glyphicon glyph='download-alt' />
					{this.props.label ? ' '+this.props.label : ''}
				</Button>
				{this.props.showXml?
					<Button onClick={this.getXml} style={{marginLeft: '5px'}} bsStyle='link' disabled={this.state.reportStatus==='processing' || this.props.disabled}>
						<Glyphicon glyph='file' />
						{' '+i18n.t('report-show-xml')}
					</Button>
					: null
				}
				
				{this.state.xml
					?
					<Modal show={true} onHide={this.close} bsSize='large' backdrop='static'>
						<Modal.Header closeButton>
							<Modal.Title style={{width: 'auto'}}>Xml</Modal.Title>
						</Modal.Header>
						
						<Modal.Body>
							<Input value={this.state.xml} type='textarea' rows={20}/>
						</Modal.Body>
						
						<Modal.Footer>
							<Button onClick={this.close} bsStyle='link'>
								<Glyphicon glyph='remove' />
								{' ' + i18n.t('exit')}
							</Button>
						</Modal.Footer>
					</Modal>
					:
					null
				}
			</span>
		);
	},
	
	close: function() {
		this.setState({xml: null});
	},
	
	generateReport: function() {
		this.context.flux.actions.report.generateReport(this.props.rest, this.props.template, this.props.id, this.props.type, this.props.name);
	},
	
	getXml: function() {
		this.context.flux.actions.report.getXml(this.props.rest+'/xml', this.props.id, function(body) {
			this.setState({xml:  body.xml});
		}.bind(this), function(body) {
			notification.showLocalizedMessage('error', 'xml-generation-failed', 'xml-generation-failed');
		}.bind(this));
	}
});
