/**
*	This class contains all property related library functions - i.e. mainly functions
*	for getting&setting property values in object (using dot convention).
*/
var PropertyUtils = {
	
	// compares two arrays; returns true if arrays contains same elements in same order and false otherwise
	arrayEquals: function(first, second) {
		return JSON.stringify(first) === JSON.stringify(second);
	},
	
	// compares two values; returns true if values are empty (null or undefined) or same and false otherwise
	valueEquals: function(first, second) {
		var firstEmpty = first === undefined || first === null || first.length === 0;
		var secondEmpty = second === undefined || second === null || second.length === 0;
		
		return (firstEmpty && secondEmpty) || (first === second);
	},
	
	
	// merges properties from source and target to target (properties of object type are merged recursively).
	deepMerge: function(source, target) {
		for(var property in source) {
			if(source[property] && source[property].constructor === Object) {
				this.deepMerge(source[property], target[property] ? target[property] : (target[property] = {}));
			} else {
				target[property] = source[property];
			}
		}
		return target;
	},
	
	// returns property value existency flag for specified object (supports 'dot convention')
	hasProperty: function(object, property) {
		if(property) {
			var value = object;
			var propertyParts = property.split('.');
			
			// iterate through property name parts and updates value
			for(var index = 0; index < propertyParts.length; index++) {
				if(value) {
					value = value[propertyParts[index]];
				}
			}
			
			return value === undefined;
		} else {
			return null;
		}
	},
	
	// returns property value for specified object (supports 'dot convention')
	getPropertyValue: function(object, property) {
		if(property) {
			var value = object;
			var propertyParts = property.split('.');
			
			// iterate through property name parts and updates value
			for(var index = 0; index < propertyParts.length; index++) {
				if(value) {
					value = value[propertyParts[index]];
				}
			}
			
			return value;
		} else {
			return null;
		}
	},
	
	// sets property value for specified object (supports 'dot convention')
	setPropertyValue: function(object, property, newValue) {
		if(property) {
			var value = object;
			var propertyParts = property.split('.');
			
			// iterate through property name parts and updates value
			for(var index = 0; index < propertyParts.length; index++) {
				if(index === propertyParts.length - 1) {
					value[propertyParts[index]] = newValue;
				} else {
					if(value[propertyParts[index]]) {
						value = value[propertyParts[index]];
					} else {
						value = {};
					}
				}
			}
		}
		
		return newValue;
	}
}

module.exports = PropertyUtils;
