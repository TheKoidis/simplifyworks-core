/**
* Filter component
*/
SW.List.Filter = React.createClass({
	
	displayName: 'SW.List.Filter',
	
	mixins: [FluxMixin, StoreWatchMixin('list')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		/**
		* parent list name (DO NOT SET MANUALLY!)
		*/
		name: React.PropTypes.string,
		
		/**
		* Callback function responsible for rendering the submit component; signature is render(filterCallback)
		*/
		renderSubmit: React.PropTypes.func,
		
		/**
		* Callback function responsible for rendering the clear component; signature is render(clearCallback)
		*/
		renderClear: React.PropTypes.func,
		
		/**
		* children
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.Section': 'multiple-required'
		})
	},
	
	getStateFromFlux: function() {
		var list = this.context.flux.store('list').getList(this.props.name);
		
		return {
			options: list.options,
			data: list.options.filter
		};
	},
	
	filter: function() {
		this.context.flux.actions.list.common.reload(this.props.name, this.context.flux.store('list').getList(this.props.name).options);
	},
	
	resetFilter: function() {
		this.context.flux.actions.list.filter.resetFilter(this.props.name);
	},
	
	valueChanged: function(property, value) {
		this.context.flux.actions.list.filter.changeValue(this.props.name, property, value);
	},
	
	onKeyDown: function(event) {
		if(event.keyCode == 13) {
			this.filter();
		}
	},
	
	render: function() {
		return (
			<Panel>
				<div>
					{ComponentUtils.cloneChildren(this.props.children, 'SW.Form.Section', {
						name: this.props.name,
						labelKeyPrefix: this.props.labelKeyPrefix,
						data: this.state.data,
						valueChanged: this.valueChanged,
						onKeyDown: this.onKeyDown
					})}
				</div>
				
				<div style={{float: 'right'}}>
					{this.props.renderClear
						?
						this.props.renderClear(this.resetFilter)
						:
						<Button bsStyle='link' onClick={this.resetFilter} title={i18n.t('filter-clear-tooltip')}>
							<Glyphicon glyph={'remove'}/>
							{' ' + i18n.t('filter-clear')}
						</Button>
					}
					
					{this.props.renderSubmit
						?
						this.props.renderSubmit(this.filter)
						:
						<Button bsStyle='primary' onClick={this.filter} title={i18n.t('filter-submit-tooltip')}>
							<Glyphicon glyph={'search'}/>
							{' ' + i18n.t('filter-submit')}
						</Button>
					}
				</div>
			</Panel>
		);
	}
});
