/**
 * Menu list (dropdown) of actions.
 */
SW.List.ActivitiesMenu = React.createClass({

	displayName: 'SW.List.ActivitiesMenu',

	propTypes: {
		/**
		 * parent list name (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * label string key
		 */
		labelKey: React.PropTypes.string.isRequired,

		/**
		 * status of parent list (DO NOT SET MANUALLY!)
		 */
		status: React.PropTypes.string,

		/**
		 * selected records in list (DO NOT SET MANUALLY!)
		 */
		selection: React.PropTypes.array,

		/**
		 * children
		 */
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.Link': 'multiple-optional',
			'SW.List.Action': 'multiple-optional'
		})
	},

	render: function() {
		return (
			<DropdownButton id={'action-menu' + this.props.labelKey} title={i18n.t(this.props.labelKey)} disabled={!this.props.status || this.props.status === 'reloading'}>
				{ComponentUtils.cloneChildren(this.props.children, 'SW.List.Link', {name: this.props.name, type: 'menuItem'})}
				{ComponentUtils.cloneChildren(this.props.children, 'SW.List.Action', {name: this.props.name, type: 'menuItem', status: this.props.status, selection: this.props.selection})}
			</DropdownButton>
		);
	}
});
