/**
 * Button for filter component
 */
SW.List.FilterButton = React.createClass({

	displayName: 'SW.List.FilterButton',

	propTypes: {
		/**
		 * execute function
		 */
		execute: React.PropTypes.func
	},

	render: function() {
		return (
			<Button onClick={this.props.execute}>
				<Glyphicon glyph='filter'/>
				{' ' + i18n.t('filter')}
			</Button>
		);
	}
});
