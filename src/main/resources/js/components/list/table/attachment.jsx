var superagent = require('superagent');
var api = require('./../../../rest/attachment.jsx');

/**
* Component for attachment property of record.
*/
SW.List.Attachment = React.createClass({
	
	displayName: 'SW.List.Attachment',
	
	/**
	* SW.List.Property
	*/
	mixins: [SW.List.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		/**
		* disable wrapping value in cell
		*/
		nowrap: React.PropTypes.bool,
		
		/**
		* Custom rest url for download and upload (default is '/api/core/attachments')
		*/
		rest: React.PropTypes.string
	}),
	
	getDefaultProps: function() {
		return {
			orderable: true,
			filterable: true,
			rest: '/api/core/attachments'
		};
	},
	
	download: function(guid) {
		api.download(this.props.rest, guid);
	},
	
	renderBody: function(value, resource) {
		return <Button onClick={this.download.bind(this, resource.guid)} bsStyle='link' className='modal-detail-link' disabled={!resource.id}><Glyphicon glyph='download-alt' /> {resource.name}</Button>;
		}
	});
