/**
* Component for string property of record.
*/
SW.List.String = React.createClass({
	
	displayName: 'SW.List.String',
	
	/**
	* SW.List.Property
	*/
	mixins: [SW.List.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		/**
		* disable wrapping value in cell
		*/
		nowrap: React.PropTypes.bool
	}),
	
	getDefaultProps: function() {
		return {
			orderable: true,
			filterable: true
		};
	},
	
	renderBody: function(value) {
		return (this.props.nowrap ? <span style={{whiteSpace:'nowrap'}}>{value}</span> : value);
	}
});
