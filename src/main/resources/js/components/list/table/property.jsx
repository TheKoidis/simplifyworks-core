/**
* Component with methods for property (table).
*/
SW.List.Property = {
	
	propTypes: {
		/**
		* localization prefix from parent list (used with property in case labelKey or label is not set) (DO NOT SET MANUALLY!)
		*/
		labelKeyPrefix: React.PropTypes.string,
		
		/**
		*	Label key (for localization purposes, used in case Label is not set)
		*/
		labelKey: React.PropTypes.string,
		
		/**
		*	Label
		*/
		label: React.PropTypes.string,
		
		/**
		 * object property
		 */
		property: React.PropTypes.string,

		/**
		 * is property orderable
		 */
		orderable: React.PropTypes.bool,

		/**
		 * is property filterable
		 */
		filterable: React.PropTypes.bool,

		/**
		 * path (route) to detail
		 */
		path: React.PropTypes.string,

		/**
		 * property for select detail identifier, shows cell value as a link to detail + shows remove link on mouse over
		 * (in case path property is set, then application is redirected to it, otherwise a modal window with detail is shown)
		 */
		pathProperty: React.PropTypes.string,
		
		/**
		 * function that is called when user clicks on modal detail link (DO NOT SET MANUALLY!)
		 */
		detailFunc: React.PropTypes.func,

		/**
		 * function that is called when user clicks on remove link (DO NOT SET MANUALLY!)
		 */
		removeFunc: React.PropTypes.func,

		/**
		 * array of render summary functions which render summary cell,
		 * function signature: function(selection - array of selected records, property - column where the summary cell is shown)
		 */
		renderSummary: React.PropTypes.array,

		/**
		 * width of table column - e.g. 15%, 15px, 15em
		 */
		width: React.PropTypes.string,
		
		/**
		 * min width of table column - e.g. 15%, 15px, 15em
		 */
		minWidth: React.PropTypes.string,
		
		/**
		 * max width of table column - e.g. 15%, 15px, 15em
		 */
		maxWidth: React.PropTypes.string,
		
		/**
		 * class name of table column
		 */
		className: React.PropTypes.string,
		
		/**
		 * name of parent table (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * type of cell in table (head, body or foot) (DO NOT SET MANUALLY!)
		 */
		type: React.PropTypes.oneOf(['head', 'body', 'foot']),

		/**
		 * whole record of row (DO NOT SET MANUALLY!)
		 */
		resource: React.PropTypes.object,

		/**
		 * state of parent list (DO NOT SET MANUALLY!)
		 */
		status: React.PropTypes.string,

		/**
		 * data of parent list (DO NOT SET MANUALLY!)
		 */
		data: React.PropTypes.object,

		/**
		 * whole list options (DO NOT SET MANUALLY!)
		 */
		options: React.PropTypes.object,
		
		/**
		 * function to call when user changes order of this column (DO NOT SET MANUALLY!)
		 */
		changeOrder: React.PropTypes.func,

		/**
		 * array of selected records (rows) (DO NOT SET MANUALLY!)
		 */
		selection: React.PropTypes.array,

		/**
		 * disables actions in column (remove link, edit link), is get form table (DO NOT SET MANUALLY!)
		 */
		disabled: React.PropTypes.bool,

		/**
		 * index of summary row in footer (in case some column has specified more than one summary functions) (DO NOT SET MANUALLY!)
		 */
		footIndex: React.PropTypes.number,
		
	},
	
	isDisabled: function() {
		return this.props.status === 'reloading' || !this.props.data || this.props.data.recordsReturned === 0;
	},
	
	getOrder: function(ordering) {
		for(var index = 0; index < ordering.length; index++) {
			if(ordering[index].property === this.props.property) {
				return ordering[index].order;
			}
		}
		
		return null;
	},
	
	getOrderRank: function(ordering) {
		for(var index = 0; index < ordering.length; index++) {
			if(ordering[index].property === this.props.property) {
				return index + 1;
			}
		}
		
		return undefined;
	},
	
	changeOrder: function(event) {
		this.props.changeOrder(this.props.property, this.getOrder(this.props.options.ordering), event.shiftKey);
	},
	
	getLabel: function() {
		var tooltipKey = LocalizationUtils.getLabelKey(this.props) + '-table-tooltip';
		return <span title={i18n.exists(tooltipKey) ? i18n.t(tooltipKey) : ''}>
			{(this.props.label ? this.props.label : i18n.t(LocalizationUtils.getLabelKey(this.props))) + ' '}
			{i18n.exists(tooltipKey) ? <Glyphicon glyph='info-sign'/> : ''}
		</span>;
	},
	
	detailFunc: function(resource, pathProperty) {
		this.props.detailFunc(resource, pathProperty);
	},
	
	linkMouseOver: function() {
		this.setState({
			showDelete: true
		});
	},
	
	linkMouseOut: function() {
		this.setState({
			showDelete: false
		});
	},
	
	confirmDelete: function(resource, pathPropertyValue, value) {
		if (this.props.removeWithoutConfirmation) {
			this.remove(resource, pathPropertyValue);
			return;
		}
		
		notification.showLocalizedMessage('warning', 'delete-confirm', 'delete-confirm-text', {record: value}, {
			label: i18n.t('delete-confirm-button'),
			callback: function() {
				this.remove(resource, pathPropertyValue);
			}.bind(this)
		}, 0);
	},
	
	remove: function(resource, pathPropertyValue) {
		this.props.removeFunc(resource, pathPropertyValue);
	},
	
	render: function() {
		switch(this.props.type) {
			case 'head':
			if (this.renderHead) {
				return (
					<th style={{width: this.props.width, minWidth: this.props.minWidth, maxWidth: this.props.maxWidth}} className={this.props.className}>
						{this.renderHead()}
					</th>
				)
			} else {
				if(this.props.orderable) {
					var order = this.getOrder(this.props.options.ordering);
					var glyph = (order === 'asc' ? 'sort-by-attributes' : (order === 'desc' ? 'sort-by-attributes-alt' : 'sort'));
					var orderRank = this.getOrderRank(this.props.options.ordering);
					return (
						<th style={{width: this.props.width, cursor: 'pointer', minWidth: this.props.minWidth, maxWidth: this.props.maxWidth}} className={this.props.className} onClick={this.changeOrder}>
							<Glyphicon glyph={glyph} title={i18n.t(glyph)}/>
							{this.props.options.ordering.length > 1 && orderRank
								?   <span>
								{' '}
								<Badge>{orderRank}</Badge>
							</span>
							:   null
						}
						{' '}
						{this.getLabel()}
					</th>
				);
			} else {
				return (
					<th style={{width: this.props.width, minWidth: this.props.minWidth, maxWidth: this.props.maxWidth}} className={this.props.className}>
						{this.getLabel()}
					</th>
				);
			}
		}
		case 'body':
		var pathPropertyValue = PropertyUtils.getPropertyValue(this.props.resource, (this.props.pathProperty ? this.props.pathProperty : this.props.property));
		var value = PropertyUtils.getPropertyValue(this.props.resource, this.props.property);
		var component = this.renderBody(value, this.props.resource);
		
		return (
			<td onMouseEnter={this.linkMouseOver} onMouseLeave={this.linkMouseOut} className={this.props.className}>
				{this.props.path
					?   <Link to={this.props.path + '/' + pathPropertyValue}>
					{component}
				</Link>
				:   (this.props.pathProperty && this.props.detailFunc ? <Button onClick={this.detailFunc.bind(this, this.props.resource, pathPropertyValue)} bsStyle='link' className='modal-detail-link'>{component}</Button> :
				component)}
				
				{this.props.pathProperty && this.state && this.state.showDelete ?
					
						this.props.removeFunc && !this.props.disabled ?
							<Button style={{float: 'right'}} bsStyle="link" bsSize="xsmall" className='modal-detail-link' title={i18n.t('table-remove')} onClick={this.confirmDelete.bind(this, this.props.resource, pathPropertyValue, value)}><Glyphicon glyph='trash'/></Button>
							: null
						
						: null}
					</td>
				);
				case 'foot':
				if (this.renderFoot) {
					<td className={this.props.className}>
						{this.renderFoot()}
					</td>
				} else {
					return (
						<td className={this.props.className}>
							{this.props.renderSummary &&  this.props.renderSummary[this.props.footIndex]
								?   this.props.renderSummary[this.props.footIndex](this.props.selection, this.props.property)
								:   null
							}
						</td>
					);
				}
			}
		}
	};
