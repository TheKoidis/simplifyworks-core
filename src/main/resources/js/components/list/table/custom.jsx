/**
 * Component for custom property of record.
 */
SW.List.Custom = React.createClass({

	displayName: 'SW.List.Custom',

	/**
	 * SW.List.Property
	 */
	mixins: [SW.List.Property],

	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		/**
		 * custom render function
		 * function signature: function(resource - the whole object of row, property - column where the cell is shown)
		 */
		render: React.PropTypes.func
	}),

	getDefaultProps: function() {
		return {
			orderable: true,
			filterable: true
		};
	},

	renderBody: function(value, resource) {
		return this.props.render(resource, this.props.property);
	}
});
