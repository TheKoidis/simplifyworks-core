/**
* Component for selection property of record.
*/
SW.List.Selection = React.createClass({
	
	displayName: 'SW.List.Selection',
	
	/**
	* SW.List.Property
	*/
	mixins: [SW.List.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		// no added attributes
	}),
	
	getDefaultProps: function() {
		return {
			orderable: false,
			filterable: false
		};
	},
	
	isSelected: function(selection) {
		return (selection.filter(function(element, index, array) {
			return PropertyUtils.getPropertyValue(element, this.props.property) === PropertyUtils.getPropertyValue(this.props.resource, this.props.property);
		}.bind(this)).length === 1);
	},
	
	selectSingle: function(event) {
		this.props.selectSingle(event.target.checked, this.props.property, this.props.resource);
	},
	
	selectPage: function() {
		this.props.selectPage(this.props.property);
	},
	
	deselectPage: function() {
		this.props.deselectPage(this.props.property);
	},
	
	renderBody: function(value) {
		return (
			<input type='checkbox' title={i18n.t('select-record')} checked={this.isSelected(this.props.selection)} onChange={this.selectSingle} />
		);
	},
	
	renderHead: function() {
		return (
			<Dropdown id={this.props.name + '-selection'} bsSize='xsmall' style={{width: '100%'}}>
				<Dropdown.Toggle style={{width: '100%'}} disabled={this.isDisabled()}>
					<Glyphicon glyph='check' />
				</Dropdown.Toggle>
				
				<Dropdown.Menu>
					<MenuItem onSelect={this.selectPage}>
						{i18n.t('select-page')}
					</MenuItem>
					
					<MenuItem divider/>
					
					<MenuItem onSelect={this.deselectPage}>
						{i18n.t('deselect-page')}
					</MenuItem>
					
					<MenuItem onSelect={this.props.deselectAll}>
						{i18n.t('deselect-all')}
					</MenuItem>
				</Dropdown.Menu>
			</Dropdown>
		);
	}
});
