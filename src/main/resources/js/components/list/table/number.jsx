/**
* Component for number property of record.
*/
SW.List.Number = React.createClass({
	
	displayName: 'SW.List.Number',
	
	/**
	* SW.List.Property
	*/
	mixins: [SW.List.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		/**
		* disable separator between numbers (e.g. for years - 2016 instead of 2 016)
		*/
		disableGroupSeparator: React.PropTypes.bool
	}),
	
	getDefaultProps: function() {
		return {
			orderable: true,
			filterable: true
		};
	},
	
	renderBody: function(value) {
		return (
			<div style={{textAlign:'right',whiteSpace:'nowrap'}}>{(value != undefined && value != null) ? NumberUtils.format(value, this.props.disableGroupSeparator ? '' : undefined) : ''}</div>
		);
	}
});
