/**
 * Component for boolean property of record.
 */
SW.List.Boolean = React.createClass({

	displayName: 'SW.List.Boolean',

	/**
	 * SW.List.Property
	 */
	mixins: [SW.List.Property],

	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		// no added attributes
	}),

	getDefaultProps: function() {
		return {
			orderable: true,
			filterable: true
		};
	},

	renderBody: function(value) {
		return (
			<span>
				{i18n.t(value === true ? 'true' : (value === false ? 'false' : 'null'))}
			</span>
		)
	}
});
