/**
 * Component for enum property of record.
 */
SW.List.Enum = React.createClass({

	displayName: 'SW.List.Enum',

	/**
	 * SW.List.Property
	 */
	mixins: [SW.List.Property],

	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		// no added attributes
	}),

	getDefaultProps: function() {
		return {
			orderable: true,
			filterable: true
		};
	},

	renderBody: function(value) {
		return i18n.t(this.props.options.enum + '-' + value);
	}
});
