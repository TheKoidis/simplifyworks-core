/**
* Component for choose property of record. (DO NOT USE MANUALLY!)
*/
SW.List.Choose = React.createClass({
	
	displayName: 'SW.List.Choose',
	
	/**
	* SW.List.Property
	*/
	mixins: [SW.List.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {		
		/**
		* function that is called when user clicks on choose button - used in codelist selection panel (DO NOT SET MANUALLY!)
		*/
		chooseFunc: React.PropTypes.func
	}),
	
	getDefaultProps: function() {
		return {
			orderable: false,
			filterable: false
		};
	},
	
	select: function(value) {
		this.props.chooseFunc(value);
	},
	
	renderBody: function(value, resource) {
		return (
			<Button bsSize="xsmall" bsStyle='primary' onClick={this.select.bind(this, resource)}>
				<Glyphicon glyph='pushpin' />
				{' ' + i18n.t('table-choose')}
			</Button>
		);
	},
	
	renderHead: function() {
		return '';
	}
});
