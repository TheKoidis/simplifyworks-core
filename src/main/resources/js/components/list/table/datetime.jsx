var moment = require('moment');

/**
 * Component for datetime property of record.
 */
SW.List.Datetime = React.createClass({

	displayName: 'SW.List.Datetime',

	/**
	 * SW.List.Property
	 */
	mixins: [SW.List.Property],

	propTypes: PropertyUtils.deepMerge(SW.List.Property.propTypes , {
		/**
		 * is time enabled
		 */
		timeEnabled: React.PropTypes.bool
	}),

	getDefaultProps: function() {
		return {
			orderable: true,
			filterable: true
		};
	},

	renderBody: function(value) {
		return value ? moment(value).format(i18n.t(this.props.timeEnabled?'patternDateTime':'patternDate')) : '';
	}
});
