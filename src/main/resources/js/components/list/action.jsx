/**
 * Action for list.
 */
SW.List.Action = React.createClass({

	displayName: 'SW.List.Action',

	propTypes: {
		/**
		 * parent list name (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * type (button, menu item)
		 */
		type: React.PropTypes.oneOf(['button', 'menuItem']),

		/**
		 * icon
		 */
		icon: React.PropTypes.string,

		/**
		 * label string key
		 */
		labelKey: React.PropTypes.string.isRequired,

		/**
		 * execute function
		 */
		execute: React.PropTypes.func.isRequired,

		/**
		 * is action enabled
		 */
		isEnabled: React.PropTypes.func.isRequired,

		/**
		 * status of parent list
		 */
		status: React.PropTypes.string,

		/**
		 * selected records in list (DO NOT SET MANUALLY!)
		 */
		selection: React.PropTypes.array
	},

	isDisabled: function() {
		return !this.props.status || this.props.status === 'reloading' || !this.props.isEnabled(this.props.selection)
	},

	execute: function() {
		this.props.execute(this.props.selection);
	},

	render: function() {
		return (
			this.props.type === 'button'
				?   <Button disabled={this.isDisabled()} onClick={this.isDisabled() ? null : this.execute}>
						{this.props.icon
							?   <Glyphicon glyph={this.props.icon} />
						:   null
						}

						{(this.props.icon ? ' ' : '') + i18n.t(this.props.labelKey)}
					</Button>
				:   <MenuItem disabled={this.isDisabled()} onClick={this.isDisabled() ? null : this.execute}>
						{this.props.icon
							?   <Glyphicon glyph={this.props.icon} />
						:   null
						}

						{(this.props.icon ? ' ' : '') + i18n.t(this.props.labelKey)}
					</MenuItem>
		);
	}
});
