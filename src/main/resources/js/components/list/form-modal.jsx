/**
*	Form represents the smallest lockable and editable part of detail. Form is divided into sections.
*/
SW.List.FormModal = React.createClass({
	
	displayName: 'SW.List.FormModal',
	
	mixins: [FluxMixin, StoreWatchMixin('form'), OnUnload ],
	
	contextTypes: {
		/**
		*	Flux context
		*/
		flux: React.PropTypes.object.isRequired,
	},
	
	propTypes: {
		/**
		*	Prefix for label keys (for localization purposes), automatically included in case of tab usage
		*/
		labelKeyPrefix: React.PropTypes.string,
		
		/**
		*	REST path for data loading and saving (also used as key for Flux store and actions)
		*/
		rest: React.PropTypes.string.isRequired,
		
		/**
		* initial value
		*/
		initialValue: React.PropTypes.object,
		
		/**
		* listener for value changes
		*/
		valueChanged: React.PropTypes.func,
		
		/**
		*
		*/
		new: React.PropTypes.bool,
		
		/**
		* Callback called when new record is successfully created or creating failed
		* (if callback returns false then toast message is shown automatically, otherwise you have to implement it manually in your callback method)
		*/
		createCallback: React.PropTypes.func,
		
		/**
		*	Function responsible for validation ('validate(data, callback)' where 'callback(isValid)')
		*/
		validate: React.PropTypes.func,
		
		/**
		*	Children structure (at least one section is required)
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.Section': 'multiple-required'
		})
	},
	
	componentDidMount: function() {
		if (!this.props.new && this.state.detail.status !== 'locked') {
			this.lock();
		}
		
		if (this.state.form.status !== 'managed') {
			if (this.props.new) {
				this.init(this.props.initialValue);
			} else if (this.state.detail.status !== 'locked' || this.state.form.status === 'new') {
				this.load();
			}
		}
	},
	
	componentDidUpdate: function(prevProps, prevState) {
		if(this.props.rest !== prevProps.rest) {
			if (this.state.form.status !== 'managed') {
				if (this.props.new) {
					this.init();
				} else if (this.state.detail.status !== 'locked' || this.state.form.status === 'new') {
					this.load();
				}
			}
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if(this.props.rest !== nextProps.rest) {
			this.setState({
				detail: this.context.flux.store('form').getDetail(this.props.rest),
				form: this.context.flux.store('form').getForm(this.props.rest, null),
			});
		}
	},
	
	getStateFromFlux: function() {
		return {
			detail: this.context.flux.store('form').getDetail(this.props.rest),
			form: this.context.flux.store('form').getForm(this.props.rest, null),
		}
	},
	
	onUnload: function() {
		if (this.state.status === 'locked') {
			clearInterval(this.state.holdLockTimer);
		}
		this.props.exit();
	},
	
	onBeforeUnload: function(e) {
		if (!this.state.detail.modified) {
			return false;
		}
		
		var confirmationMessage = i18n.t('form-unload-changes');
		
		(e || window.event).returnValue = confirmationMessage; //Gecko + IE, Chrome(mac os)
		return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
	},
	
	exit: function() {
		if (this.state.detail.modified) {
			notification.showLocalizedMessage('warning', 'form-changes-found', 'form-save-or-discard-changes', {}, {
				label: i18n.t('form-leave-changes'),
				callback: function() {
					clearInterval(this.state.holdLockTimer);
					this.context.flux.actions.form.unlock(this.props.rest);
					
					this.props.exit();
				}.bind(this)
			}, 0);
			
			return false;
		}
		
		if (this.state.detail.status === 'locked') {
			clearInterval(this.state.holdLockTimer);
			this.context.flux.actions.form.unlock(this.props.rest);
		}
		
		this.props.exit();
	},
	
	valueChanged: function(property, value, validations) {
		this.context.flux.actions.form.changeValue(this.props.rest, null, property, value, this.props.valueChanged, validations);
	},
	
	init: function() {
		this.context.flux.actions.form.formInit(this.props.rest, null, null, ValidationUtils.getValidationFromProps(this.props, this.props.labelKeyPrefix), this.props.initialValue);
	},
	
	load: function() {
		this.context.flux.actions.form.load(this.props.rest, null, null, ValidationUtils.getValidationFromProps(this.props, this.props.labelKeyPrefix));
	},
	
	lock: function() {
		this.context.flux.actions.form.lock(this.props.rest);
		
		this.setState({holdLockTimer: setInterval(function() {
			this.context.flux.actions.form.holdLock(this.props.rest);
		}.bind(this), FormConstants.HOLD_LOCK_REFRESH_INTERVAL)});
	},
	
	create: function() {
		this.context.flux.actions.form.refreshValidation(this.props.rest, null, ValidationUtils.getValidationFromProps(this.props, this.props.labelKeyPrefix));
		
		this.context.flux.actions.form.create(this.props.rest, this.props.createCallback, this.props.labelKeyPrefix, this.props.validate);
	},
	
	save: function() {
		this.context.flux.actions.form.refreshValidation(this.props.rest, null, ValidationUtils.getValidationFromProps(this.props, this.props.labelKeyPrefix));
		
		this.context.flux.actions.form.save(this.props.rest, null, null, null, this.props.validate, true);
	},
	
	render: function() {
		return (
			<Modal show={true} bsSize="large" backdrop="static" onHide={this.exit}>
				<Modal.Header closeButton>
					<Modal.Title>{this.props.renderTitle(this.props.new ? null : this.state.form.data)}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					{this.state.form.status === 'new' || this.state.form.status === 'managed'
						?
						ComponentUtils.cloneChildren(this.props.children, 'SW.Form.Section', {
							labelKeyPrefix: this.props.labelKeyPrefix,
							data: this.state.form.data,
							disabled: this.state.detail.status !== 'locked' && this.state.form.status !== 'new',
							valueChanged: this.valueChanged,
							validation: this.state.form.validation
						})
						:
						<ProgressBar active now={100} />
					}
				</Modal.Body>
				<Modal.Footer>
					{!this.state.detail.modified ?
						(
							this.state.detail.status !== 'locked' && this.state.form.status !== 'new' && this.state.form.status !== 'processing'?
							<span>
								<Glyphicon glyph='lock' />
								{' ' + i18n.t('form-read-only')}
								<Button bsStyle='link' onClick={this.lock}>
									<Glyphicon glyph='pencil' />
									{' ' + i18n.t('form-edit')}
								</Button>
							</span>
							:
							<span><Glyphicon glyph='ok' />{' ' + i18n.t('form-no-changes')}</span>
						)
						:
						<span><Glyphicon glyph='pencil' />{' ' + i18n.t('form-changes')}</span>
					}
					
					<Button onClick={this.exit} bsStyle='link'>
						<Glyphicon glyph='remove' />
						{' ' + i18n.t('exit')}
					</Button>
					
					{this.state.form.status === 'new' ?
						<Button bsStyle='primary' onClick={this.create}>
							<Glyphicon glyph='floppy-disk' />
							{' ' + i18n.t('form-create')}
						</Button>
						:
						this.state.detail.modified ?
						<Button bsStyle='primary' onClick={this.save}>
							<Glyphicon glyph='floppy-disk' />
							{' ' + i18n.t('form-save-changes')}
						</Button>
						:
						null
					}
				</Modal.Footer>
			</Modal>
		);
	},
});
