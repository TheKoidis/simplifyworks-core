/**
 * Group of actions for list.
 */
SW.List.ActivitiesGroup = React.createClass({

	displayName: 'SW.List.ActivitiesGroup',

	propTypes: {
		/**
		 * parent list name (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * status of parent list (DO NOT SET MANUALLY!)
		 */
		status: React.PropTypes.string,

		/**
		 * selected records in list (DO NOT SET MANUALLY!)
		 */
		selection: React.PropTypes.array,
		
		/**
		 * children
		 */
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.Link': 'multiple-optional',
			'SW.List.LinkModal': 'multiple-optional',
			'SW.List.Action': 'multiple-optional',
			'SW.List.ActivitiesMenu': 'multiple-optional'
		})
	},

	render: function() {
		return (
			<ButtonGroup>
				{ComponentUtils.cloneChildren(this.props.children, 'SW.List.Link', {name: this.props.name, type: 'button'})}
				{ComponentUtils.cloneChildren(this.props.children, 'SW.List.LinkModal', {name: this.props.name, type: 'button'})}
				{ComponentUtils.cloneChildren(this.props.children, 'SW.List.Action', {name: this.props.name, type: 'button', status: this.props.status, selection: this.props.selection})}
				{ComponentUtils.cloneChildren(this.props.children, 'SW.List.ActivitiesMenu', {name: this.props.name, status: this.props.status, selection: this.props.selection})}
			</ButtonGroup>
		);
	}
});
