/**
* Pagination component for table.
*/
SW.List.Pagination = React.createClass({

	displayName: 'SW.List.Pagination',

	statics: {
		//Necesarrity of ascending order in the field for the if condition
		pageSizes: [10, 15, 25, 50, 100]
	},

	propTypes: {
		/**
		 * parent list name (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		* function called when user changes page number (DO NOT SET MANUALLY!)
		*/
		changePageNumber: React.PropTypes.func,

		/**
		* Function responsible for changing size of page ("changePageSize(pageSize)")  (DO NOT SET MANUALLY!)
		*/
		changePageSize: React.PropTypes.func,

		/**
		* status of parent list (DO NOT SET MANUALLY!)
		*/
		status: React.PropTypes.string,

		/**
		* options of parent list (page size, page, filtering etc.) (DO NOT SET MANUALLY!)
		*/
		options: React.PropTypes.object,

		/**
		* data of parent list (DO NOT SET MANUALLY!)
		*/
		data: React.PropTypes.object
	},

	render: function() {
		var pageNumber = this.props.options.pageNumber;
		var pageCount = Math.floor(this.props.data.recordsFiltered / this.props.options.pageSize + (this.props.data.recordsFiltered % this.props.options.pageSize === 0 ? 0 : 1));

		if (pageCount == 0 || this.props.data.recordsFiltered == null) {
			return null;
		}

		return (
			<div style={{verticalAlign: 'initial'}}>
				{this.props.status == 'reloading' ? <div id='loading-disabled' onClick={false}></div> : null}
				{pageCount > 1
					? <Pagination first prev next last ellipsis activePage={pageNumber} items={pageCount} maxButtons={3} onSelect={this.props.changePageNumber} style={this.props.status=='reloading' ? {opacity: 0.5, margin: 0} : {margin: 0}}/>
					: null
				}

				{this.props.data.recordsFiltered <= SW.List.Pagination.pageSizes[0]
					? null
					: 	<Dropdown id='page-size' dropup pullRight style={this.props.status=='reloading' ? {verticalAlign: 'initial', marginLeft: 10, opacity: 0.5} : {verticalAlign: 'initial', marginLeft: 10}}>
							<Dropdown.Toggle>
								<Glyphicon glyph='cog' />
							</Dropdown.Toggle>

							<Dropdown.Menu>
								{SW.List.Pagination.pageSizes.map(function(element, index, array) {
									return <MenuItem key={index} eventKey={element} onClick={this.changePageSize.bind(this, element)} active={this.props.options.pageSize === element}>
												{i18n.t('page-size', {pageSize: element})}
										   </MenuItem>;
								}.bind(this))}
							</Dropdown.Menu>
						</Dropdown>
				}
			</div>
		);
	},

	changePageSize: function(pageSize) {
		this.props.changePageSize(pageSize);
	}
});
