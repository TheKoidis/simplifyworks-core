/**
*	Detail represents detailed view on some agenda. View is supposed to be complicated so the tabs are provided.
*/
SW.List.DetailModal = React.createClass({
	
	displayName: 'SW.List.DetailModal',
	
	mixins: [FluxMixin, StoreWatchMixin('form')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		/**
		*	Identifier of object to show in modal detail window
		*/
		showModalDetailValue: React.PropTypes.any,
		/**
		*	Function that is called when user wants to show modal detail window
		*/
		showModalDetail: React.PropTypes.func,
		/**
		*	Function that render title
		*/
		renderTitle: React.PropTypes.func.isRequired,
		/**
		* Callback called when new record is successfully created or creating failed
		* (if callback returns false then toast message is shown automatically, otherwise you have to implement it manually in your callback method)
		*/
		createCallback: React.PropTypes.func,
		/**
		*	Children structure (at least one tab is required)
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.FormModal': 'single-required'
		})
	},
	
	getStateFromFlux: function() {
		var form = ComponentUtils.findChild(this.props.children, 'SW.List.FormModal');
		var isNew = (this.props.showModalDetailValue === 'new');
		var rest = form.props.rest + (!isNew ? '/' + this.props.showModalDetailValue : '');
		
		return {rest: rest, isNew: isNew};
	},
	
	createCallback: function(id, error, response, body) {
		if (!this.props.createCallback && id) {
			this.exit();
		}
		
		return this.props.createCallback ? this.props.createCallback(id, error, response, body) : false;
	},
	
	exit: function() {
		this.props.showModalDetail(null, null);
	},
	
	render: function() {
		var form = ComponentUtils.findChild(this.props.children, 'SW.List.FormModal');
		
		if (this.props.showModalDetailValue===null) {
			return null;
		}
		
		return React.cloneElement(form, {
			rest: this.state.rest,
			new: this.state.isNew,
			createCallback: this.createCallback,
			exit: this.exit,
			renderTitle: this.props.renderTitle
		});
	}
});
