/**
* Information for table.
*/
SW.List.Info = React.createClass({
	
	displayName: 'SW.List.Info',
	
	propTypes: {
		/**
		* parent list name (DO NOT SET MANUALLY!)
		*/
		name: React.PropTypes.string,
		
		/**
		* status of parent list (DO NOT SET MANUALLY!)
		*/
		status: React.PropTypes.string,
		
		/**
		* number of selected records in table (DO NOT SET MANUALLY!)
		*/
		selectionCount: React.PropTypes.number,
		
		/**
		* current page number (DO NOT SET MANUALLY!)
		*/
		pageNumber: React.PropTypes.number,
		
		/**
		* page size (number of records per page) (DO NOT SET MANUALLY!)
		*/
		pageSize: React.PropTypes.number,
		
		/**
		* number of returned records (DO NOT SET MANUALLY!)
		*/
		recordsReturned: React.PropTypes.number,
		
		/**
		* number of filtered records (DO NOT SET MANUALLY!)
		*/
		recordsFiltered: React.PropTypes.number,
		
		/**
		* total number of records (DO NOT SET MANUALLY!)
		*/
		recordsTotal: React.PropTypes.number,
		
		/**
		* suffix for all label keys (to customize messages)
		*/
		labelKeySuffix: React.PropTypes.string
	},
	
	getDefaultProps: function() {
		return {
			labelKeySuffix: ''
		};
	},
	
	render: function() {
		var info = this.createSelectionInfo() + ' ' + this.createViewInfo();
		
		return (
			<span>
				{info}
			</span>
		);
	},
	
	createSelectionInfo: function() {
		switch(this.props.selectionCount) {
			case undefined:
			case 0:
			return '';
			case 1:
			return i18n.t('info-one-selected' + this.props.labelKeySuffix);
			case 2:
			case 3:
			case 4:
			return i18n.t('info-few-selected' + this.props.labelKeySuffix, {records: this.props.selectionCount});
			default:
			return i18n.t('info-many-selected' + this.props.labelKeySuffix, {records: this.props.selectionCount});
		}
	},
	
	createViewInfo: function() {
		var message = '';
		switch(this.props.recordsFiltered) {
			case undefined:
			case 0:
			return '';
			case 1:
			message = i18n.t('info-one-found' + this.props.labelKeySuffix);
			break;
			case 2:
			case 3:
			case 4:
			message = i18n.t('info-few-found' + this.props.labelKeySuffix, {records: this.props.recordsFiltered});
			break;
			default:
			message = i18n.t('info-many-found' + this.props.labelKeySuffix, {records: this.props.recordsFiltered});
		}
		
		return message + (this.props.recordsFiltered === this.props.recordsTotal ? '.' : i18n.t('info-filter' + this.props.labelKeySuffix));
	}
});
