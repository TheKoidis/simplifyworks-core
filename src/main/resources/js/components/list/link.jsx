/**
 * Link to specific page.
 */
SW.List.Link = React.createClass({

	displayName: 'SW.List.Link',

	contextTypes: {
		/**
		 * router
		 */
		router: React.PropTypes.object.isRequired
	},

	propTypes: {
		/**
		 * parent list name (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * type of component (button or menu item)
		 */
		type: React.PropTypes.oneOf(['button', 'menuItem']),

		/**
		 * icon
		 */
		icon: React.PropTypes.string,

		/**
		 * label string key
		 */
		labelKey: React.PropTypes.string.isRequired,

		/**
		 * path to specific page
		 */
		path: React.PropTypes.string.isRequired
	},

	goTo: function() {
		this.context.router.push(this.props.path);
	},

	render: function() {
		return (
			this.props.type === 'button'
				?   <Button onClick={this.goTo}>
						{this.props.icon
							?   <Glyphicon glyph={this.props.icon} />
							:   null
						}

						{(this.props.icon ? ' ' : '') + i18n.t(this.props.labelKey)}
					</Button>
				:   <MenuItem onSelect={this.goTo}>
						{this.props.icon
							?   <Glyphicon glyph={this.props.icon} />
							:   null
						}

						{(this.props.icon ? ' ' : '') + i18n.t(this.props.labelKey)}
					</MenuItem>
		);
	}
});
