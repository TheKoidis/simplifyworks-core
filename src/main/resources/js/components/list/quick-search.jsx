/**
* Component for quick search in list.
*/
SW.List.QuickSearch = React.createClass({
	
	displayName: 'SW.List.QuickSearch',
	
	propTypes: {
		/**
		* Function responsible for searching ("search(values)", values are tokens for search)
		*/
		search: React.PropTypes.func,
		
		/**
		* Size in grid for extra small display
		*/
		xs: React.PropTypes.number,
		
		/**
		* Size in grid for small display
		*/
		sm: React.PropTypes.number,
		
		/**
		* Size in grid for medium display
		*/
		md: React.PropTypes.number,
		
		/**
		* Size in grid for large display
		*/
		lg: React.PropTypes.number
	},
	
	getDefaultProps: function() {
		return {
			xs: 12, sm: 4, md: 2, lg: 2
		}
	},
	
	getInitialState: function() {
		return {
			value: this.props.initialValue ? this.props.initialValue : ''
		};
	},
	
	render: function() {
		var searchButton = (
			<Button onClick={this.search} title={i18n.t('quick-search-go')}>
				<Glyphicon glyph='search' />
			</Button>
		);
		
		return (
			<span>
				<Input autoFocus={!this.props.initialValue} type='text' ref='input' value={this.state.value}
					placeholder={i18n.t('quick-search')} size={10} onChange={this.onChange}
					onKeyDown={this.onKeyDown} buttonAfter={searchButton}/>
			</span>
		);
	},
	
	onChange: function(event) {
		this.setState({
			value: event.target.value
		});
	},
	
	onKeyDown: function(event) {
		if(event.keyCode == 13) {
			this.search();
		}
	},
	
	search: function() {
		this.props.search(this.prepareTokens(this.state.value));
	},
	
	prepareTokens: function(value) {
		return (
			value.length > 0
			?	value.split(/\s+/).filter(function(token) {
				return token.length > 0;
			})
			:	[]
		);
	}
})
