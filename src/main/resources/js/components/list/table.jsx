/**
 * Class represents table component.
 */
SW.List.Table = React.createClass({

	displayName: 'SW.List.Table',

	propTypes: {
		/**
		 * unique list name  (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * by setting false you can disable ordering for all columns
		 */
		orderable: React.PropTypes.bool,

		/**
		 * state of parent list (e.g. disables columns, shows progress bar etc.) (DO NOT SET MANUALLY!)
		 */
		status: React.PropTypes.string,

		/**
		 * all resources shown in table (DO NOT SET MANUALLY!)
		 */
		resources: React.PropTypes.array,

		/**
		 * selected records in table  (DO NOT SET MANUALLY!)
		 */
		selection: React.PropTypes.array,

		/**
		 * data of list (DO NOT SET MANUALLY!)
		 */
		data: React.PropTypes.object,

		/**
		 * options of table (current ordering, filtering etc.) (DO NOT SET MANUALLY!)
		 */
		options: React.PropTypes.object,

		/**
		 * function called when user clicks on select all records on page (DO NOT SET MANUALLY!)
		 */
		selectPage: React.PropTypes.func,

		/**
		 * function called when user clicks on deselect all records on page (DO NOT SET MANUALLY!)
		 */
		deselectPage: React.PropTypes.func,

		/**
		 * function called when user clicks on deselect all records (DO NOT SET MANUALLY!)
		 */
		deselectAll: React.PropTypes.func,

		/**
		 * function called when user changes some column order (DO NOT SET MANUALLY!)
		 */
		changeOrder: React.PropTypes.func,

		/**
		 * function called when user selects single record (DO NOT SET MANUALLY!)
		 */
		selectSingle: React.PropTypes.func,

		/**
		 * function called when user chooses record - in case table is used in codelist (DO NOT SET MANUALLY!)
		 */
		chooseFunc: React.PropTypes.func,

		/**
		 * function called when showing modal detail window (DO NOT SET MANUALLY!)
		 */
		detailFunc: React.PropTypes.func,

		/**
		 * unction called when removing record (DO NOT SET MANUALLY!)
		 */
		removeFunc: React.PropTypes.func,

		/**
		 * show confirmation dialog before remove, default is false (confirm dialog is shown)
		 */
		removeWithoutConfirmation: React.PropTypes.bool,

		/**
		 * disables actions on table (delete etc.)
		 */
		disabled: React.PropTypes.bool,

		/**
		* prefix for localization of columns (in case it is empty, table name is used)
		*/
		labelKeyPrefix: React.PropTypes.string,

		/**
		 * children
		 */
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.Datetime': 'multiple-optional',
			'SW.List.String': 'multiple-optional',
			'SW.List.Boolean': 'multiple-optional',
			'SW.List.Number': 'multiple-optional',
			'SW.List.Enum': 'multiple-optional',
			'SW.List.Selection': 'single-optional',
			'SW.List.Custom':  'multiple-optional',
			'SW.List.Attachment':  'multiple-optional',
		})
	},

	/**
	 * Available children to clone
	 */
	columnTypesToClone: ['SW.List.Datetime', 'SW.List.String', 'SW.List.Boolean', 'SW.List.Number', 'SW.List.Enum', 'SW.List.Selection', 'SW.List.Choose', 'SW.List.Custom', 'SW.List.Attachment'],

	/**
	 * returns cells for table header
	 */
	getHeads: function(chooseColumn, editColumn) {
		var headProps = {name: this.props.name, type: 'head', data: this.props.data, status: this.props.status, options: this.props.options,
			selectPage: this.props.selectPage, deselectPage: this.props.deselectPage, deselectAll: this.props.deselectAll, changeOrder: this.props.changeOrder, disabled: this.props.disabled,
			labelKeyPrefix: this.props.labelKeyPrefix};

		if (this.props.orderable === false) {
			headProps['orderable'] = false;
		}

		var heads = ComponentUtils.cloneChildren(this.props.children, this.columnTypesToClone, headProps);

		if (this.props.chooseFunc) {
			heads.unshift(ComponentUtils.cloneChildren(chooseColumn, this.columnTypesToClone, headProps));
		}
		if (this.props.detailFunc || (this.props.removeFunc && !this.props.disabled)) {
			heads.unshift(ComponentUtils.cloneChildren(editColumn, this.columnTypesToClone, headProps));
		}

		return heads;
	},

	/**
	 * returns array of table row(s) with cells for table footer
	 */
	getSummaries: function(chooseColumn, editColumn) {
		var rows = [];

		var maxFootIndex = 0;


		React.Children.forEach(this.props.children, function(child) {
			if (child && child.props.renderSummary && child.props.renderSummary.length > maxFootIndex) {
				maxFootIndex = child.props.renderSummary.length;
			}
		});

		for(var index = 0; index < maxFootIndex; index++) {
			var summariesProps = {name: this.props.name, type: 'foot', footIndex: index, selection: this.props.selection, status: this.props.status, disabled: this.props.disabled};

			var summaries = ComponentUtils.cloneChildren(this.props.children, this.columnTypesToClone, summariesProps);

			if (this.props.chooseFunc) {
				summaries.unshift(ComponentUtils.cloneChildren(chooseColumn, this.columnTypesToClone, summariesProps));
			}
			if (this.props.detailFunc || (this.props.removeFunc && !this.props.disabled)) {
				summaries.unshift(ComponentUtils.cloneChildren(editColumn, this.columnTypesToClone, summariesProps));
			}

			rows.push(
				<tr key={index}>
					{summaries}
				</tr>
			);
		}

		return rows;
	},

	/**
	 * returns array of table rows with cells for table body
	 */
	getRows: function(chooseColumn, editColumn, heads) {
		var rows = [];

		for(var index = 0; index < this.props.resources.length; index++) {
			var bodyProps = {name: this.props.name, type: 'body', resource: this.props.resources[index], selection: this.props.selection, status: this.props.status,
				selectSingle: this.props.selectSingle, chooseFunc: this.props.chooseFunc, detailFunc: this.props.detailFunc, removeFunc: this.props.disabled || !this.props.removable ? null : this.props.removeFunc,
				disabled: this.props.disabled, removeWithoutConfirmation: this.props.removeWithoutConfirmation, noData: this.props.noData};

			var row = ComponentUtils.cloneChildren(this.props.children, this.columnTypesToClone, bodyProps);
			if (this.props.chooseFunc) {
				row.unshift(ComponentUtils.cloneChildren(chooseColumn, this.columnTypesToClone, bodyProps));
			}
			if (this.props.detailFunc || (this.props.removeFunc && !this.props.disabled)) {
				row.unshift(ComponentUtils.cloneChildren(editColumn, this.columnTypesToClone, bodyProps));
			}
			rows.push(
				<tr key={index}>
					{row}
				</tr>
			);
		}

		if(rows.length === 0) {
			rows.push(
				<tr key='no-data-found'>
					<td colSpan={heads.length}>
						{this.props.status === 'initialized' ? i18n.t('table-initial') 
						: this.props.status === 'failure' ? i18n.t('table-loading-error') 
						: this.props.noData ? i18n.t(this.props.noData) : i18n.t('no-data-found')}
					</td>
				</tr>
			);
		}

		return rows;
	},

	showFooter: function() {
		return ComponentUtils.findChildren(this.props.children, this.columnTypesToClone).some(function(child) {
			return child.props.renderSummary && child.props.renderSummary.length > 0 ;
		});
	},

	render: function() {
		var chooseColumn = this.props.chooseFunc ? (
				<SW.List.Choose property='id' width='15%'/>
			) : null;

		var editColumn = null;

		var heads = this.getHeads(chooseColumn, editColumn);
		var summaries = this.getSummaries(chooseColumn, editColumn);
		var rows = this.getRows(chooseColumn, editColumn, heads);

		return (
			<div>
				{this.props.status == 'reloading' ? <div id='loading-disabled' onClick={false}><div id='loading-img'><Image src={contextName + '/images/reload.gif'} /> 
				<p> {i18n.t('table-loading')}</p></div></div> : null}
				<Table id={this.props.id} striped bordered condensed hover className={this.props.status === 'reloading' ? 'reloading' : null}>
					{this.props.resources.length > 0
						? <thead>
							<tr>
								{heads}
							</tr>
						</thead>
					: null
					}

					<tbody>
						{rows}
					</tbody>

					{this.showFooter()
						?   <tfoot>
								{summaries}
							</tfoot>
						:   null
					}
				</Table>
			</div>
		);
	}
});
