/**
 * Named filter component.
 */
SW.List.NamedFilter = React.createClass({

	displayName: 'SW.List.NamedFilter',

	propTypes: {
		/**
		 * component name
		 */
		name: React.PropTypes.string,
		/**
		 * icon
		 */
		icon: React.PropTypes.string,
		/**
		 * label string key
		 */
		labelKey: React.PropTypes.string.isRequired,
		/**
		 * status of component
		 */
		status: React.PropTypes.string
	},

	render: function() {
		return (
			<Button disabled={!this.props.status || this.props.status === 'reloading'} onClick={this.execute}>
				{this.props.icon
					?   <Glyphicon glyph={this.props.icon} />
					:   null
				}

				{(this.props.icon ? ' ' : '') + i18n.t(this.props.labelKey)}
			</Button>
		);
	}
});
