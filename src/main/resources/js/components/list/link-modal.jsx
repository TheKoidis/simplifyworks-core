/**
 * Link to specific page.
 */
SW.List.LinkModal = React.createClass({

	displayName: 'SW.List.LinkModal',

	contextTypes: {
		/**
		 * flux
		 */
		flux: React.PropTypes.object.isRequired
	},

	propTypes: {
		/**
		 * parent list name (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * type of component (button or menu item)
		 */
		type: React.PropTypes.oneOf(['button', 'menuItem']),

		/**
		 * icon
		 */
		icon: React.PropTypes.string,

		/**
		 * label string key
		 */
		labelKey: React.PropTypes.string.isRequired,
	},

	showModalDetail: function() {
		this.context.flux.actions.list.selection.showModalDetail(this.props.name, 'new');
	},

	render: function() {
		return (
			this.props.type === 'button'
				?   <Button onClick={this.showModalDetail}>
						{this.props.icon
							?   <Glyphicon glyph={this.props.icon} />
							:   null
						}

						{(this.props.icon ? ' ' : '') + i18n.t(this.props.labelKey)}
					</Button>
				:   <MenuItem onSelect={this.showModalDetail}>
						{this.props.icon
							?   <Glyphicon glyph={this.props.icon} />
							:   null
						}

						{(this.props.icon ? ' ' : '') + i18n.t(this.props.labelKey)}
					</MenuItem>
		);
	},
});
