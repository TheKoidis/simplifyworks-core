/**
 * Component for grouping filters.
 */
SW.List.FiltersGroup = React.createClass({

	displayName: 'SW.List.FiltersGroup',

	propTypes: {
		/**
		 * parent list name (DO NOT SET MANUALLY!)
		 */
		name: React.PropTypes.string,

		/**
		 * state of parent list (DO NOT SET MANUALLY!)
		 */
		status: React.PropTypes.string,

		/**
		 * children
		 */
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.NamedFilter': 'multiple-optional'
		})
	},

	render: function() {
		return (
			<ButtonGroup>
				{ComponentUtils.cloneChildren(this.props.children, 'SW.List.NamedFilter', {name: this.props.name, status: this.props.status})}
			</ButtonGroup>
		);
	}
});
