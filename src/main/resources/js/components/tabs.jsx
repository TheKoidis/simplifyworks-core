/**
*  Component for enclosing more list/workflow/cutomTabs components shown in tabs
*/
SW.Tabs = React.createClass({
	displayName: 'SW.Tabs',
	
	getInitialState: function() {
		return {
			activeTab: 0
		}
	},
	
	selectTab: function(tab) {
		if (tab === undefined || tab == this.state.activeTab) {
			return;
		}
		
		this.setState(
			{activeTab: tab}
		);
	},
	
	render: function() {
		return (
			<Grid fluid>
				<Row>
					<Col xs={12}>
						<Nav bsStyle='tabs' activeKey={this.state.activeTab} onSelect={this.selectTab} style={{marginBottom: 15}}>
							{ComponentUtils.findChildren(this.props.children, ['SW.List', 'SW.Workflow', 'SW.CustomTab']).map(function(element, index, array) {
								var tooltip = i18n.exists(element.props.labelKey+'-tooltip') ? i18n.t(element.props.labelKey+'-tooltip') : '';
								return (
									<NavItem key={'detail-tab-' + index} eventKey={index} title={tooltip}>
										{element.props.icon	?
											<span><Glyphicon glyph={element.props.icon} />{' '}</span>
											:   null
										}
										
										{element.props.label ? element.props.label : i18n.t(element.props.labelKey)}
									</NavItem>
								);
							}.bind(this))}
						</Nav>
					</Col>
				</Row>
				
				<Row>
					<Col xs={12}>
						{ComponentUtils.cloneChildren(this.props.children, ['SW.List', 'SW.Workflow', 'SW.CustomTab'],
							{})[this.state.activeTab]
						}
					</Col>
				</Row>
			</Grid>
		);
	}
});
