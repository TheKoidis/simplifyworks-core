/**
*  Component for enclosing custom components shown in detail tab
*/
SW.CustomTab = React.createClass({
	displayName: 'SW.CustomTab',
	
	propTypes: {
		/**
		*	Glyph for tab icon
		*/
		icon: React.PropTypes.string,
		
		/**
		*	Label key (for localization purposes, used in case Label is not set)
		*/
		labelKey: React.PropTypes.string,
		
		/**
		*	Label
		*/
		label: React.PropTypes.string,
	},
	
	render: function() {
		return this.props.children;
	}
});
