require('./detail.jsx');
require('./custom-tab.jsx');
require('./tabs.jsx');

require('./list.jsx');

require('./list/activities-group.jsx');
require('./list/activities-menu.jsx');
require('./list/link.jsx');
require('./list/link-modal.jsx');
require('./list/action.jsx');

require('./list/detail-modal.jsx');
require('./list/form-modal.jsx');

require('./list/filters-group.jsx');
require('./list/named-filter.jsx');
require('./list/filter.jsx');
require('./list/filter-button.jsx');

require('./list/quick-search.jsx');

require('./list/table.jsx');
require('./list/table/property.jsx');
require('./list/table/datetime.jsx');
require('./list/table/string.jsx');
require('./list/table/boolean.jsx');
require('./list/table/number.jsx');
require('./list/table/enum.jsx');
require('./list/table/selection.jsx');
require('./list/table/choose.jsx');
require('./list/table/custom.jsx');
require('./list/table/attachment.jsx');

require('./list/info.jsx');
require('./list/pagination.jsx');

require('./form.jsx');

require('./form/section.jsx');
require('./form/tag-list.jsx');

require('./form/property.jsx');
require('./form/abstract-embeddables.jsx');
require('./form/abstract-embeddables-list.jsx');
require('./form/boolean.jsx');
require('./form/datetime.jsx');
require('./form/number.jsx');
require('./form/formatted-string.jsx');

require('./form/enum.jsx');
require('./form/enums.jsx');

require('./form/string.jsx');
require('./form/strings.jsx');
require('./form/html.jsx');

require('./form/embeddable.jsx');
require('./form/embeddables.jsx');

require('./form/embeddable-form.jsx');
require('./form/embeddables-list.jsx');
require('./form/embeddables-tag-list.jsx');
require('./form/embeddables-list-create-button.jsx');

require('./form/entity.jsx');
require('./form/entity-select.jsx');
require('./form/entity-list.jsx');
require('./form/entities.jsx');
require('./form/entities-list.jsx');
require('./form/entities-tag-list.jsx');

require('./form/abstract-attachment.jsx');
require('./form/attachments.jsx');
require('./form/attachments-list.jsx');
require('./form/attachments-simple.jsx');

// TODO add other properties - File, HTML, RTF, Picture, URL, Email, etc.

require('./form/space.jsx');
require('./form/custom.jsx');

require('./workflow.jsx');
require('./workflow-simple.jsx');
require('./workflow-validation.jsx');

require('./report.jsx');
require('./validate.jsx');
