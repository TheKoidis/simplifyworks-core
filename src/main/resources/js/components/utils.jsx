var Utils = {
	
	changeOrder: function(ordering, property, order, multi) {
		var newOrdering;
		if (multi == true) {
			newOrdering = ordering.filter(function(element, index, array) {
				return element.property !== property;
			});
		} else {
			newOrdering = [];
		}
		if (order) {
			newOrdering.push({
				property: property,
				order: order
			});
		}
		return newOrdering;
	},
	
	sort: function(values, ordering) {
		if (ordering && ordering.length > 0) {
			values.sort(function(a, b) {
				var result;
				for (var i=0 ; i<ordering.length ; i++) {
					var valueA = PropertyUtils.getPropertyValue(a, ordering[i].property);
					var valueB = PropertyUtils.getPropertyValue(b, ordering[i].property);
					if (typeof valueA === "string") {
						result = valueA.localeCompare(valueB);
					} else {
						result = (valueA < valueB ? 1 : (valueA > valueB ? -1 : 0));
					}
					if (result != 0) {
						return (ordering[i].order == 'asc' ? -1 : 1) * result;
					}
				}
				
				return result;
			});
		}
	}
}

module.exports = Utils;
