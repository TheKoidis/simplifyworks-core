SW.List = React.createClass({
	
	displayName: 'SW.List',
	
	mixins: [FluxMixin, StoreWatchMixin('list')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		/**
		* unique name of list
		*/
		name: React.PropTypes.string.isRequired,
		
		/**
		* rest url to get data
		*/
		rest: React.PropTypes.string,
		
		/**
		* rest prefix
		*/
		restPrefix: React.PropTypes.string,
		
		/**
		* default ordering of table
		*/
		defaultOrdering: React.PropTypes.array,
		
		/**
		* default filter object
		*/
		defaultFilter: React.PropTypes.object,
		
		/**
		* Show delete button
		*/
		removable: React.PropTypes.bool,
		
		/**
		* children
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.ActivitiesGroup': 'multiple-optional',
			'SW.List.FiltersGroup': 'multiple-optional',
			'SW.List.Filter': 'single-optional',
			'SW.List.QuickSearch': 'single-optional',
			'SW.List.Table': 'single-required',
			'SW.List.Info': 'single-optional',
			'SW.List.Pagination': 'single-optional',
			'SW.List.DetailModal': 'single-optional'
		})
	},
	
	componentDidMount: function() {
		this.load();
	},
	
	componentDidUpdate: function(prevProps, prevState) {
		if (prevProps.rest != this.props.rest || this.props.restPrefix !== prevProps.restPrefix || prevProps.name != this.props.name) {
			this.load();
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if (nextProps.rest != this.props.rest || this.props.restPrefix !== nextProps.restPrefix || nextProps.name != this.props.name) {
			this.setState(this.getStateForProps(nextProps));
		}
	},
	
	getStateFromFlux: function() {
		return this.getStateForProps(this.props);
	},
	
	getDefaultProps: function(){
		return {
			rest: '',
			removable: true
		};
	},
	
	getStateForProps: function(props) {
		var list = this.context.flux.store('list').getList(props.name);
		
		return {
			status: list.status,
			options: list.options,
			data: list.data,
			selectionCount: list.selection.length,
			pageNumber: list.options.pageNumber,
			pageSize: list.options.pageSize,
			recordsReturned: list.data.recordsReturned,
			recordsFiltered: list.data.recordsFiltered,
			recordsTotal: list.data.recordsTotal,
			resources: (list.data && list.data.resources) ? list.data.resources : [],
			selection: list.selection,
			showModalDetailValue: list.showModalDetailValue
		};
	},
	
	load: function() {
		var options = this.context.flux.store('list').getList(this.props.name).options;
		var defaultOptions = {
			rest: this.props.rest == '' ? this.props.restPrefix : this.props.restPrefix + '/' + this.props.rest,
			
			ordering: this.props.defaultOrdering ? this.props.defaultOrdering : [],
			pageNumber: 1,
			pageSize: ComponentUtils.findChild(this.props.children, 'SW.List.Pagination') ? 10 : 2147483647,
			
			quickSearch: {},
			filters: {
				column: {
					visible: false,
					
					types: [],
					values: []
				}
			},
			showFilter: false,
			filter: this.props.defaultFilter ? this.props.defaultFilter : {}
		};
		this.context.flux.actions.list.common.reload(this.props.name, options.rest != defaultOptions.rest ? defaultOptions : options);
	},
	
	changePageNumber: function(event, selectedEvent) {
		this.context.flux.actions.list.pagination.changePageNumber(this.props.name, selectedEvent.eventKey);
	},
	
	changePageSize: function(pageSize) {
		this.context.flux.actions.list.pagination.changePageSize(this.props.name, pageSize);
	},
	
	search: function(tokens) {
		this.context.flux.actions.list.quickSearch.search(this.props.name, tokens);
	},
	
	selectPage: function(property) {
		this.context.flux.actions.list.selection.selectPage(this.props.name, property);
	},
	
	deselectPage: function(property) {
		this.context.flux.actions.list.selection.deselectPage(this.props.name, property);
	},
	
	deselectAll: function() {
		this.context.flux.actions.list.selection.deselectAll(this.props.name);
	},
	
	changeOrder: function(property, order, shiftMulti) {
		this.context.flux.actions.list.ordering.changeOrder(this.props.name, property, order === 'asc' ? 'desc' : (order === 'desc' ? null : 'asc'), shiftMulti);
	},
	
	selectSingle: function(checked, property, resource) {
		if(checked) {
			this.context.flux.actions.list.selection.selectSingle(this.props.name, property, resource);
		} else {
			this.context.flux.actions.list.selection.deselectSingle(this.props.name, property, resource);
		}
	},
	
	toggleFilter: function() {
		this.context.flux.actions.list.filter.toggleFilter(this.props.name);
	},
	
	detailFunc: function(resource, pathPropertyValue) {
		this.context.flux.actions.list.selection.showModalDetail(this.props.name, pathPropertyValue);
		if (!pathPropertyValue) {
			var options = this.context.flux.store('list').getList(this.props.name).options;
			this.context.flux.actions.list.common.reload(this.props.name, options);
		}
	},
	
	removeFunc: function(resource, pathPropertyValue) {
		this.context.flux.actions.list.common.delete(this.props.name, this.props.restPrefix + '/' + this.props.rest + '/' + pathPropertyValue);
	},
	
	render: function() {
		var quickSearch = ComponentUtils.findChild(this.props.children, 'SW.List.QuickSearch');
		return (
			<div>
				<Grid fluid>
					<Row>
						<Col
							xs={Math.max(quickSearch?12-quickSearch.props.xs:0, 8)}
							sm={Math.max(quickSearch?12-quickSearch.props.sm:0, 8)}
							md={Math.max(quickSearch?12-quickSearch.props.md:0, 8)}
							lg={Math.max(quickSearch?12-quickSearch.props.lg:0, 8)}>
							<ButtonToolbar style={{marginBottom: 15}}>
								{ComponentUtils.cloneChildren(this.props.children, 'SW.List.ActivitiesGroup', {name: this.props.name, status: this.state.status, selection: this.state.selection})}
								{ComponentUtils.cloneChildren(this.props.children, 'SW.List.FiltersGroup', {name: this.props.name, status: this.state.status})}
								{ComponentUtils.childExists(this.props.children, 'SW.List.Filter') ? <SW.List.FilterButton execute={this.toggleFilter}/> : null}
							</ButtonToolbar>
						</Col>
						<Col xs={quickSearch?quickSearch.props.xs:0} sm={quickSearch?quickSearch.props.sm:0} md={quickSearch?quickSearch.props.md:0} lg={quickSearch?quickSearch.props.lg:0}>
							{ComponentUtils.cloneChild(this.props.children, 'SW.List.QuickSearch', {name: this.props.name, search: this.search, initialValue: this.state.options.quickSearch.tokens ? this.state.options.quickSearch.tokens[0] : null})}
						</Col>
					</Row>
					
					<Row>
						<Col md={12}>
							{this.state.options.showFilter ? ComponentUtils.cloneChild(this.props.children, 'SW.List.Filter', {name: this.props.name}) : null}
						</Col>
					</Row>
					
					<Row>
						<Col xs={12}>
							{ComponentUtils.cloneChild(this.props.children, 'SW.List.Table',
								{
									name: this.props.name, status: this.state.status, resources: this.state.resources, selection: this.state.selection,
									options: this.state.options, selectPage: this.selectPage, deselectPage: this.deselectPage, deselectAll: this.deselectAll, changeOrder: this.changeOrder,
									selectSingle: this.selectSingle, data: this.state.data, detailFunc: this.detailFunc, removeFunc: this.removeFunc, removable: this.props.removable})
								}
							</Col>
						</Row>
						
						<Row>
							<Col xs={12} md={4}>
								{ComponentUtils.cloneChild(this.props.children, 'SW.List.Info', {name: this.props.name, status: this.state.status, selectionCount: this.state.selectionCount, pageNumber: this.state.pageNumber, pageSize: this.state.pageSize, recordsReturned: this.state.recordsReturned, recordsFiltered: this.state.recordsFiltered, recordsTotal: this.state.recordsTotal})}
							</Col>
							
							<Col xs={12} md={8} className='text-right'>
								{ComponentUtils.cloneChild(this.props.children, 'SW.List.Pagination', {name: this.props.name, changePageNumber: this.changePageNumber, changePageSize: this.changePageSize, status: this.state.status, options: this.state.options, data: this.state.data})}
							</Col>
						</Row>
					</Grid>
					
					{this.state.showModalDetailValue ? ComponentUtils.cloneChild(this.props.children, 'SW.List.DetailModal', {showModalDetail: this.detailFunc, showModalDetailValue: this.state.showModalDetailValue}) : null}
				</div>
			);
		}
	});
