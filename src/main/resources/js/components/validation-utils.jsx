var ValidationUtils = {
	// returns prop name 'validation-required' when value empty
	validateRequired: function(value, args) {
		if(args) {
			console.error('Required validator must have no arguments!');
		}
		
		if(value === undefined || value === null || value === '' || (value instanceof Array && value.length === 0)) {
			return {
				name: 'validation-required'
			};
		} else {
			return null;
		}
	},
	
	// returns props name and args for length validation
	validateLength: function(value, args) {
		if(!args || args.length !== 1) {
			console.error('Length validator must have exactly one argument!');
		}
		
		if(value !== undefined && value !== null && (value.length !== 0 && (value.length > args[0]))) {
			return {
				name: 'validation-length',
				args: {length: args[0]}
			};
		} else {
			return null;
		}
	},
	
	//returns props name and args for min and max length validation
	validateLengthRange: function(value, args) {
		if(!args || args.length !== 2) {
			console.error('Length range must have 2 arguments');
		}
		
		if(value !== undefined && value !== null && (value.length !== 0 && (value.length < args[0] || value.length > args[1]))) {
			return {
				name: 'validation-length-range',
				args: {min: args[0], max: args[1]}
			};
		} else {
			return null;
		}
	},
	
	// returns props name and arg for count validation
	validateCount: function(value, args) {
		if(!args || args.length !== 1) {
			console.error('Count validator must have exactly one argument!');
		}
		
		if(value !== undefined && value !== null && (value.length !== 0 && (value.length > args[0]))) {
			return {
				name: 'validation-count',
				args: {count: args[0]}
			};
		} else {
			return null;
		}
	},
	
	// returns props name and arg for min and max count validation
	validateCountRange: function(value, args) {
		if(!args || args.length !== 2) {
			console.error('Count range must have 2 arguments');
		}
		
		if(value !== undefined && value !== null && (value.length !== 0 && (value.length < args[0] || value.length > args[1]))) {
			return {
				name: 'validation-count-range',
				args: {min: args[0], max: args[1]}
			};
		} else {
			return null;
		}
	},
	
	// returns prop name for lowercase validation
	validateLowercase: function(value, args) {
		if(args) {
			console.error('Lowercase validator must have no arguments!');
		}
		
		if(value !== undefined && value !== null && value !== ''  && (value.toLowerCase() !== value)) {
			return {
				name: 'validation-lowercase'
			};
		} else {
			return null;
		}
	},
	
	//returns prop name for email validation
	validateEmail: function(value, args) {
		if(args) {
			console.error('Email validator must have no arguments!');
		}
		
		if(value !== undefined && value !== null && value !== '' && (value.indexOf('@') === -1)) {
			return {
				name: 'validation-email'
			};
		} else {
			return null;
		}
	},
	
	// returns props name and args for date format validation
	validateDateString: function(value, args) {
		if(!args || args.length !== 1) {
			console.error('Date validator must have exactly one argument!');
		}
		var key = '';
		switch (args[0]) {
			case 'MM/YYYY':
			key = '^(0[1-9]|1[012])\/\\d\\d\\d\\d$';
			break;
		}
		if(value !== undefined && value !== null && value !== '' && !value.match(key)) {
			return {
				name: 'validation-date-string',
				args: {format: args[0]}
			};
		} else {
			return null;
		}
	},
	
	// returns props name and args for min date validation
	validateMinDate: function(value, args) {
		if(!args || (args.length !== 1 && args.length !== 2)) {
			console.error('min date validator must have one or two arguments!');
		}
		if(value !== undefined && value !== null && value !== '' && value.replace('.000', '') < args[0]) {
			var date = new Date(args[0]);
			var message = args[1] ? args[1] : 'validation-min-date';
			return {
				name: message,
				args: {minDate: date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()}
			};
		} else {
			return null;
		}
	},
	
	// returns props name and args for max date validation
	validateMaxDate: function(value, args) {
		if(!args || (args.length !== 1 && args.length !== 2)) {
			console.error('max date validator must have one or two arguments!');
		}
		if(value !== undefined && value !== null && value !== '' && value.replace('.000', '') > args[0]) {
			var date = new Date(args[0]);
			var message = args[1] ? args[1] : 'validation-max-date';
			return {
				name: message,
				args: {maxDate: date.getDate() + '.' + (date.getMonth() + 1) + '.' + date.getFullYear()}
			};
		} else {
			return null;
		}
	},
	
	// returns prop name for number validation
	validateInteger: function(value, args) {
		if(args) {
			console.error('Integer validator must have no arguments!');
		}
		
		if(value !== undefined && value !== null && value !== '' && (NumberUtils.toFixedString(value)).indexOf('.') !== -1) {
			return {
				name: 'validation-integer'
			};
		} else {
			return null;
		}
	},
	
	// returns props name and args for max scale validation
	validateMaxScale: function(value, args) {
		if(!args || args.length !== 1) {
			console.error('Max scale validator must have exactly one argument!');
		}
		
		var number = (NumberUtils.toFixedString(value));
		
		if(value !== undefined && value !== null && value !== '') {
			if(number.indexOf('.') !== -1 && (number.length - number.indexOf('.') - 1) > args[0]) {
				return {
					name: 'validation-max-scale',
					args: {maxScale: args[0]}
				};
			}
		}
		
		return null;
	},
	
	// returns props name and args for min validation
	validateMin: function(value, args) {
		if(!args || args.length !== 1) {
			console.error('Min validator must have exactly one argument!');
		}
		
		if(value !== undefined && value !== null && value !== '' && value < args[0]*1) {
			return {
				name: 'validation-min',
				args: {min: args[0]}
			};
		} else {
			return null;
		}
	},
	
	// returns props name and args for max validation
	validateMax: function(value, args) {
		if(!args || args.length !== 1) {
			console.error('max validator must have exactly one argument!');
		}
		
		if(value !== undefined && value !== null && value !== '' && value > args[0]*1) {
			return {
				name: 'validation-max',
				args: {max: args[0]}
			};
		} else {
			return null;
		}
	},
	
	// returns props name and args for validating unique field
	validateUnique: function(value, args) {
		if (!args || args.length !== 1) {
			console.error('Unique validator must have exactly one argument!');
		}
		if (value === undefined || value === null) return null;
		
		var keys = [];
		var propertyParts = args[0].split('.');
		for (var i = 0; i < value.length; i++) {
			var record = value[i];
			for (var y = 0; y < propertyParts.length; y++) {
				record = record[propertyParts[y]];
			}
			keys.push(record);
		}
		
		var uniq = keys.map(function(name) {
			return {count: 1, name: name}
		}).reduce(function(a, b) {
			a[b.name] = (a[b.name] || 0) + b.count
			return a
		}, {});
		
		var duplicates = Object.keys(uniq).filter(function(a) {return uniq[a] > 1});
		
		if (duplicates.length > 0) {
			var report = '';
			for (var i = 0; i < duplicates.length; i++) {
				report = report + (i === 0 ? '' : ', ') + duplicates[i];
			}
			return {
				name: 'validation-unique',
				args: {report: report}
			};
		} else {
			return null;
		}
	},
	
	// validates using validations; when validations fails, returns fields that failed validation
	validate: function(property, value, validations) {
		if(validations) {
			var validationFails = [];
			
			validations.split(';').forEach(function(validation) {
				var regexResult = /^([^(]+)(?:(?:[(])([^)]+)(?:[)]))?$/.exec(validation);
				
				if(regexResult) {
					var validatorName = regexResult[1];
					var validatorArgs = regexResult[2] && regexResult[2].length > 0 ? regexResult[2].split(',') : null;
					
					var validationFail = ValidationUtils['validate' + validatorName[0].toUpperCase() + validatorName.slice(1)](value, validatorArgs);
					
					if(validationFail) {
						validationFails.push(validationFail);
					}
				} else {
					console.error('Property \"' + property + '\" has illegal validations: \"' + validation + '\".');
				}
			});
			
			if(validationFails && validationFails.length > 0) {
				return validationFails;
			}
		}
		
		return null;
	},
	
	// returns validations using props
	getValidationFromProps: function(props, keyPrefix) {
		var validation = {};
		React.Children.forEach(props.children, function(child) {
			React.Children.forEach(child.props.children, function(propertyComponent) {
				if (propertyComponent && propertyComponent.props.property) {
					var newProps = {};
					for (var key in propertyComponent.props) {
						newProps[key] = propertyComponent.props[key];
					}
					newProps.keyPrefix = keyPrefix;
					var labelText = LocalizationUtils.getLabelText(newProps);
					
					if (typeof labelText !== 'string') {
						newProps.label = null;
						labelText = LocalizationUtils.getLabelText(newProps);
					}
					validation[propertyComponent.props.property] = {hidden: propertyComponent.props.hidden, validations: propertyComponent.props.validations, result: '', label: labelText};
				}
			});
		});
		
		return validation;
	},
	
	// shows notification message showing fields that failed validation
	showValidationResults: function(validationFailed) {
		var validation = [];
		
		for (var formName in validationFailed) {
			var validationResults = validationFailed[formName];
			
			var formValidation = [];
			
			for (var prop in validationResults.validation) {
				var validationProp = validationResults.validation[prop].validationResult.map(
					function(element, index, array) {
						return (<li key={element.name}>{i18n.t(element.name, element.args)}</li>);
					}
				)
				formValidation.push(<li key={prop}>{validationResults.validation[prop].label} <ul>{validationProp}</ul> </li>);
			}
			
			if (validationResults.tabText) {
				validation.push(<li key={formName}>{validationResults.tabText} <ul>{formValidation}</ul> </li>);
			} else {
				validation = formValidation;
			}
		}
		
		notification.showMessage('warning', i18n.t('validation-error'), <ul>{validation}</ul> );
	}
}

module.exports = ValidationUtils;
