var api = require('./../rest/report.jsx');

SW.Validate = React.createClass({
	
	displayName: 'SW.Validate',
	
	propTypes: {
		/**
		* DO NOT SET MANUALLY
		*/
		action: React.PropTypes.func,
		disabled: React.PropTypes.bool
	},
	
	render: function() {
		return (
			<Button bsStyle='primary' onClick={this.props.action} style={{marginLeft: '5px'}} disabled={this.props.disabled}>
				<Glyphicon glyph='ok' />
				{' ' + i18n.t('validate')}
			</Button>
		);
	}
});
