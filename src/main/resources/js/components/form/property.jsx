/**
* Property component of form. It contains all common functions used in property components.
*/
SW.Form.Property = {
	
	propTypes: {
		/**
		* Value provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		value: React.PropTypes.any,
		
		/**
		* Listener for value changes provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		valueChanged: React.PropTypes.func,
		
		/**
		* Path to property of record (supports dot convention)
		*/
		property: React.PropTypes.string.isRequired,
		
		/**
		* Size of component (used by automatically created grid)
		*/
		size: React.PropTypes.number.isRequired,
		
		/**
		* Disabled flag
		*/
		disabled: React.PropTypes.bool,
		
		/**
		* Label key
		*
		* Label key prop has higher priority for usage than key prefix prop concatenated with property prop
		*/
		labelKey: React.PropTypes.string,
		
		/**
		* Label (can be string or React element - for more illustrative labels; for example with icons)
		*
		* Label prop has the highest priority for usage
		*/
		label: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
		
		/**
		* Validations string (format [validationType]?(firstArgument,secondArgument);[anotherValidationType] - e.g. 'lengthRange(0,255);required')
		*/
		validations: React.PropTypes.string,
		
		/**
		* Class name of component
		*/
		className: React.PropTypes.string,
		
		/**
		* Hides component
		*/
		hidden: React.PropTypes.bool,
		
		/**
		* Key prefix provided automatically by parent (DO NOT SET MANUALLY!)
		*
		* Key prefix prop has the lowest priority for usage
		*/
		keyPrefix: React.PropTypes.string,
		
		/**
		* Tooltip key
		*
		* Tooltip key prop has higher priority for usage than key prefix prop concatenated with property prop and 'tooltip' suffix
		*/
		tooltipKey: React.PropTypes.string,
		
		/**
		* Tooltip
		*
		* Tooltip has the highest priority for usage
		*/
		tooltip: React.PropTypes.string,
		
		/**
		* Help key
		*
		* Help key prop has higher priority for usage than key prefix prop concatenated with property prop and 'help' suffix
		*/
		helpKey: React.PropTypes.string,
		
		/**
		* Help (can be string or React element - for more illustrative helps; for example with images and paragraphs)
		*
		* Help has the highest priority for usage
		*/
		help: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
	},
	
	/**
	* Returns label for component using label text and help is defined.
	*/
	getLabel: function() {
		var labelText = LocalizationUtils.getLabelText(this.props);
		var help = LocalizationUtils.getHelp(this.props);
		
		return (
			<span>
				{labelText}
				{help
					?
					<span style={{marginLeft: 10}}>
						<OverlayTrigger trigger='click' rootClose placement='right' overlay={<Popover id={this.props.property + '-popover'} title={i18n.t('help')}>{help}</Popover>}>
							<Button bsSize='xs' bsStyle='link' style={{cursor: 'help'}} >
								<Glyphicon glyph='question-sign' />
							</Button>
						</OverlayTrigger>
					</span>
					:	null
				}
			</span>
		);
	},
	
	getStyle: function() {
		return this.props.validationResult ? 'warning' : (this.state && this.state.convertFail ? 'error' : null);
	},
	
	getInfo: function() {
		if (this.state && this.state.convertFail) {
			return (
				<Label bsStyle='danger'>
					{i18n.t(this.state.convertFail)}
				</Label>
			);
		}
		
		if (this.props.validationResult) {
			return (
				<Label bsStyle='warning'>
					{this.props.validationResult.map(function(element, index, array) {
						return i18n.t(element.name, element.args);
					}).join(' ')}
				</Label>
			);
		}
	},
	
	valueChanged: function(newValue, callback) {
		this.props.valueChanged(this.props.property, newValue, this.props.validations);
		
		if(callback) {
			callback();
		}
	}
};
