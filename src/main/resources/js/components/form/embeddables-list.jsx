/**
* Class represents output list of embeddables (table).
*/
SW.Form.EmbeddablesList = React.createClass({
	
	displayName: 'SW.Form.EmbeddablesList',
	
	mixins: [SW.Form.AbstractEmbeddablesList],
	
	propTypes: {
		/**
		* Value provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		value: React.PropTypes.arrayOf(React.PropTypes.any),
		
		/**
		* Disabled flag
		* TODO disabled flag per operation (creating/editing/removing)
		*/
		disabled: React.PropTypes.bool,
		
		/**
		* Edit value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		editValue: React.PropTypes.func,
		
		/**
		* Create value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		createValue: React.PropTypes.func,
		
		/**
		* Remove value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		removeValue: React.PropTypes.func,
		
		/**
		* default ordering of table
		*/
		defaultOrdering: React.PropTypes.array,
		
		/**
		* There have to be exactly one table as child, other components are optional. Table is used for showing the values.
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.EmbeddablesListCreateButton': 'multiple-optional',
			'SW.List.Table': 'single-required',
			'SW.List.Info': 'single-optional',
			'SW.List.Pagination': 'single-optional'
		})
	},
	
	renderButtons: function() {
		return (!this.props.disabled && this.props.addable ?
			<Row>
				<Col xs={12}>
					<ButtonToolbar style={{marginBottom: 15}}>
						{ComponentUtils.cloneChildren(this.props.children, 'SW.Form.EmbeddablesListCreateButton',
							{
								createValue: this.createValue
							})
						}
						</ButtonToolbar>
					</Col>
				</Row> : null
			);
		},
		
		render: function() {
			return this.abstractRender();
		}
	});
