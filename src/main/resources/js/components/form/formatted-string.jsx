/**
* Class represents input/output with custom converter
*/
SW.Form.FormattedString = React.createClass({
	
	displayName: 'SW.Form.FormattedString',
	
	/**
	* SW.Form.Property
	*/
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Key down handling (for example for submitting form by enter)
		*/
		onKeyDown: React.PropTypes.func,
		
		/**
		* Custom function that parses input string into field value (similarly as in number component)
		*/
		parse: React.PropTypes.func,
		
		/**
		* Custom function that format field value into string shown in input box (similarly as in number component)
		*/
		format: React.PropTypes.func
	}),
	
	format: function(value) {
		return value!==null ? this.props.format(value) : null;
	},
	
	getInitialState: function() {
		return {
			value: this.format(this.props.value),
			convertFail: null,
			validationResult: null
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if ((!this.props.parse || this.props.parse(this.state.value) != nextProps.value) && !(this.props.value == nextProps.value && this.state.convertFail)) {
			this.setState({
				value: this.format(nextProps.value),
				convertFail: null
			});
		}
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		var value = this.state.value;
		return (
			<div className={this.props.className}>
				<Input
					type='text'
					value={value}
					disabled={this.props.disabled}
					placeholder={LocalizationUtils.getPlaceholder(this.props)}
					label={this.getLabel()}
					title={LocalizationUtils.getTooltip(this.props)}
					bsStyle={this.getStyle()}
					help={this.getInfo()}
					onChange={this.onChange}
					onBlur={this.onBlur}
					onKeyDown={this.props.onKeyDown ? this.props.onKeyDown : null}/>
			</div>
		);
	},
	
	onBlur: function() {
		if (!this.state.convertFail) {
			this.setState({
				value:  this.format(this.props.value),
			});
		}
	},
	
	onChange: function(event) {
		var emptyValue = !event.target.value;
		var value = this.props.parse(event.target.value);
		if (!emptyValue && value===null) {
			this.setState({
				value: event.target.value,
				convertFail: 'number-conversion-failed'
			});
		} else {
			this.setState({
				value: event.target.value,
				convertFail: null
			}, function() {
				this.valueChanged(value);
			});
		}
	}
});
