/**
* Class represents input/output entities property component of record.
*/
SW.Form.EntitySelect = React.createClass({
	
	displayName: 'SW.Form.EntitySelect',
	
	/**
	* SW.Form.Property, FluxMixin, StoreWatchMixin('list')
	*/
	mixins: [SW.Form.Property, FluxMixin, StoreWatchMixin('list')],
	
	contextTypes: {
		/**
		* flux
		*/
		flux: React.PropTypes.object.isRequired
	},
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* custom render function
		*/
		render: React.PropTypes.func.isRequired,
		
		/**
		* unique property
		*/
		uniqueProperty: React.PropTypes.string,
		
		/**
		* rest for getting possible values
		*/
		rest: React.PropTypes.string.isRequired,
		
		/**
		* Show null value flag (n-valued enum or (n+1)-valued enum)
		*/
		showNull: React.PropTypes.bool,
		
		/**
		* default ordering of table
		*/
		defaultOrdering: React.PropTypes.array
	}),
	
	getDefaultProps: function() {
		return {
			uniqueProperty: 'id',
			showNull: true
		}
	},
	
	getInitialState: function() {
		return {
			forceReload: true
		}
	},
	
	componentDidMount: function() {
		this.load();
	},
	
	componentDidUpdate: function(prevProps, prevState) {
		if(!this.props.showNull && !this.props.value && this.state.resources && this.state.resources.length > 0) {
			this.valueChanged(this.state.resources[0]);
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if (this.props.property !== nextProps.property || this.props.disabled !== nextProps.disabled) {
			this.setState({
				forceReload: true
			});
		}
	},
	
	getStateFromFlux: function() {
		var list = this.context.flux.store('list').getList(this.props.name);
		
		return {
			resources: (list.data && list.data.resources) ? list.data.resources : []
		};
	},
	
	load: function() {
		if (!this.state.forceReload && this.state.resources && this.state.resources.length > 0) {
			return;
		}
		this.context.flux.actions.list.common.reload(this.props.name, {
			rest: this.props.rest,
			
			ordering: this.props.defaultOrdering ? this.props.defaultOrdering : [],
			pageNumber: 1,
			pageSize: 50,
			
			quickSearch: {},
			filters: {
				column: {
					visible: false,
					
					types: [],
					values: []
				}
			}
		});
		this.setState({
			forceReload: false
		});
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		var resources = this.state.resources;
		if ((!resources || resources.length == 0) && this.props.value) {
			resources = [this.props.value];
		}
		var buttons = [];
		buttons.push(
			<div className='on-top' key='button'>
				<DropdownButton pullRight={true} id='multi-input' title={''} onSelect={this.onInputChange} disabled={!resources || resources.length == 0}>
					{this.props.showNull
						?
						<MenuItem key='null' eventKey={null}>
							{i18n.t('null')}
						</MenuItem>
						:
						null
					}
					{resources.map(function(resource, index) {
						return (
							<MenuItem key={index} eventKey={resource[this.props.uniqueProperty]}>
								{this.props.render(resource)}
							</MenuItem>
						);
					}.bind(this))}
				</DropdownButton>
			</div>
		);
		
		return (
			<div className={this.props.className}>
				<Input type='text' disabled={true} label={this.getLabel()} value={this.props.value ? this.props.render(this.props.value) : i18n.t(this.props.showNull ? 'null' : 'wait-please')} bsStyle={this.getStyle()} help={this.getInfo()} buttonAfter={!this.props.disabled ? buttons : null} />
			</div>
			
		);
	},
	
	onInputChange: function(event, value) {
		var selectedValue = value;
		
		if (selectedValue) {
			this.state.resources.map(function(resource, index) {
				if (resource[this.props.uniqueProperty] == selectedValue) {
					selectedValue = resource;
				}
			}.bind(this));
		}
		
		this.valueChanged(selectedValue);
	}
});
