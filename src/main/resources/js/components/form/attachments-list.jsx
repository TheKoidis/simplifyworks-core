/**
* Class represents output list of embeddables (table).
*/
SW.Form.AttachmentsList = React.createClass({
	
	displayName: 'SW.Form.AttachmentsList',
	
	mixins: [SW.Form.AbstractEmbeddablesList, SW.Form.AbstractAttachment],
	
	propTypes: {
		/**
		* Value provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		value: React.PropTypes.arrayOf(React.PropTypes.any),
		
		/**
		* Disabled flag
		* TODO disabled flag per operation (creating/editing/removing)
		*/
		disabled: React.PropTypes.bool,
		
		/**
		* Edit value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		editValue: React.PropTypes.func,
		
		/**
		* Create value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		createValue: React.PropTypes.func,
		
		/**
		* Remove value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		removeValue: React.PropTypes.func,
		
		/**
		* default ordering of table
		*/
		defaultOrdering: React.PropTypes.array,
		
		/**
		* Allows upload more than one file (DO NOT SET MANUALLY!)
		*/
		multiple: React.PropTypes.bool,
		
		/**
		* Css style of dropzone component (DO NOT SET MANUALLY!)
		*/
		style: React.PropTypes.any,
		
		/**
		* Allows change default scc class of dropzone component (DO NOT SET MANUALLY!)
		*/
		className: React.PropTypes.string,
		
		/**
		* Localization key for text in dropzone (DO NOT SET MANUALLY!)
		*/
		titleKey: React.PropTypes.string,
		
		/**
		* Accept attribute of html file input (see http://www.w3schools.com/tags/att_input_accept.asp) (DO NOT SET MANUALLY!)
		*/
		accept: React.PropTypes.string,
		
		/**
		* Custom rest url for download and upload (default is '/api/core/attachments') (DO NOT SET MANUALLY!)
		*/
		//TODO propagate to attachment column
		rest: React.PropTypes.string,
		
		/**
		* There have to be exactly one table as child, other components are optional. Table is used for showing the values.
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.Table': 'single-required',
			'SW.List.Info': 'single-optional',
			'SW.List.Pagination': 'single-optional'
		}),
		
		onUploadStarted: React.PropTypes.func,
		onUploadFinished: React.PropTypes.func
	},
	
	getInitialState: function () {
		return {
			isDragActive: false,
			uploading: [],
			uploaded: []
		};
	},
	
	componentWillReceiveProps: function(nextProps) {
		if(this.props.value !== nextProps.value) {
			this.setState({
				uploading: [],
				uploaded: []
			});
		}
	},
	
	attachmentsValueChanged: function(value, newAttachments) {
		this.props.valueChanged(value);
		if (newAttachments.length==1) {
			var index = value.indexOf(newAttachments[0]);
			this.props.editValue(newAttachments[0], index);
		}
	},
	
	renderButtons: function() {
		var rows = [];
		
		if (this.state.uploaded) {
			this.state.uploaded.map(function(file, index, array) {
				rows.push(
					<Row key={'uploaded'+index}>
						<Col md={6} style={{paddingLeft: 0}}><Glyphicon glyph='download-alt' /> {file.name}</Col>
						<Col md={6} style={{'textAlign': 'center'}}>{i18n.t('form-attachment-uploaded')}</Col>
					</Row>
				);
			})
		}
		
		if (this.state.uploading) {
			this.state.uploading.map(function(file, index, array) {
				rows.push(
					<Row key={'uploading'+index}>
						<Col md={6} style={{paddingLeft: 0}}><Glyphicon glyph='upload' /> {file.name}</Col>
						<Col md={6} style={{'textAlign': 'right'}}><ProgressBar active now={file.percent} style={{margin: 1}}/></Col>
					</Row>
				);
			})
		}
		
		return (
			<div>
				{this.dropzoneDiv()}
				
				<Grid fluid>
					{rows}
				</Grid>
			</div>
		);
	},
	
	render: function() {
		return this.abstractRender();
	}
});
