/**
* Class represents embeddable property component. Embeddable property represents non-primitive (objectual) value of parent.
* Embeddables can be nested (for example user contains person, person contains contact, contact contains address).
*/
SW.Form.Embeddable = React.createClass({
	
	displayName: 'SW.Form.Embeddable',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Initial value for creation (properties can be predefined)
		*/
		initialValue: React.PropTypes.object,
		
		/**
		* Function responsible for rendering current value (format render(value))
		*/
		render: React.PropTypes.func.isRequired,
		
		/**
		* There have to be exactly one embeddable form as child. This form is used for creation/editing/detail.
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.EmbeddableForm': 'single-required'
		}),
	}),
	
	getInitialState: function() {
		return {
			value: null,
			validationResult: null,
			new: true
		};
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		return (
			<div className={this.props.className}>
				<Input type='text' label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} value={this.props.render(this.props.value)} disabled={true} buttonAfter={this.renderControlButtons()}/>
				
				{this.state.value
					? ComponentUtils.cloneChild(this.props.children, 'SW.Form.EmbeddableForm', {
						property: this.props.property,
						value: this.state.value,
						formValueChanged: this.formValueChanged,
						disabled: this.props.disabled,
						formDisposed: this.formDisposed,
						initialValue: this.props.initialValue,
						new: this.state.new
					}) : null
				}
			</div>
		);
	},
	
	renderControlButtons: function() {
		var controlButtons = [];
		
		if (this.props.value) {
			controlButtons.push(
				<Button onClick={this.setFormValue.bind(this, this.props.value, false)} key='open'>
					<Glyphicon glyph={!this.props.disabled ? 'pencil' : 'eye-open'} />
				</Button>
			);
			
			controlButtons.push(
				<Button disabled={this.props.disabled} onClick={this.formValueChanged.bind(this, null)} key='remove'>
					<Glyphicon glyph="remove" />
				</Button>
			);
		} else {
			controlButtons.push(
				<Button disabled={this.props.disabled} onClick={this.setFormValue.bind(this, this.props.initialValue ? this.props.initialValue : {}, true)} key='add'>
					<Glyphicon glyph="plus" />
				</Button>
			);
		}
		
		return controlButtons;
	},
	
	setFormValue: function(value, isNew) {
		this.setState({
			value: value,
			new: isNew
		});
	},
	
	formValueChanged: function(value) {
		this.valueChanged(value);
		
		this.setFormValue(null, false);
	},
	
	formDisposed: function() {
		this.setFormValue(null, false);
	}
});
