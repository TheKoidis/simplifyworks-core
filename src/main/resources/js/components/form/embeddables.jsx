/**
* Class represents multiple embeddables property component.
*/
SW.Form.Embeddables = React.createClass({
	
	displayName: 'SW.Form.Embeddables',
	
	/**
	* SW.Form.Property
	*/
	mixins: [SW.Form.Property, SW.Form.AbstractEmbeddables],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Show add button
		*/
		addable: React.PropTypes.bool,
		
		/**
		* Show delete button
		*/
		removable: React.PropTypes.bool,
		
		/**
		* Initial value for creation (properties can be predefined)
		*/
		initialValue: React.PropTypes.object,
		
		/**
		* There are two ways how represent multiple embeddables - using list with table or list with tags. Form is used for both variants for creation/editing/detail.
		*/
		children: ComponentUtils.checkChildrenStructure([{
			'SW.Form.EmbeddableForm': 'single-required',
			'SW.Form.EmbeddablesList': 'single-required'
		}, {
			'SW.Form.EmbeddableForm': 'single-required',
			'SW.Form.EmbeddablesTagList': 'single-required'
		}]),
	}),
	
	getDefaultProps: function () {
		return {
			addable: true,
			removable: true
		};
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		return (
			<div className={this.props.className}>
				<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName='wrapper'>
					{ComponentUtils.findChild(this.props.children, 'SW.Form.EmbeddablesList')
						?	ComponentUtils.cloneChild(this.props.children, 'SW.Form.EmbeddablesList', {
							value: this.props.value,
							
							editValue: this.setFormValue,
							createValue: this.setFormValue,
							removeValue: this.removeValue,
							
							disabled: this.props.disabled,
							addable: this.props.addable,
							removable :this.props.removable,
							noData: this.props.noData
						})
						:	ComponentUtils.cloneChild(this.props.children, 'SW.Form.EmbeddablesTagList', {
							value: this.props.value,
							
							editValue: this.setFormValue,
							createValue: this.setFormValue.bind(this, this.props.initialValue ? this.props.initialValue : {}, this.props.value ? this.props.value.length : 0, true),
							removeValue: this.removeValue,
							
							disabled: this.props.disabled
						})
					}
					
					{this.state.value
						? ComponentUtils.cloneChild(this.props.children, 'SW.Form.EmbeddableForm', {
							property: this.props.property,
							value: this.state.value,
							formValueChanged: this.formValueChanged,
							disabled: this.props.disabled,
							formDisposed: this.formDisposed,
							initialValue: this.props.initialValue,
							new: this.state.new
						}) : null
					}
				</Input>
			</div>
		);
	},
});
