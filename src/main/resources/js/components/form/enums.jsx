/**
* Class represents multiple enums property component of record.
*/
SW.Form.Enums = React.createClass({
	
	displayName: 'SW.Form.Enums',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Enum options (format {enum: 'sex', values: ['FEMALE', 'MALE']})
		*/
		options: React.PropTypes.object.isRequired,
	}),
	
	getInitialState: function() {
		return {
			validationResult: null
		};
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		return (
			<div className={this.props.className}>
				<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName='wrapper'>
					<SW.Form.TagList value={this.props.value} disabled={this.props.disabled} renderTag={this.renderMultiTag} renderInput={this.renderMultiInput} />
				</Input>
			</div>
		);
	},
	
	renderMultiTag: function(element) {
		return (
			<span style={{display: 'inline-block'}}>
				{i18n.t(this.props.options.enum + '-' + element)}
				
				{!this.props.disabled
					?
					<span>
						{' '}
						<Button bsSize="xsmall" bsStyle='link' onClick={this.removeValue.bind(this, element)}>
							<Glyphicon glyph='remove' />
						</Button>
					</span>
					:	null
				}
			</span>
		);
	},
	
	renderMultiInput: function() {
		if(!this.props.disabled && ((!this.props.value) || (this.props.value && this.props.value.length < this.props.options.values.length))) {
			var dropdownTitle = (
				<Glyphicon glyph='plus' />
			);
			
			return (
				<DropdownButton id='multi-input' bsSize='small' title={dropdownTitle} noCaret onSelect={this.addValue}>
					{this.props.options.values.map(function(element, index, array) {
						return (
							(!this.props.value || (this.props.value && this.props.value.indexOf(element) === -1))
							?
							<MenuItem key={'multi-input-item-' + index} eventKey={element}>
								{i18n.t(this.props.options.enum + '-' + element)}
							</MenuItem>
							:
							null
						);
					}.bind(this))}
				</DropdownButton>
			);
		} else {
			return null;
		}
	},
	
	removeValue: function(value) {
		var newValue = this.props.value.filter(function(element, index, array) {
			return element !== value;
		});
		
		this.valueChanged(newValue);
	},
	
	addValue: function(event, value) {
		var newValue = this.props.value ? this.props.value.concat(value) : [value];
		
		this.valueChanged(newValue);
	},
	
	onInputChange: function(event) {
		var value = [];
		var selectedOptions = event.target.selectedOptions;
		
		if(selectedOptions) {
			for(var index = 0; index < selectedOptions.length; index++) {
				value.push(selectedOptions[index].value);
			}
		}
		
		this.props.valueChanged(this.props.property, value);
	}
});
