/**
 * Class represents output list of embeddables (tags).
 */
SW.Form.EmbeddablesTagList = React.createClass({

	displayName: 'SW.Form.EmbeddablesTagList',

	propTypes: {
		/**
		* Value provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		value: React.PropTypes.arrayOf(React.PropTypes.any),

		/**
		* Disabled flag
		* TODO disabled flag per operation (creating/editing/removing)
		*/
		disabled: React.PropTypes.bool,

		/**
		 * Function responsible for rendering current value (format render(value))
		 */
		render: React.PropTypes.func.isRequired,

		/**
		* Edit value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		editValue: React.PropTypes.func,

		/**
		* Create value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		createValue: React.PropTypes.func,

		/**
		* Remove value function provided automatically by parent (DO NOT SET MANUALLY!)
		*/
		removeValue: React.PropTypes.func
	},

	render: function() {
		return (
			<SW.Form.TagList value={this.props.value} disabled={this.props.disabled} renderTag={this.renderMultiTag} renderInput={this.renderMultiInput} />
		);
	},

	renderMultiTag: function(element, index, highlighted) {
		return (
			<span style={{display: 'inline-block'}}>
				{this.props.render(element)}

				<span>
					{' '}
					{highlighted
						?	<ButtonGroup>
								<Button bsSize="xsmall" bsStyle='link' onClick={this.editValue.bind(this, element)}>
									<Glyphicon glyph={!this.props.disabled ? 'pencil' : 'eye-open'} />
								</Button>
								{!this.props.disabled ?
								<Button bsSize="xsmall" bsStyle='link' onClick={this.removeValue.bind(this, element)}>
									<Glyphicon glyph='remove' />
								</Button> : null }
							</ButtonGroup>
						:	null
					}
				</span>
			</span>
		);
	},

	renderMultiInput: function() {
		if(this.props.disabled) {
			return null;
		} else {
			return (
				<Button bsSize='small' onClick={this.createValue.bind(this, {})}>
					<Glyphicon glyph='plus' />
				</Button>
			);
		}
	},

	editValue: function(element) {
    	this.props.editValue(element, this.props.value.indexOf(element));
	},

	createValue: function(element) {
		this.props.createValue(element, this.props.value ? this.props.value.length : 0);
	},

	removeValue: function(element) {
		this.props.removeValue(element, this.props.value.indexOf(element));
	}
});
