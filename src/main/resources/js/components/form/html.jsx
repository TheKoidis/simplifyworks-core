var ReactQuill = require('react-quill');
var toolbar;

/**
* Class represents HTML property component of record.
*/
SW.Form.Html = React.createClass({
	
	displayName: 'SW.Form.Html',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		// no additional propTypes
	}),
	
	getInitialState: function() {
		return {
			validationResult: null
		};
	},
	
	componentWillMount: function() {
		toolbar = [
			
			{ label:'Formats', type:'group', items: [
				{ label:i18n.t('form-html-font'), type:'font', items: [
					{ label:'Sans Serif',  value:'sans-serif', selected:true },
					{ label:'Serif',	   value:'serif' },
					{ label:'Monospace',   value:'monospace' }
				]},
				{ type:'separator' },
				{ label:i18n.t('form-html-size'), type:'size', items: [
					{ label:'10px',  value:'10px' },
					{ label:'13px', value:'13px', selected:true},
					{ label:'16px', value:'16px'},
					{ label:'18px', value:'18px'},
					{ label:'24px',  value:'24px' },
					{ label:'32px',   value:'32px' },
					{ label:'48px',   value:'48px' }
				]},
				{ type:'separator' },
				{ label:i18n.t('form-html-alignment'), type:'align', items: [
					{ label:'', value:'left', selected:true },
					{ label:'', value:'center' },
					{ label:'', value:'right' },
					{ label:'', value:'justify' }
				]}
			]},
			
			{ label:'Text', type:'group', items: [
				{ type:'bold', label:i18n.t('form-html-bold') },
				{ type:'italic', label:i18n.t('form-html-italic') },
				{ type:'strike', label:i18n.t('form-html-strike') },
				{ type:'underline', label:i18n.t('form-html-underline') },
				{ type:'separator' },
				{ type:'color', label:i18n.t('form-html-color'), items:ReactQuill.Toolbar.defaultColors },
				{ type:'background', label:i18n.t('form-html-background-color'), items:ReactQuill.Toolbar.defaultColors },
				{ type:'separator' },
				{ type:'link', label:i18n.t('form-html-link') }
			]},
			
			{ label:'Blocks', type:'group', items: [
				{ type:'bullet', label:i18n.t('form-html-bullet') },
				{ type:'separator' },
				{ type:'list', label:i18n.t('form-html-list') }
			]}
		];
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		if (!this.props.disabled) {
			return (
				<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName="wrapper">
					<ReactQuill value={this.props.value} theme='snow' onChange={this.onTextChange} toolbar={toolbar}/>
				</Input>
			);
		} else {
			return (
				<div className={this.props.className}>
					<Input label={this.getLabel()} title={this.getTooltip()} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName="wrapper">
						<div className='html-component form-control' disabled='disabled' dangerouslySetInnerHTML={{__html: this.props.value}}/>
					</Input>
				</div>
			);
		}
	},
	
	onTextChange: function(value) {
		if (value == '<div><br></div>') {
			value = '';
		}
		
		this.valueChanged(value);
	}
});
