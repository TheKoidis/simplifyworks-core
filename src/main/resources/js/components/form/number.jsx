/**
* Class represents input/output number property component of record.
*/
SW.Form.Number = React.createClass({
	
	displayName: 'SW.Form.Number',
	
	/**
	* SW.Form.Property
	*/
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Key down handling (for example for submitting form by enter)
		*/
		onKeyDown: React.PropTypes.func,
		
		/**
		* Disable group separator flag (for example: 1000000 insteadof 1,000,000 in EN/US locale)
		*/
		disableGroupSeparator: React.PropTypes.bool,
	}),
	
	format: function(value) {
		return value!==null ? NumberUtils.format(value, this.props.disableGroupSeparator ? '' : undefined) : null;
	},
	
	getInitialState: function() {
		return {
			value: this.format(this.props.value),
			convertFail: null,
			validationResult: null
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if (NumberUtils.parse(this.state.value) != nextProps.value && !(this.props.value == nextProps.value && this.state.convertFail)) {
			this.setState({
				value: this.format(nextProps.value),
				convertFail: null
			});
		}
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		var value = this.state.value;
		return (
			<div className={this.props.className}>
				<Input
					style={{textAlign: 'right'}}
					type='text'
					value={value}
					disabled={this.props.disabled}
					placeholder={LocalizationUtils.getPlaceholder(this.props)}
					label={this.getLabel()}
					title={LocalizationUtils.getTooltip(this.props)}
					bsStyle={this.getStyle()}
					help={this.getInfo()}
					onChange={this.onChange}
					onBlur={this.onBlur}
					onKeyDown={this.props.onKeyDown ? this.props.onKeyDown : null}/>
			</div>
		);
	},
	
	onBlur: function() {
		if (!this.state.convertFail) {
			this.setState({
				value:  this.format(this.props.value),
			});
		}
	},
	
	onChange: function(event) {
		var emptyValue = !event.target.value;
		var inputValue = event.target.value;
		var value = NumberUtils.parse(event.target.value);
		if (!emptyValue && value===null) {
			this.setState({
				value: inputValue,
				convertFail: 'number-conversion-failed'
			});
		} else {
			this.setState({
				value: event.target.value,
				convertFail: null
			}, function() {
				this.valueChanged(value);
			});
		}
	}
});
