var api = require('./../../rest/attachment.jsx');

/**
* Class represents atachments property component.
*/
SW.Form.AttachmentsSimple = React.createClass({
	
	displayName: 'SW.Form.AttachmentsSimple',
	
	/**
	* SW.Form.Property
	*/
	mixins: [SW.Form.Property, SW.Form.AbstractAttachment],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Allows upload more than one file
		*/
		multiple: React.PropTypes.bool,
		
		/**
		* Allows change default css style of dropzone component
		*/
		style: React.PropTypes.any,
		
		/**
		* Localization key for text in dropzone
		*/
		titleKey: React.PropTypes.string,
		
		/**
		* Accept attribute of html file input (see http://www.w3schools.com/tags/att_input_accept.asp)
		*/
		accept: React.PropTypes.string,
		
		/**
		* Custom rest url for download and upload (default is '/api/core/attachments')
		*/
		rest: React.PropTypes.string,
	}),
	
	getDefaultProps: function() {
		return {
			rest: '/api/core/attachments'
		}
	},
	
	getInitialState: function () {
		return {
			isDragActive: false,
			uploading: [],
			uploaded: []
		};
	},
	
	componentWillReceiveProps: function(nextProps) {
		if(this.props.value !== nextProps.value) {
			this.setState({
				uploading: [],
				uploaded: []
			});
		}
	},
	
	removeAttachment: function(guid) {
		var attachments = this.props.value.slice();
		for(var i = attachments.length - 1; i >= 0; i--) {
			if (attachments[i].guid == guid) {
				attachments.splice(i, 1);
			}
		}
		
		this.valueChanged(attachments);
	},
	
	attachmentsValueChanged: function(value, newAttachments) {
		this.valueChanged(value);
	},
	
	download: function(guid) {
		api.download(this.props.rest, guid);
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		var rows = [];
		
		if (this.props.value) {
			this.props.value.map(function(file, index, array) {
				rows.push(
					<Row key={'props'+index}>
						<Col md={6} style={{paddingLeft: 0}}><Button onClick={this.download.bind(this, file.guid)} bsStyle='link' bsSize="xsmall" disabled={!file.id}><Glyphicon glyph='download-alt' /> {file.name}</Button></Col>
						<Col md={5} style={{'textAlign': 'right'}}>{RenderUtils.getFileSize(file.size, 1)}</Col>
						<Col md={1} style={{'textAlign': 'right', paddingRight: 0}}>{!this.props.disabled? (<Button onClick={this.removeAttachment.bind(this, file.guid)} bsStyle='link' bsSize="xsmall"><Glyphicon glyph='trash' /></Button>) : ''}</Col>
					</Row>
				);
			}.bind(this))
		}
		
		if (this.state.uploaded) {
			this.state.uploaded.map(function(file, index, array) {
				rows.push(
					<Row key={'uploaded'+index}>
						<Col md={6} style={{paddingLeft: 0}}><Glyphicon glyph='download-alt' /> {file.name}</Col>
						<Col md={6} style={{'textAlign': 'center'}}>{i18n.t('form-attachment-uploaded')}</Col>
					</Row>
				);
			})
		}
		
		if (this.state.uploading) {
			this.state.uploading.map(function(file, index, array) {
				rows.push(
					<Row key={'uploading'+index}>
						<Col md={6} style={{paddingLeft: 0}}><Glyphicon glyph='upload' /> {file.name}</Col>
						<Col md={6} style={{'textAlign': 'right'}}><ProgressBar active now={file.percent} style={{margin: 1}}/></Col>
					</Row>
				);
			})
		}
		
		if (rows.length == 0) {
			rows.push(<Row key="noAttachment"><Col md={12} style={{paddingLeft: 0}}>{i18n.t('form-attachment-no-attachment')}</Col></Row>);
		}
		
		return (
			<div className={this.props.className}>
				<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName="wrapper">
					{this.dropzoneDiv()}
					
					<Grid fluid>
						{rows}
					</Grid>
				</Input>
			</div>
		);
	},
});
