/**
 * Class represents space component of form. This component has only visual meaning.
 */
SW.Form.Space = React.createClass({

	displayName: 'SW.Form.Space',

	propTypes: {
		/**
		* Size of component (used by automatically created grid)
		*/
		size: React.PropTypes.number.isRequired
	},

	render: function() {
		return (
			<div />
		);
	}
});
