/**
* Class represents tag list component.
*/
SW.Form.TagList = React.createClass({
	
	displayName: 'SW.Form.TagList',
	
	propTypes: {
		/**
		* initial value
		*/
		value: React.PropTypes.array,
		
		/**
		* Disabled flag
		*/
		disabled: React.PropTypes.bool,
		
		/**
		* render tag
		*/
		renderTag: React.PropTypes.func.isRequired,
		/**
		* render input
		*/
		renderInput: React.PropTypes.func.isRequired
	},
	
	getInitialState: function() {
		return {
			highlighted: null
		};
	},
	
	render: function() {
		return (
			<div>
				{this.props.value && this.props.value.length > 0
					?	this.props.value.map(function(element, index, array) {
						return (
							<span key={index} className='form-control'
								style={{display: 'inline-block', width: 'auto', marginRight: 4, borderRadius: 4}}
								onMouseEnter={this.enableHighlight.bind(this, index)} onMouseLeave={this.disableHighlight}
								disabled={this.props.disabled}>
								{this.props.renderTag(element, index, this.state.highlightedIndex == index)}
							</span>
						);
					}.bind(this))
					:	i18n.t('null') + ' '
				}
				
				{this.props.renderInput()}
			</div>
		);
	},
	
	enableHighlight: function(index) {
		this.setState({
			highlightedIndex: index
		});
	},
	
	disableHighlight: function() {
		this.setState({
			highlightedIndex: null
		});
	},
});
