/**
* Class represents create button of embeddables list
*/
SW.Form.EmbeddablesListCreateButton = React.createClass({
	
	displayName: 'SW.Form.EmbeddablesListCreateButton',
	
	propTypes:{
		
		/**
		* Key prefix provided automatically by parent (DO NOT SET MANUALLY!)
		*
		* Key prefix prop has the lowest priority for usage
		*/
		keyPrefix: React.PropTypes.string,
		
		/**
		* Label key
		*
		* Label key prop has higher priority for usage than key prefix prop concatenated with property prop
		*/
		labelKey: React.PropTypes.string,
		
		/**
		* Label (can be string or React element - for more illustrative labels; for example with icons)
		*
		* Label prop has the highest priority for usage
		*/
		label: React.PropTypes.oneOfType([React.PropTypes.string, React.PropTypes.element]),
		
		/**
		* Tooltip key
		*
		* Tooltip key prop has higher priority for usage than key prefix prop concatenated with property prop and 'tooltip' suffix
		*/
		tooltipKey: React.PropTypes.string,
		
		/**
		* Tooltip
		*
		* Tooltip has the highest priority for usage
		*/
		tooltip: React.PropTypes.string,
		
		
	},
	
	createValue: function(value, index, isNew) {
		return this.props.createValue(value, index, isNew)
	},
	
	
	
	render: function() {
		return (
			<Button bsSize='small' onClick={this.createValue.bind(this, this.props.initialValue ? this.props.initialValue : {}, this.props.value ? this.props.value.length : 0, true)} title={LocalizationUtils.getTooltip(this.props)}>
				<Glyphicon glyph='plus' />{' '}{LocalizationUtils.getLabelText(this.props, 'table-add')}
			</Button>
			);
		},
		
	});
