/**
* Class represents common embeddables method.
*/
SW.Form.AbstractEmbeddables = {
	
	getInitialState: function() {
		return {
			value: null
		};
	},
	
	setFormValue: function(value, index, isNew) {
		this.setState({
			value: value,
			index: index,
			new: isNew
		});
	},
	
	formValueChanged: function(value) {
		if(value) {
			var values = [].concat(this.props.value);
			values[this.state.index] = value;
			
			this.valueChanged(values);
		}
		
		this.setFormValue(null, null);
	},
	
	removeValue: function(value, index) {
		var newValue = this.props.value.filter(function(element, index, array) {
			return element !== value;
		});
		
		this.valueChanged(newValue);
	},
	
	formDisposed: function() {
		this.setFormValue(null, null);
	}
};
