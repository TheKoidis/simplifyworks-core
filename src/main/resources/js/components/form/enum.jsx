/**
* Class represents input/output enum property component of record.
*/
SW.Form.Enum = React.createClass({
	
	displayName: 'SW.Form.Enum',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Key down handling (for example for submitting form by enter)
		*/
		onKeyDown: React.PropTypes.func,
		
		/**
		* Show null option flag (default is true)
		*/
		showNull: React.PropTypes.bool,
		
		/**
		* Enum options (format {enum: 'sex', values: ['FEMALE', 'MALE']})
		*/
		options: React.PropTypes.object.isRequired,
	}),
	
	getDefaultProps: function() {
		return {
			showNull: true
		};
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		if(!this.props.showNull && (this.props.value === undefined || this.props.value === null)) {
			console.error('Property \"' + this.props.property + '\" does not show null item but has undefined or null value.');
		}
		
		return (
			<div className={this.props.className}>
				<Input type='select' disabled={this.props.disabled} label={this.getLabel()} value={this.props.value ? this.props.value : ''}
					bsStyle={this.getStyle()} title={LocalizationUtils.getTooltip(this.props)} help={this.getInfo()} onChange={this.onInputChange}
					onKeyDown={this.props.onKeyDown ? this.props.onKeyDown : null}>
					{this.props.showNull
						?
						<option key='null' value=''>
							{i18n.t('null')}
						</option>
						:	null
					}
					{this.props.options.values.map(function(value) {
						return (
							<option key={this.props.options.enum + '-' + value} value={value}>
								{i18n.t(this.props.options.enum + '-' + value)}
							</option>
						);
					}.bind(this))}
				</Input>
			</div>
		);
	},
	
	onInputChange: function(event) {
		this.valueChanged(event.target.value);
	}
});
