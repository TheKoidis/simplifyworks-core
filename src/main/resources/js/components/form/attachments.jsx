/**
* Class represents attachments property component.
*/
SW.Form.Attachments = React.createClass({
	
	displayName: 'SW.Form.Attachments',
	
	/**
	* SW.Form.Property
	*/
	mixins: [SW.Form.Property, SW.Form.AbstractEmbeddables],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Initial value for creation (properties can be predefined)
		*/
		initialValue: React.PropTypes.object,
		
		/**
		* Show delete button
		*/
		removable: React.PropTypes.bool,
		
		/**
		* There are two ways how represent multiple embeddables - using list with table or list with tags. Form is used for both variants for creation/editing/detail.
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.EmbeddableForm': 'single-required',
			'SW.Form.AttachmentsList': 'single-required'
		}),
		
		/**
		* Allows upload more than one file
		*/
		multiple: React.PropTypes.bool,
		
		/**
		* Allows change default css style of dropzone component
		*/
		style: React.PropTypes.any,
		
		/**
		* Localization key for text in dropzone
		*/
		titleKey: React.PropTypes.string,
		
		/**
		* Accept attribute of html file input (see http://www.w3schools.com/tags/att_input_accept.asp)
		*/
		accept: React.PropTypes.string,
		
		/**
		* Custom rest url for download and upload (default is '/api/core/attachments')
		*/
		rest: React.PropTypes.string,
		
		/**
		* Optional info (allowed suffixes, etc.)
		*/
		info: React.PropTypes.object
	}),
	
	getDefaultProps: function() {
		return {
			rest: '/api/core/attachments',
			removable: true
		}
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		return (
			<div className={this.props.className}>
				<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName='wrapper'>
					{this.props.info ? this.props.info : null}
					
					{ComponentUtils.cloneChild(this.props.children, 'SW.Form.AttachmentsList', {
						value: this.props.value,
						
						editValue: this.setFormValue,
						createValue: this.setFormValue.bind(this, this.props.initialValue ? this.props.initialValue : {}, this.props.value ? this.props.value.length : 0, true),
						removeValue: this.removeValue,
						
						disabled: this.props.disabled || this.state.uploading,
						removable: this.props.removable && !this.state.uploading,
						valueChanged: this.valueChanged,
						multiple: this.props.multiple,
						style: this.props.style,
						titleKey: this.props.titleKey,
						accept: this.props.accept,
						rest: this.props.rest,
						onUploadStarted: this.onUploadStarted,
						onUploadFinished: this.onUploadFinished,
					})}
					
					{this.state.value
						?	ComponentUtils.cloneChild(this.props.children, 'SW.Form.EmbeddableForm', {
							property: this.props.property,
							value: this.state.value,
							formValueChanged: this.formValueChanged,
							disabled: this.props.disabled,
							formDisposed: this.formDisposed,
							initialValue: this.props.initialValue,
							new: this.state.new
						})
						: null
					}
				</Input>
			</div>
		);
	},
	
	onUploadStarted: function() {
		this.setState({
			uploading: true
		});
	},
	
	onUploadFinished: function() {
		this.setState({
			uploading: false
		});
	}
});
