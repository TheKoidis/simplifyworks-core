/**
 * Class represents custom read-only component of form. 
 */
SW.Form.Custom = React.createClass({

	displayName: 'SW.Form.Custom',
	
	propTypes: {
		/**
		* Size of component (used by automatically created grid)
		*/
		size: React.PropTypes.number.isRequired
	},

	render: function() {
		return (
			<div>
				{this.props.children}
			</div>
		);
	}
});
