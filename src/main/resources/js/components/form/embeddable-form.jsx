/**
* Class represents embeddable form.
*/
SW.Form.EmbeddableForm = React.createClass({
	
	displayName: 'SW.Form.EmbeddableForm',
	
	mixins: [ OnUnload, FluxMixin, StoreWatchMixin('form')],
	
	propTypes: {
		/**
		* object property
		*/
		property: React.PropTypes.string,
		
		/**
		* initial value
		*/
		value: React.PropTypes.object,
		
		/**
		* manual listener for value changes
		*/
		valueChanged: React.PropTypes.func,
		
		/**
		* listener for value changes for parent component (DO NOT SET MANUALLY!)
		*/
		formValueChanged: React.PropTypes.func,
		
		/**
		* form name
		*/
		name: React.PropTypes.string.isRequired,
		/**
		* is component disabled
		*/
		
		disabled: React.PropTypes.bool,
		
		/**
		*	Label key (for localization purposes, used in case Label is not set)
		*/
		labelKey: React.PropTypes.string,
		
		/**
		*	Label
		*/
		label: React.PropTypes.string,
		
		/**
		* When creating new record (DO NOT SET MANUALLY!)
		*/
		new: React.PropTypes.bool,
		
		/**
		*	Function responsible for validation ('validate(data, callback)' where 'callback(isValid)')
		*/
		validate: React.PropTypes.func,
		
		/**
		* children
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.Section': 'multiple-required'
		}),
		
		formDisposed: React.PropTypes.func,
	},
	
	getInitialState: function() {
		return {closingForm: false};
	},
	
	getStateFromFlux: function() {
		if (this.props.value !== undefined && this.props.value !== null && this.state && !this.state.closingForm) {
			return this.context.flux.store('form').getForm(this.props.name);
		}
		
		return {};
	},
	
	componentDidMount: function() {
		if (this.props.new) {
			this.init(this.props);
		} else {
			this.loadEmbeddableData(this.props);
		}
	},
	
	init: function(props) {
		this.context.flux.actions.form.formInit(props.name, null, null, ValidationUtils.getValidationFromProps(this.props, this.props.name), props.initialValue);
	},
	
	loadEmbeddableData: function(props) {
		this.context.flux.actions.form.loadEmbeddableData(props.name, props.value ? JSON.parse(JSON.stringify(props.value)) : null, null, ValidationUtils.getValidationFromProps(this.props, this.props.name));
	},
	
	closeModalForm: function(callback) {
		this.setState({
			closingForm: true
		}, function() {
			this.context.flux.actions.form.closeModalForm(this.props.name);
			callback();
		});
	},
	
	confirmModalForm: function(callback) {
		this.context.flux.actions.form.refreshValidation(this.props.name, null, ValidationUtils.getValidationFromProps(this.props, this.props.name));
		
		this.context.flux.actions.form.confirmModalForm(this.props.name, function() {
			this.closeModalForm(callback);
		}.bind(this), this.props.validate);
	},
	
	onUnload: function() {
		this.cancel();
	},
	
	onBeforeUnload: function(e) {
		if (!this.state.modified) {
			return false;
		}
		
		var confirmationMessage = i18n.t('form-unload-changes');
		
		(e || window.event).returnValue = confirmationMessage; //Gecko + IE, Chrome(mac os)
		return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
	},
	
	render: function() {
		return (
			<Modal show={true} onHide={this.cancel} bsSize='large' backdrop='static'>
				<Modal.Header closeButton>
					<Modal.Title style={{width: 'auto'}}>{this.props.label ? this.props.label : i18n.t(this.props.labelKey ? this.props.labelKey : this.props.name)}</Modal.Title>
				</Modal.Header>
				
				<Modal.Body>
					{this.state.data
						?
						ComponentUtils.cloneChildren(this.props.children, 'SW.Form.Section', {
							name: this.props.formName,
							data: this.state.data,
							valueChanged: this.valueChanged,
							labelKeyPrefix: this.props.name,
							disabled: this.props.disabled,
							validation: this.state.validation
						})
						:
						null
					}
				</Modal.Body>
				
				<Modal.Footer>
					{!this.state.modified ?
						<span><Glyphicon glyph='ok' />{' ' + i18n.t('form-no-changes')}</span>
						:
						<span><Glyphicon glyph='pencil' />{' ' + i18n.t('form-changes')}</span>
					}
					
					<Button onClick={this.cancel} bsStyle='link'>
						<Glyphicon glyph='remove' />
						{' ' + i18n.t('exit')}
					</Button>
					
					
					{(this.state.modified || this.props.new) && !this.props.disabled
						?
						<ButtonGroup>
							<Button bsStyle='primary' onClick={this.confirm}>
								<Glyphicon glyph='ok' />
								{' ' + i18n.t('ok')}
							</Button>
						</ButtonGroup>
						:
						null
					}
				</Modal.Footer>
			</Modal>
		);
	},
	
	confirm: function() {
		this.confirmModalForm(this.parentValueChanged.bind(this, this.state.data));
	},
	
	cancel: function() {
		if (this.state.modified) {
			notification.showLocalizedMessage('warning', 'form-changes-found', 'form-save-or-discard-changes', {}, {
				label: i18n.t('form-leave-changes'),
				callback: function() {
					this.closeModalForm(this.props.formDisposed);
				}.bind(this)
			}, 0);
			return;
		}
		
		this.closeModalForm(this.props.formDisposed);
	},
	
	parentValueChanged: function(data) {
		this.props.formValueChanged(data);
	},
	
	valueChanged: function(property, value, validations) {
		this.context.flux.actions.form.changeValue(this.props.name, null, property, value, this.props.valueChanged, validations);
	}
});
