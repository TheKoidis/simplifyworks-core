/**
* Class represents multiple strings property component of record.
*/
SW.Form.Strings = React.createClass({
	
	displayName: 'SW.Form.Strings',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		// no additional propTypes
	}),
	
	getInitialState: function() {
		return {
			value: null,
			visibleMultiInputText: false,
			validationResult: null
		};
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		return (
			<div className={this.props.className}>
				<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName='wrapper'>
					<SW.Form.TagList value={this.props.value} disabled={this.props.disabled} renderTag={this.renderMultiTag} renderInput={this.renderMultiInput} />
				</Input>
			</div>
		);
	},
	
	renderMultiTag: function(element, index, highlighted) {
		return (
			<span style={{display: 'inline-block'}}>
				{element}
				
				{!this.props.disabled && highlighted
					?
					<span>
						{' '}
						<Button bsSize="xsmall" bsStyle='link' onClick={this.removeValue.bind(this, element)}>
							<Glyphicon glyph='remove' />
						</Button>
					</span>
					:	null
				}
			</span>
		);
	},
	
	renderMultiInput: function() {
		if(this.props.disabled) {
			return null;
		} else if(this.state.visibleMultiInputText) {
			var controlButtons = [];
			
			controlButtons.push(
				<Button bsSize='small' onClick={this.addValue} key='ok'>
					<Glyphicon glyph='ok' />
				</Button>
			);
			
			controlButtons.push(
				<Button bsSize='small' onClick={this.hideMultiInputText} key='remove'>
					<Glyphicon glyph='remove' />
				</Button>
			);
			
			return (
				<Input type='text' bsSize='small' value={this.state.value} onChange={this.onMultiInputTextChange} buttonAfter={controlButtons} />
			);
		} else {
			return (
				<Button bsSize='small' onClick={this.showMultiInputText}>
					<Glyphicon glyph='plus' />
				</Button>
			);
		}
	},
	
	onMultiInputTextChange: function(event) {
		this.setState({
			value: event.target.value
		});
	},
	
	removeValue: function(value) {
		var newValue = this.props.value.filter(function(element, index, array) {
			return element !== value;
		});
		
		this.valueChanged(newValue);
	},
	
	addValue: function() {
		if (this.props.value && this.props.value.indexOf(this.state.value) > -1) {
			notification.showLocalizedMessage('error', 'validation-duplicate');
			return;
		}
		
		var newValue = this.props.value ? this.props.value.concat(this.state.value) : [this.state.value];
		
		this.valueChanged(newValue, function() {
			this.hideMultiInputText();
		}.bind(this));
	},
	
	hideMultiInputText: function() {
		this.setState({
			value: '',
			visibleMultiInputText: false
		});
	},
	
	showMultiInputText: function() {
		this.setState({
			value: '',
			visibleMultiInputText: true
		});
	}
});
