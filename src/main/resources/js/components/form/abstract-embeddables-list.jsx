/**
* Class represents output list of embeddables (table).
*/
SW.Form.AbstractEmbeddablesList = {
	
	getInitialState: function() {
		return this.getPage(this.props, ComponentUtils.findChild(this.props.children, 'SW.List.Pagination') ? 10 : 2147483647, 1,
		this.props.defaultOrdering ? this.props.defaultOrdering : []);
	},
	
	componentWillReceiveProps: function(nextProps) {
		this.setState(this.getPage(nextProps, this.state.options.pageSize, this.state.options.pageNumber, this.state.options.ordering));
	},
	
	changePageNumber: function(event, selectedEvent) {
		this.setState(this.getPage(this.props, this.state.options.pageSize, selectedEvent.eventKey, this.state.options.ordering));
	},
	
	changePageSize: function(pageSize) {
		this.setState(this.getPage(this.props, pageSize, 1, this.state.options.ordering));
	},
	
	changeOrder: function(property, order, shiftMulti) {
		order = (order === 'asc' ? 'desc' : (order === 'desc' ? null : 'asc'));
		var newOrdering = Utils.changeOrder(this.state.options.ordering, property, order, shiftMulti);
		this.setState(this.getPage(this.props, this.state.options.pageSize, this.state.options.pageNumber, newOrdering));
	},
	
	getPage: function(props, pageSize, pageNumber, ordering) {
		var value = props.value ? props.value : [];
		Utils.sort(value, ordering);
		
		if (value.length <= (pageNumber-1)*pageSize) {
			//get max page when page is empty
			pageNumber = Math.floor((value.length-1)/pageSize+1);
		}
		
		if (pageNumber < 1) {
			pageNumber = 1;
		}
		
		return {
			options: {
				pageNumber: pageNumber,
				pageSize: pageSize,
				ordering: ordering
			},
			status: 'initialized',
			data: this.getData(props, pageSize, pageNumber, ordering)
		}
	},
	
	getData: function(props, pageSize, pageNumber, ordering) {
		var value = props.value ? props.value : [];
		var page = value.slice((pageNumber-1)*pageSize, pageNumber*pageSize);
		
		return {
			resources: page,
			recordsTotal: value.length,
			recordsFiltered: value.length,
			recordsReturned: page.length
		}
	},
	
	createValue: function(element) {
		this.props.createValue(element, this.props.value ? this.props.value.length : 0);
	},
	
	removeValue: function(element) {
		this.props.removeValue(element, this.props.value.indexOf(element));
	},
	
	editValue: function(element) {
		this.props.editValue(element, this.props.value.indexOf(element));
	},
	
	abstractRender: function() {
		return (
			<Grid fluid style={{padding: 0}}>
				{this.renderButtons()}
				<Row>
					<Col xs={12}>
						{ComponentUtils.cloneChild(this.props.children, 'SW.List.Table',
							{
								name: this.props.name, status: this.state.status, resources: this.state.data.resources,
								options: this.state.options, data: this.state.data, detailFunc: this.editValue, removeFunc: this.removeValue,
								disabled: this.props.disabled, changeOrder: this.changeOrder, removable: this.props.removable, noData: this.props.noData
							})
						}
					</Col>
				</Row>
				<Row>
					<Col xs={12} md={4}>
						{ComponentUtils.cloneChild(this.props.children, 'SW.List.Info', {name: this.props.name, status: this.state.status, pageNumber: this.state.options.pageNumber, pageSize: this.state.options.pageSize, selectionCount: 0, recordsReturned: this.state.data.recordsReturned, recordsFiltered: this.state.data.recordsFiltered, recordsTotal: this.state.data.recordsTotal})}
					</Col>
					<Col xs={12} md={8} className='text-right'>
						{ComponentUtils.cloneChild(this.props.children, 'SW.List.Pagination', {name: this.props.name, changePageNumber: this.changePageNumber, changePageSize: this.changePageSize, status: this.state.status, options: this.state.options, data: this.state.data})}
					</Col>
				</Row>
			</Grid>
		);
	}
};
