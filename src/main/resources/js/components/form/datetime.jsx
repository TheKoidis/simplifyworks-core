var DateTime = require('react-datetime');
var moment = require('moment');

/**
* Class represents input/output datetime property component of record.
*/
SW.Form.Datetime = React.createClass({
	
	displayName: 'SW.Form.Datetime',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Show time flag (default is false)
		*
		* TODO rename to showTime
		*/
		timeEnabled: React.PropTypes.bool,
	}),
	
	getInitialState: function() {
		return {
			value: this.props.value ? moment(this.props.value) : null,
			convertFail: null,
			validationResult: null
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if (this.toString(this.state.value) != nextProps.value && !(this.props.value == nextProps.value && this.state.convertFail)) {
			this.setState({
				value: this.toMoment(nextProps.value),
				convertFail: null
			});
		}
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		if (!this.props.disabled) {
			return (
				<div className={this.props.className}>
					<Input label={this.getLabel()} wrapperClassName="wrapper" help={this.getInfo()}>
						<div title={LocalizationUtils.getTooltip(this.props)}>
							<DateTime value={this.state.value} bsStyle={this.getStyle()} closeOnSelect={true} onChange={this.onChange}
								dateFormat={i18n.t('patternDate')}
								timeFormat={this.props.timeEnabled?i18n.t('patternTime'):false}
								locale={i18n.t('dateTimeLocale')}/>
						</div>
					</Input>
				</div>
			);
		} else {
			return (
				<div className={this.props.className}>
					<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} disabled={true} type='text' help={this.getInfo()} value={this.state.value ? moment(this.state.value).format(i18n.t(this.props.timeEnabled?'patternDateTime':'patternDate')) : null}/>
				</div>
			);
		}
	},
	
	onChange: function(value) {
		var emptyValue = !value;
		if (!emptyValue && !(value instanceof moment)) {
			this.setState({
				value: value,
				convertFail: 'date-conversion-failed'
			});
		} else {
			this.valueChanged(this.toString(value), function() {
				this.setState({
					value: value,
					convertFail: null
				});
			}.bind(this));
		}
	},
	
	toString: function(value) {
		if (!(value instanceof moment)) {
			return value;
		}
		
		if (this.props.timeEnabled) {
			return value ? value.toISOString() : null;
		} else {
			//converting to date without time
			return value ? value.format('YYYY-MM-DD')+'T00:00:00.000Z' : null;
		}
	},
	
	toMoment: function(value) {
		if (this.props.timeEnabled) {
			return value ? moment(new Date(value)) : null;
		} else {
			//removing time information
			return value ? moment(new Date(value.substring(0, 10))) : null;
		}
	}
});
