/**
* Class represents section component of form.
*/
SW.Form.Section = React.createClass({
	
	displayName: 'SW.Form.Section',
	
	propTypes: {
		/**
		* data
		*/
		// automatically set properties (via children cloning)
		data: React.PropTypes.object,
		/**
		*
		*/
		disabled: React.PropTypes.bool,
		/**
		* listener for value changes
		*/
		valueChanged: React.PropTypes.func,
		/**
		*	Prefix for label keys (for localization purposes), automatically included in case of tab usage
		*/
		labelKeyPrefix: React.PropTypes.string,
		// manually set properties
		/**
		* icon
		*/
		icon: React.PropTypes.string,
		/**
		* label string key
		*/
		labelKey: React.PropTypes.string,
		/**
		* Key down handling (for example for submitting form by enter)
		*/
		onKeyDown: React.PropTypes.func,
		/**
		* children
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form.Boolean': 'multiple-optional',
			'SW.Form.Datetime': 'multiple-optional',
			'SW.Form.Number': 'multiple-optional',
			'SW.Form.Embeddable': 'multiple-optional',
			'SW.Form.Embeddables': 'multiple-optional',
			'SW.Form.Entity': 'multiple-optional',
			'SW.Form.EntitySelect': 'multiple-optional',
			'SW.Form.Entities': 'multiple-optional',
			'SW.Form.Enum': 'multiple-optional',
			'SW.Form.Enums': 'multiple-optional',
			'SW.Form.String': 'multiple-optional',
			'SW.Form.Strings': 'multiple-optional',
			'SW.Form.Html': 'multiple-optional',
			'SW.Form.Attachments': 'multiple-optional',
			'SW.Form.AttachmentsSimple': 'multiple-optional',
			'SW.Form.Space': 'multiple-optional',
			'SW.Form.FormattedString': 'multiple-optional',
			'SW.Form.Custom': 'multiple-optional',
		})
	},
	
	render: function() {
		return (
			<Grid fluid>
				{this.createRows()}
			</Grid>
		);
	},
	
	createRows: function() {
		var counter = 0;
		var cells = [[]];
		var row = 0;
		var col = 0;
		
		React.Children.forEach(this.props.children, function(child) {
			if (!child) {
				return;
			}
			var size = child.props.size;
			
			if(col + (child.props.hidden ? 0 : size) > 12) {
				cells.push([]);
				row++;
				col = 0;
			}
			
			col += (child.props.hidden ? 0 : size);
			
			cells[row].push(
				<Col key={'cell-' + cells[row].length} xs={12} sm={12} md={size}
					xsHidden={child.props.hidden} smHidden={child.props.hidden} mdHidden={child.props.hidden} lgHidden={child.props.hidden}>
					{React.cloneElement(child, {
						ref: counter++,
						value: PropertyUtils.getPropertyValue(this.props.data, child.props.property),
						validationResult: this.props.validation && this.props.validation[child.props.property] ? this.props.validation[child.props.property].result : null,
						valueChanged: this.props.valueChanged,
						disabled: this.props.disabled || child.props.disabled,
						keyPrefix: this.props.labelKeyPrefix,
						onKeyDown: this.props.onKeyDown
					})}
				</Col>
			);
		}.bind(this));
		
		return cells.map(function(element, index, array) {
			return (
				<Row key={'row-' + index}>
					{element}
				</Row>
			);
		});
	}
});
