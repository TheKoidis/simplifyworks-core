/**
 * Class represents input list of entities.
 */
SW.List.EntityList = React.createClass({

	displayName: 'SW.List.EntityList',

	/**
	 * FluxMixin, StoreWatchMixin('list')
	 */
	mixins: [FluxMixin, StoreWatchMixin('list')],

	contextTypes: {
		/**
		 * flux
		 */
		flux: React.PropTypes.object.isRequired
	},

	propTypes: {
		/**
		 * component name
		 */
		name: React.PropTypes.string.isRequired,
		/**
		 * REST URL for loading data
		 */
		rest: React.PropTypes.string.isRequired,
		/**
		 * close function
		 */
		close: React.PropTypes.func,
		/**
		 * show modal
		 */
		showModal: React.PropTypes.bool,
		/**
		 * choose function
		 */
		chooseFunc: React.PropTypes.func,
		/**
		 * default ordering of table
		 */
		defaultOrdering: React.PropTypes.array,
		/**
		* prefilled default filter object
		*/
		defaultFilter: React.PropTypes.object,
		/*
		* True if list should be empty on initialize
		*/
		init: React.PropTypes.bool,
		/**
		 * children
		 */
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.FiltersGroup': 'multiple-optional',
			'SW.List.Filter': 'single-optional',
			'SW.List.QuickSearch': 'single-optional',
			'SW.List.Table': 'single-required',
			'SW.List.Info': 'single-optional',
			'SW.List.Pagination': 'single-optional'
		})
	},

	componentDidMount: function() {
		this.props.init ? this.init() : this.reload(this.props, true)
	},

	componentWillReceiveProps: function(nextProps) {
		this.reload(nextProps, false);
	},
	
	init: function() {
		this.context.flux.actions.list.common.init(this.props.name, {
			rest: this.props.rest,

			ordering: this.props.defaultOrdering ? this.props.defaultOrdering : [],
			pageNumber: 1,
			pageSize: ComponentUtils.findChild(this.props.children, 'SW.List.Pagination') ? 10 : 2147483647,

			quickSearch: {},
			filters: {
				column: {
					visible: false,

					types: [],
					values: []
				}
			},
			filter: this.props.defaultFilter ? this.props.defaultFilter : {}
		});	
	},

	reload: function(nextProps, forceReload) {
		if (!forceReload && nextProps.name == this.props.name && nextProps.rest == this.props.rest) {
			return;
		}

		this.context.flux.actions.list.common.reload(nextProps.name, {
			rest: nextProps.rest,

			ordering: this.props.defaultOrdering ? this.props.defaultOrdering : [],
			pageNumber: 1,
			pageSize: ComponentUtils.findChild(nextProps.children, 'SW.List.Pagination') ? 10 : 2147483647,

			quickSearch: {},
			filters: {
				column: {
					visible: false,

					types: [],
					values: []
				}
			},
			filter: this.props.defaultFilter ? this.props.defaultFilter : {}
		});
	},

	getStateFromFlux: function() {
		var list = this.context.flux.store('list').getList(this.props.name);

		return {
			status: list.status,
			options: list.options,
			data: list.data,
			selectionCount: list.selection.length,
			pageNumber: list.options.pageNumber,
			pageSize: list.options.pageSize,
			recordsReturned: list.data.recordsReturned,
			recordsFiltered: list.data.recordsFiltered,
			recordsTotal: list.data.recordsTotal,
			resources: (list.data && list.data.resources) ? list.data.resources : [],
			selection: list.selection
		};
	},

	changePageNumber: function(event, selectedEvent) {
		this.context.flux.actions.list.pagination.changePageNumber(this.props.name, selectedEvent.eventKey);
	},

	changePageSize: function(pageSize) {
		this.context.flux.actions.list.pagination.changePageSize(this.props.name, pageSize);
	},

	search: function(tokens) {
		this.context.flux.actions.list.quickSearch.search(this.props.name, tokens);
	},

	selectPage: function(property) {
		this.context.flux.actions.list.selection.selectPage(this.props.name, property);
	},

	deselectPage: function(property) {
		this.context.flux.actions.list.selection.deselectPage(this.props.name, property);
	},

	deselectAll: function() {
		this.context.flux.actions.list.selection.deselectAll(this.props.name);
	},

	changeOrder: function(property, order, multiShift) {
		this.context.flux.actions.list.ordering.changeOrder(this.props.name, property, order === 'asc' ? 'desc' : (order === 'desc' ? null : 'asc'), multiShift);
	},

	selectSingle: function(checked, property, resource) {
		if(checked) {
			this.context.flux.actions.list.selection.selectSingle(this.props.name, property, resource);
		} else {
			this.context.flux.actions.list.selection.deselectSingle(this.props.name, property, resource);
		}
	},

	chooseFunc: function(value) {
		this.props.chooseFunc(value);
		this.close();
	},
	
	close: function() {
		if(this.props.init) {
			this.context.flux.actions.list.common.removeFromStore(this.props.name);
		} 
		this.props.close();	
	},

	render: function() {
		return (
			<Modal show={this.props.showModal} onHide={this.close} bsSize="large" backdrop="static">
				<Modal.Header closeButton>
					<Modal.Title>{i18n.t(this.props.name)}</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Grid fluid>
						<Row>
							<Col xs={10}>
								<ButtonToolbar style={{marginBottom: 15}}>
									{ComponentUtils.cloneChildren(this.props.children, 'SW.List.FiltersGroup', {name: this.props.name, status: this.state.status})}
								</ButtonToolbar>
							</Col>

							<Col xs={12}>
								{ComponentUtils.cloneChild(this.props.children, 'SW.List.QuickSearch', {name: this.props.name, search: this.search})}
							</Col>
						</Row>

						<Row>
							<Col xs={12}>
								{ComponentUtils.cloneChild(this.props.children, 'SW.List.Filter', {name: this.props.name})}
							</Col>
						</Row>

						<Row>
							<Col xs={12}>
								{ComponentUtils.cloneChild(this.props.children, 'SW.List.Table',
									{
										name: this.props.name, status: this.state.status,	resources: this.state.resources, selection: this.state.selection,
										options: this.state.options, selectPage: this.selectPage, deselectPage: this.deselectPage, deselectAll: this.deselectAll, changeOrder: this.changeOrder,
										selectSingle: this.selectSingle, data: this.state.data, chooseFunc: this.chooseFunc})
									}
							</Col>
						</Row>

						<Row>
							<Col xs={4}>
								{ComponentUtils.cloneChild(this.props.children, 'SW.List.Info', {name: this.props.name, status: this.state.status, selectionCount: this.state.selectionCount, pageNumber: this.state.pageNumber, pageSize: this.state.pageSize, recordsReturned: this.state.recordsReturned, recordsFiltered: this.state.recordsFiltered, recordsTotal: this.state.recordsTotal})}
							</Col>

							<Col xs={8} className='text-right'>
								{ComponentUtils.cloneChild(this.props.children, 'SW.List.Pagination', {name: this.props.name, changePageNumber: this.changePageNumber, changePageSize: this.changePageSize, status: this.state.status, options: this.state.options, data: this.state.data})}
							</Col>
						</Row>
					</Grid>
				</Modal.Body>
				<Modal.Footer>
					<Button bsStyle='link' onClick={this.close}>
						<Glyphicon glyph='remove' />
						{' ' + i18n.t('cancel')}
					</Button>
				</Modal.Footer>
			</Modal>
		);
	}
});
