/**
* Class represents input/output string property component of record.
*/
SW.Form.String = React.createClass({
	
	displayName: 'SW.Form.String',
	
	/**
	* SW.Form.Property
	*/
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Key down handling (for example for submitting form by enter)
		*/
		onKeyDown: React.PropTypes.func,
		
		/**
		* Number of rows in textarea
		*/
		rows: React.PropTypes.number,
		
		/**
		* Type of text ('text' for common purposes, 'area' for longer texts like description, 'password' for secret data)
		*/
		type: React.PropTypes.oneOf(['text', 'area', 'password']),
	}),
	
	getDefaultProps: function() {
		return {
			type: 'text'
		}
	},
	
	getInitialState: function() {
		return {
			value: this.props.value ? this.props.value : ''
		};
	},
	
	componentWillReceiveProps: function(nextProps) {
		this.setState({
			value: nextProps.value,
		});
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		return (
			<div className={this.props.className}>
				<Input
					rows={this.props.rows}
					type={this.props.type === 'area' ? 'textarea': this.props.type}
					value={this.state.value ? this.state.value : ''}
					disabled={this.props.disabled}
					placeholder={LocalizationUtils.getPlaceholder(this.props)}
					label={this.getLabel()}
					title={LocalizationUtils.getTooltip(this.props)}
					bsStyle={this.getStyle()}
					help={this.getInfo()}
					onChange={this.onInputChange}
					onKeyDown={this.onKeyDown}
					onBlur={this.onBlur}
					autoComplete={'off'} />
			</div>
		);
	},
	
	onInputChange: function(event) {
		// we use timer for render optimalization
		if(this.state.timer) {
			clearTimeout(this.state.timer);
		}
		
		var value = event.target.value;
		
		this.setState({
			value: value,
			timer: setTimeout(function() {
				this.valueChanged(value);
			}.bind(this), 250)
		});
	},
	
	onKeyDown: function(event) {
		if(event.keyCode == 13) {
			this.valueChanged(event.target.value);
		}
		
		if(this.props.onKeyDown) {
			this.props.onKeyDown(event);
		}
	},
	
	onBlur: function(event) {
		this.valueChanged(event.target.value);
	}
});
