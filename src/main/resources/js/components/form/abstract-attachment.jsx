var api = require('./../../rest/attachment.jsx');

/**
* Class represents common attachments methods.
*/
SW.Form.AbstractAttachment = {
	onDragLeave: function (e) {
		this.setState({
			isDragActive: false
		});
	},
	
	onDragOver: function (e) {
		e.preventDefault();
		e.dataTransfer.dropEffect = 'copy';
		
		this.setState({
			isDragActive: true
		});
	},
	
	onDrop: function (e) {
		e.preventDefault();
		
		this.setState({
			isDragActive: false
		});
		
		var files;
		if (e.dataTransfer) {
			files = e.dataTransfer.files;
		} else if (e.target) {
			files = e.target.files;
		}
		
		var uploading = this.state.uploading.slice();
		var maxFiles = (this.props.multiple) ? files.length : 1;
		for (var i = 0; i < maxFiles; i++) {
			files[i].preview = URL.createObjectURL(files[i]);
			uploading.push(files[i]);
		}
		
		this.setState({
			'uploading': uploading,
			'uploaded': []
		});
		
		for (var i = 0; i < maxFiles; i++) {
			this.upload(files[i]);
		}
	},
	
	onClick: function () {
		this.open();
	},
	
	open: function() {
		var dom =  ReactDOM.findDOMNode(this);
		var fileInput = this.refs.fileInput;
		var fileInputNode = ReactDOM.findDOMNode(fileInput);
		fileInputNode.value = null;
		fileInputNode.click();
	},
	
	upload: function(file) {
		api.upload(this.props.rest, file, function(e) {
			if(this.props.onUploadStarted) {
				this.props.onUploadStarted();
			}
			
			var percent = 100/e.total*e.loaded;
			var uploading = this.state.uploading;
			for(var i = uploading.length - 1; i >= 0; i--) {
				if (uploading[i].name == file.name) {
					uploading[i].percent = e.percent;
				}
			}
			this.setState({
				'uploading': uploading
			});
		}.bind(this), function(err, res) {
			if(this.props.onUploadFinished) {
				this.props.onUploadFinished();
			}
			
			var body = res.body;
			if (err) {
				if (body && body.length>0 && body[0].humanMessageKey) {
					notification.showLocalizedMessage('error', body[0].humanMessageKey, file.name);
				} else {
					console.log("error", file.name, err);
					notification.showLocalizedMessage('error', 'form-attachment-upload-failed', file.name);
				}
			}
			
			var response = !err ? res.body : {
				'name': file.name
			};
			
			//remove file form array of files to upload
			var uploading = this.state.uploading;
			for(var i = uploading.length - 1; i >= 0; i--) {
				if (uploading[i].name == response.name) {
					uploading.splice(i, 1);
				}
			}
			
			//add file to array of uploaded files
			var uploaded = this.state.uploaded;
			
			if (!err) {
				uploaded.push(response);
			}
			
			this.setState({
				'uploading': uploading,
				'uploaded': uploaded
			});
			
			//when all files were uploaded, then notify valueChanged with new array
			if (uploading.length == 0)  {
				var files = this.props.value ? this.props.value.slice() : [];
				var newFiles = [];
				for(var i = uploaded.length - 1; i >= 0; i--) {
					files.push(uploaded[i]);
					newFiles.push(uploaded[i]);
				}
				this.attachmentsValueChanged(files, newFiles);
			}
		}.bind(this));
	},
	
	dropzoneDiv: function() {
		var className = this.props.className || 'dropzone';
		if (this.state.isDragActive) {
			className += ' active';
		}
		
		var style = this.props.style || {
			borderWidth: 1,
			borderStyle: this.state.isDragActive ? 'solid' : 'dashed',
			backgroundColor: this.state.uploading.length == 0 ? (this.state.isDragActive ? 'aliceblue' : '') : '#eee',
			padding: 20,
			marginBottom: 10
		};
		
		var label =
		<span className='dropzone-text'>
			<Glyphicon glyph='upload' />
			{' '+i18n.t(
				this.state.uploading.length == 0 ?
				(this.props.titleKey ? this.props.titleKey : (this.props.multiple ? 'form-attachment-dropfiles' : 'form-attachment-dropfile')) :
				'form-attachment-uploading'
			)}
		</span>
		
		return this.props.disabled ? null : React.createElement('div', {
			className: className,
			style: style,
			onClick: this.state.uploading.length == 0 ? this.onClick : null,
			onDragLeave: this.state.uploading.length == 0 ? this.onDragLeave : null,
			onDragOver: this.state.uploading.length == 0 ? this.onDragOver : null,
			onDrop: this.state.uploading.length == 0 ? this.onDrop : null
		},
		React.createElement('input', {
			style: {display: 'none'},
			type: 'file',
			multiple: this.props.multiple,
			ref: 'fileInput',
			onChange: this.onDrop,
			accept: this.props.accept
		}),
		label,
		null
	);
},
};
