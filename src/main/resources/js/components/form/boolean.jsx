/**
* Class represents input/output boolean property component of record.
*/
SW.Form.Boolean = React.createClass({
	
	displayName: 'SW.Form.Boolean',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {		
		/**
		* Key down handling (for example for submitting form by enter)
		*/
		onKeyDown: React.PropTypes.func,
		
		/**
		* Show null value flag (2-valued boolean or 3-valued boolean; default is true/3-valued boolean)
		*/
		showNull: React.PropTypes.bool,
	}),
	
	getDefaultProps: function() {
		return {
			showNull: true
		};
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		if(!this.props.showNull && (this.props.value === undefined || this.props.value === null)) {
			console.error('Property \"' + this.props.property + '\" does not show null item but has undefined or null value.');
		}
		
		if(this.props.showNull) {
			return (
				<div className={this.props.className}>
					<OverlayTrigger placement='top' overlay={<Tooltip id='x'><strong>Holy guacamole!</strong> Check this info.</Tooltip>}>
						<Input label={this.getLabel()} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName='wrapper'>
							<Row title={LocalizationUtils.getTooltip(this.props)} style={{marginLeft: -12}}>
								<Col xs={4} style={{paddingTop: 6, paddingBottom: 6, paddingLeft: 12, paddingRight: 12}}>
									<input type='radio' name={this.props.property} disabled={this.props.disabled} checked={this.props.value === undefined || this.props.value === null} onChange={this.onRadioChange.bind(this, null)}
										onKeyDown={this.props.onKeyDown ? this.props.onKeyDown : null}/>
									
									{' ' + i18n.t('null')}
								</Col>
								
								<Col xs={4} style={{paddingTop: 6, paddingBottom: 6, paddingLeft: 12, paddingRight: 12}}>
									<input type='radio' name={this.props.property} disabled={this.props.disabled} checked={this.props.value === true} onChange={this.onRadioChange.bind(this, true)}
										onKeyDown={this.props.onKeyDown ? this.props.onKeyDown : null}/>
									
									{' ' + i18n.t('true')}
								</Col>
								
								<Col xs={4} style={{paddingTop: 6, paddingBottom: 6, paddingLeft: 12, paddingRight: 12}}>
									<input type='radio' name={this.props.property} disabled={this.props.disabled} checked={this.props.value === false} onChange={this.onRadioChange.bind(this, false)}
										onKeyDown={this.props.onKeyDown ? this.props.onKeyDown : null}/>
									
									{' ' + i18n.t('false')}
								</Col>
							</Row>
						</Input>
					</OverlayTrigger>
				</div>
			);
		} else {
			return (
				<div className={this.props.className}>
					<Input label='&nbsp;' bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName="wrapper">
						<Input type='checkbox' checked={this.props.value} label={this.getLabel()} disabled={this.props.disabled} title={LocalizationUtils.getTooltip(this.props)} onChange={this.onCheckboxChange}/>
					</Input>
				</div>
			);
		}
	},
	
	onRadioChange: function(value) {
		this.valueChanged(value);
	},
	
	onCheckboxChange: function(event) {
		this.valueChanged(event.target.checked);
	}
});
