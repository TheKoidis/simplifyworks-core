/**
* Class represents input/output entity property component of record.
*/
SW.Form.Entity = React.createClass({
	
	displayName: 'SW.Form.Entity',
	
	/**
	* SW.Form.Property
	*/
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Function responsible for rendering current value (format render(value))
		*/
		render: React.PropTypes.func.isRequired,
		
		/**
		* There have to be exactly one entity list as child. This list is used for selection.
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.List.EntityList': 'single-required'
		}),
	}),
	
	getInitialState: function() {
		return {
			showModal: false,
			validationResult: null
		};
	},
	
	close: function() {
		this.setState({ showModal: false });
	},
	
	open: function() {
		this.setState({ showModal: true });
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		var buttons = [];
		
		buttons.push(
			<Button key='list' onClick={this.open} disabled={this.props.disabled} title={i18n.t('click-to-show-list')}><Glyphicon glyph="list"/></Button>
		);
		
		if (this.props.value) {
			buttons.push(
				<Button key='remove' disabled={this.props.disabled} onClick={this.chooseFunc.bind(this, null)} title={i18n.t('click-to-clear')}><Glyphicon glyph="remove" /></Button>
			);
		}
		
		return (
			<div className={this.props.className}>
				<Input
					type='text'
					value={this.props.value ? this.props.render(this.props.value) : ''}
					disabled={true}
					placeholder={LocalizationUtils.getPlaceholder(this.props)}
					label={this.getLabel()}
					title={LocalizationUtils.getTooltip(this.props)}
					bsStyle={this.getStyle()}
					help={this.getInfo()}
					className={!this.props.disabled ? 'entity-enabled' : null}
					buttonAfter={!this.props.disabled ? buttons : null}/>
				
				{this.state.showModal ? ComponentUtils.cloneChildren(this.props.children, 'SW.List.EntityList', {close: this.close, showModal: this.state.showModal, chooseFunc: this.chooseFunc}) : null}
			</div>
		);
	},
	
	chooseFunc: function(value) {
		this.valueChanged(value);
	}
});
