/**
* Class represents input/output entities property component of record.
*/
SW.Form.Entities = React.createClass({
	
	displayName: 'SW.Form.Entities',
	
	mixins: [SW.Form.Property],
	
	propTypes: PropertyUtils.deepMerge(SW.Form.Property.propTypes , {
		/**
		* Unique property for selection
		*/
		uniqueProperty: React.PropTypes.string,
		
		/**
		* There are two ways how represent multiple entities - using list with table or list with tags. Entity list is used for selection.
		*/
		children: ComponentUtils.checkChildrenStructure([{
			'SW.List.EntityList': 'single-required',
			'SW.Form.EntitiesList': 'single-required'
		}, {
			'SW.List.EntityList': 'single-required',
			'SW.Form.EntitiesTagList': 'single-required'
		}]),
	}),
	
	getDefaultProps: function() {
		return {
			uniqueProperty: 'id'
		}
	},
	
	getInitialState: function() {
		return {
			showModal: false
		};
	},
	
	close: function() {
		this.setState({
			showModal: false
		});
	},
	
	open: function(value, index) {
		this.setState({
			showModal: true,
			index: index
		});
	},
	
	render: function() {
		if (this.props.hidden) {
			return null;
		}
		return (
			<div className={this.props.className}>
				<Input label={this.getLabel()} title={LocalizationUtils.getTooltip(this.props)} bsStyle={this.getStyle()} help={this.getInfo()} wrapperClassName='wrapper'>
					{ComponentUtils.findChild(this.props.children, 'SW.Form.EntitiesList')
						?	ComponentUtils.cloneChild(this.props.children, 'SW.Form.EntitiesList', {
							value: this.props.value,
							
							createValue: this.open,
							removeValue: this.removeValue,
							
							disabled: this.props.disabled
						})
						:	ComponentUtils.cloneChild(this.props.children, 'SW.Form.EntitiesTagList', {
							value: this.props.value,
							
							createValue: this.open,
							removeValue: this.removeValue,
							
							disabled: this.props.disabled
						})
					}
					
					{this.state.showModal ? ComponentUtils.cloneChildren(this.props.children, 'SW.List.EntityList', {close: this.close, showModal: this.state.showModal, chooseFunc: this.chooseFunc}) : null}
				</Input>
			</div>
		);
	},
	
	chooseFunc: function(value) {
		if(value) {
			var values = [].concat(this.props.value);
			// TODO unique property works only in one level (object.property1.property2 is not working)
			var uniqueProperty = this.props.uniqueProperty;
			
			var ids = this.props.value ? this.props.value.map(function(element, index, array) {
				return element[uniqueProperty];
			}) : [];
			
			if (ids.indexOf(value[uniqueProperty])>-1) {
				notification.showLocalizedMessage('error', 'validation-duplicate');
				return;
			}
			
			values[this.state.index] = value;
			
			this.valueChanged(values);
		}
	},
	
	removeValue: function(value, index) {
		var newValue = this.props.value.filter(function(element, index, array) {
			return element !== value;
		});
		
		this.valueChanged(newValue);
	}
});
