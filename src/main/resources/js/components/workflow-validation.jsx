
SW.WorkflowValidation = React.createClass({
	
	displayName: 'SW.WorkflowValidation',
	
	propTypes: {
		data: React.PropTypes.object,
		skipValidationFunc: React.PropTypes.func,
		closeFunc: React.PropTypes.func.isRequired
	},
	
	renderAlert: function(validationFails, type) {
		var alert = validationFails.filter(function(value) {return value.type == (type=='danger' ? 'MANDATORY' : 'OPTIONAL')}).map(function(variable, i) {
			return (
				<li key={type+i}>{i18n.t(variable.humanMessageKey, variable.params)}</li>
			)
		});
		
		if (!alert || alert.length==0) {
			return null;
		}
		
		return (
			<Alert bsStyle={type} key={type}>
				<ul>
					{alert}
				</ul>
			</Alert>
		);
	},
	
	render: function() {
		if (this.props.data) {
			if (this.props.data.state == 'warnings' || this.props.data.state == 'errors') {
				return (
					<Modal show={true} bsSize="large" backdrop="static" onHide={this.props.closeFunc}>
						<Modal.Header closeButton>
							<Modal.Title>{i18n.t('check-result')}</Modal.Title>
						</Modal.Header>
						<Modal.Body>
							{this.renderAlert(this.props.data.validationFails, 'danger')}
							{this.renderAlert(this.props.data.validationFails, 'warning')}
						</Modal.Body>
						<Modal.Footer>
							{(this.props.skipValidationFunc && this.props.data.state == 'warnings') ?	(<Button onClick={this.props.skipValidationFunc}>{i18n.t('ignore-warning')}</Button>) : null}
							<Button onClick={this.props.closeFunc}>{i18n.t('close')}</Button>
						</Modal.Footer>
					</Modal>
				)
			}
		}
		
		return null;
	}
});
