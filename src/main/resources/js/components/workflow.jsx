var api = require('./../rest/workflow.jsx');

SW.Workflow = React.createClass({
	
	displayName: 'SW.Workflow',
	
	mixins: [FluxMixin, StoreWatchMixin('workflow', 'list')],
	
	contextTypes: {
		flux: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		objectType: React.PropTypes.string.isRequired,
		objectGroup: React.PropTypes.string,
		objectId: React.PropTypes.string,
		
		objectPath: React.PropTypes.string,
	},
	
	componentDidMount: function() {
		this.init();
	},
	
	componentDidUpdate: function(prevProps, prevState) {
		if(prevProps.objectType != this.props.objectType || prevProps.objectGroup != this.props.objectGroup) {
			this.init();
		}
	},
	
	componentWillReceiveProps: function(nextProps) {
		if(nextProps.objectType != this.props.objectType || nextProps.objectGroup != this.props.objectGroup) {
			this.setState(this.context.flux.store('workflow').getWorkflow(nextProps));
		}
	},
	
	getInitialState: function() {
		return {
			taskDefinitionKey: ''
		};
	},
	
	getStateFromFlux: function() {
		return {
			workflow: this.context.flux.store('workflow').getWorkflow(this.props.objectType),
			list: this.context.flux.store('list').getList(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId))
		};
	},
	
	init: function() {
		this.context.flux.actions.workflow.searchTaskDefinitions(this.props.objectType);
	},
	
	render: function() {
		return (
			<div>
				<Grid fluid>
					<Row>
						<Col xs={12} sm={4}>
							<Input type='select' value={this.state.taskDefinitionKey} onChange={this.onTaskDefinitionChange}>
								<option key='null' value=''>
									Všechny úkoly
								</option>
								
								{this.state.workflow.status === 'success'
									?	this.state.workflow.taskDefinitions.map(function(taskDefinition) {
										return (
											<option key={taskDefinition.id} value={taskDefinition.id}>
												{taskDefinition.name}
											</option>
										)
									})
									:	null
								}
							</Input>
						</Col>
						
						<Col xs={12} sm={8}>
							<ButtonToolbar>
								{this.state.taskDefinitionKey
									?	this.state.workflow.taskDefinitions.find(function(taskDefinition) {
										return taskDefinition.id === this.state.taskDefinitionKey;
									}.bind(this)).actions.map(function(action) {
										var selection = this.state.list.selection;
										
										return WorkflowUtils.renderAction(action, !selection || selection.length === 0, this.onActionClick.bind(this, action.id));
									}.bind(this))
									:	null
								}
							</ButtonToolbar>
						</Col>
					</Row>
				</Grid>
				
				<SW.List name={WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId)} restPrefix={WorkflowUtils.getRest(this.props.objectType, this.props.objectGroup, this.props.objectId)}>
					<SW.List.Table>
						<SW.List.Selection property='id' width='2.5%' />
						<SW.List.Datetime property='created' labelKey='workflowTaskCreated' width='10%' orderable={false} />
						<SW.List.Boolean property='mine' labelKey='workflowTaskMine' width='5%' orderable={false} />
						<SW.List.String property='definition.name' labelKey='workflowTaskDefinitionName' width='25%' orderable={false} />
						<SW.List.String property='subject' labelKey='workflowTaskSubject' orderable={false} path={this.props.objectPath} pathProperty='objectId' />
					</SW.List.Table>
					
					<SW.List.Pagination />
					<SW.List.Info />
				</SW.List>
			</div>
		);
	},
	
	onTaskDefinitionChange: function(event) {
		var taskDefinitionKey = event.target.value;
		
		this.setState({
			taskDefinitionKey: taskDefinitionKey
		}, function() {
			if(taskDefinitionKey && !this.context.flux.store('list').getList(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId)).selection.every(function(element) {
				return element.definition.id === taskDefinitionKey;
			})) {
				// we deselect all in case when some task has different definition
				this.context.flux.actions.list.selection.deselectAll(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId));
			}
			
			this.context.flux.actions.list.filter.changeValue(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId), 'definitionKey', taskDefinitionKey ? taskDefinitionKey : null);
			this.context.flux.actions.list.common.reload(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId), this.context.flux.store('list').getList(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId)).options);
		});
	},
	
	onActionClick: function(actionId) {
		var taskIds = this.state.list.selection.map(function(resource) {
			return resource.id;
		});
		
		this.context.flux.actions.workflow.completeTasks(this.props.objectType, taskIds, actionId, function(){
			this.context.flux.actions.list.selection.deselectAll(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId));
			this.context.flux.actions.list.common.reload(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId), this.context.flux.store('list').getList(WorkflowUtils.getName(this.props.objectType, this.props.objectGroup, this.props.objectId)).options)
		}.bind(this));
	},
	
	createActivitiesGroup: function() {
		if(this.state.status === 'success') {
			return (
				<SW.List.ActivitiesGroup>
					{this.state.taskDefinitions.map(function(taskDefinition) {
						return (
							<SW.List.ActivitiesMenu key={taskDefinition.id} labelKey={taskDefinition.name}>
								{taskDefinition.actions.map(function(action) {
									return (
										<SW.List.Action key={action.id} labelKey={action.name} isEnabled={this.isActionEnabled.bind(this, taskDefinition.id)} execute={this.executeAction.bind(this, action.id)}/>
									);
								}.bind(this))}
							</SW.List.ActivitiesMenu>
						);
					}.bind(this))}
				</SW.List.ActivitiesGroup>
			);
		} else {
			return null;
		}
	},
});
