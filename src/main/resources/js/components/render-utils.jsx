var RenderUtils = {
	
	// returns span with detail title
	renderDetailTitle: function(objectType, id, title) {
		return (
			<span>
				{id !== 'new' ? i18n.t(objectType + '-detail', {title: title}) : i18n.t(objectType + '-new')}
			</span>
		);
	},
	
	// returns size of file in appropriate units
	getFileSize: function(bytes, scale) {
		if (bytes===undefined || bytes===null) {
			return null;
		}
		
		var pow = scale ? Math.pow(10, scale) : 1;
		
		var num = bytes/1000000000;
		var unit = 'GB';
		
		if (bytes < 1000) {
			num = bytes;
			unit = 'B';
		} else if (bytes < 1000000) {
			num = bytes/1000;
			unit = 'kB';
		} else if (bytes < 1000000000) {
			num = bytes/1000000;
			unit = 'MB';
		}
		
		return NumberUtils.format(Math.round(num*pow)/pow) +' '+unit;
	},
}

module.exports = RenderUtils;
