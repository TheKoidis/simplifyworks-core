/**
*	This class contains all localization related library functions - i.e. mainly functions
*	for label/tooltip/help localization.
*/
var LocalizationUtils = {
	// Returns label text using priority algorithm (label > labelKey > keyPrefix + property > property).
	getLabelText: function(props, defaultLabel) {
		if(props.label) {
			return props.label;
		} else if(props.labelKey) {
			return i18n.t(props.labelKey);
		} else if(props.keyPrefix) {
			return i18n.t(props.keyPrefix + '-' + props.property);
		} else if(defaultLabel){
			return i18n.t(defaultLabel);
		} else {
			console.warn('No label/labelKey/keyPrefix defined, using property');
			return props.property;
		}
	},
	
	// Returns tooltip using priority algorithm (tooltip > tooltipKey > keyPrefix + property).
	getTooltip: function(props){
		if(props.tooltip) {
			return props.tooltip;
		} else if(props.tooltipKey) {
			return i18n.t(props.tooltipKey);
		} else if(props.keyPrefix && i18n.exists(props.keyPrefix + '-' + props.property + '-tooltip')) {
			return i18n.t(props.keyPrefix + '-' + props.property + '-tooltip');
		} else {
			return null;
		}
	},
	
	// Returns tab text with priority label > labelKey
	getTabText: function(props) {
		return props.label ? props.label : i18n.t(props.labelKey);
	},
	
	// Returns labelKey with priority labelKey > labelPrefix > name
	getLabelKey: function(props) {
		if (props.labelKey) {
			return props.labelKey;
		} else {
			return (props.labelKeyPrefix ? props.labelKeyPrefix : props.name) + '-' + props.property.replace(/[.]/g, '-');
		}
	},
	
	// Returns help using priority algorithm (help > helpKey > keyPrefix + property).
	getHelp: function(props) {
		if(props.help) {
			return props.help;
		} else if(props.helpKey) {
			return i18n.t(props.helpKey)
		} else if(props.keyPrefix && i18n.exists(props.keyPrefix + '-' + props.property + '-help')) {
			return i18n.t(props.keyPrefix + '-' + props.property + '-help')
		} else {
			return null;
		}
	},
	
	// Returns placeholder using priority algorithm (placeholder > placeholderKey > keyPrefix + property).
	getPlaceholder: function(props) {
		if(props.placeholder) {
			return props.placeholder;
		} else if(props.placeholderKey) {
			return i18n.t(props.placeholderKey);
		} else if(props.keyPrefix && i18n.exists(props.keyPrefix + '-' + props.property + '-placeholder')) {
			return i18n.t(props.keyPrefix + '-' + props.property + '-placeholder');
		} else {
			return null;
		}
	},
	
	
}

module.exports = LocalizationUtils;
