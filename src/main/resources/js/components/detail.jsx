/**
*	Detail represents detailed view on some agenda. View is supposed to be complicated so the tabs are provided.
*/
SW.Detail = React.createClass({
	
	displayName: 'SW.Detail',
	
	mixins: [ OnUnload, FluxMixin, StoreWatchMixin('form') ],
	
	contextTypes: {
		/**
		*	Flux context
		*/
		flux: React.PropTypes.object.isRequired,
		
		/**
		* React router
		*/
		router: React.PropTypes.object.isRequired,
		
		location: React.PropTypes.object.isRequired,
		
		/**
		* Current route (to handle transitions from unsaved detail)
		*/
		route: React.PropTypes.object.isRequired
	},
	
	propTypes: {
		/**
		* Path where to go on detail exit
		*/
		exitPath: React.PropTypes.string,
		
		rest: React.PropTypes.string.isRequired,
		
		restId: React.PropTypes.string.isRequired,
		
		disabled: React.PropTypes.bool,
		
		/**
		* Function responsible for validation ('validate(data, callback)' where 'callback(isValid)')
		*/
		validate: React.PropTypes.func,
		
		/**
		* Show delete button
		*/
		removable: React.PropTypes.bool,
		
		/**
		*	Children structure
		*/
		children: ComponentUtils.checkChildrenStructure({
			'SW.Form': 'multiple-optional',
			'SW.List': 'multiple-optional',
			'SW.Workflow': 'single-optional',
			'SW.WorkflowSimple': 'single-optional',
			'SW.Report': 'multiple-optional',
			'SW.Validate': 'single-optional',
			'SW.CustomTab': 'multiple-optional'
		})
	},
	
	getDefaultProps: function(){
		return {
			removable: true
		};
	},
	
	getStateFromFlux: function() {
		return this.context.flux.store('form').getDetail(this.getRestPrefix());
	},
	
	componentDidMount: function() {
		if (this.props.restId === 'new') {
			this.init();
		}
		
		if (this.props.restId !== 'new' && this.state.status !== 'locked' && !this.props.disabled) {
			this.lock();
		}
		
		if (!this.props.disabled) {
			this.context.router.setRouteLeaveHook(this.context.route, this.routerWillLeave);
		}
	},
	
	componentDidUpdate: function(prevProps, prevState) {
		if (this.props.rest !== prevProps.rest || this.props.restId !== prevProps.restId) {
			if (this.props.restId === 'new') {
				this.init();
			}
			
			if (this.props.restId !== 'new' && this.state.status !== 'locked' && !this.props.disabled) {
				this.lock();
			}
		}
	},
	
	init: function() {
		this.context.flux.actions.form.detailInit(this.getRestPrefix());
	},
	
	lock: function() {
		this.context.flux.actions.form.lock(this.getRestPrefix(), function() {
			this.startHoldlockTimer();
		}.bind(this));
	},
	
	startHoldlockTimer: function() {
		var timer = setInterval(function() {
			this.context.flux.actions.form.holdLock(this.getRestPrefix());
		}.bind(this), FormConstants.HOLD_LOCK_REFRESH_INTERVAL);
		
		this.setState({holdLockTimer: timer});
	},
	
	unlock: function(callback) {
		clearInterval(this.state.holdLockTimer);
		this.context.flux.actions.form.unlock(this.getRestPrefix(), callback);
	},
	
	selectTab: function(tab) {
		if (tab === undefined || tab == this.props.tabName) {
			return;
		}
		
		var childTab = this.findChildTab();
		var tabName = this.props.tabName == 'default' ? '' : this.props.tabName;
		if (childTab.type.displayName=='SW.Form') {
			this.context.flux.actions.form.refreshValidation(this.getRestPrefix(), tabName, ValidationUtils.getValidationFromProps(childTab.props));
		}
		
		//regex replaces url ending (after last /) with tab
		var url = this.context.location.pathname.replace(/\/[^\/]*$/,'/'+tab);
		this.context.router.push(url);
	},
	
	routerWillLeave: function(nextLocation) {
		var form = this.context.flux.store('form').getDetail(this.getRestPrefix());
		if(!this.isMounted()){
			// Lock might be called once after unlock from manual log out
			clearInterval(this.state.holdLockTimer);
			return true;
		}
		if (this.context.location.pathname.split(/\/[^\/]*$/)[0] != nextLocation.pathname.split(/\/[^\/]*$/)[0]){
			if (form.modified) {
				notification.showLocalizedMessage('warning', 'form-changes-found', 'form-save-or-discard-changes', {}, {
					label: i18n.t('form-leave-changes'),
					callback: function() {
						this.unlock(function() {
							this.context.router.push(nextLocation);
						}.bind(this));
					}.bind(this)
				}, 0);
				
				return false;
			} else if (form.status === 'locked') {
				this.unlock(function() {
					this.context.router.push(nextLocation);
				}.bind(this));
				
				return false;
			}
			
			// Lock might be called once after unlock from manual log out
			clearInterval(this.state.holdLockTimer);
		}
		return true;
		
	},
	
	onUnload: function() {
		if (this.state.status === 'locked') {
			clearInterval(this.state.holdLockTimer);
		}
	},
	
	onBeforeUnload: function(e) {
		if (!this.state.modified) {
			return false;
		}
		
		var confirmationMessage = i18n.t('form-unload-changes');
		
		(e || window.event).returnValue = confirmationMessage; //Gecko + IE, Chrome(mac os)
		return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.*/
	},
	
	exit: function() {
		this.context.router.push(this.props.exitPath);
	},
	
	findChildTab: function() {
		var tabName = this.props.tabName == 'default' ? '' : this.props.tabName;
		var children = this.props.children instanceof Array ? this.props.children : [this.props.children];
		for (var child in children){
			if(children[child]){
				if(children[child].props.tabName == tabName || children[child].props.rest == tabName){
					return children[child];
				}
			}
		}
		return undefined;
	},
	
	save: function() {
		var childTab = this.findChildTab();
		var tabName = this.props.tabName == 'default' ? '' : this.props.tabName;
		if (childTab.type.displayName=='SW.Form') {
			this.context.flux.actions.form.refreshValidation(this.getRestPrefix(), tabName, ValidationUtils.getValidationFromProps(childTab.props, childTab.props.labelKeyPrefix));
		}
		
		this.context.flux.actions.form.save(this.getRestPrefix(), null, null, childTab.type.displayName=='SW.Form' ? tabName : null, this.props.validate, childTab.type.displayName=='SW.Form');
	},
	
	create: function() {
		var childTab = this.findChildTab();
		var tabName = this.props.tabName == 'default' ? '' : this.props.tabName;
		this.context.flux.actions.form.refreshValidation(this.getRestPrefix(), tabName, ValidationUtils.getValidationFromProps(childTab.props, childTab.props.labelKeyPrefix));
		
		this.context.flux.actions.form.create(this.getRestPrefix(), function(action, id) {
			if (id) {
				if (this.props.newCallback) {
					this.props.newCallback(action, id);
				}
			}
		}.bind(this), null, childTab.type.displayName=='SW.Form' ? tabName : null, this.props.validate);
	},
	
	getRestPrefix: function() {
		return this.props.restId !== 'new' ? this.props.rest + '/' + this.props.restId : this.props.rest;
	},
	
	delete: function() {
		notification.showLocalizedMessage('warning', 'delete-confirm', 'delete-confirm-detail-text', {}, {
			label: i18n.t('delete-confirm-button'),
			callback: function() {
				this.unlock(function() {
					this.context.flux.actions.form.delete(this.getRestPrefix(), function(type, result) {
						if (result === FormConstants.DETAIL_DELETE_SUCCESS) {
							this.exit();
						}
					}.bind(this));
				}.bind(this));
			}.bind(this)
		}, 0);
	},
	
	render: function() {
		var hasAdminRole = this.context.flux.store('security').hasRole('admin');
		
		var tabName = this.props.tabName;
		var childTab = this.findChildTab();
		return (
			<Grid fluid>
				{ComponentUtils.findChildren(this.props.children, ['SW.Form', 'SW.List', 'SW.Workflow', 'SW.CustomTab']).length > 1
					?
					<Row>
						<Col xs={12}>
							<Nav bsStyle='tabs' activeKey={this.props.tabName} onSelect={this.selectTab} style={{marginBottom: 15}}>
								{ComponentUtils.findChildren(this.props.children, ['SW.Form', 'SW.List', 'SW.Workflow', 'SW.CustomTab']).map(function(element, index, array) {
									var tooltip = i18n.exists(element.props.labelKey+'-tooltip') ? i18n.t(element.props.labelKey+'-tooltip') : '';
									
									var validationFailed = false;
									if (this.state.forms[element.props.rest]) {
										for (var key in this.state.forms[element.props.rest].validation) {
											if (this.state.forms[element.props.rest].validation[key].result) {
												validationFailed = true;
											}
										}
									}
									
									return (
										<NavItem key={'detail-tab-' + index} eventKey={element.props.tabName ? element.props.tabName : element.props.rest} title={tooltip}>
											{element.props.icon	?
												<span><Glyphicon glyph={element.props.icon} />{' '}</span>
												:   null
											}
											
											{LocalizationUtils.getTabText(element.props)}
											
											{
												validationFailed || (this.state.validation[element.props.tabName ? element.props.tabName : element.props.rest])
												?
												<span>{' '}<Glyphicon glyph='warning-sign' bsStyle='warning' bsSize='xsmall'/></span>
												: null
											}
										</NavItem>
									);
								}.bind(this))}
							</Nav>
						</Col>
					</Row>
					:
					null
				}
				
				<Row>
					<Col xs={12}>
						{ComponentUtils.cloneChild(childTab, ['SW.Form', 'SW.List', 'SW.Workflow', 'SW.CustomTab'],
							{ref: 'currentTab', restPrefix: this.getRestPrefix(), new: this.props.restId === 'new', locked: this.state.status === 'locked'})
						}
					</Col>
				</Row>
				
				<Row>
					<Col xs={12}>
						<div style={{float: 'right', height: '64px'}} >
							&nbsp;
						</div>
						
						<div style={{position: 'fixed', right: '0px', bottom: '0px', backgroundColor: 'white', width: '100%', zIndex: '100'}}>
							<div style={{float: 'left', padding: '15px'}}>
								{(!this.props.disabled && this.props.removable && this.props.restId !== 'new')
									?
									<Button bsStyle='default' title={i18n.t('form-delete-tooltip')} onClick={this.delete} style={{marginLeft: '5px'}}>
										<Glyphicon glyph='trash' />
									</Button>
									: null
								}
								
								{ComponentUtils.cloneChild(this.props.children, ['SW.WorkflowSimple'],	{restPrefix: this.getRestPrefix(), new: this.props.restId === 'new', locked: this.state.status === 'locked', disabled: this.state.modified, detail: this})}
								
								{this.props.restId !== 'new' ? ComponentUtils.cloneChildren(this.props.children, ['SW.Report'],
									{id: this.props.restId, showXml: hasAdminRole, disabled: this.state.modified}) : null
								}
								
								{ComponentUtils.cloneChild(this.props.children, ['SW.Validate'], {childTab: this.findChildTab(), tabName: this.props.tabName, validate: this.props.validate, restPrefix: this.getRestPrefix(), disabled: this.state.modified})}
							</div>
							<div style={{textAlign: 'center'}}>
								
							</div>
							<div style={{float: 'right', padding: '15px'}}>
								{!this.props.disabled ?
									(
										!this.state.modified ?
										(
											this.state.status !== 'locked' && this.state.status !== 'new' && this.state.status !== 'processing'?
											<span>
												<Glyphicon glyph='lock' />
												{' ' + i18n.t('form-read-only')}
												<Button bsStyle='link' onClick={this.lock} style={{marginLeft: '5px'}}>
													<Glyphicon glyph='pencil' />
													{' ' + i18n.t('form-edit')}
												</Button>
											</span>
											:
											<span><Glyphicon glyph='pencil' />{' ' + i18n.t('form-no-changes')}</span>
										)
										:
										<span><Glyphicon glyph='pencil' />{' ' + i18n.t('form-changes')}</span>
									)
									:
									null
								}
								
								{this.props.exitPath ?
									<Button bsStyle='link' onClick={this.exit} style={{marginLeft: '5px'}}>
										<Glyphicon glyph='remove' />
										{' ' + i18n.t('exit')}
									</Button>
									: null
								}
								
								{this.props.restId === 'new' ?
									<Button bsStyle='primary' onClick={this.create} style={{marginLeft: '5px'}}>
										<Glyphicon glyph='floppy-disk' />
										{' ' + i18n.t('form-create')}
									</Button>
									:
									this.state.modified ?
									<Button bsStyle='primary' onClick={this.save} style={{marginLeft: '5px'}}>
										<Glyphicon glyph='floppy-disk' />
										{' ' + i18n.t('form-save-changes')}
									</Button>
									:
									null
								}
							</div>
						</div>
					</Col>
				</Row>
			</Grid>
		);
	},
});
