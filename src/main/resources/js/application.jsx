require('./globals.jsx');

/**
* Application represents entry point (it also merges core with modules).
*/
var Application = {
	
	/**
	* Initializes flux and router.
	*/
	init: function() {
		console.log('Initializing application ...');
		
		if(!global.modules) {
			global.modules = [];
			console.error("Modules not set!");
		}
		
		if(!global.customization) {
			global.customization = {};
			console.warn("Customization not set. Empty is being used.");
		}
		
		this.initI18N(this.initRouter.bind(this, this.initFlux()));
		console.log('Application initialized');
	},
	
	/**
	* Initializes internationalization with merged localization resources.
	*/
	initI18N: function(callback) {
		var resources = {};
		
		PropertyUtils.deepMerge(require('./localization/localization.jsx'), resources);
		
		for(var index = 0; index < global.modules.length; index++) {
			PropertyUtils.deepMerge(global.modules[index].localization, resources);
		}
		
		i18n.init({
			lng: 'cs',
			resources: resources,
			interpolation: {
				escapeValue: false,
				format: function(value, format, lng) {
					if (format == 'number') return NumberUtils.format(value);
					return value;
				}
			}
		}, function() {
			callback();
		});
	},
	
	/**
	* Initializes flux with merged actions and stores.
	*/
	initFlux: function() {
		var actions = require('./actions/all.jsx');
		var stores = require('./stores/all.jsx');
		
		for(var index = 0; index < global.modules.length; index++) {
			PropertyUtils.deepMerge(global.modules[index].actions, actions);
			PropertyUtils.deepMerge(global.modules[index].stores, stores);
		}
		
		var flux = new Fluxxor.Flux(stores, actions);
		
		// fix for multiple action dispatching in componentDidMount callback (see http://fluxxor.com/guides/react.html)
		flux.setDispatchInterceptor(function(action, dispatch) {
			ReactDOM.unstable_batchedUpdates(function() {
				dispatch(action);
			});
		});
		
		flux.on('dispatch', function(type, payload) {
			if (console && console.log) {
				console.log('[Dispatch]', new Date().toISOString(), type, payload);
			}
		});
		
		//TODO move!!!!!!
		global['auth-token'] = flux.store('security').getAuthToken();
		//TODO move!!!!!!
		
		return flux;
	},
	
	/**
	* Wraps every component in wrapper (every component will have flux, route etc. in context).
	*/
	wrapElement: function(flux, Component, props) {
		var Wrapper = require('./wrapper.jsx');
		
		return (
			<Wrapper flux={flux} location={props.location} route={props.route}>
				<Component {...props} flux={flux} />
			</Wrapper>
		);
	},
	
	/**
	* Initializes router with flux and merged routes.
	*/
	initRouter: function(flux) {
		var SystemRoutes = require('./system-routes.jsx');
		var Template = require('./template.jsx');
		var Dashboard = global.customization.dashboard ? global.customization.dashboard : require('./views/dashboard.jsx');
		var NotFound = require('./views/not-found.jsx');
		
		var history = require('history');
		var browserHistory = useRouterHistory(history.createHistory)({
			basename: contextName
		});
		
		ReactDOM.render(
			(
				<Router history={browserHistory} createElement={this.wrapElement.bind(this, flux)}>
					<Route path='/' component={Template}>
						<IndexRoute components={{title: Dashboard.title, main: Dashboard.main}} />
						<Route path='dashboard' components={{title: Dashboard.title, main: Dashboard.main}} />
						
						{global.modules.map(function (module) {
							return module.routes;
						})}
						
						{SystemRoutes.systemRoute}
						
						<Route path='*' components={{title: NotFound.title, main: NotFound.main}} />
					</Route>
				</Router>
			),
			document.getElementById('root')
		);
	}
}

module.exports = Application;
