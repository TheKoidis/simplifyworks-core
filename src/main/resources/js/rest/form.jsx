var FormRest = {
	
	load: function(rest, success, failure) {
		request({
			method: 'GET',
			url: hostName + contextName + rest,
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(JSON.parse(body));
			} else {
				failure(error, response, body);
			}
		});
	},
	
	lock: function(rest, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/api/core/locks',
			qs: {name: rest},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(body);
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},
	
	unlock: function(rest, success, failure) {
		request({
			method: 'DELETE',
			url: hostName + contextName + '/api/core/locks',
			qs: {name: rest},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(body);
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},
	
	holdLock: function(rest, success, failure) {
		request({
			method: 'PUT',
			url: hostName + contextName + '/api/core/locks',
			qs: {name: rest},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(body);
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},
	
	create: function(rest, data, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + rest,
			json: data,
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && (response.statusCode == 200 || response.statusCode == 204)) {
				success(body);
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},
	
	save: function(rest, data, success, failure) {
		request({
			method: 'PUT',
			url: hostName + contextName + rest,
			json: data,
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && (response.statusCode == 200 || response.statusCode == 204)) {
				success(body);
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},
	
	delete: function(rest, success, failure) {
		request({
			method: 'DELETE',
			url: hostName + contextName + rest,
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function(error, response, body) {
			if (!error && (response.statusCode == 200 || response.statusCode == 204)) {
				success();
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	}
}

module.exports = FormRest;
