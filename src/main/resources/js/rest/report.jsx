var ReportRest = {
	
	getXml: function(rest, id, success, failure) {
		request({
			method: 'GET',
			url: hostName + contextName + rest,
			qs: {ids: id},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(JSON.parse(body));
			} else {
				failure(error);
			}
		});
	},
	
	generateReport: function(rest, template, id, type, name) {
		window.open(hostName + contextName + rest + '/generate?auth-token='+global['auth-token']+'&ids='+id+'&type='+type+'&template='+template+'&name='+name);
	},
}

module.exports = ReportRest;
