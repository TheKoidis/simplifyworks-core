var TaskRest = {
	
	loadTasks: function (success, failure) {
		request({
			method: 'GET',
			json: true,
			url: hostName + contextName + '/api/scheduler/tasks',
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(body._embedded.taskList);
			} else {
				failure(error);
			}
		});
	},
	
	saveOneTimeTrigger: function (taskName, description, time, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/api/scheduler/tasks/' + taskName + '/triggers/one-time',
			json: {type: 'OneTimeTaskTrigger', name: taskName, description: description, fireTime: time},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success();
			} else {
				failure(error);
			}
		});
	},
	
	saveCronTrigger: function (taskName, description, cron, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/api/scheduler/tasks/' + taskName + '/triggers/cron',
			json: {type: 'CronTaskTrigger', name: taskName, description: description, cron: cron},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success();
			} else {
				failure(error);
			}
		});
	},
	
	runNow: function (taskName, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/api/scheduler/tasks/' + taskName + '/triggers/one-time',
			json: {type: 'OneTimeTaskTrigger', fireTime: new Date(), name: taskName},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success();
			} else {
				failure(error);
			}
		});
	},
	
	changeTriggerState: function (taskName, triggerName, state, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/api/scheduler/tasks/' + taskName + '/triggers/' + triggerName + '/' + state,
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error) {
				success();
			} else {
				failure(error);
			}
		});
	},
	
	removeTrigger: function (taskName, triggerName, success, failure) {
		request({
			method: 'DELETE',
			url: hostName + contextName + '/api/scheduler/tasks/' + taskName + '/triggers/' + triggerName,
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error) {
				success();
			} else {
				failure(error);
			}
		});
	}
	
}

module.exports = TaskRest;
