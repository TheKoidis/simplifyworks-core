var ListRest = {

	reload: function(name, options, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + options.rest + '/filter',
			json: this.createSearchParameters(options),
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(name, options, {
					recordsReturned: body.recordsReturned,
					recordsFiltered: body.recordsFiltered,
					recordsTotal: body.recordsTotal,
					resources: body.resources
				});
			} else {
				failure(name, options, error);
			}
		});
	},

	createSearchParameters: function(options) {
		var params = {
			'sorts': options.ordering,
			'pageNumber': options.pageNumber,
			'pageSize': options.pageSize,
			'quickSearchTokens': options.quickSearch.tokens
		};

		var properties = this.createFilterProperties(options.filter);
		for (var i = 0; i < properties.length; i++) {
			var property = properties[i];
			params[property] = options.filter[property];
		}
		return params;
	},

	createFilterProperties: function(filter) {
		if (!filter) {
			return [];
		}

		var properties = [];
		Object.keys(filter).forEach(function(key, index) {
			properties.push(key);
		});
		return properties;
	},

	lock: function(rest, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/api/core/locks',
			qs: {name: rest},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success();
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},

	unlock: function(rest, success, failure) {
		request({
			method: 'DELETE',
			url: hostName + contextName + '/api/core/locks',
			qs: {name: rest},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success();
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},

	save: function(rest, data, success, failure) {
		request({
			method: 'PUT',
			url: hostName + contextName + rest,
			json: data,
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				this.unlock(rest, success, failure)
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	},
	
	delete: function(rest, success, failure) {
		request({
			method: 'DELETE',
			url: hostName + contextName + rest,
			headers: {
	 			'Authorization': 'Bearer ' + global['auth-token']
  			}
		}, function(error, response, body) {
			if (!error && response.statusCode == 200) {
				success();
			} else {
				failure(error, response, body);
			}
		}.bind(this));
	}
}

module.exports = ListRest;
