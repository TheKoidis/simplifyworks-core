var ApplicationRest = {
	
	loadAvailableDescriptors: function(success, failure) {
		request({
			method: 'GET',
			url: hostName + contextName + '/api/build-descriptions',
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(JSON.parse(body));
			} else {
				failure(error);
			}
		});
	}
}

module.exports = ApplicationRest;
