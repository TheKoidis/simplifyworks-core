var superagent = require('superagent');

var AttachmentRest = {

	download: function(rest, guid) {
		window.open(hostName + contextName + rest + '/download/'+guid+"?auth-token="+global['auth-token']);
	},

	upload: function(rest, file, progress, end) {
		var formData = new FormData();
		formData.append('file', file);

		superagent.post(hostName + contextName + rest + '/upload')
			.accept('application/json')
			.set('authorization', 'Bearer ' + global['auth-token'])
			.send(formData)
			.on('progress', progress)
			.end(end);
	},


}

module.exports = AttachmentRest;
