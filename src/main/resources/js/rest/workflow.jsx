var WorkflowRest = {

	searchTaskDefinitions: function(objectType, success, failure) {
		request({
           method: 'GET',
           url: hostName + contextName + '/api/workflow/task-definitions/' + objectType,
		   headers: {
			   'Authorization': 'Bearer ' + global['auth-token']
		   }
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                success(JSON.parse(body));
            } else {
                failure(error);
            }
        });
	},

	completeTasks: function(taskIds, actionId, success, failure) {
		request({
           method: 'PUT',
           url: hostName + contextName + '/api/workflow/task-instances/completion/' + actionId,
		   json: taskIds,
		   headers: {
			   'Authorization': 'Bearer ' + global['auth-token']
		   }
        }, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                success();
            } else {
                failure(error);
            }
        });
	}
}

module.exports = WorkflowRest;
