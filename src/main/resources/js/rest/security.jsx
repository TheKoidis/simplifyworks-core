var SecurityRest = {
	
	changePassword: function(newPassword, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/api/account/change-password',
			qs: {newPassword: newPassword},
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 204) {
				success();
			} else {
				failure();
			}
		});
	},
	
	logInUsingForm: function(username, password, success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/auth/log-in/username-password',
			qs: {username: username, password: password}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(body);
			} else {
				failure(JSON.parse(body));
			}
		});
	},
	
	logInUsingSso: function(success, failure) {
		request({
			method: 'POST',
			url: hostName + contextName + '/auth/log-in/single-sign-on'
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(body);
			} else {
				failure(JSON.parse(body));
			}
		});
	},
	
	loadAllRoles: function(success, failure) {
		request({
			method: 'GET',
			url: hostName + contextName + '/auth/security/all-roles',
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(JSON.parse(body));
			} else {
				failure(JSON.parse(body));
			}
		});
	},
	
	loadRolesInOrganizations: function(success, failure) {
		request({
			method: 'GET',
			url: hostName + contextName + '/auth/security/roles-in-organizations',
			headers: {
				'Authorization': 'Bearer ' + global['auth-token']
			}
		}, function (error, response, body) {
			if (!error && response.statusCode == 200) {
				success(JSON.parse(body));
			} else {
				failure(JSON.parse(body));
			}
		});
	}
}

module.exports = SecurityRest;
