var LanguageSelector = React.createClass({
	
	propTypes: {
		template: React.PropTypes.object.isRequired
	},
	
	render: function() {
		var selectedLanguage = (
			<span>
				<Image src={contextName + "/images/flags/16/"+ (i18n.language === 'cs' ? 'cz' : 'uk') +".png"} responsive />
				{i18n.t('language-' + i18n.language)}
			</span>
		);
		
		return (
			<SplitButton id='language'  title={selectedLanguage}>
				<MenuItem key='language-cs' disabled={i18n.language === 'cs'} onClick={this.changeLanguage.bind(this, 'cs')}>
					<Image src={contextName + '/images/flags/32/cz.png'} responsive />
					{i18n.t('language-cs')}
				</MenuItem>
				<MenuItem key='language-en' disabled={i18n.language === 'en'} onClick={this.changeLanguage.bind(this, 'en')}>
					<Image src={contextName + "/images/flags/32/uk.png"} responsive />
					{i18n.t('language-en')}
				</MenuItem>
			</SplitButton>
		);
	},
	
	changeLanguage: function(language) {
		i18n.changeLanguage(language, function(err) {
			if(err) {
				console.error(err);
			} else {
				this.props.template.forceUpdate();
			}
		}.bind(this));
	}
});

module.exports = LanguageSelector;
