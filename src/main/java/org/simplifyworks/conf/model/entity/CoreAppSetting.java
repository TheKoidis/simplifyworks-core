package org.simplifyworks.conf.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 * Application seting key / value
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Entity
public class CoreAppSetting extends AbstractEntity {

	@NotNull
	@Size(max = 255)
	@Column(name = "SETTING_KEY", nullable = false, length = 255)
	private String settingKey;
	
	@Size(max = 2048)
	@Column(name = "VALUE", length = 2048)
	private String value;
	
	@NotNull
	@Column(name = "SYSTEM", nullable = false)
	private boolean system;

	public CoreAppSetting() {
	}

	public CoreAppSetting(String settingKey, String value) {
		this.settingKey = settingKey;
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean getSystem() {
		return system;
	}

	public void setSystem(boolean system) {
		this.system = system;
	}

	public boolean isSystem() {
		return system;
	}

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}
}
