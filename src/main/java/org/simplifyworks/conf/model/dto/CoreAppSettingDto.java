package org.simplifyworks.conf.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 * Application setting
 * * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Permission(createRoles = "admin", deleteRoles = "admin", readRoles = "admin", writeRoles = "admin")
public class CoreAppSettingDto extends AbstractDto {

	@NotNull
	@Size(max = 255)
	private String settingKey;
	@Size(max = 2048)
	private String value;
	@NotNull
	private boolean system;

	public CoreAppSettingDto() {
	}

	public CoreAppSettingDto(Long id) {
		super(id);
	}

	public String getSettingKey() {
		return settingKey;
	}

	public void setSettingKey(String settingKey) {
		this.settingKey = settingKey;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean isSystem() {
		return system;
	}

	public void setSystem(boolean system) {
		this.system = system;
	}
}
