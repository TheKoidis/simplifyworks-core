package org.simplifyworks.conf.web.controller.rest;

import java.util.List;

import org.simplifyworks.conf.BuildDescriptor;
import org.simplifyworks.conf.service.BuildDescriptorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * REST controller for {@link BuildDescriptorsService}
 *   
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping(value = "/api/build-descriptions")
public class BuildDescriptorsController {

	@Autowired
	private BuildDescriptorsService buildDescriptorsService;

	@RequestMapping(method = RequestMethod.GET, value = "")
	public List<BuildDescriptor> getAvailableDescriptors() {
		return buildDescriptorsService.getAvailableDescriptors();
	}
}
