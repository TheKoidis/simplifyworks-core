
package org.simplifyworks.conf.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.conf.model.dto.CoreAppSettingDto;
import org.simplifyworks.conf.service.CoreAppSettingManager;
import org.simplifyworks.core.model.dto.EmptyFilterDto;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Application setting
 * 
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping("/api/core/settings")
public class CoreAppSettingController {

	@Autowired
	private CoreAppSettingManager manager;
	
	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<CoreAppSettingDto> getIssues(@Valid @RequestBody EmptyFilterDto filter) {
		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody(required = true) CoreAppSettingDto dto) {
		return manager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		manager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public CoreAppSettingDto find(@PathVariable("id") Long id) {
		return manager.find(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void update(@PathVariable("id") Long id, @Valid @RequestBody CoreAppSettingDto dto) {
		manager.update(id, dto);
	}
        
        @RequestMapping(method = RequestMethod.GET, value = "/key/{key}")
	public String getSetting(@PathVariable("key") String key) {
		return manager.getSetting(key);
	}
}
