package org.simplifyworks.conf;

import org.springframework.stereotype.Service;

/**
 * Core implementation of {@link BuildDescriptorService}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class CoreBuildDescriptorService extends AbstractBuildDescriptorService {
	
	@Override
	protected String getPropertiesResourceName() {
		return "/build-core.properties";
	}

	@Override
	protected boolean isModule() {
		return true;
	}
}
