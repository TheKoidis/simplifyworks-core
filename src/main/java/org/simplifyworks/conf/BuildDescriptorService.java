package org.simplifyworks.conf;

import org.simplifyworks.conf.web.controller.rest.BuildDescriptorsController;

/**
 * Common interface for build descriptor service which is processed by {@link BuildDescriptorsController}.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface BuildDescriptorService {

	/**
	 * Returns build descriptor.
	 * 
	 * @return build descriptor
	 */
	public BuildDescriptor getDescriptor();
}
