package org.simplifyworks.conf;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

/**
 * Build descriptor contains information related to the build of the application or module.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class BuildDescriptor {

	private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private final String EXPRESSION_REGEX = "\\$\\{(.*)\\}";
	private final String NOT_AVAILABLE = "N/A";
	
	public static final String BUILD_NAME = "build.name";
	public static final String BUILD_VERSION = "build.version";
	public static final String BUILD_REVISION = "build.revision";
	public static final String BUILD_TIMESTAMP = "build.timestamp";
	public static final String BUILD_NUMBER = "build.number";			
	
	private String name;
	private String version;
	private String revision;
	private String timestamp;
	private String number;
	private boolean module;
	
	/**
	 * Creates new instance using properties and module flag.
	 * This constructor also converts missing values or expressions in properties. 
	 * 
	 * @param properties properties to process
	 * @param module module flag (true for module, false for application)
	 */
	public BuildDescriptor(Properties properties, boolean module) {
		this.name = processStringExpression(properties.getProperty(BUILD_NAME));
		this.version = processStringExpression(properties.getProperty(BUILD_VERSION));
		this.revision = processStringExpression(properties.getProperty(BUILD_REVISION));
		this.timestamp = processDateExpression(properties.getProperty(BUILD_TIMESTAMP));
		this.number = processStringExpression(properties.getProperty(BUILD_NUMBER));
		this.module = module;
	}
	
	private String processStringExpression(String value) {
		return containsExpression(value) ? NOT_AVAILABLE : value;
	}
	
	private String processDateExpression(String value) {
		return containsExpression(value) ? NOT_AVAILABLE : DATE_FORMAT.format(new Date(Long.parseLong(value)));
	}
	
	private boolean containsExpression(String value) {
		return value == null || value.matches(EXPRESSION_REGEX);
	}

	public String getName() {
		return name;
	}
	
	public String getVersion() {
		return version;
	}
	
	public String getRevision() {
		return revision;
	}
	
	public String getTimestamp() {
		return timestamp;
	}
	
	public String getNumber() {
		return number;
	}
	
	public boolean isModule() {
		return module;
	}
}
