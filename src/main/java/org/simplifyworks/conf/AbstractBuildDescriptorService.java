package org.simplifyworks.conf;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.simplifyworks.core.exception.CoreException;

/**
 * Abstract implementation of {@link BuildDescriptorService} based on template-method pattern.
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class AbstractBuildDescriptorService implements BuildDescriptorService {

	@Override
	public BuildDescriptor getDescriptor() {		
		Properties properties = new Properties();
		
		try {
			InputStream resourceStream = getClass().getResourceAsStream(getPropertiesResourceName());
			
			if(resourceStream == null) {
				throw new CoreException("No resource stream for resource " + getPropertiesResourceName());
			}
			
			properties.load(resourceStream);
		} catch(IOException e) {
			throw new Error(e);
		}
		
		return new BuildDescriptor(properties, isModule());
	}
	
	/**
	 * Returns properties resource (file) name (path).
	 * 
	 * @return properties resource name
	 */
	protected abstract String getPropertiesResourceName();
	
	/**
	 * Returns module flag.
	 * 
	 * @return module flag
	 */
	protected abstract boolean isModule();
}
