package org.simplifyworks.conf.service;

import org.simplifyworks.conf.model.dto.CoreAppSettingDto;
import org.simplifyworks.conf.model.entity.CoreAppSetting;
import org.simplifyworks.core.model.dto.EmptyFilterDto;
import org.simplifyworks.core.service.SearchFilterManager;


/**
 * Application setting
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CoreAppSettingManager extends SearchFilterManager<CoreAppSettingDto, CoreAppSetting, EmptyFilterDto> {
	
	public String getSetting(String key);
	
	public boolean getBooleanSetting(String key);

	public Long create(CoreAppSettingDto dto);

	public void delete(Long id);

	public CoreAppSettingDto find(Long id);

	public void update(Long id, CoreAppSettingDto dto);
}
