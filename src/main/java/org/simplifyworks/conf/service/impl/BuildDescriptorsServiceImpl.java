package org.simplifyworks.conf.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.simplifyworks.conf.BuildDescriptor;
import org.simplifyworks.conf.BuildDescriptorService;
import org.simplifyworks.conf.service.BuildDescriptorsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link BuildDescriptorsService} which uses {@link BuildDescriptorService} as marking interface.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class BuildDescriptorsServiceImpl implements BuildDescriptorsService {

	@Autowired
	private ApplicationContext context;
	
	@Override
	public List<BuildDescriptor> getAvailableDescriptors() {
		List<BuildDescriptor> descriptors = new ArrayList<>();
		
		for(BuildDescriptorService service : context.getBeansOfType(BuildDescriptorService.class).values()) {
			descriptors.add(service.getDescriptor());
		}
		
		return descriptors;
	}
}
