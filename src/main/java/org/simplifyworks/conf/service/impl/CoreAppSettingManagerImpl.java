package org.simplifyworks.conf.service.impl;

import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.simplifyworks.conf.model.dto.CoreAppSettingDto;
import org.simplifyworks.conf.model.entity.CoreAppSetting;
import org.simplifyworks.conf.model.entity.CoreAppSetting_;
import org.simplifyworks.conf.service.CoreAppSettingManager;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.EmptyFilterDto;
import org.simplifyworks.core.service.impl.AbstractBaseManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Nastaveni aplikace
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
@Transactional
public class CoreAppSettingManagerImpl extends AbstractBaseManager<CoreAppSettingDto, CoreAppSetting> implements CoreAppSettingManager {

	@Override
	public ExtendedArrayList<CoreAppSettingDto> getAllWrapped(EmptyFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[] {"settingKey", "value"};
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreAppSetting> root, EmptyFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		// do nothing
	}

	@Override
	public String getSetting(String key) {
		CriteriaBuilder builder = dataRepository.getCriteriaBuilder();
		CriteriaQuery<CoreAppSetting> criteria = builder.createQuery(CoreAppSetting.class);
		Root<CoreAppSetting> root = criteria.from(CoreAppSetting.class);

		criteria.where(builder.equal(root.get(CoreAppSetting_.settingKey), key));

		try {
			return dataRepository.createQuery(criteria).getSingleResult().getValue();
		} catch(NoResultException e) {
			return null;
		}
	}

	@Override
	public boolean getBooleanSetting(String key) {
		String value = getSetting(key);

		return value != null && Boolean.parseBoolean(value);
	}

	@Override
	public Long create(CoreAppSettingDto dto) {
		CoreAppSetting entity = toEntity(dto);

		dataRepository.persist(entity);

		return entity.getId();
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(getEntityClass(), id);
	}

	@Override
	public CoreAppSettingDto find(Long id) {
		return toDto(dataRepository.find(getEntityClass(), id));
	}

	@Override
	public void update(Long id, CoreAppSettingDto dto) {
		CoreAppSetting entity = toEntity(dto);
		entity = dataRepository.update(entity);
	}

	@Override
	public List<CoreAppSetting> read(EmptyFilterDto filter) {
		return searchManager.read(filter, this);
	}
}
