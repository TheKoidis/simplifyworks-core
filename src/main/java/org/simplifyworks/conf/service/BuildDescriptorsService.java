package org.simplifyworks.conf.service;

import java.util.List;

import org.simplifyworks.conf.BuildDescriptor;

/**
 * Service which provides build descriptors.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface BuildDescriptorsService {

	/**
	 * Returns all available descriptors.
	 * 
	 * @return all available descriptors
	 */
	public List<BuildDescriptor> getAvailableDescriptors();
}
