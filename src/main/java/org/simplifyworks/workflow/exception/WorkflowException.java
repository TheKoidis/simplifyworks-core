package org.simplifyworks.workflow.exception;

import java.util.Collections;

import org.activiti.engine.ActivitiException;
import org.simplifyworks.core.exception.CoreException;

public class WorkflowException extends CoreException {

	private static final long serialVersionUID = -4880801103691538991L;

	public WorkflowException(ActivitiException cause) {
		super("Workflow error occured", "workflow-error", Collections.singletonMap("cause", cause.getMessage()), cause);
	}
}
