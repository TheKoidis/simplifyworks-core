package org.simplifyworks.workflow.service;

import java.util.List;

public interface WorkflowSecurityService {

	/**
	 * Returns string for workflow purposes that represents current user.
	 * 
	 * @return string that represents current user
	 */
	public String getUser();
	
	/**
	 * Returns list of strings for workflow purposes that represent current user groups (concatenated role and organization).
	 * 
	 * @return list of strings that represent current user groups
	 */
	public List<String> getGroups();
}
