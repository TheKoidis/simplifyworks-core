package org.simplifyworks.workflow.service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowService {

	public static final String PROCESS_VARIABLE_OBJECT_TYPE_NAME = "objectType";
	public static final String PROCESS_VARIABLE_OBJECT_GROUP_NAME = "objectGroup";
	public static final String PROCESS_VARIABLE_OBJECT_ID_NAME = "objectId";
	public static final String PROCESS_VARIABLE_OBJECT_DESCRIPTION_NAME = "objectDescription";
	
	public static final String FORM_PROPERTY_ACTION_TYPE_PARAMETER = "action";
	public static final String FORM_VALUE_ID_NAME_PARAMETER = "name";
	public static final String FORM_VALUE_ID_STYLE_PARAMETER = "style";
	public static final String FORM_VALUE_ID_ICON_PARAMETER = "icon";
	
	public static final String FORM_VALUE_ID_KEY_PARAMETER = "key";
	public static final String FORM_VALUE_ID_SIGNATURE_REQUIRED_PARAMETER = "signatureRequired";
	public static final String FORM_VALUE_ID_CONFIRMATION_REQUIRED_PARAMETER = "confirmationRequired";
	public static final String FORM_VALUE_ID_COMMENT_REQUIRED_PARAMETER = "commentRequired";
	public static final String FORM_VALUE_ID_PRINT_TEMPLATE_PARAMETER = "printTemplate";
	
	public static final String PARAMETER_ENTITY = "entity";
    public static final String PARAMETER_NEW = "new";
	public static final String PARAMETER_WF_ACTION = "action";
    public static final String PARAMETER_ENTITY_ID = "entityId";
    public static final String PARAMETER_ENTITY_TYPE = "entityType";
    public static final String PARAMETER_PROCESS_INSTANCE = "processInstanceId";
	public static final String PARAMETER_PROCESS_DEFINITION_ID = "processDefinitionId";
	public static final String VALIDATION_FAILS_KEY = "validationFails";
	public static final String VALIDATION_STATE_KEY = "state";
	public static final String VALIDATION_STATE_VALID = "valid";
	public static final String VALIDATION_STATE_ERRORS = "errors";
	public static final String VALIDATION_STATE_WARNINGS = "warnings";
}
