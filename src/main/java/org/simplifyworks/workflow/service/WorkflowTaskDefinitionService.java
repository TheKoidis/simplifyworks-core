package org.simplifyworks.workflow.service;

import java.util.List;

import org.simplifyworks.workflow.model.dto.WorkflowTaskDefinitionDto;

public interface WorkflowTaskDefinitionService {

	/**
	 * Searches for task definitions using type of the object.
	 * 
	 * @param objectType type of object
	 * @return task definitions
	 */
	public List<WorkflowTaskDefinitionDto> searchForTaskDefinitions(String objectType);
}
