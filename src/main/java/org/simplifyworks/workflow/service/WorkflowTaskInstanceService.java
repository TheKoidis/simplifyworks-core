package org.simplifyworks.workflow.service;

import java.util.List;


import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowTaskActionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskFilterDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskInstanceDto;

public interface WorkflowTaskInstanceService {

	/**
	 * Searches for mine tasks for the specified group of objects.
	 *
	 * @param objectType type of object
	 * @param objectGroup group of object
	 * @param filter filter with other attributes and pagination
	 * @return mine tasks related to the objects of specified type and group
	 */
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchMineTasksForObjectGroup(String objectType, String objectGroup, WorkflowTaskFilterDto filter);

	/**
	 * Searches for mine tasks for the specified id of objects.
	 *
	 * @param objectType type of object
	 * @param objectId id of object
	 * @param filter filter with other attributes and pagination
	 * @return mine tasks related to the objects of specified type and id
	 */
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchMineTasksForObjectId(String objectType, String objectId, WorkflowTaskFilterDto filter);

	/**
	 * Searches for tasks for the specified id of objects.
	 *
	 * @param objectType type of object
	 * @param objectId id of object
	 * @param filter filter with other attributes and pagination
	 * @return mine tasks related to the objects of specified type and id
	 */
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchTasksForObjectId(String objectType, String objectId, WorkflowTaskFilterDto filter);

	/**
	 * Completes task (also does assignment).
	 *
	 * @param taskId id of task
	 * @param actionId id of completion action (like approve, disaprove etc.)
         * @param comment string for comment
	 */
	public void completeTask(String taskId, String actionId, String comment);

	/**
	 * Completes tasks (also does assignment).
	 *
	 * @param dto information needed to complete task (ActionId, TaskIds, Comment)
	 */
	public void completeTasks(WorkflowTaskActionDto dto);

}
