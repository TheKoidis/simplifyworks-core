package org.simplifyworks.workflow.service.impl;

import java.util.List;

import org.activiti.engine.HistoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.domain.WorkflowTaskUserCategory;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskInstanceDto;
import org.simplifyworks.workflow.service.WorkflowConverter;
import org.simplifyworks.workflow.service.WorkflowSecurityService;
import org.simplifyworks.workflow.service.WorkflowUserCategorizedTaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkflowUserCategorizedTaskServiceImpl implements WorkflowUserCategorizedTaskService {
	
	@Autowired
	private WorkflowSecurityService securityService;
	
	@Autowired
	private WorkflowConverter converter;
		
	@Autowired
	private TaskService taskService;
	
	@Autowired
	private HistoryService historyService;
	
	@Override
	public ExtendedArrayList<? extends WorkflowTaskInstanceDto> searchTasksByType(String objectType, WorkflowTaskUserCategory userCategory) {
		if(WorkflowTaskUserCategory.ASSIGNEE.equals(userCategory)) {
			TaskQuery query = taskService.createTaskQuery();
			
			query.active();
			query.taskAssignee(securityService.getUser());
			
			query.includeProcessVariables();
			query.orderByTaskCreateTime().desc();
			
			long count = query.count();
			List<Task> tasks = query.listPage(0, MAX_RESULTS);

			return new ExtendedArrayList<WorkflowTaskInstanceDto>(count, converter.convertTasks(objectType, tasks));
		} else if(WorkflowTaskUserCategory.CANDIDATE.equals(userCategory)) {
			TaskQuery query = taskService.createTaskQuery();
			
			query.active();
			query.taskCandidateUser(securityService.getUser());
			query.taskCandidateGroupIn(securityService.getGroups());
			
			query.includeProcessVariables();
			query.orderByTaskCreateTime().desc();
			
			long count = query.count();
			List<Task> tasks = query.listPage(0, MAX_RESULTS);

			return new ExtendedArrayList<WorkflowTaskInstanceDto>(count, converter.convertTasks(objectType, tasks));
		} else if(WorkflowTaskUserCategory.HISTORIC_ASSIGNEE.equals(userCategory)) {
			HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery();
			
			query.taskAssignee(securityService.getUser());
			
			query.includeProcessVariables();
			query.orderByTaskCreateTime().desc();
			
			long count = query.count();
			List<HistoricTaskInstance> tasks = query.listPage(0, MAX_RESULTS);

			return new ExtendedArrayList<WorkflowHistoricTaskInstanceDto>(count, converter.convertHistoricTaskInstances(objectType, tasks));
		} else {
			throw new IllegalArgumentException("Unusupported userCategory " + userCategory);
		}
	}
}
