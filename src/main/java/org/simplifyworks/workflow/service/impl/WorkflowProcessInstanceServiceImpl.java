/**
 *
 */
package org.simplifyworks.workflow.service.impl;

import java.util.List;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.runtime.ProcessInstanceQuery;
import org.simplifyworks.workflow.service.WorkflowProcessInstanceService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class WorkflowProcessInstanceServiceImpl implements WorkflowProcessInstanceService {

	@Autowired
	private RuntimeService runtimeService;

	@Override
	public void createProcessInstance(String processDefinitionKey, String objectType, String objectGroup, String objectId, String objectDescription) {
		runtimeService.createProcessInstanceBuilder()
						.processDefinitionKey(processDefinitionKey)
						.addVariable(WorkflowService.PROCESS_VARIABLE_OBJECT_TYPE_NAME, objectType)
						.addVariable(WorkflowService.PROCESS_VARIABLE_OBJECT_GROUP_NAME, objectGroup)
						.addVariable(WorkflowService.PROCESS_VARIABLE_OBJECT_ID_NAME, objectId)
						.addVariable(WorkflowService.PROCESS_VARIABLE_OBJECT_DESCRIPTION_NAME, objectDescription)
						.start();
	}

	@Override
	public List<ProcessInstance> activeProcessInstances(String objectType, String objectId) {
		ProcessInstanceQuery query = getProcessInstanceQuery(objectType, objectId);
		query.active();
		return query.list();
	}

	@Override
	public List<ProcessInstance> suspendedProcessInstances(String objectType, String objectId) {
		ProcessInstanceQuery query = getProcessInstanceQuery(objectType, objectId);
		query.suspended();
		return query.list();
	}

	private ProcessInstanceQuery getProcessInstanceQuery(String objectType, String objectId) {
		ProcessInstanceQuery query = runtimeService.createProcessInstanceQuery();
		if(objectType != null) {
                    query.variableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_TYPE_NAME, objectType);
                }
		query.variableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_ID_NAME, objectId);
		query.includeProcessVariables();
		query.orderByProcessInstanceId().asc();
		return query;
	}

	@Override
	public void deleteProcessInstance(String processInstanceId, String deleteReason) {
		runtimeService.deleteProcessInstance(processInstanceId, deleteReason);
	}

}
