package org.simplifyworks.workflow.service.impl;

import java.io.FileNotFoundException;

import org.activiti.engine.RepositoryService;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.service.CoreAttachmentManager;
import org.simplifyworks.workflow.model.domain.WorkflowUploadDto;
import org.simplifyworks.workflow.service.WorkflowDeploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class WorkflowDeploymentServiceImpl implements WorkflowDeploymentService {

	@Autowired
	private RepositoryService repositoryService;

	@Autowired
	private CoreAttachmentManager attachmentManager;

	@Override
	public void upload(WorkflowUploadDto data) {
		try {
			for (CoreAttachmentDto attachment : data.getAttachments()) {
				repositoryService
					.createDeployment()
					.addInputStream(attachment.getName(), attachmentManager.downloadAttachment(attachment.getGuid()))
					.name(attachment.getName())
					.deploy();
			}
		} catch (FileNotFoundException e) {
			throw new Error(e);
		}
	}
}
