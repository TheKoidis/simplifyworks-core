package org.simplifyworks.workflow.service.impl;

import java.io.InputStream;
import java.util.List;

import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.repository.ProcessDefinitionQuery;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.SortDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionFilterDto;
import org.simplifyworks.workflow.service.WorkflowConverter;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of {@link WorkflowProcessDefinitionService}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
@Transactional
public class WorkflowProcessDefinitionServiceImpl implements WorkflowProcessDefinitionService {

	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private WorkflowConverter converter;
	
	@Override
	public ExtendedArrayList<WorkflowProcessDefinitionDto> search(WorkflowProcessDefinitionFilterDto filter) {
		ProcessDefinitionQuery query = repositoryService.createProcessDefinitionQuery();
		
		// TODO add to filter
		query.active();
		query.latestVersion();
		
		applyFilter(query, filter);		
		applySorts(query, filter);
		
		long count = query.count();
		List<ProcessDefinition> processDefinitions = query.listPage((filter.getPageNumber() - 1) * filter.getPageSize(), filter.getPageSize());
		
		return new ExtendedArrayList<>(count, converter.convertProcessDefinitions(processDefinitions));
	}
	
	@Override
	public InputStream getDiagram(String processDefinitionId) {
		return repositoryService.getProcessDiagram(processDefinitionId);
	}
	
	private void applyFilter(ProcessDefinitionQuery query, WorkflowProcessDefinitionFilterDto filter) {		
		if(filter.getCategory() != null) {
			query.processDefinitionCategory(filter.getCategory());
		}
	}
	
	private void applySorts(ProcessDefinitionQuery query, WorkflowProcessDefinitionFilterDto filter) {
		if(!filter.getSorts().isEmpty()) {
			// only one sort is applied			
			SortDto sort = filter.getSorts().get(0);
			
			switch(sort.getProperty()) {
				case "key":
					query.orderByProcessDefinitionKey();
					break;
				case "name":
					query.orderByProcessDefinitionName();
					break;
				case "category":
					query.orderByProcessDefinitionCategory();
					break;
				case "version":
					query.orderByProcessDefinitionVersion();
					break;
				default:
					query.orderByProcessDefinitionId();
					break;
			}
			
			if("ASC".equalsIgnoreCase(sort.getOrder())) {
				query.asc();
			} else {
				query.desc();
			}
		}
	}
}
