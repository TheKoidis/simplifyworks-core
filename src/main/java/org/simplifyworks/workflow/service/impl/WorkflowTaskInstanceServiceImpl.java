package org.simplifyworks.workflow.service.impl;

import java.util.Collections;
import java.util.List;

import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowTaskActionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskFilterDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskInstanceDto;
import org.simplifyworks.workflow.service.WorkflowConverter;
import org.simplifyworks.workflow.service.WorkflowSecurityService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.simplifyworks.workflow.service.WorkflowTaskInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Service
public class WorkflowTaskInstanceServiceImpl implements WorkflowTaskInstanceService {

	@Autowired
	private WorkflowSecurityService securityService;

	@Autowired
	private TaskService taskService;

	@Autowired
	private WorkflowConverter converter;

	@Override
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchMineTasksForObjectGroup(String objectType, String objectGroup, WorkflowTaskFilterDto filter) {
		return searchTasks(objectType, objectGroup, null, filter);
	}

	@Override
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchMineTasksForObjectId(String objectType, String objectId, WorkflowTaskFilterDto filter) {
		return searchTasks(objectType, null, objectId, filter);
	}

	@Override
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchTasksForObjectId(String objectType, String objectId, WorkflowTaskFilterDto filter) {
		return searchTasksForAll(objectType, null, objectId, filter);
	}

	public ExtendedArrayList<WorkflowTaskInstanceDto> searchTasks(String objectType, String objectGroup, String objectId, WorkflowTaskFilterDto filter) {
		Assert.hasText(objectType, "Object type cannot be null");

		TaskQuery query = createQuery(objectType, objectGroup, objectId, filter);

		long count = query.count();
		List<Task> tasks = query.listPage((filter.getPageNumber() - 1) * filter.getPageSize(), filter.getPageSize());

		return new ExtendedArrayList<>(count, converter.convertTasks(objectType, tasks));
	}

	private ExtendedArrayList<WorkflowTaskInstanceDto> searchTasksForAll(String objectType, String objectGroup, String objectId, WorkflowTaskFilterDto filter) {
		Assert.hasText(objectType, "Object type cannot be null");

		TaskQuery query = createQueryForAll(objectType, objectGroup, objectId, filter);

		long count = query.count();
		List<Task> tasks = query.listPage((filter.getPageNumber() - 1) * filter.getPageSize(), filter.getPageSize());

		return new ExtendedArrayList<>(count, converter.convertTasks(objectType, tasks));
	}

	private TaskQuery createQuery(String objectType, String objectGroup, String objectId, WorkflowTaskFilterDto filter) {
		TaskQuery query = createQueryForAll(objectType, objectGroup, objectId, filter);
		query.taskCandidateOrAssigned(securityService.getUser());
		query.taskCandidateGroupIn(securityService.getGroups());

		return query;
	}

	private TaskQuery createQueryForAll(String objectType, String objectGroup, String objectId, WorkflowTaskFilterDto filter) {
		TaskQuery query = taskService.createTaskQuery();

		query.active();
		query.includeProcessVariables();

		// TODO use correct attribute
		query.processDefinitionKey(objectType);

		if (objectGroup != null && !objectGroup.isEmpty()) {
			query.processVariableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_GROUP_NAME, objectGroup);
		}

		if (objectId != null && !objectId.isEmpty()) {
			query.processVariableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_ID_NAME, objectId);
		}

		if (filter.getDefinitionKey() != null) {
			query.taskDefinitionKey(filter.getDefinitionKey());
		}

		if (filter.getCategory() != null) {
			query.taskCategory(filter.getCategory());
		}

		return query;
	}

	@Override
	public void completeTask(String taskId, String actionId, String comment) {
		taskService.setAssignee(taskId, securityService.getUser());
                taskService.addComment(taskId, null, comment);
		taskService.complete(taskId, Collections.singletonMap("action", actionId));
	}

	@Override
	public void completeTasks(WorkflowTaskActionDto dto) {
		for (String taskId : dto.getTaskIds()) {
			completeTask(taskId, dto.getActionId(), dto.getComment());
		}
	}

}
