package org.simplifyworks.workflow.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.activiti.bpmn.model.BpmnModel;
import org.activiti.bpmn.model.FlowElement;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.ProcessDefinition;
import org.simplifyworks.workflow.model.dto.WorkflowTaskDefinitionDto;
import org.simplifyworks.workflow.service.WorkflowConverter;
import org.simplifyworks.workflow.service.WorkflowTaskDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkflowTaskDefinitionServiceImpl implements WorkflowTaskDefinitionService {

	@Autowired
	private RepositoryService repositoryService;
	
	@Autowired
	private WorkflowConverter converter;
	
	@Override
	public List<WorkflowTaskDefinitionDto> searchForTaskDefinitions(String objectType) {
		List<WorkflowTaskDefinitionDto> taskDefinitions = new ArrayList<>();
		Map<String, BpmnModel> models = searchForModels(objectType);
		
		for(Map.Entry<String, BpmnModel> entry : models.entrySet()) {
			for(FlowElement flowElement : entry.getValue().getProcessById(objectType).getFlowElements()) {
				if(flowElement instanceof UserTask) {
					taskDefinitions.add(converter.convertTaskDefinition(entry.getKey(), (UserTask) flowElement));
				}
			}
		}
		
		return taskDefinitions;
	}
	
	private Map<String, BpmnModel> searchForModels(String objectType) {
		List<ProcessDefinition> processDefinitions = repositoryService.createProcessDefinitionQuery().processDefinitionKey(objectType).orderByProcessDefinitionVersion().desc().list();
		
		Map<String, BpmnModel> models = new LinkedHashMap<>();
		
		for(ProcessDefinition processDefinition : processDefinitions) {
			models.put(processDefinition.getId(), repositoryService.getBpmnModel(processDefinition.getId()));
		}
		
		return models;
	}
}
