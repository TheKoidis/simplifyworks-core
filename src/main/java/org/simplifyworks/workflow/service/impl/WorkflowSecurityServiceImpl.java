package org.simplifyworks.workflow.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.workflow.service.WorkflowSecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkflowSecurityServiceImpl implements WorkflowSecurityService {

	@Autowired
	private SecurityService securityService;
	
	@Override
	public String getUser() {
		return securityService.getUsername();
	}
	
	@Override
	public List<String> getGroups() {
		List<String> groups = new ArrayList<>();
		
		for(RoleOrganizationGrantedAuthority authority : ((JwtUserAuthentication) securityService.getAuthentication()).getRoleOrganizationAuthorities()) {
			groups.add(authority.getRoleName() + "-" + authority.getOrganizationCode());
		}
		
		return groups;
	}

}
