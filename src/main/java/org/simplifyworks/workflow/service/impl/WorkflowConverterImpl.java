package org.simplifyworks.workflow.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.FormValue;
import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskActionDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskInstanceDto;
import org.simplifyworks.workflow.service.WorkflowConverter;
import org.simplifyworks.workflow.service.WorkflowSecurityService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.simplifyworks.workflow.service.WorkflowTaskDefinitionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class WorkflowConverterImpl implements WorkflowConverter {

	private static final Logger LOG = LoggerFactory.getLogger(WorkflowConverterImpl.class);
	
	@Autowired
	private WorkflowSecurityService securityService;

	@Autowired
	private WorkflowTaskDefinitionService taskDefinitionService;

	@Override
	public WorkflowTaskDefinitionDto convertTaskDefinition(String processDefinitionId, UserTask task) {
		WorkflowTaskDefinitionDto taskDefinitionDto = new WorkflowTaskDefinitionDto();

		taskDefinitionDto.setId(task.getId());
		taskDefinitionDto.setName(task.getName());
		taskDefinitionDto.setProcessDefinitionId(processDefinitionId);

		for (FormProperty formProperty : task.getFormProperties()) {
			if (WorkflowService.FORM_PROPERTY_ACTION_TYPE_PARAMETER.equals(formProperty.getType())) {
				WorkflowTaskActionDefinitionDto taskActionDefinitionDto = new WorkflowTaskActionDefinitionDto();

				taskActionDefinitionDto.setId(formProperty.getId());

				for (FormValue formValue : formProperty.getFormValues()) {
					switch (formValue.getId()) {
						case WorkflowService.FORM_VALUE_ID_NAME_PARAMETER:
							taskActionDefinitionDto.setName(formValue.getName());
							break;
						case WorkflowService.FORM_VALUE_ID_STYLE_PARAMETER:
							taskActionDefinitionDto.setStyle(formValue.getName());
							break;
						case WorkflowService.FORM_VALUE_ID_ICON_PARAMETER:
							taskActionDefinitionDto.setIcon(formValue.getName());
							break;
						case WorkflowService.FORM_VALUE_ID_KEY_PARAMETER:
							taskActionDefinitionDto.setKey(formValue.getName());
							break;
						case WorkflowService.FORM_VALUE_ID_SIGNATURE_REQUIRED_PARAMETER:
							taskActionDefinitionDto.setSignatureRequired(Boolean.parseBoolean(formValue.getName()));
							break;
						case WorkflowService.FORM_VALUE_ID_CONFIRMATION_REQUIRED_PARAMETER:
							taskActionDefinitionDto.setConfirmationRequired(Boolean.parseBoolean(formValue.getName()));
							break;
						case WorkflowService.FORM_VALUE_ID_COMMENT_REQUIRED_PARAMETER:
							taskActionDefinitionDto.setCommentRequired(Boolean.parseBoolean(formValue.getName()));
							break;
						case WorkflowService.FORM_VALUE_ID_PRINT_TEMPLATE_PARAMETER:
							taskActionDefinitionDto.setPrintTemplate(formValue.getName());
							break;
						default:
							LOG.warn("Unknown form value with id [{}] and name [{}]", formValue.getId(), formValue.getName());
							break;
					}
				}

				taskDefinitionDto.getActions().add(taskActionDefinitionDto);
			}
		}

		return taskDefinitionDto;
	}

	@Override
	public List<WorkflowTaskInstanceDto> convertTasks(String objectType, List<Task> tasks) {
		List<WorkflowTaskDefinitionDto> taskDefinitions = taskDefinitionService.searchForTaskDefinitions(objectType);

		List<WorkflowTaskInstanceDto> dtos = new ArrayList<>(tasks.size());

		for (Task task : tasks) {
			WorkflowTaskInstanceDto dto = new WorkflowTaskInstanceDto();
			convertTaskInfo(dto, task, taskDefinitions);
			// @todo : remaining attributes
			dtos.add(dto);
		}

		return dtos;
	}

	@Override
	public List<WorkflowTaskInstanceDto> convertTaskInfos(String objectType, List<TaskInfo> tasks) {
		List<WorkflowTaskDefinitionDto> taskDefinitions = taskDefinitionService.searchForTaskDefinitions(objectType);

		List<WorkflowTaskInstanceDto> dtos = new ArrayList<>(tasks.size());

		for (TaskInfo task : tasks) {
			WorkflowTaskInstanceDto dto = new WorkflowTaskInstanceDto();
			convertTaskInfo(dto, task, taskDefinitions);
			dtos.add(dto);
		}

		return dtos;
	}

	public List<WorkflowHistoricTaskInstanceDto> convertHistoricTaskInstances(String objectType,
			List<HistoricTaskInstance> tasks) {
		List<WorkflowTaskDefinitionDto> taskDefinitions = taskDefinitionService.searchForTaskDefinitions(objectType);

		List<WorkflowHistoricTaskInstanceDto> dtos = new ArrayList<>(tasks.size());

		for (HistoricTaskInstance task : tasks) {
			WorkflowHistoricTaskInstanceDto dto = new WorkflowHistoricTaskInstanceDto();
			convertTaskInfo(dto, task, taskDefinitions);
			// remaining attributes
			dto.setDeleteReason(task.getDeleteReason());
			dto.setStartTime(task.getStartTime());
			dto.setEndTime(task.getEndTime());
			dto.setClaimTime(task.getClaimTime());
			dto.setTime(task.getTime());
			//
			dtos.add(dto);
		}

		return dtos;
	}

	@Override
	public List<WorkflowProcessDefinitionDto> convertProcessDefinitions(List<ProcessDefinition> processDefinitions) {
		List<WorkflowProcessDefinitionDto> dtos = new ArrayList<>(processDefinitions.size());

		for (ProcessDefinition processDefinition : processDefinitions) {
			WorkflowProcessDefinitionDto dto = new WorkflowProcessDefinitionDto();

			dto.setId(processDefinition.getId());
			dto.setKey(processDefinition.getKey());
			dto.setName(processDefinition.getName());
			dto.setDescription(processDefinition.getDescription());
			dto.setCategory(processDefinition.getCategory());
			dto.setVersion(processDefinition.getVersion());

			dtos.add(dto);
		}

		return dtos;
	}

	private void convertTaskInfo(WorkflowTaskInstanceDto dto, TaskInfo taskInfo,
			List<WorkflowTaskDefinitionDto> taskDefinitions) {

		dto.setId(taskInfo.getId());
		dto.setCreated(taskInfo.getCreateTime());
		dto.setMine(securityService.getUser().equals(taskInfo.getAssignee()));
		dto.setAssignee(taskInfo.getAssignee());

		Map<String, Object> processVariables = taskInfo.getProcessVariables();
		Map<String, Object> otherProcessVariables = new HashMap<>();

		for (Map.Entry<String, Object> variable : processVariables.entrySet()) {

			switch (variable.getKey()) {
			case WorkflowService.PROCESS_VARIABLE_OBJECT_TYPE_NAME:
				dto.setObjectType((String) processVariables.get(WorkflowService.PROCESS_VARIABLE_OBJECT_TYPE_NAME));
				break;
			case WorkflowService.PROCESS_VARIABLE_OBJECT_GROUP_NAME:
				dto.setObjectGroup((String) processVariables.get(WorkflowService.PROCESS_VARIABLE_OBJECT_GROUP_NAME));
				break;
			case WorkflowService.PROCESS_VARIABLE_OBJECT_ID_NAME:
				dto.setObjectId((String) processVariables.get(WorkflowService.PROCESS_VARIABLE_OBJECT_ID_NAME));
				break;
			case WorkflowService.PROCESS_VARIABLE_OBJECT_DESCRIPTION_NAME:
				dto.setSubject((String) processVariables.get(WorkflowService.PROCESS_VARIABLE_OBJECT_DESCRIPTION_NAME));
				break;
			default:
				otherProcessVariables.put(variable.getKey(), variable.getValue());
				break;
			}
		}

		dto.setProcessVariables(otherProcessVariables);

		for (WorkflowTaskDefinitionDto taskDefinition : taskDefinitions) {
			if (taskInfo.getProcessDefinitionId().equals(taskDefinition.getProcessDefinitionId()) && taskInfo.getTaskDefinitionKey().equals(taskDefinition.getId())) {
				dto.setDefinition(taskDefinition);
			}
		}
	}

}
