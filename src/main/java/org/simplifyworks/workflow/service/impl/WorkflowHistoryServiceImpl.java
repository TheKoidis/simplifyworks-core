package org.simplifyworks.workflow.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.activiti.engine.HistoryService;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricActivityInstanceQuery;
import org.activiti.engine.history.HistoricProcessInstance;
import org.activiti.engine.history.HistoricProcessInstanceQuery;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.history.HistoricTaskInstanceQuery;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricActivityInstanceFullDto;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskFilterDto;
import org.simplifyworks.workflow.service.WorkflowConverter;
import org.simplifyworks.workflow.service.WorkflowHistoryService;
import org.simplifyworks.workflow.service.WorkflowSecurityService;
import org.simplifyworks.workflow.service.WorkflowService;
import org.simplifyworks.workflow.web.domain.WorkflowHistoricProcessMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
@Service
public class WorkflowHistoryServiceImpl implements WorkflowHistoryService {

	@Autowired
	private WorkflowSecurityService securityService;

	@Autowired
	private HistoryService historyService;

	@Autowired
	private WorkflowConverter converter;

	@Override
	public ExtendedArrayList<WorkflowHistoricTaskInstanceDto> searchTasksForObjectId(String processDefinitionKey, String objectId, WorkflowTaskFilterDto filter) {
		return searchTasksForAll(processDefinitionKey, null, objectId, filter);
	}

	private ExtendedArrayList<WorkflowHistoricTaskInstanceDto> searchTasksForAll(String processDefinitionKey, String objectGroup, String objectId, WorkflowTaskFilterDto filter) {
		Assert.hasText(processDefinitionKey, "Object type cannot be null");

		HistoricTaskInstanceQuery query = createQueryForAll(processDefinitionKey, objectGroup, objectId, filter);

		long count = query.count();
		List<HistoricTaskInstance> tasks = query.listPage((filter.getPageNumber() - 1) * filter.getPageSize(), filter.getPageSize());

		return new ExtendedArrayList<>(count, converter.convertHistoricTaskInstances(processDefinitionKey, tasks));
	}

	private HistoricTaskInstanceQuery createQuery(String processDefinitionKey, String objectGroup, String objectId, WorkflowTaskFilterDto filter) {
		HistoricTaskInstanceQuery query = createQueryForAll(processDefinitionKey, objectGroup, objectId, filter);
		query.taskAssignee(securityService.getUser());
		query.taskCandidateGroupIn(securityService.getGroups());

		return query;
	}

	private HistoricTaskInstanceQuery createQueryForAll(String processDefinitionKey, String objectGroup, String objectId, WorkflowTaskFilterDto filter) {
		HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery();

		query.includeProcessVariables();

		query.processDefinitionKey(processDefinitionKey);

		if (objectGroup != null && !objectGroup.isEmpty()) {
			query.processVariableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_GROUP_NAME, objectGroup);
		}

		if (objectId != null && !objectId.isEmpty()) {
			query.processVariableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_ID_NAME, objectId);
		}

		if (filter.getDefinitionKey() != null) {
			query.taskDefinitionKey(filter.getDefinitionKey());
		}

		if (filter.getCategory() != null) {
			query.taskCategory(filter.getCategory());
		}

		return query;
	}

	@Override
	public ExtendedArrayList<WorkflowHistoricTaskInstanceDto> searchMineHistoricTasks(String processDefinitionKey, WorkflowTaskFilterDto filter) {
		HistoricTaskInstanceQuery query = getMineHistoricTasksQuery(processDefinitionKey, filter);
		long count = query.count();
		List<HistoricTaskInstance> tasks = query.listPage((filter.getPageNumber() - 1) * filter.getPageSize(), filter.getPageSize());
		return new ExtendedArrayList<>(count, converter.convertHistoricTaskInstances(processDefinitionKey, tasks));
	}

	private HistoricTaskInstanceQuery getMineHistoricTasksQuery(String processDefinitionKey, WorkflowTaskFilterDto filter) {
		HistoricTaskInstanceQuery query = historyService.createHistoricTaskInstanceQuery();
		query.includeProcessVariables();
		query.processDefinitionKey(processDefinitionKey);
		query.taskAssignee(securityService.getUser());
		query.orderByHistoricTaskInstanceEndTime().desc();

		if (filter.getDefinitionKey() != null) {
			query.taskDefinitionKey(filter.getDefinitionKey());
		}

		if (filter.getCategory() != null) {
			query.taskCategory(filter.getCategory());
		}
		return query;
	}

	@Override
	public ExtendedArrayList<WorkflowHistoricProcessMetadata> historicProcessWithActivities(String objectType, String objectId) {
		List<HistoricProcessInstance> historicProcessInstances = getHistoricProcessInstances(objectType, objectId);
		List<WorkflowHistoricProcessMetadata> dtos = new ArrayList<>(historicProcessInstances.size());
		for (HistoricProcessInstance historicProcessInstance : historicProcessInstances) {
			List<HistoricActivityInstance> acts = getHistoricActivityInstances(historicProcessInstance.getId());
			dtos.add(new WorkflowHistoricProcessMetadata(historicProcessInstance, acts));
		}
		return new ExtendedArrayList<>(dtos.size(), dtos);
	}

	private List<HistoricActivityInstance> getHistoricActivityInstances(String processInstanceId) {
		HistoricActivityInstanceQuery query = historyService.createHistoricActivityInstanceQuery();
		query.processInstanceId(processInstanceId);
		query.orderByHistoricActivityInstanceStartTime().desc();
		query.orderByHistoricActivityInstanceId().desc();
		return query.list();
	}

	private List<HistoricProcessInstance> getHistoricProcessInstances(String objectType, String objectId) {
		HistoricProcessInstanceQuery query = historyService.createHistoricProcessInstanceQuery();
		query.variableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_TYPE_NAME, objectType);
		query.variableValueEquals(WorkflowService.PROCESS_VARIABLE_OBJECT_ID_NAME, objectId);
		query.includeProcessVariables();
		query.orderByProcessInstanceStartTime().desc();
		query.orderByProcessInstanceId().desc();
		return query.list();
	}
}
