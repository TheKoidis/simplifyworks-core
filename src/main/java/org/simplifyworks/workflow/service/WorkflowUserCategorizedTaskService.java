package org.simplifyworks.workflow.service;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.domain.WorkflowTaskUserCategory;
import org.simplifyworks.workflow.model.dto.WorkflowTaskInstanceDto;

/**
 * Service which provides limited lists of tasks using user categorization
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowUserCategorizedTaskService {

	/**
	 * Maximum count of returned results
	 */
	public static final int MAX_RESULTS = 1000;
	
	/**
	 * Returns list of tasks using user categorization
	 * 
	 * @param objectType type of object (workflow)
	 * @param userCategory user category
	 * @return extended list of tasks (size is limited by {@link WorkflowUserCategorizedTaskService.MAX_RESULTS}
	 */
	public ExtendedArrayList<? extends WorkflowTaskInstanceDto> searchTasksByType(String objectType, WorkflowTaskUserCategory userCategory);
}
