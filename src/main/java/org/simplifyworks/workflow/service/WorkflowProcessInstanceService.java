package org.simplifyworks.workflow.service;

import java.util.List;
import org.activiti.engine.runtime.ProcessInstance;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowProcessInstanceService extends WorkflowService {

	/**
	 * Creates (and begins) new process instance.
	 *
	 * @param processDefinitionKey key of process definition
	 * @param objectType type of related object
	 * @param objectGroup group of related object
	 * @param objectId id of related object
	 * @param objectDescription description of related object
	 */
	public void createProcessInstance(String processDefinitionKey, String objectType, String objectGroup, String objectId, String objectDescription);

	public List<ProcessInstance> activeProcessInstances(String objectType, String objectId);

	public List<ProcessInstance> suspendedProcessInstances(String objectType, String objectId);

	public void deleteProcessInstance(String processInstanceId, String deleteReason);
}
