package org.simplifyworks.workflow.service;


import java.io.InputStream;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionFilterDto;

/**
 * Interface for process definition service
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowProcessDefinitionService extends WorkflowService {

	public ExtendedArrayList<WorkflowProcessDefinitionDto> search(WorkflowProcessDefinitionFilterDto filter);
	
	public InputStream getDiagram(String processDefinitionId);
}
