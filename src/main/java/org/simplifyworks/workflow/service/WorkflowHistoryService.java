package org.simplifyworks.workflow.service;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskFilterDto;
import org.simplifyworks.workflow.web.domain.WorkflowHistoricProcessMetadata;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public interface WorkflowHistoryService {

	/**
	 * Searches for mine historic tasks.
	 *
	 * @param objectType type of object
	 * @param filter filter with other attributes and pagination
	 * @return mine historic tasks related to the objects of specified type
	 */
	public ExtendedArrayList<WorkflowHistoricTaskInstanceDto> searchMineHistoricTasks(String processDefinitionKey, WorkflowTaskFilterDto filter);
	public ExtendedArrayList<WorkflowHistoricTaskInstanceDto> searchTasksForObjectId(String processDefinitionKey, String objectId, WorkflowTaskFilterDto filter);
	public ExtendedArrayList<WorkflowHistoricProcessMetadata> historicProcessWithActivities(String objectType, String objectId);

}
