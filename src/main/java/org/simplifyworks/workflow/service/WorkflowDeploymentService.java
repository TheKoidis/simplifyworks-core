package org.simplifyworks.workflow.service;

import org.simplifyworks.workflow.model.domain.WorkflowUploadDto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface WorkflowDeploymentService extends WorkflowService {
	
	public void upload(WorkflowUploadDto data);
}
