package org.simplifyworks.workflow.service;

import java.util.List;

import org.activiti.bpmn.model.UserTask;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskInfo;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskInstanceDto;

public interface WorkflowConverter {

	/**
	 * Converts task definition to DTO.
	 *
	 * @param processDefinitionId ID of process definition
	 * @param task definition of some user task
	 * @return task definition DTO
	 */
	public WorkflowTaskDefinitionDto convertTaskDefinition(String processDefinitionId, UserTask task);

	/**
	 * Converts list of tasks to list of DTOs.
	 *
	 * @param objectType type of object related to all tasks (it is used for task definition lookup)
	 * @param tasks tasks to convert (must contain process variables)
	 * @return list of DTOs
	 */
	public List<WorkflowTaskInstanceDto> convertTasks(String objectType, List<Task> tasks);
	public List<WorkflowTaskInstanceDto> convertTaskInfos(String objectType, List<TaskInfo> tasks);
	public List<WorkflowHistoricTaskInstanceDto> convertHistoricTaskInstances(String objectType, List<HistoricTaskInstance> tasks);

	/**
	 * Converts list of process definitions to list of DTOs.
	 *
	 * @param processDefinitions process definitions to convert
	 * @return list of DTOs
	 */
	public List<WorkflowProcessDefinitionDto> convertProcessDefinitions(List<ProcessDefinition> processDefinitions);
}
