package org.simplifyworks.workflow.domain;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.activiti.engine.impl.bpmn.behavior.MailActivityBehavior;
import org.activiti.engine.impl.pvm.delegate.ActivityExecution;
import org.simplifyworks.email.domain.EmailSetting;
import org.simplifyworks.email.service.Emailer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Custom Mail (replaces default implementation in activiti)
 * @author svanda
 */
public class CustomMailActivityBehavior extends MailActivityBehavior {

	private static final transient Logger logger = LoggerFactory.getLogger(CustomMailActivityBehavior.class);
	private Emailer emailer;

	@Override
	public void execute(ActivityExecution execution) {
		EmailSetting emailSetting = new EmailSetting();
		emailSetting.setRecipients(prepareAddresses(getStringFromField(to, execution)));
		if(emailSetting.getRecipients().isEmpty()){
			logger.info("Recipeints is not found!");
			leave(execution);
			return;
		}
		emailSetting.setFrom(getStringFromField(from, execution));
		emailSetting.setCc(prepareAddresses(getStringFromField(cc, execution)));
		emailSetting.setBcc(prepareAddresses(getStringFromField(bcc, execution)));
		emailSetting.setSubject(getStringFromField(subject, execution));		
		emailSetting.setText(getStringFromField(text, execution));
		emailSetting.setHtml(getStringFromField(html, execution));
		emailSetting.setCharset(getStringFromField(charset, execution));
		emailer.sendEmail(emailSetting);
		leave(execution);
	}

	public void setEmailer(Emailer emailer) {
		this.emailer = emailer;
	}

	private List<String> prepareAddresses(String addresses) {
		List<String> results = new ArrayList<>();
		String[] tos = splitAndTrim(addresses);
		if (tos != null) {
			results.addAll(Arrays.asList(tos));
		}
		return results;
	}
}
