package org.simplifyworks.workflow.web.domain;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import org.activiti.engine.history.HistoricActivityInstance;
import org.activiti.engine.history.HistoricProcessInstance;
import org.simplifyworks.workflow.model.domain.WorkflowHistoricProcessInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricActivityInstanceFullDto;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public class WorkflowHistoricProcessMetadata {

	private WorkflowHistoricProcessInstanceDto historicProcessInstance;
	private List<WorkflowHistoricActivityInstanceFullDto> historicActivityInstances;

	public WorkflowHistoricProcessMetadata() {

	}

	public WorkflowHistoricProcessMetadata(HistoricProcessInstance historicProcessInstance) {
		this(historicProcessInstance, null);
	}

	public WorkflowHistoricProcessMetadata(HistoricProcessInstance historicProcessInstance, List<HistoricActivityInstance> historicActivityInstances) {
		this.historicProcessInstance = new WorkflowHistoricProcessInstanceDto(historicProcessInstance);

		if (historicActivityInstances != null) {
			this.historicActivityInstances = new ArrayList(historicActivityInstances.size());
			for (HistoricActivityInstance act : historicActivityInstances) {
				this.historicActivityInstances.add(new WorkflowHistoricActivityInstanceFullDto(act));
			}
		}
	}

	public WorkflowHistoricProcessInstanceDto getHistoricProcessInstance() {
		return historicProcessInstance;
	}

	public void setHistoricProcessInstance(WorkflowHistoricProcessInstanceDto historicProcessInstance) {
		this.historicProcessInstance = historicProcessInstance;
	}

	public List<WorkflowHistoricActivityInstanceFullDto> getHistoricActivityInstances() {
		return historicActivityInstances;
	}

	public void setHistoricActivityInstances(List<WorkflowHistoricActivityInstanceFullDto> historicActivityInstances) {
		this.historicActivityInstances = historicActivityInstances;
	}
}
