package org.simplifyworks.workflow.web.controller.rest;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.commons.io.IOUtils;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionDto;
import org.simplifyworks.workflow.model.dto.WorkflowProcessDefinitionFilterDto;
import org.simplifyworks.workflow.service.WorkflowProcessDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/workflow/process-definitions")
public class WorkflowProcessDefinitionController {

	@Autowired
	private WorkflowProcessDefinitionService processDefinitionService;
	
	@RequestMapping(method = RequestMethod.POST, value = "filter")
	public ExtendedArrayList<WorkflowProcessDefinitionDto> search(@Valid @RequestBody(required = true) WorkflowProcessDefinitionFilterDto filter) {		
		return processDefinitionService.search(filter);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "diagrams/{processDefinitionId}", produces = "image/png")
	public byte[] getDiagram(HttpServletResponse response, @PathVariable String processDefinitionId) throws IOException {
		return IOUtils.toByteArray(processDefinitionService.getDiagram(processDefinitionId));
	}
}
