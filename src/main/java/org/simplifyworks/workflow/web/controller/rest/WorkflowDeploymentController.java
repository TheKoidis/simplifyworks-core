package org.simplifyworks.workflow.web.controller.rest;

import java.io.IOException;

import javax.validation.Valid;

import org.simplifyworks.workflow.model.domain.WorkflowUploadDto;
import org.simplifyworks.workflow.service.WorkflowDeploymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/workflow/deployments")
public class WorkflowDeploymentController {

	@Autowired
	private WorkflowDeploymentService deploymentService;
	
	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void upload(@RequestBody(required = true) @Valid WorkflowUploadDto data) throws IOException {
		deploymentService.upload(data);
	}
}
