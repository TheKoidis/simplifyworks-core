package org.simplifyworks.workflow.web.controller.rest;

import java.util.List;

import org.simplifyworks.workflow.model.dto.WorkflowTaskDefinitionDto;
import org.simplifyworks.workflow.service.WorkflowTaskDefinitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/workflow/task-definitions")
public class WorkflowTaskDefinitionController {

	@Autowired
	private WorkflowTaskDefinitionService taskDefinitionService;
	
	@RequestMapping(method = RequestMethod.GET, value="/{objectType}")
	public List<WorkflowTaskDefinitionDto> searchForActions(@PathVariable String objectType) {
		return taskDefinitionService.searchForTaskDefinitions(objectType);
	}
}
