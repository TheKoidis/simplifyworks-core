package org.simplifyworks.workflow.web.controller.rest;

import javax.validation.Valid;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskFilterDto;
import org.simplifyworks.workflow.service.WorkflowHistoryService;
import org.simplifyworks.workflow.web.domain.WorkflowHistoricProcessMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
@RestController
@RequestMapping(value = "/api/workflow/history")
public class WorkflowHistoryController {

	@Autowired
	private WorkflowHistoryService historyService;

	@RequestMapping(method = RequestMethod.POST, value = "{processDefinitionKey}/mine-tasks/filter")
	public ExtendedArrayList<WorkflowHistoricTaskInstanceDto> searchMineHistoricTasks(@PathVariable String processDefinitionKey, @Valid @RequestBody(required = true) WorkflowTaskFilterDto filter) {
		return historyService.searchMineHistoricTasks(processDefinitionKey, filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "{processDefinitionKey}/for-id/{objectId}/filter")
	public ExtendedArrayList<WorkflowHistoricTaskInstanceDto> searchTasksForObjectId(@PathVariable String processDefinitionKey, @PathVariable String objectId, @Valid @RequestBody(required = true) WorkflowTaskFilterDto filter) {
		return historyService.searchTasksForObjectId(processDefinitionKey, objectId, filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "{objectType}/activities/{objectId}")
	public ExtendedArrayList<WorkflowHistoricProcessMetadata> historicProcessWithActivities(@PathVariable String objectType, @PathVariable String objectId) {
		return historyService.historicProcessWithActivities(objectType, objectId);
	}

}
