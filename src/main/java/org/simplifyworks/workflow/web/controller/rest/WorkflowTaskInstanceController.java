package org.simplifyworks.workflow.web.controller.rest;

import java.util.List;

import javax.validation.Valid;


import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.workflow.model.dto.WorkflowHistoricTaskInstanceDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskActionDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskFilterDto;
import org.simplifyworks.workflow.model.dto.WorkflowTaskInstanceDto;
import org.simplifyworks.workflow.service.WorkflowTaskInstanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/workflow/task-instances")
public class WorkflowTaskInstanceController {

	@Autowired
	private WorkflowTaskInstanceService taskInstanceService;

	@RequestMapping(method = RequestMethod.POST, value = "{objectType}/mine-for-group/{objectGroup}/filter")
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchMineTasksForObjectGroup(@PathVariable String objectType, @PathVariable String objectGroup, @Valid @RequestBody(required = true) WorkflowTaskFilterDto filter) {
		return taskInstanceService.searchMineTasksForObjectGroup(objectType, objectGroup, filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "{objectType}/mine-for-id/{objectId}/filter")
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchMineTasksForObjectId(@PathVariable String objectType, @PathVariable String objectId, @Valid @RequestBody(required = true) WorkflowTaskFilterDto filter) {
		return taskInstanceService.searchMineTasksForObjectId(objectType, objectId, filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "{objectType}/for-id/{objectId}/filter")
	public ExtendedArrayList<WorkflowTaskInstanceDto> searchTasksForObjectId(@PathVariable String objectType, @PathVariable String objectId, @Valid @RequestBody(required = true) WorkflowTaskFilterDto filter) {
		return taskInstanceService.searchTasksForObjectId(objectType, objectId, filter);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "completion")
	public void completeTasks(@RequestBody(required = true) WorkflowTaskActionDto dto) {
		taskInstanceService.completeTasks(dto);
	}

}
