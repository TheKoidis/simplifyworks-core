package org.simplifyworks.workflow.model.dto;

import java.text.MessageFormat;
import java.util.Date;
import java.util.Map;

public class WorkflowTaskInstanceDto {

	private String id;
	private Date created;

	private boolean mine;
	private String assignee;

	private String objectType;
	private String objectGroup;
	private String objectId;

	private String subject;

	private Map<String, Object> processVariables;

	private WorkflowTaskDefinitionDto definition;

	public WorkflowTaskInstanceDto() {
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public boolean isMine() {
		return mine;
	}

	public void setMine(boolean mine) {
		this.mine = mine;
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}

	public String getObjectGroup() {
		return objectGroup;
	}

	public void setObjectGroup(String objectGroup) {
		this.objectGroup = objectGroup;
	}

	public String getObjectId() {
		return objectId;
	}

	public void setObjectId(String objectId) {
		this.objectId = objectId;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public WorkflowTaskDefinitionDto getDefinition() {
		return definition;
	}

	public void setDefinition(WorkflowTaskDefinitionDto definition) {
		this.definition = definition;
	}

	public Map<String, Object> getProcessVariables() {
		return processVariables;
	}

	public void setProcessVariables(Map<String, Object> processVariables) {
		this.processVariables = processVariables;
	}

	@Override
	public String toString() {
		return MessageFormat.format("{0} ({1})", getClass().getSimpleName(), getId()); 
	}
}
