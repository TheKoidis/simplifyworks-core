package org.simplifyworks.workflow.model.dto;

import org.simplifyworks.core.model.dto.AbstractFilterDto;

public class WorkflowProcessDefinitionFilterDto extends AbstractFilterDto {

	private String category;
		
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
}
