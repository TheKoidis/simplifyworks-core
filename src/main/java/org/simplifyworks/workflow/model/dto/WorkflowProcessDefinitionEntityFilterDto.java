package org.simplifyworks.workflow.model.dto;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractFilterDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Deprecated
public class WorkflowProcessDefinitionEntityFilterDto extends AbstractFilterDto {

	private List<Object> processDefinitionKeys;
	@NotNull
	@Size(max = 255)
	private String entityClassName;

	public List<Object> getProcessDefinitionKeys() {
		return processDefinitionKeys;
	}

	public void setProcessDefinitionKeys(List<Object> processDefinitionKeys) {
		this.processDefinitionKeys = processDefinitionKeys;
	}

	public String getEntityClassName() {
		return entityClassName;
	}

	public void setEntityClassName(String entityClassName) {
		this.entityClassName = entityClassName;
	}

}
