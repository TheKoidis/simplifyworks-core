package org.simplifyworks.workflow.model.dto;

// TODO merge with AbstractFilterDto
public class WorkflowTaskFilterDto {
	
	private String definitionKey;
	private String category;
	
	private Integer pageNumber = 1;
	private Integer pageSize = Integer.MAX_VALUE;
	
	public WorkflowTaskFilterDto() {
	}
	
	public String getDefinitionKey() {
		return definitionKey;
	}
	
	public void setDefinitionKey(String definitionKey) {
		this.definitionKey = definitionKey;
	}
	
	public String getCategory() {
		return category;
	}
	
	public void setCategory(String category) {
		this.category = category;
	}
	
	public Integer getPageNumber() {
		return pageNumber;
	}
	
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	
	public Integer getPageSize() {
		return pageSize;
	}
	
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
}
