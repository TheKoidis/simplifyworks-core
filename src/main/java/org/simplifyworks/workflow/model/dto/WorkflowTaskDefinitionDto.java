package org.simplifyworks.workflow.model.dto;

import java.util.ArrayList;
import java.util.List;

public class WorkflowTaskDefinitionDto {

	private String id;
	private String name;
	private List<WorkflowTaskActionDefinitionDto> actions = new ArrayList<>();
	private String processDefinitionId;
	
	public WorkflowTaskDefinitionDto() {
	}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<WorkflowTaskActionDefinitionDto> getActions() {
		return actions;
	}
	
	public void setActions(List<WorkflowTaskActionDefinitionDto> actions) {
		this.actions = actions;
	}
	
	public String getProcessDefinitionId() {
		return processDefinitionId;
	}
	
	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}
}
