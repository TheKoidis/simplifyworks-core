package org.simplifyworks.workflow.model.dto;

import java.util.Date;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public class WorkflowHistoricTaskInstanceDto extends WorkflowTaskInstanceDto {

	/**
	 * The reason why this task was deleted {'completed' | 'deleted' | any other user defined string }.
	 */
	private String deleteReason;

	/**
	 * Time when the task started.
	 */
	private Date startTime;

	/**
	 * Time when the task was deleted or completed.
	 */
	private Date endTime;

	/**
	 * Time when the task was claimed.
	 */
	private Date claimTime;

	/**
	 * HistoricData
	 */
	private Date time;

	public String getDeleteReason() {
		return deleteReason;
	}

	public void setDeleteReason(String deleteReason) {
		this.deleteReason = deleteReason;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Date getClaimTime() {
		return claimTime;
	}

	public void setClaimTime(Date claimTime) {
		this.claimTime = claimTime;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

}
