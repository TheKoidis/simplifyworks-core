/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.workflow.model.dto;

import java.util.List;

/**
 *
 * @author Zavřel Lukáš <zavrel@ders.cz>
 */
public class WorkflowTaskActionDto {
    
    private List<String> taskIds;
    private String actionId;
    private String comment;

    public WorkflowTaskActionDto() {
    }

    public List<String> getTaskIds() {
        return taskIds;
    }

    public void setTaskIds(List<String> taskIds) {
        this.taskIds = taskIds;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
