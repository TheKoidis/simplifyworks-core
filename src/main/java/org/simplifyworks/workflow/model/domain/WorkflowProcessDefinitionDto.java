package org.simplifyworks.workflow.model.domain;

import org.activiti.engine.repository.ProcessDefinition;

/**
 * Created by Svanda on 30.6.2015.
 */
public class WorkflowProcessDefinitionDto {

	private String id;
	private String category;
	private String name;
	private String key;
	private String description;
	private int version;
	private String resourceName;
	private String deploymentId;
	private String diagramResourceName;
	private boolean hasStartFormKey;
	private boolean hasGraphicalNotation;
	private boolean suspended;
	private String tenantId;

	public WorkflowProcessDefinitionDto() {
	}

	public WorkflowProcessDefinitionDto(ProcessDefinition processDefinition) {
		this.id = processDefinition.getId();
		this.category = processDefinition.getCategory();
		this.name = processDefinition.getName();
		this.key = processDefinition.getKey();
		this.description = processDefinition.getDescription();
		this.version = processDefinition.getVersion();
		this.resourceName = processDefinition.getResourceName();
		this.deploymentId = processDefinition.getDeploymentId();
		this.diagramResourceName = processDefinition.getDiagramResourceName();
		this.hasStartFormKey = processDefinition.hasStartFormKey();
		this.hasGraphicalNotation = processDefinition.hasGraphicalNotation();
		this.suspended = processDefinition.isSuspended();
		this.tenantId = processDefinition.getTenantId();				
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public String getDiagramResourceName() {
		return diagramResourceName;
	}

	public void setDiagramResourceName(String diagramResourceName) {
		this.diagramResourceName = diagramResourceName;
	}

	public boolean isHasStartFormKey() {
		return hasStartFormKey;
	}

	public void setHasStartFormKey(boolean hasStartFormKey) {
		this.hasStartFormKey = hasStartFormKey;
	}

	public boolean isHasGraphicalNotation() {
		return hasGraphicalNotation;
	}

	public void setHasGraphicalNotation(boolean hasGraphicalNotation) {
		this.hasGraphicalNotation = hasGraphicalNotation;
	}

	public boolean isSuspended() {
		return suspended;
	}

	public void setSuspended(boolean suspended) {
		this.suspended = suspended;
	}

	public String getTenantId() {
		return tenantId;
	}

	public void setTenantId(String tenantId) {
		this.tenantId = tenantId;
	}
}
