package org.simplifyworks.workflow.model.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import org.activiti.engine.form.AbstractFormType;
import org.simplifyworks.core.exception.CoreException;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class WorkflowActionFormType extends AbstractFormType {
	private static final String WF_PATTERN = "([^\\(]+)[\\(]([^\\)]*)[\\)]";
	public static final Pattern PERMISSIONS_PATTERN = Pattern.compile(WF_PATTERN);
	public static final Pattern FUNCTION_PATTERN = Pattern.compile(WF_PATTERN);
	
	public static final String NAME = "action";
	
	public static final String LABEL_PARAMETER_NAME = "label";
	public static final String TOOLTIP_PARAMETER_NAME = "tooltip";
	public static final String STYLE_PARAMETER_NAME = "style";
	public static final String FUNCTIONS_PARAMETER_NAME = "functions";
		
	public static final String CONFIRMATION_REQUIRED_PARAMETER_NAME = "confirmationRequired";
	
	public static final String PERMISSIONS_PARAMETER_NAME = "permissions";
	
	private ObjectMapper objectMapper = new ObjectMapper();
	protected Map<String, String> values;
	
	public WorkflowActionFormType(Map<String, String> values) {
		this.values = values;
	}
	
	public String getLabel() {
		return values.get(LABEL_PARAMETER_NAME);
	}
	
	public String getTooltip() {
		return values.get(TOOLTIP_PARAMETER_NAME);
	}
	
	public String getStyle() {
		return values.get(STYLE_PARAMETER_NAME);
	}
	
	public boolean isConfirmationRequired() {
		return values.containsKey(CONFIRMATION_REQUIRED_PARAMETER_NAME) && Boolean.parseBoolean(values.get(CONFIRMATION_REQUIRED_PARAMETER_NAME));
	}
	
	public List<WorkflowFunctionDto> getFunctions() {
	    if(values.containsKey(FUNCTIONS_PARAMETER_NAME)) {
		String functionsValue = values.get(FUNCTIONS_PARAMETER_NAME);
		
		return WorkflowUtils.getFunctions(functionsValue);
	    }
	    
	    return Collections.emptyList();
	}
	
	public List<WorkflowPermissionDto> getPermissions() {
		if(values.containsKey(PERMISSIONS_PARAMETER_NAME)) {

			String permissionsValue = values.get(PERMISSIONS_PARAMETER_NAME);
			
			return WorkflowUtils.getPermissions(permissionsValue);
		}
		
		return Collections.emptyList();
	}

	@Override
	public Object getInformation(String key) {
		return values.get(key);
	}
	
	@Override
	public String getName() {
		return NAME;
	}

	@Override
	public Object convertFormValueToModelValue(String propertyValue) {
        try {
            return objectMapper.readValue(propertyValue, WorkflowActionDto.class);
        } catch (IOException e) {
            throw new CoreException(e);
        }
	}

	@Override
	public String convertModelValueToFormValue(Object modelValue) {
		try {
			return objectMapper.writeValueAsString(modelValue);
		} catch (IOException e) {
			throw new CoreException(e);
		}
	}

	public static void main(String[] args) {
		for(String a : "roleInOrganization(admin, ders);role(iga_user);roleInOrganization(iga_test, test);xy(a)".split(";")) {
			System.out.println(a);
		}
	}
}
