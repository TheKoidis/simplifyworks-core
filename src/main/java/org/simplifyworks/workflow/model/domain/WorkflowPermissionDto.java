package org.simplifyworks.workflow.model.domain;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class WorkflowPermissionDto {

	private String type;
	private String[] parameters;
	
	public WorkflowPermissionDto(String type, String[] parameters) {
		this.type = type;
		this.parameters = parameters;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String[] getParameters() {
		return parameters;
	}
	
	public void setParameters(String[] parameters) {
		this.parameters = parameters;
	}
}
