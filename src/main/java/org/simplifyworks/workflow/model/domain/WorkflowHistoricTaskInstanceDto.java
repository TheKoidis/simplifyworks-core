package org.simplifyworks.workflow.model.domain;

import java.util.Date;
import org.activiti.engine.history.HistoricTaskInstance;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class WorkflowHistoricTaskInstanceDto {

	private String assignee;
	private Date startTime;
	private Date endTime;
	private String name;
	private String processDefinitionId;

	public WorkflowHistoricTaskInstanceDto(HistoricTaskInstance historicTaskInstance) {
		assignee = historicTaskInstance.getAssignee();
		startTime = historicTaskInstance.getStartTime();
		endTime = historicTaskInstance.getEndTime();
		name = historicTaskInstance.getName();
		processDefinitionId = historicTaskInstance.getProcessDefinitionId();
	}

	public String getAssignee() {
		return assignee;
	}

	public void setAssignee(String assignee) {
		this.assignee = assignee;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getProcessDefinitionId() {
		return processDefinitionId;
	}

	public void setProcessDefinitionId(String processDefinitionId) {
		this.processDefinitionId = processDefinitionId;
	}

}
