package org.simplifyworks.workflow.model.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import org.simplifyworks.core.exception.CoreException;
import static org.simplifyworks.workflow.model.domain.WorkflowActionFormType.PERMISSIONS_PATTERN;
import static org.simplifyworks.workflow.model.domain.WorkflowActionFormType.FUNCTION_PATTERN;
/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class WorkflowUtils {
	private static final String FUNCTION_SPLIT_STRING = "\\s*\\|\\s*";
	private static final String PARAMETERS_SPLIT_STRING = "\\s*[,]\\s*";

	public static List<WorkflowPermissionDto> getPermissions(String permissionsValue) throws CoreException {
		List<WorkflowPermissionDto> permissions = new ArrayList<>();

		for (String permission : permissionsValue.split(FUNCTION_SPLIT_STRING)) {
			Matcher matcher = PERMISSIONS_PATTERN.matcher(permission);

			if (matcher.matches()) {
				String permissionType = matcher.group(1);
				String[] permissionParameters = matcher.group(2).split(PARAMETERS_SPLIT_STRING);

				permissions.add(new WorkflowPermissionDto(permissionType, permissionParameters));
			} else {
				throw new CoreException("Permission in unknown format: " + permission);
			}
		}

		return permissions;
	}
	
	public static List<WorkflowFunctionDto> getFunctions(String functionsValue) throws CoreException {
		List<WorkflowFunctionDto> functions = new ArrayList<>();

		for (String function : functionsValue.split(FUNCTION_SPLIT_STRING)) {
			Matcher matcher = FUNCTION_PATTERN.matcher(function);

			if (matcher.matches()) {
				String functionType = matcher.group(1);
				String[] functionParameters = matcher.group(2).split(PARAMETERS_SPLIT_STRING);

				functions.add(new WorkflowFunctionDto(functionType, functionParameters));
			} else {
				throw new CoreException("Function in unknown format: " + function);
			}
		} 

		return functions;
	}
}
