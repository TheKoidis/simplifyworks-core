/**
 * 
 */
package org.simplifyworks.workflow.model.domain;

import java.util.LinkedHashMap;
import java.util.Map;

import org.activiti.bpmn.model.FormProperty;
import org.activiti.bpmn.model.FormValue;
import org.activiti.engine.form.AbstractFormType;
import org.activiti.engine.impl.form.BooleanFormType;
import org.activiti.engine.impl.form.DateFormType;
import org.activiti.engine.impl.form.DoubleFormType;
import org.activiti.engine.impl.form.FormTypes;
import org.activiti.engine.impl.form.LongFormType;
import org.activiti.engine.impl.form.StringFormType;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class WorkflowFormTypes extends FormTypes {

	public WorkflowFormTypes() {
		addFormType(new StringFormType());
		addFormType(new LongFormType());
		addFormType(new DateFormType("dd/MM/yyyy"));
		addFormType(new BooleanFormType());
		addFormType(new DoubleFormType());
	}

	@Override
	public AbstractFormType parseFormPropertyType(FormProperty formProperty) {
		if (WorkflowActionFormType.NAME.equals(formProperty.getType())) {
			Map<String, String> values = new LinkedHashMap<>();
			
			for(FormValue formValue: formProperty.getFormValues()) {
				values.put(formValue.getId(), formValue.getName());
			}
			
			return new WorkflowActionFormType(values);
		} else {
			return super.parseFormPropertyType(formProperty);
		}
	}
}
