package org.simplifyworks.workflow.model.domain;

import java.util.List;

import javax.validation.constraints.Size;

import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;

public class WorkflowUploadDto {

	@Size(min = 1)
	private List<CoreAttachmentDto> attachments;
	
	public List<CoreAttachmentDto> getAttachments() {
		return attachments;
	}
	
	public void setAttachments(List<CoreAttachmentDto> attachments) {
		this.attachments = attachments;
	}
}
