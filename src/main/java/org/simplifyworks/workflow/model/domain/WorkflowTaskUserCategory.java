package org.simplifyworks.workflow.model.domain;

public enum WorkflowTaskUserCategory {

	ASSIGNEE,
	CANDIDATE,
	HISTORIC_ASSIGNEE
}
