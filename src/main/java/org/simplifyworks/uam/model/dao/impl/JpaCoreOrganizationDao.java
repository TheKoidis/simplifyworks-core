package org.simplifyworks.uam.model.dao.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.model.dao.impl.JpaAnyTypeDao;
import org.simplifyworks.uam.model.dao.CoreOrganizationDao;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kakrda
 */
@Repository
public class JpaCoreOrganizationDao extends JpaAnyTypeDao<CoreOrganization> implements CoreOrganizationDao {
    
    @Override
    public List<CoreOrganization> getSubOrganizations(Long parentId) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	CriteriaQuery<CoreOrganization> cq = cb.createQuery(clazz);
	Root<CoreOrganization> root = cq.from(clazz);
        
        Join<CoreOrganization, CoreOrganization> join = root.join(CoreOrganization_.organization);
	cq.where(
            cb.equal(
                join.get(CoreOrganization_.id),
                parentId
            )
	);	
	return entityManager.createQuery(cq).getResultList();        
    }
    
}
