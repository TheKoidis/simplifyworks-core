package org.simplifyworks.uam.model.dao;

import org.simplifyworks.core.model.dao.AnyTypeDao;
import org.simplifyworks.uam.model.entity.CoreRole;

/**
 *
 * @author kakrda
 */
public interface CoreRoleDao extends AnyTypeDao<CoreRole> {
    
    public CoreRole getByName(String name);
    
}
