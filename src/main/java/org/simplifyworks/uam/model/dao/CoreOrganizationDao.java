package org.simplifyworks.uam.model.dao;

import java.util.List;
import org.simplifyworks.core.model.dao.AnyTypeDao;
import org.simplifyworks.uam.model.entity.CoreOrganization;

/**
 *
 * @author kakrda
 */
public interface CoreOrganizationDao extends AnyTypeDao<CoreOrganization> {
    
    public List<CoreOrganization> getSubOrganizations(Long parentId);
    
}
