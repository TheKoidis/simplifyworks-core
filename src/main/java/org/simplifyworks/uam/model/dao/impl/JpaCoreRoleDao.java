package org.simplifyworks.uam.model.dao.impl;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.model.dao.impl.JpaAnyTypeDao;
import org.simplifyworks.uam.model.dao.CoreRoleDao;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.springframework.stereotype.Repository;

/**
 *
 * @author kakrda
 */
@Repository
public class JpaCoreRoleDao extends JpaAnyTypeDao<CoreRole> implements CoreRoleDao {
    
    @Override
    public CoreRole getByName(String name) {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	CriteriaQuery<CoreRole> cq = cb.createQuery(clazz);
	Root<CoreRole> root = cq.from(clazz);
        
	cq.where(
            cb.equal(root.get(CoreRole_.name), name)
	);	
	return entityManager.createQuery(cq).getSingleResult();        
    }
    
}
