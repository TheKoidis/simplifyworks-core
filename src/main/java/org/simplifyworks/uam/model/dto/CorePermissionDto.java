package org.simplifyworks.uam.model.dto;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.uam.model.domain.PermissionFor;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
public class CorePermissionDto extends AbstractDto {

    @NotNull
    private Boolean active;
    @NotNull
    private Boolean canDelete;
    @Size(max = 100)
    private String objectIdentifier;
    @NotNull
    @Size(max = 150)
    private String objectType;
	private CoreUserDto user;
    @NotNull
    private Boolean canWrite;
    private PermissionFor permissionFor;
    @NotNull
    private Boolean canRead;
    private CoreOrganizationDto organization;
    private CoreRoleDto role;
    @Size(max = 150)
    private String relType;
    @Max(999999999999999999L)
    private Long relId;
    private Date validFrom;
    private Date validTill;

    public CorePermissionDto() {
    }

    public CorePermissionDto(Long id) {
        super(id);
    }

    public Boolean isActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Boolean getCanDelete() {
        return canDelete;
    }

    public void setCanDelete(Boolean canDelete) {
        this.canDelete = canDelete;
    }

    public String getObjectIdentifier() {
        return objectIdentifier;
    }

    public void setObjectIdentifier(String objectIdentifier) {
        this.objectIdentifier = objectIdentifier;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

	public CoreUserDto getUser() {
        return user;
    }

	public void setUser(CoreUserDto coreUser) {
        this.user = coreUser;
    }

    public Boolean getCanWrite() {
        return canWrite;
    }

    public void setCanWrite(Boolean canWrite) {
        this.canWrite = canWrite;
    }

    public PermissionFor getPermissionFor() {
        return permissionFor;
    }

    public void setPermissionFor(PermissionFor permissionFor) {
        this.permissionFor = permissionFor;
    }

    public Boolean getCanRead() {
        return canRead;
    }

    public void setCanRead(Boolean canRead) {
        this.canRead = canRead;
    }

    public CoreOrganizationDto getOrganization() {
        return organization;
    }

    public void setOrganization(CoreOrganizationDto organization) {
        this.organization = organization;
    }

    public CoreRoleDto getRole() {
        return role;
    }

    public void setRole(CoreRoleDto role) {
        this.role = role;
    }

    public String getRelType() {
        return relType;
    }

    public void setRelType(String relType) {
        this.relType = relType;
    }

    public Long getRelId() {
        return relId;
    }

    public void setRelId(Long relId) {
        this.relId = relId;
    }

    public Date getValidFrom() {
        return validFrom;
    }

    public void setValidFrom(Date validFrom) {
        this.validFrom = validFrom;
    }

    public Date getValidTill() {
        return validTill;
    }

    public void setValidTill(Date validTill) {
        this.validTill = validTill;
    }
     
}
