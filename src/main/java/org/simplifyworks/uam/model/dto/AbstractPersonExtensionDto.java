package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public abstract class AbstractPersonExtensionDto extends AbstractDto {

	private CorePersonDto corePerson;
	
	public CorePersonDto getCorePerson() {
		return corePerson;
	}
	
	public void setCorePerson(CorePersonDto corePerson) {
		this.corePerson = corePerson;
	}
}
