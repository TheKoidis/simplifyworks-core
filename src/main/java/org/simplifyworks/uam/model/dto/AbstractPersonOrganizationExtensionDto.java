package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "@class")
public abstract class AbstractPersonOrganizationExtensionDto extends AbstractDto {

	private CorePersonOrganizationDto corePersonOrganization;
	
	public CorePersonOrganizationDto getCorePersonOrganization() {
		return corePersonOrganization;
	}
	
	public void setCorePersonOrganization(CorePersonOrganizationDto corePersonOrganization) {
		this.corePersonOrganization = corePersonOrganization;
	}
}
