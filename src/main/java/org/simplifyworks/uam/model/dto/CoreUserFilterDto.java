package org.simplifyworks.uam.model.dto;

import javax.validation.constraints.Size;
import org.simplifyworks.core.model.dto.AbstractFilterDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreUserFilterDto extends AbstractFilterDto {

	private Long personId;
	@Size(max = 50)
	private String username;

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

}
