package org.simplifyworks.uam.model.dto;

import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractFilterDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CorePersonFilterDto extends AbstractFilterDto {

	@Size(max = 255)
	private String personalNumber;
	@Size(max = 100)
	private String firstname;
	@Size(max = 100)
	private String surname;
	@Size(max = 255)
    private String email;

	public String getPersonalNumber() {
		return personalNumber;
	}

	public void setPersonalNumber(String personalNumber) {
		this.personalNumber = personalNumber;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
        

}
