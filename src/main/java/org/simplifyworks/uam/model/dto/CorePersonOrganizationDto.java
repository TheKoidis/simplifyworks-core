package org.simplifyworks.uam.model.dto;

import java.math.BigDecimal;
import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CorePersonOrganizationDto extends AbstractDto {

	@NotNull
	private CorePersonDto person;
	@NotNull
	private CoreOrganizationDto organization;
	@Size(max = 50)
	private String category;
	private Date validFrom;
	private Date validTo;
	@Min(0)
	@Max(1)
	private BigDecimal obligation;

	public CorePersonOrganizationDto() {
	}

	public CorePersonOrganizationDto(Long id) {
		super(id);
	}

	public CorePersonDto getPerson() {
		return person;
	}

	public void setPerson(CorePersonDto person) {
		this.person = person;
	}

	public CoreOrganizationDto getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganizationDto organization) {
		this.organization = organization;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public BigDecimal getObligation() {
		return obligation;
	}

	public void setObligation(BigDecimal obligation) {
		this.obligation = obligation;
	}
}
