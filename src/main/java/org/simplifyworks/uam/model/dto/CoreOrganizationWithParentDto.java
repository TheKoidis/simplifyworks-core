package org.simplifyworks.uam.model.dto;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class CoreOrganizationWithParentDto extends CoreOrganizationDto {

	private CoreOrganizationDto organization;
	
	public CoreOrganizationDto getOrganization() {
		return organization;
	}
	
	public void setOrganization(CoreOrganizationDto organization) {
		this.organization = organization;
	}
}
