/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Date;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * Security user
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class)
public class CoreUserDto extends AbstractDto {

	@NotNull
	@Size(min = 3, max = 50)
	private String username;
	@Size(max = 255)
	@JsonIgnore
	@XmlTransient
	private char[] password;
	private Date lastLogin;
	private char[] newPassword;
	private CorePersonDto person;

	public CoreUserDto() {
	}

	public CoreUserDto(Long id) {
		super(id);
	}

	public CoreUserDto(String username, char[] password) {
		this.username = username;
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public char[] getPassword() {
		return password;
	}

	public void setPassword(char[] password) {
		this.password = password;
	}

	public Date getLastLogin() {
		return lastLogin;
	}

	public void setLastLogin(Date lastLogin) {
		this.lastLogin = lastLogin;
	}

	public char[] getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(char[] newPassword) {
		this.newPassword = newPassword;
	}

	public CorePersonDto getPerson() {
		return person;
	}

	public void setPerson(CorePersonDto person) {
		this.person = person;
	}

}
