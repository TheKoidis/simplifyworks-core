/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreOrganizationWithoutRelationsDto extends AbstractDto {

	@NotNull
	@Size(max = 100)
	private String name;
	@Size(max = 50)
	private String code;
	@Size(max = 50)
	private String abbreviation;
	@Max(999999999999999999L)
	private Long unit;
	@NotNull
	private boolean active = true;
	private Date validFrom;
	private Date validTo;

	public CoreOrganizationWithoutRelationsDto() {
	}

	public CoreOrganizationWithoutRelationsDto(Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Long getUnit() {
		return unit;
	}

	public void setUnit(Long unit) {
		this.unit = unit;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

}
