package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * @author SimplifyWorks Generator
 * @since 2015-07-16
 */
public class CorePermissionWorkflowDto extends CorePermissionDto {

private String wfDefinitionId;
private String wfUserTask;
private String wfActionType;
private String wfActionKey;


public CorePermissionWorkflowDto() { }

public CorePermissionWorkflowDto(Long id) { super(id); }


    public String getWfDefinitionId() {
        return wfDefinitionId;
    }

    public void setWfDefinitionId(String wfDefinitionId) {
        this.wfDefinitionId = wfDefinitionId;
    }

    public String getWfUserTask() {
        return wfUserTask;
    }


    public void setWfUserTask(String wfUserTask) {
        this.wfUserTask = wfUserTask;
    }


    public String getWfActionType() {
        return wfActionType;
    }


    public void setWfActionType(String wfActionType) {
        this.wfActionType = wfActionType;
    }


    public String getWfActionKey() {
        return wfActionKey;
    }

    public void setWfActionKey(String wfActionKey) {
        this.wfActionKey = wfActionKey;
    }
}