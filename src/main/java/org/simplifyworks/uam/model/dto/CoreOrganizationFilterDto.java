package org.simplifyworks.uam.model.dto;

import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractFilterDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreOrganizationFilterDto extends AbstractFilterDto {

	private Long parentId;
	@Size(max = 100)
	private String name;
	@Size(max = 50)
	private String code;
	@Size(max = 50)
	private String abbreviation;

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

}
