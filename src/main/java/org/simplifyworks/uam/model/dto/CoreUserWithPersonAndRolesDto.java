package org.simplifyworks.uam.model.dto;

import java.util.List;

public class CoreUserWithPersonAndRolesDto extends CoreUserDto {

	private List<CoreUserRoleOrganizationDto> roles;
	
	public CoreUserWithPersonAndRolesDto() {
	}

	public List<CoreUserRoleOrganizationDto> getRoles() {
		return roles;
	}
	
	public void setRoles(List<CoreUserRoleOrganizationDto> roles) {
		this.roles = roles;
	}
}
