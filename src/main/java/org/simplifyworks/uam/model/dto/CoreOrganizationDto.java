/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreOrganizationDto extends CoreOrganizationWithoutRelationsDto {

	private CoreOrganizationWithoutRelationsDto organization;

	public CoreOrganizationDto() {
	}

	public CoreOrganizationDto(Long id) {
		super(id);
	}

	public CoreOrganizationWithoutRelationsDto getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganizationWithoutRelationsDto organization) {
		this.organization = organization;
	}

}
