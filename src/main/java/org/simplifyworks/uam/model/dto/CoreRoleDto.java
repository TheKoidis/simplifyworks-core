package org.simplifyworks.uam.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreRoleDto extends AbstractDto {

    @Size(min = 1, max = 64)
    @NotNull
    private String name;
    @Size(max = 255)
    private String description;

    public CoreRoleDto() {
    }

    public CoreRoleDto(Long id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
