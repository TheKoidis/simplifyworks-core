package org.simplifyworks.uam.model.dto;

import java.util.Collection;
import org.simplifyworks.core.model.dto.AbstractFilterDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreUserRoleOrganizationFilterDto extends AbstractFilterDto {

	private Long organizationId;
	private Long roleId;
	private Long userId;
	private String userName;
	private Collection<String> roleNames;
	private Collection<String> organizationCodes;

	public Long getOrganizationId() {
		return organizationId;
	}

	public void setOrganizationId(Long organizationId) {
		this.organizationId = organizationId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public Long getRoleId() {
		return roleId;
	}

	public void setRoleId(Long roleId) {
		this.roleId = roleId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Collection<String> getRoleNames() {
		return roleNames;
	}

	public void setRoleNames(Collection<String> roleNames) {
		this.roleNames = roleNames;
	}

	public Collection<String> getOrganizationCodes() {
		return organizationCodes;
	}

	public void setOrganizationCodes(Collection<String> organizationCodes) {
		this.organizationCodes = organizationCodes;
	}

}
