/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.dto;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import java.util.Date;
import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class)
public class CoreUserRoleOrganizationDto extends AbstractDto {

	private Date validFrom;
	private Date validTill;
	private CoreUserDto user;
	private CoreRoleDto role;
	private CoreOrganizationDto organization;

	public CoreUserRoleOrganizationDto() {
	}

	public CoreUserRoleOrganizationDto(Long id) {
		super(id);
	}

	public CoreRoleDto getRole() {
		return role;
	}

	public void setRole(CoreRoleDto role) {
		this.role = role;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	public CoreOrganizationDto getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganizationDto organization) {
		this.organization = organization;
	}

	public CoreUserDto getUser() {
		return user;
	}

	public void setUser(CoreUserDto user) {
		this.user = user;
	}

}
