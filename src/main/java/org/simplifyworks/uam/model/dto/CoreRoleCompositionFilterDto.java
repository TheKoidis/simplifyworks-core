package org.simplifyworks.uam.model.dto;

import org.simplifyworks.core.model.dto.AbstractFilterDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreRoleCompositionFilterDto extends AbstractFilterDto {

	private Long superiorRoleId;
	private Long subRoleId;

	public Long getSuperiorRoleId() {
		return superiorRoleId;
	}

	public void setSuperiorRoleId(Long superiorRoleId) {
		this.superiorRoleId = superiorRoleId;
	}

	public Long getSubRoleId() {
		return subRoleId;
	}

	public void setSubRoleId(Long subRoleId) {
		this.subRoleId = subRoleId;
	}

}
