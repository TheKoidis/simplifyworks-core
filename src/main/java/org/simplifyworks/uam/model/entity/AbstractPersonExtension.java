package org.simplifyworks.uam.model.entity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "CORE_PERSON_EXT")
public abstract class AbstractPersonExtension extends AbstractEntity {

	@JoinColumn(name = "CORE_PERSON_ID", referencedColumnName = "ID")
	@OneToOne
	private CorePerson corePerson;

	public CorePerson getCorePerson() {
		return corePerson;
	}

	public void setCorePerson(CorePerson corePerson) {
		this.corePerson = corePerson;
	}

}
