package org.simplifyworks.uam.model.entity;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "CORE_PERSON_ORGANIZATION_EXT")
public abstract class AbstractPersonOrganizationExtension extends AbstractEntity {

	@JoinColumn(name = "CORE_PERSON_ORGANIZATION_ID", referencedColumnName = "ID")
	@OneToOne
	private CorePersonOrganization corePersonOrganization;

	public CorePersonOrganization getCorePersonOrganization() {
		return corePersonOrganization;
	}

	public void setCorePersonOrganization(CorePersonOrganization corePersonOrganization) {
		this.corePersonOrganization = corePersonOrganization;
	}

}
