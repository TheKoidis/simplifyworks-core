package org.simplifyworks.uam.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;

import org.hibernate.annotations.BatchSize;
import org.simplifyworks.core.model.entity.AbstractEntity;

@Entity
@Table(name = "CORE_FLAT_ORGANIZATION")
public class CoreFlatOrganization extends AbstractEntity implements LevelInterface {

	@JoinColumn(name = "OWNER_ORG_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	@BatchSize(size = 100)
	private CoreOrganization owner;

	@JoinColumn(name = "OWNEE_ORG_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	@BatchSize(size = 100)
	private CoreOrganization ownee;

	@Max(999999)
	@Column(name = "LVL", precision = 6, scale = 0)
	private Integer level;

	public CoreFlatOrganization() {
	}

	public CoreFlatOrganization(CoreOrganization owner, CoreOrganization ownee) {
		this.owner = owner;
		this.ownee = ownee;
	}

	public CoreFlatOrganization(CoreOrganization owner, CoreOrganization ownee, Integer level) {
		this.owner = owner;
		this.ownee = ownee;
		this.level = level;
	}

	public CoreOrganization getOwner() {
		return owner;
	}

	public void setOwner(CoreOrganization owner) {
		this.owner = owner;
	}

	public CoreOrganization getOwnee() {
		return ownee;
	}

	public void setOwnee(CoreOrganization ownee) {
		this.ownee = ownee;
	}
	@Override
	public Integer getLevel() {
		return level;
	}

	@Override
	public void setLevel(Integer level) {
		this.level = level;
	}

}
