/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import java.util.Date;
import java.util.Objects;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.OrderBy;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.BatchSize;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Permission(createRoles = {"core_organization_write"}, readRoles = {"core_user", "core_organization_read"}, writeRoles = {"core_organization_write"}, deleteRoles = {"core_organization_delete"})
@BatchSize(size = 100)
public class CoreOrganization extends AbstractEntity implements LevelInterface {

	@NotNull
	@Basic(optional = false)
	@Column(nullable = false, length = 100)
	@Size(max = 100)
	private String name;
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PARENT_ID", referencedColumnName = "ID")
	@BatchSize(size = 100)
	private CoreOrganization organization;
	@Size(max = 50)
	@Column(length = 50)
	private String code;
	@Size(max = 50)
	@Column(length = 50)
	private String abbreviation;
	@Max(999999999999999999L)
	@Column(name = "UNIT", precision = 18, scale = 0)
	private Long unit;
	@NotNull
	@Column(name = "ACTIVE", nullable = false)
	private boolean active = true;
	@Column(name = "VALID_FROM")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date validFrom;
	@Column(name = "VALID_TO")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date validTo;

	@OneToOne(mappedBy = "coreOrganization", cascade = {CascadeType.ALL}, fetch = FetchType.LAZY)
	@BatchSize(size = 100)
	private AbstractOrganizationExtension organizationExtension;

	@OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
	private Set<CoreUserRoleOrganization> userRoles;
	
	@OrderBy("name")
	@OneToMany(mappedBy = "organization", fetch = FetchType.LAZY)
	private Set<CoreOrganization> organizations;

	@Transient
	private Integer level;

	public CoreOrganization() {
	}

	public CoreOrganization(Long id) {
		super(id);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CoreOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganization organization) {
		this.organization = organization;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAbbreviation() {
		return abbreviation;
	}

	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}

	public Long getUnit() {
		return unit;
	}

	public void setUnit(Long unit) {
		this.unit = unit;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTo() {
		return validTo;
	}

	public void setValidTo(Date validTo) {
		this.validTo = validTo;
	}

	public AbstractOrganizationExtension getOrganizationExtension() {
		return organizationExtension;
	}

	public void setOrganizationExtension(AbstractOrganizationExtension organizationExtension) {
		this.organizationExtension = organizationExtension;
	}

	public Set<CoreUserRoleOrganization> getUserRoles() {
		return userRoles;
	}

	public void setUserRoles(Set<CoreUserRoleOrganization> userRoles) {
		this.userRoles = userRoles;
	}

	public Set<CoreOrganization> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(Set<CoreOrganization> organizations) {
		this.organizations = organizations;
	}

	@Override
	public Integer getLevel() {
		return level;
	}

	@Override
	public void setLevel(Integer level) {
		this.level = level;
	}
        
        @Override
        public boolean equals(Object o) {
            boolean value = false;
            if (o instanceof CoreOrganization) {
                CoreOrganization org = (CoreOrganization) o;
                value = Objects.equals(org.getId(), getId());
            }
            return value;
        }

        @Override
        public int hashCode() {
            int hash = 7;
            hash = 17 * hash + (getId() != null ? getId().hashCode() : 0);
            return hash;
        }

}
