package org.simplifyworks.uam.model.entity;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Table(name = "CORE_ORGANIZATION_EXT")
@BatchSize(size = 100)
public abstract class AbstractOrganizationExtension extends AbstractEntity {

	@JoinColumn(name = "CORE_ORGANIZATION_ID", referencedColumnName = "ID")
	@OneToOne(fetch = FetchType.LAZY)
	private CoreOrganization coreOrganization;

	public CoreOrganization getCoreOrganization() {
		return coreOrganization;
	}

	public void setCoreOrganization(CoreOrganization coreOrganization) {
		this.coreOrganization = coreOrganization;
	}

}
