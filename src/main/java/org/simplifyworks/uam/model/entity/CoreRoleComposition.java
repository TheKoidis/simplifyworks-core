/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.model.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.uam.model.annotation.Permission;

/**
 * SuperiorRole inculde subRole (Admin > User)
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Entity
@Permission(createRoles = {"core_role_composition_write"}, readRoles = {"core_user", "core_role_composition_read"}, writeRoles = {"core_role_composition_write"}, deleteRoles = {"core_role_composition_delete"})
public class CoreRoleComposition extends AbstractEntity {

	@JoinColumn(name = "SUPERIOR_ROLE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private CoreRole superiorRole;
	@JoinColumn(name = "SUB_ROLE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	private CoreRole subRole;

	public CoreRoleComposition() {
	}

	public CoreRoleComposition(Long id) {
		super(id);
	}

	public CoreRole getSuperiorRole() {
		return superiorRole;
	}

	public void setSuperiorRole(CoreRole superiorRole) {
		this.superiorRole = superiorRole;
	}

	public CoreRole getSubRole() {
		return subRole;
	}

	public void setSubRole(CoreRole subRole) {
		this.subRole = subRole;
	}

}
