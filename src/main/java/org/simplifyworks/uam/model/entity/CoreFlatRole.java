package org.simplifyworks.uam.model.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.simplifyworks.core.model.entity.AbstractEntity;

@Entity
@Table(name = "CORE_FLAT_ROLE")
public class CoreFlatRole extends AbstractEntity {

	@JoinColumn(name = "OWNER_ROLE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	@BatchSize(size = 100)
	private CoreRole owner;
	
	@JoinColumn(name = "OWNEE_ROLE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = false)
	@BatchSize(size = 100)
	private CoreRole ownee;
	
	public CoreFlatRole() {
	}
	
	public CoreFlatRole(CoreRole owner, CoreRole ownee) {
		this.owner = owner;
		this.ownee = ownee;
	}

	public CoreRole getOwner() {
		return owner;
	}
	
	public void setOwner(CoreRole owner) {
		this.owner = owner;
	}
	
	public CoreRole getOwnee() {
		return ownee;
	}
	
	public void setOwnee(CoreRole ownee) {
		this.ownee = ownee;
	}
}
