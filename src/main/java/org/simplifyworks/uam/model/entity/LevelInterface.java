package org.simplifyworks.uam.model.entity;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public interface LevelInterface {

	public Integer getLevel();

	public void setLevel(Integer level);
}
