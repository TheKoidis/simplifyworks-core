package org.simplifyworks.uam.model.domain;

import java.util.Objects;

import org.simplifyworks.uam.model.entity.LevelInterface;
import org.springframework.util.Assert;

/**
 * Object for flat structures building purposes
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class FlatElement<E> {

	private Long id;
	
	private E owner;
	private E ownee;

	private Integer level;

	protected FlatElement() {
	}

	/**
	 * Creates a new instance
	 * 
	 * @param owner owner (required)
	 * @param ownee ownee (required)
	 */
	public FlatElement(E owner, E ownee) {
		this(null, owner, ownee);
	}
	
	/**
	 * Creates a new instance
	 *
	 * @param id id of entity
	 * @param owner owner (required)
	 * @param ownee ownee (required)
	 */
	public FlatElement(Long id, E owner, E ownee) {
		Assert.notNull(owner, "owner must be filled");
		Assert.notNull(ownee, "ownee must be filled");

		this.id = id;
		
		this.owner = owner;
		this.ownee = ownee;
		
		if (ownee instanceof LevelInterface) setLevel(((LevelInterface) ownee).getLevel());
	}
	
	public Long getId() {
		return id;
	}

	public E getOwner() {
		return owner;
	}

	protected void setOwner(E owner) {
		this.owner = owner;
	}

	public E getOwnee() {
		return ownee;
	}

	protected void setOwnee(E ownee) {
		this.ownee = ownee;
	}

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof FlatElement) {
			FlatElement<?> that = (FlatElement<?>) obj;

			return Objects.equals(this.owner, that.owner) && Objects.equals(this.ownee, that.ownee);
		}

		return false;
	}

	@Override
	public int hashCode() {
		return Objects.hash(owner, ownee);
	}
}
