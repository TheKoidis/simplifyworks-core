package org.simplifyworks.uam.model.domain;

/**
 * Enum define type of wf action
 * Created by Svanda on 16.7.2015.
 */
public enum PermissionActionType {
    FORWARD,BACKWARDS,NONE;
}
