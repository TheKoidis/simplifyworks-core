package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.SearchFilterManager;

import org.simplifyworks.uam.model.dto.CoreUserDto;
import org.simplifyworks.uam.model.dto.CoreUserFilterDto;
import org.simplifyworks.uam.model.entity.CoreUser;

/**
 * User manager
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CoreUserManager extends SearchFilterManager<CoreUserDto, CoreUser, CoreUserFilterDto> {

	public CoreUserDto load(Long id);

	public Long save(Long id, CoreUserDto dto);

	public Long create(CoreUserDto dto);

	public void delete(Long id);

	public CoreUser readUser(String username);
}
