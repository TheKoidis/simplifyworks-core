package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationDto;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationFilterDto;
import org.simplifyworks.uam.model.entity.CorePersonOrganization;

/**
 * Bludistak
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CorePersonOrganizationManager extends SearchLoadWriteManager<CorePersonOrganizationDto, CorePersonOrganization, CorePersonOrganizationFilterDto> {

}
