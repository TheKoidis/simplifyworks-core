package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.SearchFilterManager;

import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreRoleFilterDto;
import org.simplifyworks.uam.model.entity.CoreRole;

/**
 * Role manager
 *
 * @author VŠ
 */
public interface CoreRoleManager extends SearchFilterManager<CoreRoleDto, CoreRole, CoreRoleFilterDto> {
	
	/**
	 * Searches for role using its name
	 * 
	 * @param roleName name of role
	 * @return role or null if not found
	 */
	public CoreRoleDto searchUsingName(String roleName);

	public CoreRoleDto load(Long id);

	public Long save(Long id, CoreRoleDto dto);

	public Long create(CoreRoleDto dto);

	public void delete(Long id);
}
