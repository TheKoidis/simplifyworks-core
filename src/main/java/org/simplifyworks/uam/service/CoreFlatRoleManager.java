package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.uam.model.entity.CoreRole;

/**
 * Interface of flat role structure manager.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface CoreFlatRoleManager extends CoreFlatElementManager<CoreRole> {
	
	public List<String> findAllDescendantRoleNames(List<String> roleNames);

	public List<String> findAllAncestorRoleNames(String roleName);
}
