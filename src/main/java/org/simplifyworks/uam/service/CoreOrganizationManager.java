package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreOrganizationFilterDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;

/**
 * Organization manager
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CoreOrganizationManager extends SearchLoadWriteManager<CoreOrganizationDto, CoreOrganization, CoreOrganizationFilterDto> {
}
