package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.SearchFilterManager;

import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionFilterDto;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;

/**
 * Role manager
 *
 * @author VŠ
 */
public interface CoreRoleCompositionManager extends SearchFilterManager<CoreRoleCompositionDto, CoreRoleComposition, CoreRoleCompositionFilterDto> {

	public CoreRoleCompositionDto load(Long id);

	public Long save(Long id, CoreRoleCompositionDto dto);

	public Long create(CoreRoleCompositionDto dto);

	public void delete(Long id);
}
