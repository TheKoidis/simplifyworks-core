package org.simplifyworks.uam.service;

import java.util.Collection;
import java.util.List;
import org.simplifyworks.core.model.dto.SortDto;
import org.simplifyworks.core.service.SearchFilterManager;

import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationFilterDto;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization;

/**
 *
 * @author Svanda
 */
public interface CoreUserRoleOrganizationManager extends SearchFilterManager<CoreUserRoleOrganizationDto, CoreUserRoleOrganization, CoreUserRoleOrganizationFilterDto> {

	public CoreUserRoleOrganizationDto load(Long id);

	public Long save(Long id, CoreUserRoleOrganizationDto dto);

	public Long create(CoreUserRoleOrganizationDto dto);

	public void delete(Long id);

	public Collection<CoreUserRoleOrganizationDto> findByUsername(String username);

	/**
	 * Gets users that have role in organization (with role and organization
	 * composition)
	 *
	 * @param roleName
	 * @param organizationName
	 * @return users
	 */
	public Collection<CoreUserRoleOrganizationDto> findByOrganizationAndRole(String roleName, String organizationName);

	public Collection<CoreUserRoleOrganizationDto> findByOrganizationAndRoleOrderedByLevel(String roleName, String organizationCode);

}
