/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreOrganizationFilterDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.service.CoreOrganizationManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
@Transactional
public class CoreOrganizationManagerImpl extends AbstractSearchLoadWriteManager<CoreOrganizationDto, CoreOrganization, CoreOrganizationFilterDto> implements CoreOrganizationManager {

	@Override
	public String[] getQuickSearchProperties() {
		String[] quickSearchProperties = {CoreOrganization_.name.getName(), CoreOrganization_.abbreviation.getName()};
		return quickSearchProperties;
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreOrganization> root, CoreOrganizationFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//parentId
		if (filter.getParentId() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(CoreOrganization_.organization).get(CoreOrganization_.id),
											filter.getParentId()
							)
			);
		}
		//name;
		if (filter.getName() != null) {
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(CoreOrganization_.name)),
											"%" + filter.getName().toLowerCase() + "%"
							)
			);
		}
		//code;
		if (filter.getCode() != null) {
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(CoreOrganization_.code)),
											filter.getCode().toLowerCase() + "%"
							)
			);
		}
		//abbreviation;
		if (filter.getAbbreviation() != null) {
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(CoreOrganization_.abbreviation)),
											"%" + filter.getAbbreviation().toLowerCase() + "%"
							)
			);
		}
	}

}
