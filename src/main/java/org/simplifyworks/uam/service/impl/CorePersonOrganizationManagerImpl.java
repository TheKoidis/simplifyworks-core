/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationDto;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationFilterDto;
import org.simplifyworks.uam.model.entity.AbstractPersonOrganizationExtension;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CorePersonOrganization;
import org.simplifyworks.uam.model.entity.CorePersonOrganization_;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.service.CorePersonOrganizationManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
@Transactional
public class CorePersonOrganizationManagerImpl extends AbstractSearchLoadWriteManager<CorePersonOrganizationDto, CorePersonOrganization, CorePersonOrganizationFilterDto> implements CorePersonOrganizationManager {

	@Override
	public CorePersonOrganization toEntity(CorePersonOrganizationDto object) {
		CorePersonOrganization entity = super.toEntity(object);
		AbstractPersonOrganizationExtension personOrganizationExtension = entity.getPersonOrganizationExtension();
		if (personOrganizationExtension != null) {
			personOrganizationExtension.setCorePersonOrganization(entity);
		}

		return entity;
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[0];
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CorePersonOrganization> root, CorePersonOrganizationFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//organizationId
		if (filter.getOrganizationId() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(CorePersonOrganization_.organization).get(CoreOrganization_.id),
											filter.getOrganizationId()
							)
			);
		}

		//personId
		if (filter.getPersonId() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(CorePersonOrganization_.person).get(CorePerson_.id),
											filter.getPersonId()
							)
			);
		}
	}
        
        @Override
	public CorePersonOrganizationDto load(Long id) {
		CorePersonOrganization entity = dataRepository.find(CorePersonOrganization.class, id);

		return toDto(entity);
	}

	@Override
	public Long save(Long id, CorePersonOrganizationDto dto) {
		CorePersonOrganization entity = (id == null ? new CorePersonOrganization() : dataRepository.find(CorePersonOrganization.class, id));

		mapper.map(dto, entity);

		if (entity.getId() == null) {
			dataRepository.persist(entity);
		} else {
			entity = dataRepository.update(entity);
		}

		return entity.getId();
	}

	@Override
	public Long create(CorePersonOrganizationDto dto) {
		return save(null, dto);
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(CorePersonOrganization.class, id);
	}
}
