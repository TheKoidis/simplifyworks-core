/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.service.impl.AbstractBaseManager;
import org.simplifyworks.uam.model.dto.CoreUserDto;
import org.simplifyworks.uam.model.dto.CoreUserFilterDto;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
@Transactional
public class CoreUserManagerImpl extends AbstractBaseManager<CoreUserDto, CoreUser> implements CoreUserManager {

	@Override
	public ExtendedArrayList<CoreUserDto> getAllWrapped(CoreUserFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		String[] quickSearchProperties = {CoreUser_.username.getName(), CoreUser_.person.getName() + "." + CorePerson_.personalNumber.getName(),
                    CoreUser_.person.getName() + "." + CorePerson_.surname.getName(), CoreUser_.person.getName() + "." + CorePerson_.firstname.getName()};
		return quickSearchProperties;
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreUser> root, CoreUserFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//personId
		if (filter.getPersonId() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(CoreUser_.person).get(CorePerson_.id),
											filter.getPersonId()
							)
			);
		}

		//username
		if (!StringUtils.isEmpty(filter.getUsername())) {
			filterParameters.add(
							criteriaBuilder.equal(
											criteriaBuilder.lower(root.get(CoreUser_.username)),
											filter.getUsername().toLowerCase()
							)
			);
		}
	}

	@Override
	public CoreUserDto load(Long id) {
		CoreUser entity = dataRepository.find(CoreUser.class, id);

		return toDto(entity);
	}

	@Override
	public Long save(Long id, CoreUserDto dto) {
		CoreUser entity = (id == null ? new CoreUser() : dataRepository.find(CoreUser.class, id));

		mapper.map(dto, entity);

		if (entity.getId() == null) {
			dataRepository.persist(entity);
		} else {
			entity = dataRepository.update(entity);
		}

		return entity.getId();
	}

	@Override
	public Long create(CoreUserDto dto) {
		return save(null, dto);
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(CoreUser.class, id);
	}

	@Override
	public List<CoreUser> read(CoreUserFilterDto filter) {
		return searchManager.read(filter, this);
	}

	public CoreUser readUser(String username) {
		CriteriaBuilder builder = dataRepository.getCriteriaBuilder();
		CriteriaQuery<CoreUser> criteria = builder.createQuery(CoreUser.class);

		Root<CoreUser> root = criteria.from(CoreUser.class);
		criteria.where(builder.equal(root.get(CoreUser_.username), username));

		return dataRepository.createQuery(criteria).getSingleResult();
	}
}
