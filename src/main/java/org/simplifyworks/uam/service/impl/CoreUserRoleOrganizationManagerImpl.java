/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.service.impl.AbstractBaseManager;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationFilterDto;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreFlatOrganizationManager;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.simplifyworks.uam.service.CoreUserRoleOrganizationManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
@Transactional
public class CoreUserRoleOrganizationManagerImpl extends AbstractBaseManager<CoreUserRoleOrganizationDto, CoreUserRoleOrganization> implements CoreUserRoleOrganizationManager {

	private static final Logger LOG = LoggerFactory.getLogger(CoreUserRoleOrganizationManagerImpl.class);

	@Autowired
	private CoreFlatRoleManager flatRoleManager;

	@Autowired
	private CoreFlatOrganizationManager flatOrganizationManager;

	@Override
	public Collection<CoreUserRoleOrganizationDto> findByUsername(String username) {
		Assert.hasLength(username, "Username cannot be null or empty");

		CoreUserRoleOrganizationFilterDto filter = new CoreUserRoleOrganizationFilterDto();
		filter.setUserName(username);
		return getAllWrapped(filter);
	}

	@Override
	public Collection<CoreUserRoleOrganizationDto> findByOrganizationAndRole(String roleName, String organizationCode) {
		List<String> ancestorRoles = flatRoleManager.findAllAncestorRoleNames(roleName);
		List<String> ancestorOrganizations = flatOrganizationManager.findAllAncestorOrganizationCodes(organizationCode);

		CoreUserRoleOrganizationFilterDto filter = new CoreUserRoleOrganizationFilterDto();
		filter.setOrganizationCodes(ancestorOrganizations);
		filter.setRoleNames(ancestorRoles);

		//TODO without security
		return getAllWrapped(filter);
	}

	@Override
	public Collection<CoreUserRoleOrganizationDto> findByOrganizationAndRoleOrderedByLevel(String roleName, String organizationCode) {
		List<String> ancestorRoles = flatRoleManager.findAllAncestorRoleNames(roleName);
		List<CoreOrganization> ancestorOrganizations = flatOrganizationManager.findAllAncestorsWithLevel(organizationCode);
		Map<String, Integer> map = new HashMap();
		//
		List<String> ancestorOrganizationCodes = new ArrayList<>();
		for (CoreOrganization ancestorOrganization : ancestorOrganizations) {
			ancestorOrganizationCodes.add(ancestorOrganization.getCode());
			map.put(ancestorOrganization.getCode(), ancestorOrganization.getLevel());
		}

		CoreUserRoleOrganizationFilterDto filter = new CoreUserRoleOrganizationFilterDto();
		filter.setOrganizationCodes(ancestorOrganizationCodes);
		filter.setRoleNames(ancestorRoles);

		ExtendedArrayList<CoreUserRoleOrganizationDto> allWrapped = getAllWrapped(filter);
		allWrapped.sort(new CoreUserRoleOrganizationDtoLevelCompratator(map));

		//TODO without security
		return allWrapped;
	}

	@Override
	public ExtendedArrayList<CoreUserRoleOrganizationDto> getAllWrapped(CoreUserRoleOrganizationFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[]{
			CoreUserRoleOrganization_.organization.getName() + "." + CoreOrganization_.name.getName(),
			CoreUserRoleOrganization_.role.getName() + "." + CoreRole_.name.getName(),
			CoreUserRoleOrganization_.user.getName() + "." + CoreUser_.username.getName()
		};
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreUserRoleOrganization> root, CoreUserRoleOrganizationFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//organizationId
		if (filter.getOrganizationId() != null) {
			filterParameters.add(
					criteriaBuilder.equal(
							root.get(CoreUserRoleOrganization_.organization).get(CoreOrganization_.id),
							filter.getOrganizationId()
					)
			);
		}
		//roleId
		if (filter.getRoleId() != null) {
			filterParameters.add(
					criteriaBuilder.equal(
							root.get(CoreUserRoleOrganization_.role).get(CoreRole_.id),
							filter.getRoleId()
					)
			);
		}
		//userId
		if (filter.getUserId() != null) {
			filterParameters.add(
					criteriaBuilder.equal(
							root.get(CoreUserRoleOrganization_.user).get(CoreUser_.id),
							filter.getUserId()
					)
			);
		}
		//username
		if (!StringUtils.isEmpty(filter.getUserName())) {
			filterParameters.add(
					criteriaBuilder.equal(
							criteriaBuilder.lower(root.get(CoreUserRoleOrganization_.user).get(CoreUser_.username)),
							filter.getUserName().toLowerCase()
					)
			);
		}
		//roleNames
		if (filter.getRoleNames() != null && !filter.getRoleNames().isEmpty()) {
			filterParameters.add(
					root.get(CoreUserRoleOrganization_.role).get(CoreRole_.name).in(
					filter.getRoleNames()));
		}
		//organizationCodes
		if (filter.getOrganizationCodes() != null && !filter.getOrganizationCodes().isEmpty()) {
			filterParameters.add(
					root.get(CoreUserRoleOrganization_.organization).get(CoreOrganization_.code).in(
					filter.getOrganizationCodes()));
		}
	}

	@Override
	public CoreUserRoleOrganizationDto load(Long id) {
		CoreUserRoleOrganization entity = dataRepository.find(CoreUserRoleOrganization.class, id);

		return toDto(entity);
	}

	@Override
	public Long save(Long id, CoreUserRoleOrganizationDto dto) {
		CoreUserRoleOrganization entity = (id == null ? new CoreUserRoleOrganization() : dataRepository.find(CoreUserRoleOrganization.class, id));

		mapper.map(dto, entity);

		if (entity.getId() == null) {
			dataRepository.persist(entity);
		} else {
			entity = dataRepository.update(entity);
		}

		return entity.getId();
	}

	@Override
	public Long create(CoreUserRoleOrganizationDto dto) {
		return save(null, dto);
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(CoreUserRoleOrganization.class, id);
	}

	@Override
	public List<CoreUserRoleOrganization> read(CoreUserRoleOrganizationFilterDto filter) {
		return searchManager.read(filter, this);
	}

	private class CoreUserRoleOrganizationDtoLevelCompratator implements Comparator<CoreUserRoleOrganizationDto> {

		private final Map<String, Integer> orgCodeLevelMap;

		public CoreUserRoleOrganizationDtoLevelCompratator(Map<String, Integer> orgCodeLevelMap) {
			this.orgCodeLevelMap = orgCodeLevelMap;
		}

		@Override
		public int compare(CoreUserRoleOrganizationDto o1, CoreUserRoleOrganizationDto o2) {
			LOG.debug("o1: " + o1.getOrganization().getCode() + "/" + orgCodeLevelMap.get(o1.getOrganization().getCode()).toString());
			LOG.debug("o2: " + o2.getOrganization().getCode() + "/" + orgCodeLevelMap.get(o2.getOrganization().getCode()).toString());
			return orgCodeLevelMap.get(o1.getOrganization().getCode()).compareTo(orgCodeLevelMap.get(o2.getOrganization().getCode()));
		}

	}
}
