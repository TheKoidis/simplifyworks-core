package org.simplifyworks.uam.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.simplifyworks.uam.model.domain.FlatElement;
import org.simplifyworks.uam.model.entity.CoreFlatOrganization;
import org.simplifyworks.uam.model.entity.CoreFlatOrganization_;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreOrganization_;
import org.simplifyworks.uam.service.CoreFlatOrganizationManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of {@link CoreFlatOrganizationManager}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
@Transactional
public class CoreFlatOrganizationManagerImpl extends AbstractCoreFlatElementManagerImpl<CoreOrganization, CoreOrganization, CoreFlatOrganization> implements CoreFlatOrganizationManager {

	@Override
	public List<String> findAllAncestorOrganizationCodes(String organizationCode) {
		List<String> ancestorOrganizationCodes = new ArrayList<>();
		for(CoreOrganization ancestorOrganization : findAllAncestorOrganization(organizationCode)) {
			ancestorOrganizationCodes.add(ancestorOrganization.getCode());
		}

		return ancestorOrganizationCodes;
	}

	@Override
	public List<CoreOrganization> findAllAncestorOrganization(String organizationCode) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreOrganization> query = criteriaBuilder.createQuery(CoreOrganization.class);

		Root<CoreOrganization> from = query.from(CoreOrganization.class);
		query.where(criteriaBuilder.equal(from.get(CoreOrganization_.code), organizationCode));

		CoreOrganization organization = entityManager.createQuery(query).getSingleResult();
		return findAllAncestors(organization);
	}

	public List<CoreOrganization> findAllAncestorsWithLevel(String organizationCode) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreOrganization> q = criteriaBuilder.createQuery(CoreOrganization.class);
		Root<CoreOrganization> f = q.from(CoreOrganization.class);
		q.where(criteriaBuilder.equal(f.get(CoreOrganization_.code), organizationCode));
		CoreOrganization organization = entityManager.createQuery(q).getSingleResult();

		CriteriaQuery<CoreFlatOrganization> query = criteriaBuilder.createQuery(CoreFlatOrganization.class);
		Root<CoreFlatOrganization> from = query.from(getFlatEntityClass());
		query.select(from);
		query.where(criteriaBuilder.equal(from.get(getOwneeAttribute()), organization));

		List<CoreFlatOrganization> lfo = entityManager.createQuery(query).getResultList();
		List<CoreOrganization> lo = new ArrayList<>();
		for (CoreFlatOrganization fo : lfo) {
			CoreOrganization owner = fo.getOwner();
			owner.setLevel(fo.getLevel());
			lo.add(owner);
		}
		return lo;
	}

	@Override
	protected Class<CoreOrganization> getEntityClass() {
		return CoreOrganization.class;
	}

	@Override
	protected Class<CoreOrganization> getCompositionClass() {
		return CoreOrganization.class;
	}

	@Override
	protected Class<CoreFlatOrganization> getFlatEntityClass() {
		return CoreFlatOrganization.class;
	}

	@Override
	protected CoreOrganization getOwner(CoreOrganization organization) {
		return organization.getOrganization();
	}

	@Override
	protected CoreOrganization getOwnee(CoreOrganization organization) {
		return organization;
	}

	@Override
	protected CoreFlatOrganization convert(FlatElement<CoreOrganization> element) {
		return new CoreFlatOrganization(element.getOwner(), element.getOwnee(), element.getLevel());
	}
	
	@Override
	protected FlatElement<CoreOrganization> convert(Long id, CoreOrganization owner, CoreOrganization ownee) {
		return new FlatElement<CoreOrganization>(id, owner, ownee);
	}
	
	@Override
	protected SingularAttribute<CoreFlatOrganization, CoreOrganization> getOwneeAttribute() {
		return CoreFlatOrganization_.ownee;
	}

	@Override
	protected SingularAttribute<CoreFlatOrganization, CoreOrganization> getOwnerAttribute() {
		return CoreFlatOrganization_.owner;
	}	
}
