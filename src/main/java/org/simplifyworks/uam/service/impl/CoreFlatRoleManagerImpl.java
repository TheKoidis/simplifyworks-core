package org.simplifyworks.uam.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.simplifyworks.uam.model.domain.FlatElement;
import org.simplifyworks.uam.model.entity.CoreFlatRole;
import org.simplifyworks.uam.model.entity.CoreFlatRole_;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of {@link CoreFlatRoleManager}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
@Transactional
public class CoreFlatRoleManagerImpl extends AbstractCoreFlatElementManagerImpl<CoreRole, CoreRoleComposition, CoreFlatRole> implements CoreFlatRoleManager {
	
	@Override
	public List<String> findAllDescendantRoleNames(List<String> roleNames) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreRole> query = criteriaBuilder.createQuery(CoreRole.class);
				
		Root<CoreRole> from = query.from(CoreRole.class);
		query.where(from.get(CoreRole_.name).in(roleNames));
		
		List<CoreRole> roles = entityManager.createQuery(query).getResultList();
		
		List<String> descendantRoleNames = new ArrayList<>();
		for(CoreRole descendantRole : findAllDescendants(roles)) {
			descendantRoleNames.add(descendantRole.getName());
		}
		
		return descendantRoleNames;
	}
	
	@Override
	public List<String> findAllAncestorRoleNames(String roleName) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoreRole> query = criteriaBuilder.createQuery(CoreRole.class);
				
		Root<CoreRole> from = query.from(CoreRole.class);
		query.where(criteriaBuilder.equal(from.get(CoreRole_.name), roleName));
		
		CoreRole role = entityManager.createQuery(query).getSingleResult();
		
		List<String> ancestorRoleNames = new ArrayList<>();
		for(CoreRole ancestorRole : findAllAncestors(role)) {
			ancestorRoleNames.add(ancestorRole.getName());
		}
		
		return ancestorRoleNames;
	}
	
	@Override
	protected Class<CoreRole> getEntityClass() {
		return CoreRole.class;
	}
	
	 @Override
	protected Class<CoreRoleComposition> getCompositionClass() {
		return CoreRoleComposition.class;
	}
	
	@Override
	protected Class<CoreFlatRole> getFlatEntityClass() {
		return CoreFlatRole.class;
	}

	@Override
	protected CoreRole getOwner(CoreRoleComposition composition) {
		return composition.getSuperiorRole();
	}

	@Override
	protected CoreRole getOwnee(CoreRoleComposition composition) {
		return composition.getSubRole();
	}
	
	@Override
	protected CoreFlatRole convert(FlatElement<CoreRole> element) {
		return new CoreFlatRole(element.getOwner(), element.getOwnee());
	}
	
	@Override
	protected FlatElement<CoreRole> convert(Long id, CoreRole owner, CoreRole ownee) {
		return new FlatElement<CoreRole>(id, owner, ownee); 
	}
	
	@Override
	protected SingularAttribute<CoreFlatRole, CoreRole> getOwneeAttribute() {
		return CoreFlatRole_.ownee;
	}	
	
	@Override
	protected SingularAttribute<CoreFlatRole, CoreRole> getOwnerAttribute() {
		return CoreFlatRole_.owner;
	}
}
