package org.simplifyworks.uam.service.impl;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.model.entity.AbstractEntity_;
import org.simplifyworks.uam.model.domain.FlatElement;
import org.simplifyworks.uam.model.entity.LevelInterface;
import org.simplifyworks.uam.service.CoreFlatElementManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Abstract implementation of flat element manager based on template method pattern.
 * This implementation uses clear all and create all pattern.
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
@Transactional
public abstract class AbstractCoreFlatElementManagerImpl<E, C, F extends AbstractEntity> implements CoreFlatElementManager<E> {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractCoreFlatElementManagerImpl.class);

	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;

	public List<E> findAllDescendants(E entity) {
		return findAllDescendants(Collections.singletonList(entity));
	}

	public List<E> findAllDescendants(List<E> entities) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> query = criteriaBuilder.createQuery(getEntityClass());

		Root<F> from = query.from(getFlatEntityClass());
		query.select(from.get(getOwneeAttribute()));
		query.where(from.get(getOwnerAttribute()).in(entities));

		return entityManager.createQuery(query).getResultList();
	}

	public List<E> findAllAncestors(E entity) {
		return findAllAncestors(Collections.singletonList(entity));
	}

	public List<E> findAllAncestors(List<E> entities) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> query = criteriaBuilder.createQuery(getEntityClass());

		Root<F> from = query.from(getFlatEntityClass());
		query.select(from.get(getOwnerAttribute()));
		query.where(from.get(getOwneeAttribute()).in(entities));

		return entityManager.createQuery(query).getResultList();
	}

	public void rebuild() {
		long start = System.currentTimeMillis();

		Set<FlatElement<E>> allFoundElements = findAllFlatElements();
		Set<FlatElement<E>> allPreparedElements = prepareAllFlatElements();
				
		int deletedRecords = removeOldElements(allFoundElements, allPreparedElements);
		int createdRecords = createNewElements(allFoundElements, allPreparedElements);

		if(LOG.isInfoEnabled()) {
			LOG.info("Rebuild of {} completed in {} ms - {} record(s) deleted, {} record(s) created", getFlatEntityClass().getSimpleName(), System.currentTimeMillis() - start, deletedRecords, createdRecords);
		}
	}
	
	/**
	 * Removes old elements (these which are in found-set and not in prepared-set)
	 *
	 * @return count of removed elements
	 */
	private int removeOldElements(Set<FlatElement<E>> allFoundElements, Set<FlatElement<E>> allPreparedElements) {
		Set<FlatElement<E>> elementsToRemove = new HashSet<>(allFoundElements);
		elementsToRemove.removeAll(allPreparedElements);
				
		for(FlatElement<E> element : elementsToRemove) {
			entityManager.remove(convert(element));
		}
		
		return elementsToRemove.size();
	}
	
	/**
	 * Creates new elements (these which are in prepared-set and not in found-set)
	 * 
	 * @return count of created elements
	 */
	private int createNewElements(Set<FlatElement<E>> allFoundElements, Set<FlatElement<E>> allPreparedElements) {
		Set<FlatElement<E>> elementsToCreate = new HashSet<>(allPreparedElements);
		elementsToCreate.removeAll(allFoundElements);
		
		for(FlatElement<E> element : elementsToCreate) {
			entityManager.persist(convert(element));
		}
		
		return elementsToCreate.size();
	}

	/**
	 * Finds all flat elements
	 * 
	 * @return set of all existing flat elements
	 */
	@SuppressWarnings("unchecked")
	private Set<FlatElement<E>> findAllFlatElements() {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Object[]> query = criteriaBuilder.createQuery(Object[].class);
		Root<F> root = query.from(getFlatEntityClass());
		
		query.multiselect(root.get(AbstractEntity_.id), root.get(getOwnerAttribute()), root.get(getOwneeAttribute()));

		List<Object[]> results = entityManager.createQuery(query).getResultList();
		Set<FlatElement<E>> elements = new HashSet<>();
		
		for(Object[] result : results) {
			elements.add(convert((Long) result[0], (E) result[1], (E) result[2]));
		}
		
		return elements;
	}

	/**
	 * Prepares all flat elements using descendants of all objects.
	 *
	 * @return set of all flat elements (uses entities and compositions)
	 */
	protected Set<FlatElement<E>> prepareAllFlatElements() {
		Set<FlatElement<E>> structureElements = new HashSet<>();

		List<E> entities = findAllEntities();
		List<C> compositions = findAllCompositions();

		for (E entity : entities) {
			for (E descendant : prepareDescendants(entity, compositions, 0)) {
				structureElements.add(new FlatElement<E>(entity, descendant));
			}
		}

		return structureElements;
	}

	/**
	 * Finds all entities using entity class.
	 *
	 * @return all entities
	 */
	private List<E> findAllEntities() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> query = builder.createQuery(getEntityClass());
		query.from(getEntityClass());

		return entityManager.createQuery(query).getResultList();
	}

	/**
	 * Finds all compositions using composition class.
	 *
	 * @return all compositions
	 */
	private List<C> findAllCompositions() {
		CriteriaBuilder builder = entityManager.getCriteriaBuilder();
		CriteriaQuery<C> query = builder.createQuery(getCompositionClass());
		query.from(getCompositionClass());

		return entityManager.createQuery(query).getResultList();
	}

	/**
	 * Prepares all descendants (all generations of children) of parent.
	 * This method uses recursion for searching.
	 */
	private Set<E> prepareDescendants(E entity, List<C> compositions, int level) {
		Set<E> descendants = new HashSet<>();
		descendants.add(entity);
		if(entity instanceof LevelInterface) ((LevelInterface) entity).setLevel(level);

		for (E child : prepareChildren(entity, compositions)) {
			descendants.addAll(prepareDescendants(child, compositions, level+1));
		}

		return descendants;
	}

	/**
	 * Prepares children (only nearest generation) of parent.
	 */
	private Set<E> prepareChildren(E entity, List<C> compositions) {
		Set<E> childs = new HashSet<>();

		for (C composition : compositions) {
			if (entity.equals(getOwner(composition))) {
				childs.add(getOwnee(composition));
			}
		}

		return childs;
	}

	/**
	 * Returns entity class (type of nodes in composition tree).
	 *
	 * @return entity class
	 */
	protected abstract Class<E> getEntityClass();

	/**
	 * Returns composition class (type of edges in composition tree).
	 *
	 * @return composition class
	 */
	protected abstract Class<C> getCompositionClass();

	/**
	 * Returns flat entity class (type of final flat structures).
	 *
	 * @return
	 */
	protected abstract Class<F> getFlatEntityClass();

	/**
	 * Returns owner entity for specified composition.
	 *
	 * @param composition composition (returned from {@link findAllCompositions})
	 * @return owner entity for composition
	 */
	protected abstract E getOwner(C composition);

	/**
	 * Returns ownee entity for specified composition
	 *
	 * @param composition composition (returned from {@link findAllCompositions})
	 * @return ownee entity for composition
	 */
	protected abstract E getOwnee(C composition);

	/**
	 * Converts common flat element into specific one.
	 *
	 * @param element converted flat element
	 * @return final flat element
	 */
	protected abstract F convert(FlatElement<E> element);
	
	/**
	 * Converts result into flat element
	 * 
	 * @param id id of entity
	 * @param owner owner
	 * @param ownee ownee
	 * @return flat element
	 */
	protected abstract FlatElement<E> convert(Long id, E owner, E ownee);
	
	protected abstract SingularAttribute<F, E> getOwneeAttribute();

	protected abstract SingularAttribute<F, E> getOwnerAttribute();
}
