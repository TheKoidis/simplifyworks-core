/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.service.impl.AbstractBaseManager;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreRoleFilterDto;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreRole_;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
@Transactional
public class CoreRoleManagerImpl extends AbstractBaseManager<CoreRoleDto, CoreRole> implements CoreRoleManager {

	@Override
	public CoreRoleDto searchUsingName(String roleName) {
		CoreRoleFilterDto filter = new CoreRoleFilterDto();
		filter.setRoleName(roleName);
		Collection<CoreRoleDto> result = getAllWrapped(filter);
		return result.isEmpty() ? null : result.iterator().next();
	}

	@Override
	public ExtendedArrayList<CoreRoleDto> getAllWrapped(CoreRoleFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[]{CoreRole_.name.getName()};
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreRole> root, CoreRoleFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//roleName
		if (!StringUtils.isEmpty(filter.getRoleName())) {
			filterParameters.add(
							criteriaBuilder.equal(
											criteriaBuilder.lower(root.get(CoreRole_.name)),
											filter.getRoleName().toLowerCase()
							)
			);
		}
	}

	@Override
	public CoreRoleDto load(Long id) {
		CoreRole entity = dataRepository.find(CoreRole.class, id);

		return toDto(entity);
	}

	@Override
	public Long save(Long id, CoreRoleDto dto) {
		CoreRole entity = (id == null ? new CoreRole() : dataRepository.find(CoreRole.class, id));

		mapper.map(dto, entity);

		if (entity.getId() == null) {
			dataRepository.persist(entity);
		} else {
			entity = dataRepository.update(entity);
		}

		return entity.getId();
	}

	@Override
	public Long create(CoreRoleDto dto) {
		return save(null, dto);
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(CoreRole.class, id);
	}

	@Override
	public List<CoreRole> read(CoreRoleFilterDto filter) {
		return searchManager.read(filter, this);
	}

}
