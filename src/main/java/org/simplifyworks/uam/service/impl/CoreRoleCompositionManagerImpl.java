/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.service.impl.AbstractBaseManager;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionFilterDto;
import org.simplifyworks.uam.model.entity.CoreRoleComposition;
import org.simplifyworks.uam.model.entity.CoreRoleComposition_;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.simplifyworks.uam.service.CoreRoleCompositionManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Svanda
 */
@Service
@Transactional
public class CoreRoleCompositionManagerImpl extends AbstractBaseManager<CoreRoleCompositionDto, CoreRoleComposition> implements CoreRoleCompositionManager {

	@Autowired
	private CoreFlatRoleManager flatRoleManager;

	@Override
	public Long create(CoreRoleCompositionDto dto) {
		return save(null, dto);
	}

	@Override
	public Long save(Long id, CoreRoleCompositionDto dto) {
		if (flatRoleManager.findAllAncestorRoleNames(dto.getSuperiorRole().getName()).contains(dto.getSubRole().getName())) {
			throw new CoreException("Role tree inconsistency");
		}

		CoreRoleComposition entity = (id == null ? new CoreRoleComposition() : dataRepository.find(CoreRoleComposition.class, id));

		mapper.map(dto, entity);

		if (entity.getId() == null) {
			dataRepository.persist(entity);
		} else {
			entity = dataRepository.update(entity);
		}

		return entity.getId();
	}

	@Override
	public CoreRoleCompositionDto load(Long id) {
		CoreRoleComposition entity = dataRepository.find(CoreRoleComposition.class, id);

		return toDto(entity);
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(CoreRoleComposition.class, id);
	}

	@Override
	public ExtendedArrayList<CoreRoleCompositionDto> getAllWrapped(CoreRoleCompositionFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[0];
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreRoleComposition> root, CoreRoleCompositionFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//subRoleId
		if (filter.getSubRoleId() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(CoreRoleComposition_.subRole),
											filter.getSubRoleId()
							)
			);
		}
		//superRoleId
		if (filter.getSuperiorRoleId() != null) {
			filterParameters.add(
							criteriaBuilder.equal(
											root.get(CoreRoleComposition_.superiorRole),
											filter.getSuperiorRoleId()
							)
			);
		}
	}

	@Override
	public List<CoreRoleComposition> read(CoreRoleCompositionFilterDto filter) {
		return searchManager.read(filter, this);
	}
}
