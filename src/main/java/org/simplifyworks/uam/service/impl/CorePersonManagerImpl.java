/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.uam.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractSearchLoadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.dto.CorePersonFilterDto;
import org.simplifyworks.uam.model.entity.CorePerson;
import org.simplifyworks.uam.model.entity.CorePerson_;
import org.simplifyworks.uam.service.CorePersonManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Service
@Transactional
public class CorePersonManagerImpl extends AbstractSearchLoadWriteManager<CorePersonDto, CorePerson, CorePersonFilterDto> implements CorePersonManager {

	@Override
	public String[] getQuickSearchProperties() {
		String[] quickSearchProperties = {CorePerson_.firstname.getName(), CorePerson_.surname.getName(), CorePerson_.personalNumber.getName(),
																			CorePerson_.email.getName()};
		return quickSearchProperties;
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CorePerson> root, CorePersonFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//personalNumber
		if (!StringUtils.isEmpty(filter.getPersonalNumber())) {
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(CorePerson_.personalNumber)),
											"%" + filter.getPersonalNumber().toLowerCase() + "%"
							)
			);
		}
		//firstname
		if (!StringUtils.isEmpty(filter.getFirstname())) {//TODO case insensitive - create some parent class to handle this in all managers
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(CorePerson_.firstname)),
											"%" + filter.getFirstname().toLowerCase() + "%"
							)
			);
		}
		//surname
		if (!StringUtils.isEmpty(filter.getSurname())) {
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(CorePerson_.surname)),
											"%" + filter.getSurname().toLowerCase() + "%"
							)
			);
		}
		//email
		if (!StringUtils.isEmpty(filter.getEmail())) {
			filterParameters.add(
							criteriaBuilder.like(
											criteriaBuilder.lower(root.get(CorePerson_.email)),
											"%" + filter.getEmail().toLowerCase() + "%"
							)
			);
		}
	}

}
