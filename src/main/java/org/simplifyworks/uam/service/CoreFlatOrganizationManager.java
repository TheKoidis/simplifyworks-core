package org.simplifyworks.uam.service;

import java.util.List;

import org.simplifyworks.uam.model.entity.CoreOrganization;

/**
 * Interface of flat organization structure manager.
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface CoreFlatOrganizationManager extends CoreFlatElementManager<CoreOrganization> {

	public List<String> findAllAncestorOrganizationCodes(String organizationCode);

	public List<CoreOrganization> findAllAncestorOrganization(String organizationCode);

	public List<CoreOrganization> findAllAncestorsWithLevel(String organizationCode);

}
