package org.simplifyworks.uam.service;

import java.util.List;

/**
 * Common interface for managers processing flat structures.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface CoreFlatElementManager<E> {

	public List<E> findAllDescendants(E entity);
	
	public List<E> findAllDescendants(List<E> entities);
	
	public List<E> findAllAncestors(E entity);
	
	public List<E> findAllAncestors(List<E> entities);
	
	/**
	 * Rebuilds completely flat structure.
	 */
	public void rebuild();
}
