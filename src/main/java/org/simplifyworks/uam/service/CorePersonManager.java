package org.simplifyworks.uam.service;

import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.dto.CorePersonFilterDto;
import org.simplifyworks.uam.model.entity.CorePerson;

/**
 * Bludistak
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface CorePersonManager extends SearchLoadWriteManager<CorePersonDto, CorePerson, CorePersonFilterDto> {
}
