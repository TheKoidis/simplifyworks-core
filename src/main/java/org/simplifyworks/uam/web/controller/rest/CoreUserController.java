package org.simplifyworks.uam.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.uam.model.dto.CoreUserDto;
import org.simplifyworks.uam.model.dto.CoreUserFilterDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationFilterDto;
import org.simplifyworks.uam.service.CoreUserManager;
import org.simplifyworks.uam.service.CoreUserRoleOrganizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping("/api/core/users")
public class CoreUserController {

	@Autowired
	private CoreUserManager manager;
	@Autowired
	private CoreUserRoleOrganizationManager userRoleOrganizationManager;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<CoreUserDto> getPersons(@Valid @RequestBody CoreUserFilterDto filter) {
		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody CoreUserDto dto) {
		char[] password = dto.getNewPassword();

		StringBuilder sb = new StringBuilder();
		sb.append(password);
		password = passwordEncoder.encode(sb).toCharArray();
		dto.setPassword(password);

		return manager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		manager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public CoreUserDto load(@PathVariable("id") Long id) {
		return manager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void save(@PathVariable("id") Long id, @Valid @RequestBody CoreUserDto dto) {
		char[] password = dto.getNewPassword();

		if (password == null || password.length == 0) {
			CoreUserDto refresh = manager.load(dto.getId());
			dto.setPassword(refresh.getPassword());
		} else {
			StringBuilder sb = new StringBuilder();
			sb.append(password);
			password = passwordEncoder.encode(sb).toCharArray();
			dto.setPassword(password);
		}

		manager.save(id, dto);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/role-organizations/filter")
	public ExtendedArrayList<CoreUserRoleOrganizationDto> getUserRoles(@PathVariable("id") Long id, @Valid @RequestBody CoreUserRoleOrganizationFilterDto filter) {
		filter.setUserId(id);

		return userRoleOrganizationManager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/role-organizations")
	public Long createUserRole(@Valid @RequestBody CoreUserRoleOrganizationDto dto) {
		return userRoleOrganizationManager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{userId}/role-organizations/{id}")
	public void deleteUserRole(@PathVariable("userId") Long userId, @PathVariable("id") Long id) {
		userRoleOrganizationManager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{userId}/role-organizations/{id}")
	public CoreUserRoleOrganizationDto loadUserRole(@PathVariable("userId") Long userId, @PathVariable("id") Long id) {
		return userRoleOrganizationManager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{userId}/role-organizations/{id}")
	public void saveUserRole(@PathVariable("userId") Long userId, @PathVariable("id") Long id, @Valid @RequestBody CoreUserRoleOrganizationDto dto) {
		userRoleOrganizationManager.save(id, dto);
	}
}
