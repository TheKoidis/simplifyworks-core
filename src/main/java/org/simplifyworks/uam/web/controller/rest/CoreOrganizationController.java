package org.simplifyworks.uam.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreOrganizationFilterDto;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationDto;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationFilterDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationFilterDto;
import org.simplifyworks.uam.service.CoreOrganizationManager;
import org.simplifyworks.uam.service.CorePersonOrganizationManager;
import org.simplifyworks.uam.service.CoreUserRoleOrganizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping(value = "/api/core/organizations")
public class CoreOrganizationController {

	@Autowired
	private CoreOrganizationManager manager;
	@Autowired
	private CoreUserRoleOrganizationManager userRoleOrganizationManager;
	@Autowired
	private CorePersonOrganizationManager personOrganizationManager;

	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<CoreOrganizationDto> getOrganizations(@Valid @RequestBody CoreOrganizationFilterDto filter) {
		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody CoreOrganizationDto dto) {
		return manager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		manager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public CoreOrganizationDto load(@PathVariable("id") Long id) {
		return manager.load(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/organizations/filter")
	public ExtendedArrayList<CoreOrganizationDto> getSubOrganizations(@PathVariable("id") Long id, @Valid @RequestBody CoreOrganizationFilterDto filter) {
		filter.setParentId(id);

		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/user-roles/filter")
	public ExtendedArrayList<CoreUserRoleOrganizationDto> getUserRoles(@PathVariable("id") Long id, @Valid @RequestBody CoreUserRoleOrganizationFilterDto filter) {
		filter.setOrganizationId(id);

		return userRoleOrganizationManager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/user-roles")
	public Long createUserRole(@Valid @RequestBody CoreUserRoleOrganizationDto dto) {
		return userRoleOrganizationManager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{organizationId}/user-roles/{id}")
	public void deleteUserRole(@PathVariable("organizationId") Long organizationId, @PathVariable("id") Long id) {
		userRoleOrganizationManager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{organizationId}/user-roles/{id}")
	public CoreUserRoleOrganizationDto loadUserRole(@PathVariable("organizationId") Long organizationId, @PathVariable("id") Long id) {
		return userRoleOrganizationManager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{organizationId}/user-roles/{id}")
	public void saveUserRole(@PathVariable("organizationId") Long organizationId, @PathVariable("id") Long id, @Valid @RequestBody CoreUserRoleOrganizationDto dto) {
		userRoleOrganizationManager.save(id, dto);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/persons/filter")
	public ExtendedArrayList<CorePersonOrganizationDto> getPersons(@PathVariable("id") Long id, @Valid @RequestBody CorePersonOrganizationFilterDto filter) {
		filter.setOrganizationId(id);

		return personOrganizationManager.getAllWrapped(filter);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/{id}/persons")
	public void createPersonOrganization(@Valid @RequestBody CorePersonOrganizationDto dto) {
		personOrganizationManager.create(dto);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/{organizationId}/persons/{id}")
	public void deletePersonOrganization(@PathVariable("organizationId") Long organizationId, @PathVariable("id") Long id) {
		personOrganizationManager.delete(id);
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/{organizationId}/persons/{id}")
	public CorePersonOrganizationDto savePersonOrganization(@PathVariable("organizationId") Long organizationId, @PathVariable("id") Long id) {
		return personOrganizationManager.load(id);
	}	
	
	@RequestMapping(method = RequestMethod.PUT, value = "/{organizationId}/persons/{id}")
	public void savePersonOrganization(@PathVariable("id") Long id, @Valid @RequestBody CorePersonOrganizationDto dto) {
		personOrganizationManager.save(id, dto);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void save(@PathVariable("id") Long id, @Valid @RequestBody CoreOrganizationDto dto) {
		manager.save(id, dto);
	}

}
