package org.simplifyworks.uam.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionDto;
import org.simplifyworks.uam.model.dto.CoreRoleCompositionFilterDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreRoleFilterDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationFilterDto;
import org.simplifyworks.uam.service.CoreRoleCompositionManager;
import org.simplifyworks.uam.service.CoreRoleManager;
import org.simplifyworks.uam.service.CoreUserRoleOrganizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Svanda
 */
@RestController
@RequestMapping("/api/core/roles")
public class CoreRoleController {

	@Autowired
	private CoreRoleManager manager;
	@Autowired
	private CoreUserRoleOrganizationManager userRoleOrganizationManager;
	@Autowired
	private CoreRoleCompositionManager roleCompositionManager;

	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<CoreRoleDto> getRoles(@Valid @RequestBody CoreRoleFilterDto filter) {
		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody CoreRoleDto dto) {
		return manager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		manager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public CoreRoleDto load(@PathVariable("id") Long id) {
		return manager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void save(@PathVariable("id") Long id, @Valid @RequestBody CoreRoleDto dto) {
		manager.save(id, dto);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/user-organizations/filter")
	public ExtendedArrayList<CoreUserRoleOrganizationDto> getUserRoles(@PathVariable("id") Long id, @Valid @RequestBody CoreUserRoleOrganizationFilterDto filter) {
		filter.setRoleId(id);

		return userRoleOrganizationManager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/user-organizations")
	public Long createUserRole(@Valid @RequestBody CoreUserRoleOrganizationDto dto) {
		return userRoleOrganizationManager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{roleId}/user-organizations/{id}")
	public void deleteUserRole(@PathVariable("roleId") Long roleId, @PathVariable("id") Long id) {
		userRoleOrganizationManager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{roleId}/user-organizations/{id}")
	public CoreUserRoleOrganizationDto loadUserRole(@PathVariable("roleId") Long roleId, @PathVariable("id") Long id) {
		return userRoleOrganizationManager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{roleId}/user-organizations/{id}")
	public void saveUserRole(@PathVariable("roleId") Long roleId, @PathVariable("id") Long id, @Valid @RequestBody CoreUserRoleOrganizationDto dto) {
		userRoleOrganizationManager.save(id, dto);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/sub-roles/filter")
	public ExtendedArrayList<CoreRoleCompositionDto> getSubRoles(@PathVariable("id") Long id, @Valid @RequestBody CoreRoleCompositionFilterDto filter) {
		filter.setSuperiorRoleId(id);

		return roleCompositionManager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{superRoleId}/sub-roles")
	public Long createSubRole(@PathVariable("superRoleId") Long superRoleId, @Valid @RequestBody CoreRoleCompositionDto dto) {
		CoreRoleDto superRole = manager.load(superRoleId);
		if (superRole != null) {
			dto.setSuperiorRole(superRole);
		}
		return roleCompositionManager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{superRoleId}/sub-roles/{id}")
	public void deleteSubRole(@PathVariable("superRoleId") Long superRoleId, @PathVariable("id") Long id) {
		roleCompositionManager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{superRoleId}/sub-roles/{id}")
	public CoreRoleCompositionDto loadSubRole(@PathVariable("superRoleId") Long superRoleId, @PathVariable("id") Long id) {
		return roleCompositionManager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{superRoleId}/sub-roles/{id}")
	public void saveSubRole(@PathVariable("superRoleId") Long superRoleId, @PathVariable("id") Long id, @Valid @RequestBody CoreRoleCompositionDto dto) {
		CoreRoleDto superRole = manager.load(superRoleId);
		if (superRole != null) {
			dto.setSuperiorRole(superRole);
		}
		roleCompositionManager.save(id, dto);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/super-roles/filter")
	public ExtendedArrayList<CoreRoleCompositionDto> getSuperRoles(@PathVariable("id") Long id, @Valid @RequestBody CoreRoleCompositionFilterDto filter) {
		filter.setSubRoleId(id);

		return roleCompositionManager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{subRoleId}/super-roles")
	public Long createSuperRole(@PathVariable("subRoleId") Long subRoleId, @Valid @RequestBody CoreRoleCompositionDto dto) {
		CoreRoleDto subRole = manager.load(subRoleId);
		if (subRole != null) {
			dto.setSubRole(subRole);
		}
		return roleCompositionManager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{subRoleId}/super-roles/{id}")
	public void deleteSuperRole(@PathVariable("subRoleId") Long subRoleId, @PathVariable("id") Long id) {
		roleCompositionManager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{subRoleId}/super-roles/{id}")
	public CoreRoleCompositionDto loadSuperRole(@PathVariable("subRoleId") Long subRoleId, @PathVariable("id") Long id) {
		return roleCompositionManager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{subRoleId}/super-roles/{id}")
	public void saveSuperRole(@PathVariable("subRoleId") Long subRoleId, @PathVariable("id") Long id, @Valid @RequestBody CoreRoleCompositionDto dto) {
		CoreRoleDto subRole = manager.load(subRoleId);
		if (subRole != null) {
			dto.setSubRole(subRole);
		}
		roleCompositionManager.save(id, dto);
	}
}
