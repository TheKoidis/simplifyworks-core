package org.simplifyworks.uam.web.controller.rest;

import javax.validation.Valid;


import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.dto.CorePersonFilterDto;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationDto;
import org.simplifyworks.uam.model.dto.CorePersonOrganizationFilterDto;
import org.simplifyworks.uam.model.dto.CoreUserDto;
import org.simplifyworks.uam.model.dto.CoreUserFilterDto;
import org.simplifyworks.uam.service.CorePersonManager;
import org.simplifyworks.uam.service.CorePersonOrganizationManager;
import org.simplifyworks.uam.service.CoreUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@RestController
@RequestMapping(value = "/api/core/persons")
public class CorePersonController {

	@Autowired
	private CorePersonManager manager;
	@Autowired
	private CoreUserManager userManager;
	@Autowired
	private CorePersonOrganizationManager personOrganizationManager;

	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<CorePersonDto> getPersons(@Valid @RequestBody CorePersonFilterDto filter) {
		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody CorePersonDto dto) {
		return manager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		manager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public CorePersonDto load(@PathVariable("id") Long id) {
		return manager.load(id);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/users/filter")
	public ExtendedArrayList<CoreUserDto> getUsers(@PathVariable("id") Long id, @Valid @RequestBody CoreUserFilterDto filter) {
		filter.setPersonId(id);

		return userManager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST, value = "/{id}/organizations/filter")
	public ExtendedArrayList<CorePersonOrganizationDto> getOrganizations(@PathVariable("id") Long id, @Valid @RequestBody CorePersonOrganizationFilterDto filter) {
		filter.setPersonId(id);

		return personOrganizationManager.getAllWrapped(filter);
	}
        
        @RequestMapping(method = RequestMethod.POST, value = "/{id}/organizations")
	public Long createPersonOrganization(@Valid @RequestBody CorePersonOrganizationDto dto) {
		return personOrganizationManager.create(dto);
	}
        
        @RequestMapping(method = RequestMethod.DELETE, value = "/{userId}/organizations/{id}")
	public void deleteUserRole(@PathVariable("userId") Long userId, @PathVariable("id") Long id) {
		personOrganizationManager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{userId}/organizations/{id}")
	public CorePersonOrganizationDto loadUserRole(@PathVariable("userId") Long userId, @PathVariable("id") Long id) {
		return personOrganizationManager.load(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{userId}/organizations/{id}")
	public void saveUserRole(@PathVariable("userId") Long userId, @PathVariable("id") Long id, @Valid @RequestBody CorePersonOrganizationDto dto) {
		personOrganizationManager.save(id, dto);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void save(@PathVariable("id") Long id, @Valid @RequestBody CorePersonDto dto) {
		manager.save(id, dto);
	}
}
