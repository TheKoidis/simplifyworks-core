package org.simplifyworks.monitoring.service.config;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.simplifyworks.scheduler.service.impl.AbstractTaskExecutionService;
import org.springframework.aop.Pointcut;
import org.springframework.aop.support.Pointcuts;
import org.springframework.aop.support.annotation.AnnotationMatchingPointcut;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.boot.context.embedded.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import net.bull.javamelody.MonitoringFilter;
import net.bull.javamelody.MonitoringSpringAdvisor;
import net.bull.javamelody.SessionListener;
import net.bull.javamelody.SpringDataSourceBeanPostProcessor;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Configuration
@ConditionalOnProperty(prefix = "monitoring", name = "enabled", matchIfMissing = true)
public class MonitoringConfig implements ServletContextInitializer {
	
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		servletContext.addListener(new SessionListener());
	}

	@Bean
	public FilterRegistrationBean monitoringFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setFilter(new MonitoringFilter());
		registration.addUrlPatterns("/*");
		registration.setName("javamelody");
		registration.setDispatcherTypes(DispatcherType.REQUEST, DispatcherType.ASYNC);
		registration.setAsyncSupported(true);

		return registration;
	}

	@Bean
	public SpringDataSourceBeanPostProcessor springDataSourceBeanPostProcessor() {
		return new SpringDataSourceBeanPostProcessor();
	}

	@Bean
	public MonitoringSpringAdvisor monitoringAdvisor() {
		MonitoringSpringAdvisor advisor = new MonitoringSpringAdvisor();

		Pointcut pointcut = new AnnotationMatchingPointcut(Component.class);
		pointcut = Pointcuts.union(pointcut, new AnnotationMatchingPointcut(Repository.class));
		pointcut = Pointcuts.union(pointcut, new AnnotationMatchingPointcut(Service.class));
		pointcut = Pointcuts.union(pointcut, new AnnotationMatchingPointcut(Controller.class));

		pointcut = Pointcuts.intersection(pointcut, new ExcludePointcut(AbstractTaskExecutionService.class));

		advisor.setPointcut(pointcut);

		return advisor;
	}

}
