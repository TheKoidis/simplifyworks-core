package org.simplifyworks.scheduler.service;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.simplifyworks.scheduler.service.impl.AbstractTaskExecutionService;
import org.simplifyworks.uam.service.CoreFlatOrganizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Scheduler service which rebuilds flat organization structure.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 * @see CoreFlatOrganizationManager
 */
@Service
public class CoreFlatOrganizationSchedulerService extends AbstractTaskExecutionService {

	@Autowired
	private CoreFlatOrganizationManager flatOrganizationManager;
	
	@Override
	public void executeWithSecurity(JobExecutionContext context) throws JobExecutionException {
		flatOrganizationManager.rebuild();
	}
}
