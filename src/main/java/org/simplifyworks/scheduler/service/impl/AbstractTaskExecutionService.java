package org.simplifyworks.scheduler.service.impl;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.simplifyworks.scheduler.service.TaskExecutionService;
import org.simplifyworks.security.model.domain.RobotAuthentication;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Template for task execution services. This template establishes security context before own task invocation.
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class AbstractTaskExecutionService implements TaskExecutionService {

	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		SecurityContextHolder.getContext().setAuthentication(new RobotAuthentication());

		executeWithSecurity(context);
	}

	public boolean isEnabled() {
		return true;
	}

	public abstract void executeWithSecurity(JobExecutionContext context) throws JobExecutionException;

}
