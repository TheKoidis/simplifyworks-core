package org.simplifyworks.scheduler.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.SimpleTrigger;
import org.quartz.Trigger;
import org.quartz.Trigger.TriggerState;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.utils.Key;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.scheduler.model.domain.AbstractTaskTrigger;
import org.simplifyworks.scheduler.model.domain.CronTaskTrigger;
import org.simplifyworks.scheduler.model.domain.OneTimeTaskTrigger;
import org.simplifyworks.scheduler.model.domain.Task;
import org.simplifyworks.scheduler.model.domain.TaskTriggerState;
import org.simplifyworks.scheduler.service.SchedulerService;
import org.simplifyworks.scheduler.service.TaskExecutionService;
import org.simplifyworks.security.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link SchedulerService}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
@ConditionalOnProperty(prefix = "scheduler", name = "enabled", matchIfMissing = true)
public class DefaultSchedulerService implements SchedulerService {

	/**
	 * Name of task group
	 */
	public static final String DEFAULT_GROUP_NAME = "default";

	private static final Logger LOG = LoggerFactory.getLogger(DefaultSchedulerService.class);

	@Autowired
	private ApplicationContext context;

	@Autowired
	private Scheduler scheduler;

	@Autowired
	private SecurityService securityService;

	public void init() throws SchedulerException {
		for (Map.Entry<String, TaskExecutionService> entry : context.getBeansOfType(TaskExecutionService.class)
						.entrySet()) {
			JobKey jobKey = new JobKey(entry.getKey(), DEFAULT_GROUP_NAME);

			if (scheduler.checkExists(jobKey)) {
				if (LOG.isDebugEnabled()) {
					LOG.debug("Job '{}' ({}) is already registered", entry.getKey(), entry.getValue().getClass());
				}
			} else {
				JobDetail jobDetail = JobBuilder.newJob().withIdentity(entry.getKey(), DEFAULT_GROUP_NAME)
								.ofType(entry.getValue().getClass()).storeDurably().build();

				scheduler.addJob(jobDetail, false);

				if (LOG.isDebugEnabled()) {
					LOG.debug("Job '{}' ({}) created and registered", entry.getKey(), entry.getValue().getClass());
				}
			}
		}
	}

	@Override
	public List<Task> getAllTasks() {
//		if(!securityService.hasAllRoles("admin_scheduler")) {
//			throw new CoreException("Not allowed");
//		}

		try {
			List<Task> tasks = new ArrayList<>();

			for (Map.Entry<String, TaskExecutionService> entry : context.getBeansOfType(TaskExecutionService.class)
							.entrySet()) {
				if (!entry.getValue().isEnabled()) {
					continue;
				}
				JobKey jobKey = new JobKey(entry.getKey(), DEFAULT_GROUP_NAME);
				List<? extends Trigger> triggers = scheduler.getTriggersOfJob(jobKey);

				Task task = new Task();
				task.setName(jobKey.getName());
				task.setTriggers(new ArrayList<>());

				for (Trigger trigger : triggers) {
					TriggerState state = scheduler.getTriggerState(trigger.getKey());

					if (trigger instanceof CronTrigger) {
						task.getTriggers().add(new CronTaskTrigger((CronTrigger) trigger, TaskTriggerState.convert(state)));
					} else if (trigger instanceof SimpleTrigger) {
						task.getTriggers().add(new OneTimeTaskTrigger((SimpleTrigger) trigger, TaskTriggerState.convert(state)));
					} else {
						if (LOG.isWarnEnabled()) {
							LOG.warn("Job '{}' ({}) has registered trigger of unsupported type {}", entry.getKey(),
											entry.getValue().getClass(), trigger);
						}
					}
				}

				tasks.add(task);
			}

			return tasks;
		} catch (SchedulerException e) {
			throw new CoreException(e);
		}
	}

	@Override
	public AbstractTaskTrigger addTrigger(String taskName, AbstractTaskTrigger trigger) {
//		if(!securityService.hasAllRoles("admin_scheduler")) {
//			throw new CoreException("Not allowed");
//		}

		try {
			String triggerName = Key.createUniqueName(taskName);
			trigger.setName(triggerName);

			if (trigger instanceof CronTaskTrigger) {
				scheduler.scheduleJob(TriggerBuilder.newTrigger().withIdentity(triggerName, taskName)
								.forJob(taskName, DEFAULT_GROUP_NAME).withDescription(trigger.getDescription())
								.withSchedule(CronScheduleBuilder.cronSchedule(((CronTaskTrigger) trigger).getCron())
												.withMisfireHandlingInstructionDoNothing().inTimeZone(TimeZone.getDefault()))
								.startNow().build());
			} else if (trigger instanceof OneTimeTaskTrigger) {
				scheduler.scheduleJob(TriggerBuilder.newTrigger().withIdentity(triggerName, taskName)
								.forJob(taskName, DEFAULT_GROUP_NAME).withDescription(trigger.getDescription())
								.withSchedule(SimpleScheduleBuilder.simpleSchedule())
								.startAt(((OneTimeTaskTrigger) trigger).getFireTime()).build());
			} else {
				throw new CoreException("Unsupported type of task trigger " + trigger);
			}

			return trigger;
		} catch (SchedulerException e) {
			throw new CoreException(e);
		}
	}

	@Override
	public void removeTrigger(String taskName, String triggerName) {
//		if(!securityService.hasAllRoles("admin_scheduler")) {
//			throw new CoreException("Not allowed");
//		}

		try {
			scheduler.unscheduleJob(new TriggerKey(triggerName, taskName));
		} catch (SchedulerException e) {
			throw new CoreException(e);
		}
	}

	@Override
	public void pauseTrigger(String taskName, String triggerName) {
//		if(!securityService.hasAllRoles("admin_scheduler")) {
//			throw new CoreException("Not allowed");
//		}

		try {
			scheduler.pauseTrigger(new TriggerKey(triggerName, taskName));
		} catch (SchedulerException e) {
			throw new CoreException(e);
		}
	}

	@Override
	public void resumeTrigger(String taskName, String triggerName) {
		if(!securityService.hasAllRoles("admin_scheduler")) {
			throw new CoreException("Not allowed");
		}

		try {
			scheduler.resumeTrigger(new TriggerKey(triggerName, taskName));
		} catch (SchedulerException e) {
			throw new CoreException(e);
		}
	}
}
