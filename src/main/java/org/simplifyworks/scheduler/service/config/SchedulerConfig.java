package org.simplifyworks.scheduler.service.config;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.impl.StdSchedulerFactory;
import org.simplifyworks.scheduler.service.impl.AutowiringSpringBeanJobFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Configuration
@ConditionalOnProperty(prefix = "scheduler", name = "enabled", matchIfMissing = true)
public class SchedulerConfig {

	@Autowired
	private ApplicationContext context;

	@Bean(destroyMethod = "shutdown")
	public Scheduler scheduler() throws SchedulerException {
		Scheduler scheduler = new StdSchedulerFactory("quartz.properties").getScheduler();

		scheduler.setJobFactory(jobFactory());

		return scheduler;
	}

	@Bean
	public AutowiringSpringBeanJobFactory jobFactory() {
		AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
	    jobFactory.setApplicationContext(context);

		return jobFactory;
	}
}
