package org.simplifyworks.scheduler.service;

import java.util.List;
import org.quartz.SchedulerException;

import org.simplifyworks.scheduler.model.domain.AbstractTaskTrigger;
import org.simplifyworks.scheduler.model.domain.Task;

/**
 * Interface for scheduler service
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface SchedulerService {

	public void init() throws SchedulerException;

	/**
	 * Returns all tasks
	 *
	 * @return all tasks
	 */
	public List<Task> getAllTasks();

	/**
	 * Adds trigger for task
	 *
	 * @param taskName name of task
	 * @param trigger trigger to add
	 * @return trigger containing name
	 */
	public AbstractTaskTrigger addTrigger(String taskName, AbstractTaskTrigger trigger);

	/**
	 * Removes trigger
	 *
	 * @param taskName name of task
	 * @param triggerName name of trigger
	 */
	public void removeTrigger(String taskName, String triggerName);

	/**
	 * Pauses trigger
	 *
	 * @param taskName name of task
	 * @param triggerName name of trigger
	 */
	public void pauseTrigger(String taskName, String triggerName);

	/**
	 * Resumes trigger
	 *
	 * @param taskName name of task
	 * @param triggerName name of trigger
	 */
	public void resumeTrigger(String taskName, String triggerName);
}
