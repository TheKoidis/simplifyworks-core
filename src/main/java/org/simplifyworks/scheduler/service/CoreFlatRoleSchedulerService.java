package org.simplifyworks.scheduler.service;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.simplifyworks.scheduler.service.impl.AbstractTaskExecutionService;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Scheduler service which rebuilds flat role structure.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 * @see CoreFlatRoleManager
 */
@Service
public class CoreFlatRoleSchedulerService extends AbstractTaskExecutionService {

	@Autowired
	private CoreFlatRoleManager flatRoleManager;
	
	@Override
	public void executeWithSecurity(JobExecutionContext context) throws JobExecutionException {
		flatRoleManager.rebuild();
	}

}
