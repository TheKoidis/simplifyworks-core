package org.simplifyworks.scheduler.model.domain;

import java.util.Date;

import javax.validation.constraints.NotNull;

import org.codehaus.jackson.annotate.JsonSubTypes;
import org.quartz.Trigger;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * Base class for task triggers
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({
	@JsonSubTypes.Type(value = OneTimeTaskTrigger.class),
	@JsonSubTypes.Type(value = CronTaskTrigger.class)
})
public abstract class AbstractTaskTrigger {

	@NotNull
	private String name;
	private String description;
		
	private Date nextFireTime;	
	private Date previousFireTime;
	
	private TaskTriggerState state;
	
	public AbstractTaskTrigger() {
	}
	
	/**
	 * Creates a new instance using trigger and state
	 * 
	 * @param trigger trigger
	 * @param state state
	 */
	public AbstractTaskTrigger(Trigger trigger, TaskTriggerState state) {
		this.name = trigger.getKey().getName();
		this.description = trigger.getDescription();
		this.nextFireTime = trigger.getNextFireTime();
		this.previousFireTime = trigger.getPreviousFireTime();
		this.state = state;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public Date getNextFireTime() {
		return nextFireTime;
	}
	
	public void setNextFireTime(Date nextFireTime) {
		this.nextFireTime = nextFireTime;
	}
	
	public Date getPreviousFireTime() {
		return previousFireTime;
	}
	
	public void setPreviousFireTime(Date previousFireTime) {
		this.previousFireTime = previousFireTime;
	}
	
	public TaskTriggerState getState() {
		return state;
	}
	
	public void setState(TaskTriggerState state) {
		this.state = state;
	}
}