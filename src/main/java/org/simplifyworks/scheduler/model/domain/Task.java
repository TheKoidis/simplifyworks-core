package org.simplifyworks.scheduler.model.domain;

import java.util.List;

/**
 * Scheduled task with unique name
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class Task {

	private String name;	
	private List<AbstractTaskTrigger> triggers;
	
	public Task() {
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public List<AbstractTaskTrigger> getTriggers() {
		return triggers;
	}
	
	public void setTriggers(List<AbstractTaskTrigger> triggers) {
		this.triggers = triggers;
	}
}
