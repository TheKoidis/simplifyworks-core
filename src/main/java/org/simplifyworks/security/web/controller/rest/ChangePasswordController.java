package org.simplifyworks.security.web.controller.rest;

import org.simplifyworks.security.service.ChangePasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/account/change-password")
public class ChangePasswordController {

	@Autowired
	private ChangePasswordService changePasswordService;
	
	@RequestMapping(value = "", method = RequestMethod.POST)
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void changePassword(String newPassword) {
		changePasswordService.changePassword(newPassword);
	}
}
