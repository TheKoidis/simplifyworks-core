package org.simplifyworks.security.web.controller.rest;

import java.security.Principal;

import org.simplifyworks.security.service.SingleSignOnLogInService;
import org.simplifyworks.security.service.UsernamePasswordLogInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@RestController
@RequestMapping(value = "/auth/log-in")
public class LogInController {
	
	@Autowired
	private SingleSignOnLogInService singleSignOnLogInService;
	
	@Autowired
	private UsernamePasswordLogInService usernamePasswordLogInService;
	
	@RequestMapping(value = "/single-sign-on", method = RequestMethod.POST)
	public String logInUsingSingleSignOn(Principal principal, WebRequest request) {
		return singleSignOnLogInService.logIn(request.getRemoteUser());
	}
	
	@RequestMapping(value = "/username-password", method = RequestMethod.POST)
	public String logInUsingUsernameAndPassword(@RequestParam(value = "username") String username, @RequestParam(value = "password") String password) {
		return usernamePasswordLogInService.logIn(username, password);
	}
	
}
