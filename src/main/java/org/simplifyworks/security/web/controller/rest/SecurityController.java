package org.simplifyworks.security.web.controller.rest;

import java.util.Collection;
import java.util.Set;

import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/auth/security")
public class SecurityController {

	@Autowired
	private SecurityService securityService;
	
	@RequestMapping(value = "/all-roles", method = RequestMethod.GET)
	public Set<String> getAllRoles() {
		return securityService.getAllRoleNames();
	}
	
	@RequestMapping(value = "/roles-in-organizations", method = RequestMethod.GET)
	public Collection<RoleOrganizationGrantedAuthority> getRolesInOrganizations() {
		return ((JwtUserAuthentication) securityService.getAuthentication()).getRoleOrganizationAuthorities();
	}
}
