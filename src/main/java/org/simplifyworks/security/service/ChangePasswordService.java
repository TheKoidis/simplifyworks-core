package org.simplifyworks.security.service;

public interface ChangePasswordService {

	public void changePassword(String newPassword);
}
