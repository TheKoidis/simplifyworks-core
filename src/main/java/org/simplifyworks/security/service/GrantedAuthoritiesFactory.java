package org.simplifyworks.security.service;

import java.util.List;

import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;

/**
 * Factory which returns granted authorities for users
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface GrantedAuthoritiesFactory {

	/**
	 * Returns granted authorities for user
	 * 
	 * @param username username
	 * @return granted authorities (never null)
	 */
	public List<RoleOrganizationGrantedAuthority> getGrantedAuthorities(String username);
}
