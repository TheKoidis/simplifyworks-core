package org.simplifyworks.security.service.impl;

import javax.persistence.NoResultException;

import org.simplifyworks.security.model.domain.exception.BadCredentialsException;
import org.simplifyworks.security.service.UsernamePasswordLogInService;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UsernamePasswordLogInServiceImpl extends AbstractLogInServiceImpl implements UsernamePasswordLogInService {

	private static final Logger LOG = LoggerFactory.getLogger(UsernamePasswordLogInServiceImpl.class);

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public String logIn(String username, String password) {
		LOG.info("Authenticating user [{}]", username);

		CoreUser user = findUser(username);

		if (!matchesUserPassword(user, password)) {
			LOG.debug("Username and/or password for user [{}] are incorrect", username);
			
			throw new BadCredentialsException();
		}

		return createJwtToken(user);
	}

	private boolean matchesUserPassword(CoreUser user, String password) {
		try {
			return passwordEncoder.matches(password == null ? "" : password, user == null ? "" : new String(user.getPassword()));
		} catch (NoResultException e) {
			return false;
		}
	}
}