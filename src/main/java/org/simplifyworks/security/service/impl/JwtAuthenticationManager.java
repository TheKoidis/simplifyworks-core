package org.simplifyworks.security.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.model.domain.exception.ExpiredJwtUserAuthenticationException;
import org.simplifyworks.security.model.domain.exception.NoJwtUserAuthenticationException;
import org.simplifyworks.security.model.domain.exception.NoRolesException;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.simplifyworks.uam.service.CoreUserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Simple implementation which checks type of {@link Authentication} argument.
 * 
 * Only {@link JwtUserAuthentication} is allowed.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class JwtAuthenticationManager implements AuthenticationManager {
		
	private static final Logger LOG = LoggerFactory.getLogger(JwtAuthenticationManager.class);
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private CoreFlatRoleManager flatRoleManager;

	@Autowired
	private GrantedAuthoritiesFactory grantedAuthoritiesFactory;
	
	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		if(!(authentication instanceof JwtUserAuthentication)) {
			throw new NoJwtUserAuthenticationException("Only " + JwtUserAuthentication.class.getSimpleName() + " is allowed");
		}
		
		JwtUserAuthentication jwtUserAuthentication = (JwtUserAuthentication) authentication;
		Date now = new Date();
		
		if(now.after(jwtUserAuthentication.getExpiration())) {
			throw new ExpiredJwtUserAuthenticationException("Token expired in " + jwtUserAuthentication.getExpiration());
		}
		
		CoreUser user = findUser(jwtUserAuthentication.getCurrentUsername());
		
		List<RoleOrganizationGrantedAuthority> currentUserGrantedAuthorities = grantedAuthoritiesFactory.getGrantedAuthorities(user.getUsername());
		
		if (currentUserGrantedAuthorities.isEmpty()) {
			LOG.debug("User [{}] does not exist or has no roles", user.getUsername());

			throw new NoRolesException(user.getUsername());
		}
		
		jwtUserAuthentication.setRoleOrganizationAuthorities(currentUserGrantedAuthorities);

		LOG.info("User [{}] authenticated", user.getUsername());

		List<String> explicitlyAddedRoleNames = new ArrayList<>();

		for(RoleOrganizationGrantedAuthority grantedAuthority: currentUserGrantedAuthorities) {
			explicitlyAddedRoleNames.add(grantedAuthority.getRoleName());
		}
		
		jwtUserAuthentication.setRoles(flatRoleManager.findAllDescendantRoleNames(explicitlyAddedRoleNames));
		
		return authentication;		
	}
	
	protected CoreUser findUser(String username) {
		try {
			return coreUserManager.readUser(username);
		} catch (NoResultException e) {
			return null;
		}
	}
}