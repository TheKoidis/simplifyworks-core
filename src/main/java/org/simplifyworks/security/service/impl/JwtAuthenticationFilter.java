package org.simplifyworks.security.service.impl;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.exception.ExpiredJwtUserAuthenticationException;
import org.simplifyworks.security.model.domain.exception.NoJwtUserAuthenticationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.InvalidSignatureException;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.jwt.crypto.sign.SignerVerifier;
import org.springframework.web.filter.GenericFilterBean;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Main security filter for API which uses JSON Web Tokens (JWT).
 * 
 * Here is authentication process:
 * <ol>
 * 	<li>extract token from header or parameter,</li>
 * 	<li>decode and verify token using MAC with configurable secret,</li>
 * 	<li>deserialize JSON content from JWT into authentication object,</li>
 * 	<li>set authentication to security context.</li>
 * </ol>
 * 
 * In case of exception error status is sent (400 Bad Request).
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class JwtAuthenticationFilter extends GenericFilterBean {

	/**
	 * HTTP Authorization header name
	 */
	public static final String AUTHORIZATION_HEADER = "Authorization";
	
	/**
	 * HTTP Authorization header value prefix
	 */
	public static final String AUTHORIZATION_HEADER_VALUE_PREFIX = "Bearer ";
	
	/**
	 * HTTP Authentication parameter name
	 */
	public static final String AUTH_TOKEN_PARAMETER = "auth-token";
	
	@Value("${security.authentication.secret:simplifyworks}")
	private String secret;
	
	@Autowired
	private JwtAuthenticationManager authenticationManager;
	
	@Autowired
	private ObjectMapper jsonMapper;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;

		String header = httpRequest.getHeader(AUTHORIZATION_HEADER);
		String authToken = httpRequest.getParameter(AUTH_TOKEN_PARAMETER);

		if (authToken != null || (header != null && header.startsWith(AUTHORIZATION_HEADER_VALUE_PREFIX))) {
			String token = authToken != null ? authToken : header.substring(AUTHORIZATION_HEADER_VALUE_PREFIX.length());
			SignerVerifier verifier = new MacSigner(secret);

			try {
				String info = JwtHelper.decodeAndVerify(token, verifier).getClaims();

				SecurityContextHolder.getContext().setAuthentication(authenticationManager.authenticate(jsonMapper.readValue(info, JwtUserAuthentication.class)));
			} catch(NoJwtUserAuthenticationException | ExpiredJwtUserAuthenticationException e) {
				httpResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED);
				
				return;
			} catch (JsonParseException | InvalidSignatureException e) {
				httpResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);

				return;
			}
		}

		chain.doFilter(request, response);
	}
}
