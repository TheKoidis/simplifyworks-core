package org.simplifyworks.security.service.impl;

import org.simplifyworks.security.model.domain.exception.NoRemoteUserException;
import org.simplifyworks.security.model.domain.exception.NoUserException;
import org.simplifyworks.security.service.SingleSignOnLogInService;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class SingleSignOnLogInServiceImpl extends AbstractLogInServiceImpl implements SingleSignOnLogInService {
	
	private static final Logger LOG = LoggerFactory.getLogger(SingleSignOnLogInServiceImpl.class);
	
	@Override
	public String logIn(String username) {
		LOG.info("Authenticating user [{}]", username);
				
		if(username == null || username.isEmpty()) {
			throw new NoRemoteUserException();
		}
		
		CoreUser user = findUser(username);
		
		if(user == null) {
			throw new NoUserException(username);
		}	
		
		return createJwtToken(user);
	}

}
