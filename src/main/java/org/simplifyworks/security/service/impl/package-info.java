/**
 * Security services implementations package
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
package org.simplifyworks.security.service.impl;