package org.simplifyworks.security.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationDto;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserRoleOrganizationManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Component
public class DefaultGrantedAuthoritiesFactory implements GrantedAuthoritiesFactory {

	@Autowired
	private CoreUserRoleOrganizationManager coreUserRoleOrganizationManager;
	
	@Override
	public List<RoleOrganizationGrantedAuthority> getGrantedAuthorities(String username) {
		SearchParameters parameters = new SearchParameters();
		parameters.addFilter(new FilterValue(CoreUserRoleOrganization_.user.getName() + "." + CoreUser_.username.getName(), FilterOperator.EQUALS, Collections.singletonList(username)));
		
		List<RoleOrganizationGrantedAuthority> grantedAuthorities = new ArrayList<>();
		
		for (CoreUserRoleOrganizationDto coreUserRole : coreUserRoleOrganizationManager.findByUsername(username)) {
			CoreRoleDto role = coreUserRole.getRole();
			CoreOrganizationDto organization = coreUserRole.getOrganization();
				
			grantedAuthorities.add(new RoleOrganizationGrantedAuthority(role.getName(), organization == null ? null : organization.getCode()));
		}
		
		return grantedAuthorities;
	}
}
