package org.simplifyworks.security.service.impl;

import org.simplifyworks.security.service.ChangePasswordService;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.service.CoreUserManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

@Service
@Transactional
public class ChangePasswordServiceImpl implements ChangePasswordService {
	
	@Autowired
	private SecurityService securityService;
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Override
	public void changePassword(String newPassword) {
		Assert.hasText(newPassword, "Password must contain some text");
		
		coreUserManager.readUser(securityService.getOriginalUsername()).setPassword(passwordEncoder.encode(newPassword).toCharArray());
	}
}
