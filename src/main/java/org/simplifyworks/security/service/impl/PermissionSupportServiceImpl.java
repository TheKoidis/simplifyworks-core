package org.simplifyworks.security.service.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.SingularAttribute;

import org.simplifyworks.security.model.domain.AccessType;
import org.simplifyworks.security.model.entity.CorePermission;
import org.simplifyworks.security.model.entity.CorePermission_;
import org.simplifyworks.security.model.entity.Protected;
import org.simplifyworks.security.service.PermissionSupportService;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.entity.CoreFlatOrganization;
import org.simplifyworks.uam.model.entity.CoreFlatOrganization_;
import org.simplifyworks.uam.model.entity.CoreFlatRole;
import org.simplifyworks.uam.model.entity.CoreFlatRole_;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link PermissionSupportService} which uses subqueries.
 * 
 * @author Stepan Osmik (osmik@ders.cz)
 */
@Service
public class PermissionSupportServiceImpl implements PermissionSupportService {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Autowired
	private SecurityService securityService;
	
	@Override
	public <P extends Protected<R>, R extends CorePermission, I> AccessType getAccessType(Class<P> protectedClass, I protectedId, SingularAttribute<? super P, I> idAttribute, SingularAttribute<R, P> protectedAttribute, Class<R> permissionClass) {
		if(hasAccessType(protectedClass, protectedId, idAttribute, protectedAttribute, permissionClass, AccessType.ALL)) {
			return AccessType.ALL;
		} else if(hasAccessType(protectedClass, protectedId, idAttribute, protectedAttribute, permissionClass, AccessType.READ_DELETE)) {
			return AccessType.READ_DELETE;
		} else if(hasAccessType(protectedClass, protectedId, idAttribute, protectedAttribute, permissionClass, AccessType.READ_UPDATE)) {
			return AccessType.READ_UPDATE;
		} else if(hasAccessType(protectedClass, protectedId, idAttribute, protectedAttribute, permissionClass, AccessType.READ_ONLY)) {
			return AccessType.READ_ONLY;
		} else {
			return null;
		}
	}
	
	private <P extends Protected<R>, R extends CorePermission, I> boolean hasAccessType(Class<P> protectedClass, I protectedId, SingularAttribute<? super P, I> idAttribute, SingularAttribute<R, P> protectedAttribute, Class<R> permissionClass, AccessType accessType) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<P> query = criteriaBuilder.createQuery(protectedClass);
		Root<P> root = query.from(protectedClass);
		
		query.distinct(true);
		
		Predicate condition = createCondition(criteriaBuilder, query, root, protectedAttribute, permissionClass, Collections.singletonList(accessType));		
		query.where(
			criteriaBuilder.equal(root.get(idAttribute), protectedId),
			condition
		);
		
		switch(entityManager.createQuery(query).getResultList().size()) {
			case 0:
				return false;
			case 1:
				return true;
			default:
				throw new Error("More results found");
		}
	}
	
	@Override
	public <P extends Protected<R>, R extends CorePermission> Predicate createCondition(CriteriaBuilder criteriaBuilder, CriteriaQuery<P> query, Root<P> root, SingularAttribute<R, P> protectedAttribute, Class<R> permissionClass) {
		return createCondition(criteriaBuilder, query, root, protectedAttribute, permissionClass, Arrays.asList(AccessType.values()));
	}
	
	@Override
	public <P extends Protected<R>, R extends CorePermission> Predicate createCondition(CriteriaBuilder criteriaBuilder, CriteriaQuery<P> query, Root<P> root, SingularAttribute<R, P> protectedAttribute, Class<R> permissionClass, List<AccessType> accessTypes) {
		Subquery<R> permissionSubquery = query.subquery(permissionClass);
		Root<R> permissionSubqueryRoot = permissionSubquery.from(permissionClass);
		
		permissionSubquery.select(permissionSubqueryRoot);
		permissionSubquery.where(
			criteriaBuilder.equal(permissionSubqueryRoot.get(protectedAttribute), root),
			permissionSubqueryRoot.get(CorePermission_.accessType).in(accessTypes),
			createValidityCondition(criteriaBuilder, permissionSubqueryRoot),
			criteriaBuilder.or(
				createUserCondition(criteriaBuilder, permissionSubqueryRoot),
				createRoleOrganizationCondition(criteriaBuilder, permissionSubquery, permissionSubqueryRoot)
			)
		);
		
		return criteriaBuilder.exists(permissionSubquery);
	}
	
	/*
	 * Creates validity condition (uses active, validFrom and validTill properties).
	 */
	private <P extends Protected<R>, R extends CorePermission> Predicate createValidityCondition(CriteriaBuilder criteriaBuilder, Root<R> permissionSubqueryRoot) {
		Date now = new Date();
		
		return criteriaBuilder.and(
			criteriaBuilder.isTrue(permissionSubqueryRoot.get(CorePermission_.active)),
			criteriaBuilder.or(criteriaBuilder.isNull(permissionSubqueryRoot.get(CorePermission_.validFrom)), criteriaBuilder.greaterThanOrEqualTo(permissionSubqueryRoot.get(CorePermission_.validFrom), now)),
			criteriaBuilder.or(criteriaBuilder.isNull(permissionSubqueryRoot.get(CorePermission_.validTill)), criteriaBuilder.lessThanOrEqualTo(permissionSubqueryRoot.get(CorePermission_.validTill), now))
		);
	}
	
	/*
	 * Creates user condition (uses user property). 
	 */
	private <P extends Protected<R>, R extends CorePermission> Predicate createUserCondition(CriteriaBuilder criteriaBuilder, Root<R> permissionSubqueryRoot) {
		return criteriaBuilder.equal(permissionSubqueryRoot.join(CorePermission_.user, JoinType.LEFT).get(CoreUser_.username), securityService.getUsername());
	}

	/*
	 * Creates role-only and/or role in organization condition (uses role and organization properties)
	 */
	private <P extends Protected<R>, R extends CorePermission> Predicate createRoleOrganizationCondition(CriteriaBuilder criteriaBuilder, Subquery<R> permissionSubquery, Root<R> permissionSubqueryRoot) {			
		Subquery<CoreUserRoleOrganization> userRoleOrganizationSubquery = permissionSubquery.subquery(CoreUserRoleOrganization.class);
		Root<CoreUserRoleOrganization> userRoleOrganizationSubqueryRoot = userRoleOrganizationSubquery.from(CoreUserRoleOrganization.class);		
		Join<CoreUserRoleOrganization, CoreUser> userRoleOrganizationUserJoin = userRoleOrganizationSubqueryRoot.join(CoreUserRoleOrganization_.user, JoinType.LEFT);
		userRoleOrganizationSubquery.select(userRoleOrganizationSubqueryRoot);
		
		// flat roles subquery
		Subquery<CoreFlatRole> flatRoleSubquery = permissionSubquery.subquery(CoreFlatRole.class);
		Root<CoreFlatRole> flatRoleSubqueryRoot = flatRoleSubquery.from(CoreFlatRole.class);
		flatRoleSubquery.select(flatRoleSubqueryRoot);
		
		flatRoleSubquery.where(
			criteriaBuilder.equal(flatRoleSubqueryRoot.join(CoreFlatRole_.ownee), permissionSubqueryRoot.get(CorePermission_.role)),
			criteriaBuilder.equal(flatRoleSubqueryRoot.join(CoreFlatRole_.owner), userRoleOrganizationSubqueryRoot.get(CoreUserRoleOrganization_.role))
		);
		
		// flat organizations subquery
		Subquery<CoreFlatOrganization> flatOrganizationSubquery = permissionSubquery.subquery(CoreFlatOrganization.class);
		Root<CoreFlatOrganization> flatOrganizationSubqueryRoot = flatOrganizationSubquery.from(CoreFlatOrganization.class);
		flatOrganizationSubquery.select(flatOrganizationSubqueryRoot);
		
		flatOrganizationSubquery.where(
			criteriaBuilder.equal(flatOrganizationSubqueryRoot.join(CoreFlatOrganization_.ownee), permissionSubqueryRoot.get(CorePermission_.organization)),
			criteriaBuilder.equal(flatOrganizationSubqueryRoot.join(CoreFlatOrganization_.owner), userRoleOrganizationSubqueryRoot.get(CoreUserRoleOrganization_.organization))
		);
		
		// user role organization subquery condition
		userRoleOrganizationSubquery.where(
			criteriaBuilder.equal(userRoleOrganizationUserJoin.get(CoreUser_.username), securityService.getUsername()),
			criteriaBuilder.exists(flatRoleSubquery),
			criteriaBuilder.or(
				criteriaBuilder.isNull(permissionSubqueryRoot.get(CorePermission_.organization)),
				criteriaBuilder.exists(flatOrganizationSubquery)
			)
		);			
				
		return criteriaBuilder.exists(userRoleOrganizationSubquery);
	}
}
