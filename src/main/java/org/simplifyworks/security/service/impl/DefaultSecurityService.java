package org.simplifyworks.security.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.simplifyworks.security.model.domain.AbstractAuthentication;
import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.service.CoreFlatOrganizationManager;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * Default implementation of {@link SecurityService}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
class DefaultSecurityService implements SecurityService {

	@Autowired
	private CoreFlatRoleManager flatRoleManager;

	@Autowired
	private CoreFlatOrganizationManager flatOrganizationManager;

	@Override
	public boolean isAuthenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication instanceof JwtUserAuthentication;
	}

	@Override
	public AbstractAuthentication getAuthentication() {
		return (AbstractAuthentication) SecurityContextHolder.getContext().getAuthentication();
	}

	@Override
	public String getUsername() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof AbstractAuthentication) {
			return ((AbstractAuthentication) authentication).getCurrentUsername();
		}
		return null;
	}

	@Override
	public String getOriginalUsername() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication instanceof AbstractAuthentication) {
			return ((AbstractAuthentication) authentication).getOriginalUsername();
		}
		return null;
	}

	@Override
	public boolean isSwitchedUser() {
		AbstractAuthentication authentication = (AbstractAuthentication) SecurityContextHolder.getContext().getAuthentication();
		return !authentication.getCurrentUsername().equals(authentication.getOriginalUsername());
	}

	@Override
	public Set<String> getAllRoleNames() {
		List<String> roleNames = new ArrayList<>();
		for (RoleOrganizationGrantedAuthority authority : ((JwtUserAuthentication) SecurityContextHolder.getContext().getAuthentication()).getRoleOrganizationAuthorities()) {
			roleNames.add(authority.getRoleName());
		}
		return new HashSet<>(flatRoleManager.findAllDescendantRoleNames(roleNames));
	}

	@Override
	public boolean hasAnyRole(String... roleNames) {
		Assert.notEmpty(roleNames, "roleNames must be filled");

		Set<String> requiredRoleNames = new HashSet<>(Arrays.asList(roleNames));
		Set<String> allRoleNames = getAllRoleNames();

		return CollectionUtils.containsAny(requiredRoleNames, allRoleNames);
	}

	@Override
	public boolean hasAllRoles(String... roleNames) {
		Assert.notEmpty(roleNames, "roleNames must be filled");

		Set<String> requiredRoleNames = new HashSet<>(Arrays.asList(roleNames));
		Set<String> allRoleNames = getAllRoleNames();

		return CollectionUtils.isSubCollection(requiredRoleNames, allRoleNames);
	}

	@Override
	public boolean hasExplicitRoleInOrganization(String roleName, String organizationName) {
		Assert.notNull(roleName, "roleName must be filled");
		Assert.notNull(organizationName, "organizationName must be filled");

		for (RoleOrganizationGrantedAuthority authority : ((JwtUserAuthentication) SecurityContextHolder.getContext().getAuthentication()).getRoleOrganizationAuthorities()) {
			return isExplicitRoleInOrganization(roleName, organizationName, authority);
		}
		return false;
	}

	@Override
	public boolean hasRoleInOrganization(String roleName, String organizationName) {
		Assert.notNull(roleName, "roleName must be filled");
		Assert.notNull(organizationName, "organizationName must be filled");

		for (RoleOrganizationGrantedAuthority authority : ((JwtUserAuthentication) SecurityContextHolder.getContext().getAuthentication()).getRoleOrganizationAuthorities()) {
			return isRoleInOrganization(roleName, organizationName, authority);
		}
		return false;
	}

	private boolean isRoleInOrganization(String roleName, String organizationCode, RoleOrganizationGrantedAuthority authority) {
		for (String role : flatRoleManager.findAllAncestorRoleNames(roleName)) {
			for (String organization : flatOrganizationManager.findAllAncestorOrganizationCodes(organizationCode)) {
				return isExplicitRoleInOrganization(role, organization, authority);
			}
		}
		return false;
	}

	private boolean isExplicitRoleInOrganization(String roleName, String organizationName, RoleOrganizationGrantedAuthority authority) {
		return roleName.equals(authority.getRoleName()) && organizationName.equals(authority.getOrganizationCode());
	}
}
