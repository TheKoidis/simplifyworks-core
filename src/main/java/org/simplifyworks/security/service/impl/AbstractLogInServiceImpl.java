package org.simplifyworks.security.service.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.simplifyworks.core.util.Mapper;
import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.model.domain.exception.NoRolesException;
import org.simplifyworks.security.service.GrantedAuthoritiesFactory;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.simplifyworks.uam.service.CoreUserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.jwt.JwtHelper;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service
public abstract class AbstractLogInServiceImpl {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractLogInServiceImpl.class);
	
	@Autowired
	private CoreUserManager coreUserManager;
	
	@Autowired
	private CoreFlatRoleManager flatRoleManager;

	@Autowired
	private GrantedAuthoritiesFactory grantedAuthoritiesFactory;
	
	@Autowired
	private Mapper mapper;
	
	@Autowired
	private ObjectMapper jsonMapper;

	@Value("${security.authentication.secret:simplifyworks}")
	private String secret;

	@Value("${security.authentication.expirationTimeout:28800000}")
	private long expirationTimeout;
	
	protected String createJwtToken(CoreUser user) {
		List<RoleOrganizationGrantedAuthority> currentUserGrantedAuthorities = grantedAuthoritiesFactory.getGrantedAuthorities(user.getUsername());

		if (currentUserGrantedAuthorities.isEmpty()) {
			LOG.debug("User [{}] does not exist or has no roles", user.getUsername());

			throw new NoRolesException(user.getUsername());
		}

		LOG.info("User [{}] authenticated", user.getUsername());
		
		Date expiration = new Date(System.currentTimeMillis() + expirationTimeout);
		
		Authentication authentication = new JwtUserAuthentication(user.getUsername(), mapper.map(user.getPerson(), CorePersonDto.class), expiration);
		try {
			return JwtHelper.encode(jsonMapper.writeValueAsString(authentication), new MacSigner(secret)).getEncoded();
		} catch (JsonProcessingException e) {
			throw new Error(e);
		}
	}

	protected CoreUser findUser(String username) {
		try {
			return coreUserManager.readUser(username);
		} catch (NoResultException e) {
			return null;
		}
	}
	
}
