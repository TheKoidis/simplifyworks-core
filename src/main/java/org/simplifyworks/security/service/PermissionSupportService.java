package org.simplifyworks.security.service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.SingularAttribute;

import org.simplifyworks.security.model.domain.AccessType;
import org.simplifyworks.security.model.entity.CorePermission;
import org.simplifyworks.security.model.entity.Protected;

/**
 * Interface of service which provides permission related support.
 *  
 * @author Stepan Osmik (osmik@ders.cz)
 */
public interface PermissionSupportService {

	/**
	 * Creates security condition for reading.
	 * 
	 * @param criteriaBuilder criteria builder to use
	 * @param query query for protected records
	 * @param root root of query for protected records
	 * @param protectedAttribute metamodel attribute defining relation between permission and its protected record 
	 * @param permissionClass class of permissions related to protected records
	 * @return single security read-access predicate
	 */
	public <P extends Protected<R>, R extends CorePermission> Predicate createCondition(CriteriaBuilder criteriaBuilder, CriteriaQuery<P> query, Root<P> root, SingularAttribute<R, P> protectedAttribute, Class<R> permissionClass);
	
	/**
	 * Creates security condition for specified type of access.
	 * 
	 * @param criteriaBuilder criteria builder to use
	 * @param query query for protected records
	 * @param root root of query for protected records
	 * @param protectedAttribute metamodel attribute defining relation between permission and its protected record 
	 * @param permissionClass class of permissions related to protected records
	 * @param accessTypes list of enumerated types of access
	 * @return single security read-access predicate
	 */
	public <P extends Protected<R>, R extends CorePermission> Predicate createCondition(CriteriaBuilder criteriaBuilder, CriteriaQuery<P> query, Root<P> root, SingularAttribute<R, P> protectedAttribute, Class<R> permissionClass, List<AccessType> accessTypes);
	
	/**
	 * Returns access type for specified protected object (using protectedClass and protectedId).
	 * 
	 * @param protectedClass class of protected object
	 * @param protectedId id of protected object
	 * @param idAttribute metamodel attribute defining id of protected objects
	 * @param protectedAttribute metamodel attribute defining relation between permission and its protected record 
	 * @param permissionClass class of permissions related to protected records
	 * @return access type to protected object or null if there is no access
	 */
	public <P extends Protected<R>, R extends CorePermission, I> AccessType getAccessType(Class<P> protectedClass, I protectedId, SingularAttribute<? super P, I> idAttribute, SingularAttribute<R, P> protectedAttribute, Class<R> permissionClass);
}
