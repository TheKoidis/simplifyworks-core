package org.simplifyworks.security.service.config;

import org.simplifyworks.security.service.impl.JwtAuthenticationFilter;
import org.simplifyworks.security.service.impl.JwtAuthenticationManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.servlet.configuration.EnableWebMvcSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.security.web.context.NullSecurityContextRepository;
import org.springframework.security.web.context.SecurityContextRepository;

/**
 * Security configuration
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Configuration
@EnableWebMvcSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable();

		http.addFilterAfter(jwtAuthenticationFilter(), BasicAuthenticationFilter.class)				
				.authorizeRequests()
				.antMatchers(HttpMethod.GET, "/api/**") 
				.fullyAuthenticated()
				.and()
				.authorizeRequests()
				.antMatchers(HttpMethod.POST, "/api/**") 
				.fullyAuthenticated()
				.and()
				.authorizeRequests()
				.antMatchers(HttpMethod.PUT, "/api/**")
				.fullyAuthenticated()
				.and()
				.authorizeRequests()
				.antMatchers(HttpMethod.DELETE, "/api/**") 
				.fullyAuthenticated()
				.and()
				.anonymous()
				.disable();

		http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	@Bean
	public JwtAuthenticationManager jwtAuthenticationManager() {
		return new JwtAuthenticationManager();
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/js/**", "/css/**", "/img/**", "/libs/**", "/favicons/**", "/api/attachments/download/**", "/auth/**");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public SecurityContextRepository securityContextRepository() {
		return new NullSecurityContextRepository();
	}
}
