package org.simplifyworks.security.service;

public interface UsernamePasswordLogInService {

	public String logIn(String username, String password);
}
