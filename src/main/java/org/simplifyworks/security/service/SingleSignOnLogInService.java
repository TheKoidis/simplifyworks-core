package org.simplifyworks.security.service;

public interface SingleSignOnLogInService {

	public String logIn(String username);
}
