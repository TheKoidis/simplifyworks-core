package org.simplifyworks.security.model.domain.exception;

import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Thrown if authentication request uses expired {@link JwtUserAuthentication}.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class ExpiredJwtUserAuthenticationException extends AuthenticationException {

	private static final long serialVersionUID = -9069535709621678999L;

	public ExpiredJwtUserAuthenticationException(String msg) {
		super(msg);
	}

}
