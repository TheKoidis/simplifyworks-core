package org.simplifyworks.security.model.domain.exception;

import java.util.Collections;

public class NoUserException extends AuthenticationException {

	private static final long serialVersionUID = -5559814540409247972L;

	public NoUserException(String username) {
		super("No user", "no-user", Collections.singletonMap("username", username));
	}
}
