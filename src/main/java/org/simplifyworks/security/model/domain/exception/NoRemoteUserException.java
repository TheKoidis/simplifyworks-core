package org.simplifyworks.security.model.domain.exception;

public class NoRemoteUserException extends AuthenticationException {

	private static final long serialVersionUID = 7943679227519423385L;

	public NoRemoteUserException() {
		super("No remote user", "no-remote-user");
	}
}
