package org.simplifyworks.security.model.domain.exception;

import java.util.Map;

import org.simplifyworks.core.exception.CoreException;

public class AuthenticationException extends CoreException {

	private static final long serialVersionUID = 3773881771502275268L;

	public AuthenticationException(String message, String messageKey) {
		super(message, messageKey);
	}

	public AuthenticationException(String message, String messageKey, Map<String, Object> parameters) {
		super(message, messageKey, parameters);
	}
}
