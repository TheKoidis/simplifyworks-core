package org.simplifyworks.security.model.domain.exception;

import java.util.Collections;

public class NoRolesException extends AuthenticationException {

	private static final long serialVersionUID = 3959336006934411333L;
	
	public NoRolesException(String username) {
		super("No roles", "no-roles", Collections.singletonMap("username", username));
	}
}
