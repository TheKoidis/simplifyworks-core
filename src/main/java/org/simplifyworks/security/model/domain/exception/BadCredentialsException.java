package org.simplifyworks.security.model.domain.exception;

public class BadCredentialsException extends AuthenticationException {

	private static final long serialVersionUID = -909820759793123691L;

	public BadCredentialsException() {
		super("Bad credentials", "bad-credentials");
	}
}
