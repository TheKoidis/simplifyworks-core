package org.simplifyworks.security.model.domain;

import java.util.Collection;
import java.util.Collections;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

/**
 * Immutable implementation of {@link Authentication} for robots (initializer, scheduler, etc.).
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class RobotAuthentication extends AbstractAuthentication {

	private static final long serialVersionUID = -4843471807899285958L;
	
	/**
	 * Name which is used as current/original username
	 */
	public static final String NAME = "robot";

	/**
	 * Creates a new instance
	 */
	public RobotAuthentication() {
		super(NAME, NAME);
	}

	/**
	 * Always returns empty collection.
	 * 
	 * @return empty collection
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Collections.emptyList();
	}
}
