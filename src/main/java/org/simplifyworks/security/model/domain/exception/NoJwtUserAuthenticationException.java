package org.simplifyworks.security.model.domain.exception;

import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.springframework.security.core.AuthenticationException;

/**
 * Thrown if authentication request does not use {@link JwtUserAuthentication}.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class NoJwtUserAuthenticationException extends AuthenticationException {

	private static final long serialVersionUID = -7365242045629884809L;

	public NoJwtUserAuthenticationException(String msg) {
		super(msg);
	}
}
