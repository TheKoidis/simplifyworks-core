package org.simplifyworks.security.model.domain;

public enum AccessType {

	READ_ONLY,
	READ_UPDATE,
	READ_DELETE,
	ALL;
}
