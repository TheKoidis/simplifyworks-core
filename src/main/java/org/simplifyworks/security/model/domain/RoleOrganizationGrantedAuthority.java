package org.simplifyworks.security.model.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 	Default implementation of granted authority which uses names of role and organization
 *
 * 	@author Svanda
 * 	@author Štěpán Osmík (osmik@ders.cz)
 */
@JsonIgnoreProperties({"authority"})
public class RoleOrganizationGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = -2393472063873247571L;
		
	private String roleName;
	private String organizationCode;
	
	public RoleOrganizationGrantedAuthority() {
	}
	
	/**
	 * Creates a new instance.
	 * 
	 * @param roleName name of role (required)
	 * @param organizationName name of organization
	 */
	public RoleOrganizationGrantedAuthority(String roleName, String organizationCode) {
		Assert.notNull(roleName, "roleName must be filled");
		
		this.roleName = roleName;
		this.organizationCode = organizationCode;
	}
	
	public String getRoleName() {
		return roleName;
	}
	
	public String getOrganizationCode() {
		return organizationCode;
	}

	@Override
	public String getAuthority() {
		return roleName + (organizationCode == null ? "" : "@" + organizationCode);
	}
	
	@Override
	public String toString() {
		return getAuthority();
	}
}
