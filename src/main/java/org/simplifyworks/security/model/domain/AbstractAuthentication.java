package org.simplifyworks.security.model.domain;

import org.simplifyworks.core.model.jpa.AuditInfoCharger;
import org.springframework.security.core.Authentication;

/**
 * Base authentication class which contains current and original username.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 * @see AuditInfoCharger
 */
public abstract class AbstractAuthentication implements Authentication {

	private static final long serialVersionUID = 4527993610233590487L;
	
	private String currentUsername;
	private String originalUsername;
	
	/**
	 * Creates a new empty instance (for JSON serialization/deserialization)
	 */
	public AbstractAuthentication() {
	}
	
	/**
	 * Creates a new instance
	 * 
	 * @param currentUsername current username
	 * @param originalUsername original username
	 */
	public AbstractAuthentication(String currentUsername, String originalUsername) {
		this.currentUsername = currentUsername;
		this.originalUsername = originalUsername;
	}

	/**
	 * Always returns current username.
	 */
	@Override
	public String getName() {
		return currentUsername;
	}
	
	/**
	 * Always returns current username.
	 */
	@Override
	public Object getPrincipal() {
		return currentUsername;
	}

	/**
	 * Always returns empty string.
	 */
	@Override
	public Object getCredentials() {
		return "";
	}

	/**
	 * Always returns null.
	 */
	@Override
	public Object getDetails() {
		return null;
	}

	/**
	 * Always returns true.
	 */
	@Override
	public boolean isAuthenticated() {
		return true;
	}

	/**
	 * Always throws exception.
	 */
	@Override
	public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
		throw new IllegalArgumentException("Authentication flag cannot be changed");
	}
	
	/**
	 * Returns current username.
	 * 
	 * @return current username
	 */
	public String getCurrentUsername() {
		return currentUsername;
	}

	/**
	 * Returns original username.
	 * 
	 * @return original username
	 */
	public String getOriginalUsername() {
		return originalUsername;
	}
}
