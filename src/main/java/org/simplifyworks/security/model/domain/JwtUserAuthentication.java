package org.simplifyworks.security.model.domain;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Immutable implementation of {@link Authentication} which can be serialized to / deserialized from JSON.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@JsonIgnoreProperties({"name", "principal", "authenticated", "credentials", "details", "roleOrganizationAuthorities", "roles", "authorities"})
public class JwtUserAuthentication extends AbstractAuthentication {

	private static final long serialVersionUID = -4426564485592604994L;
	
	private CorePersonDto person;
	
	private Date expiration;
	
	private List<String> roles;
	private Collection<RoleOrganizationGrantedAuthority> authorities;
	
	/**
	 * Creates a new empty instance (for JSON serialization/deserialization)
	 */
	public JwtUserAuthentication() {
	}
	
	/**
	 * Creates a new instance
	 * 
	 * @param username current and/or original username
	 * @param person person for the original user
	 * @param expiration expiration date
	 */
	public JwtUserAuthentication(String username, CorePersonDto person, Date expiration) {
		this(username, username, person, expiration, Collections.emptyList(), Collections.emptyList());
	}
	
	/**
	 * Creates a new instance
	 * 
	 * @param username current and/or original username
	 * @param person person for the original user
	 * @param expiration expiration date
	 * @param roles all roles
	 * @param authorities authorities (only roles in organizations)
	 */
	public JwtUserAuthentication(String username, CorePersonDto person, Date expiration, List<String> roles, Collection<RoleOrganizationGrantedAuthority> authorities) {
		this(username, username, person, expiration, roles, authorities);
	}

	/**
	 * Creates a new instance
	 * 
	 * @param currentUsername current username
	 * @param originalUsername original username
	 * @param person person for the original user
	 * @param expiration expiration date
	 * @param roles all roles
	 * @param authorities authorities (only roles in organizations)
	 */
	public JwtUserAuthentication(String currentUsername, String originalUsername, CorePersonDto person, Date expiration, List<String> roles, Collection<RoleOrganizationGrantedAuthority> authorities) {
		super(currentUsername, originalUsername);
		
		this.person = person;
		this.expiration = expiration;
		this.roles = Collections.unmodifiableList(roles);
		this.authorities = Collections.unmodifiableList(new ArrayList<>(authorities));
	}

	/**
	 * Returns explicitly assigned authorities (only roles in organization).
	 * 
	 * @see RoleOrganizationGrantedAuthority
	 */
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}
	
	/**
	 * Returns person for the original user.
	 * 
	 * @return person for the original user
	 */
	public CorePersonDto getPerson() {
		return person;
	}
	
	/**
	 * Returns date when this authentication expires.
	 * 
	 * @return expiration date
	 */
	public Date getExpiration() {
		return expiration;
	}
	
	/**
	 * Returns all roles which current authentication represents (roles are added using flat role structure).
	 * 
	 * @return all roles which current authentication represents.
	 */
	public List<String> getRoles() {
		return roles;
	}
	
	/**
	 * Sets all roles which current authentication represents (roles are added using flat role structure).
	 * 
	 * @param roles all roles which current authentication represents
	 */
	public void setRoles(List<String> roles) {
		this.roles = roles;
	}
	
	/**
	 * Returns type-safe collection of authorities.
	 * 
	 * @return type-safe collection of authorities
	 */
	public Collection<RoleOrganizationGrantedAuthority> getRoleOrganizationAuthorities() {
		return authorities;
	}
	
	/**
	 * Sets type-safe collection of authorities.
	 * 
	 * @param authorities type-safe collection of authorities
	 */
	public void setRoleOrganizationAuthorities(Collection<RoleOrganizationGrantedAuthority> authorities) {
		this.authorities = authorities;
	}
}
