package org.simplifyworks.security.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;

import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.security.model.domain.AccessType;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreUser;

/**
 * Base class for permissions (modules should extend this class and/or add relationship attribute).
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@MappedSuperclass
public class CorePermission extends AbstractEntity {
	
	private static final long serialVersionUID = -3125467195228607318L;

	@JoinColumn(name = "CORE_USER_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true)
	private CoreUser user;
	
	@JoinColumn(name = "CORE_ROLE_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true)
	private CoreRole role;
	
	@JoinColumn(name = "CORE_ORGANIZATION_ID", referencedColumnName = "ID")
	@ManyToOne(optional = true)
	private CoreOrganization organization;
	
	@NotNull
	@Column(name = "ACTIVE", nullable = false)
	private Boolean active = Boolean.TRUE;
	
	@Column(name = "VALID_FROM")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validFrom;
	
	@Column(name = "VALID_TILL")
	@Temporal(TemporalType.TIMESTAMP)
	private Date validTill;
	
	@NotNull
	@Enumerated(EnumType.STRING)
	@Column(name = "ACCESS_TYPE", nullable = false)
	private AccessType accessType;

	public CorePermission() {
	}
	
	public CorePermission(CoreUser user, AccessType accessType) {
		this.user = user;
		this.accessType = accessType;
	}

	public CorePermission(CoreRole role, AccessType accessType) {
		this.role = role;
		this.accessType = accessType;
	}

	public CorePermission(CoreRole role, CoreOrganization organization, AccessType accessType) {
		this.role = role;
		this.organization = organization;
		this.accessType = accessType;
	}

	public CoreUser getUser() {
		return user;
	}

	public void setUser(CoreUser user) {
		this.user = user;
	}

	public CoreRole getRole() {
		return role;
	}

	public void setRole(CoreRole role) {
		this.role = role;
	}

	public CoreOrganization getOrganization() {
		return organization;
	}

	public void setOrganization(CoreOrganization organization) {
		this.organization = organization;
	}

	public Boolean getActive() {
		return active;
	}

	public void setActive(Boolean active) {
		this.active = active;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidTill() {
		return validTill;
	}

	public void setValidTill(Date validTill) {
		this.validTill = validTill;
	}

	public AccessType getAccessType() {
		return accessType;
	}

	public void setAccessType(AccessType accessType) {
		this.accessType = accessType;
	}
}
