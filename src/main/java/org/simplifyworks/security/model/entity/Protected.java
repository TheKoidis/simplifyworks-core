package org.simplifyworks.security.model.entity;

import java.util.Set;

import org.simplifyworks.security.service.PermissionSupportService;

/**
 * Interface for classes which are protected by security
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 *
 * @param <P> extended permission type
 * @see PermissionSupportService
 */
public interface Protected<P extends CorePermission> {

	public Set<P> getPermissions();
	
	public void setPermissions(Set<P> permissions);
}
