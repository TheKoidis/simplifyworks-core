package org.simplifyworks;

import org.simplifyworks.core.util.Mapper;
import org.simplifyworks.core.util.Wrapper;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;

/**
 * Application Entry Point
 *
 * @author Radek Tomiška <tomiska@simplifyworks.org>
 * @author Jirka Pech <pech@simplifyworks.org>
 *
 */
@SpringBootApplication
@EnableAutoConfiguration(exclude= { FlywayAutoConfiguration.class })
public class Application extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		application.showBanner(false);
		return application.sources(Application.class);
	}

	@Bean
	public Mapper objectMapper() {
		return new Mapper();
	}

	@Bean
	public Wrapper resourceWrapper() {
		return new Wrapper();
	}
}
