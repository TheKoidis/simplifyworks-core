package org.simplifyworks.client;

import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.simplifyworks.email.service.Emailer;
import org.simplifyworks.scheduler.service.SchedulerService;
import org.simplifyworks.security.model.domain.RobotAuthentication;
import org.simplifyworks.uam.service.CoreFlatOrganizationManager;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.simplifyworks.uam.service.CoreRoleHierarchy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * Initializes application
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Component
public class Initializer implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger LOG = LoggerFactory.getLogger(Initializer.class);

	@Autowired
	private Emailer emailer;

	@Autowired
	private CoreRoleHierarchy coreRoleHierarchy;

	@Autowired
	private CoreFlatRoleManager flatRoleManager;

	@Autowired
	private CoreFlatOrganizationManager flatOrganizationManager;

	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		SecurityContextHolder.getContext().setAuthentication(new RobotAuthentication());

		coreRoleHierarchy.init();

		emailer.init();

		flatRoleManager.rebuild();
		flatOrganizationManager.rebuild();
	}

	@Component
	@ConditionalOnProperty(prefix = "scheduler", name = "enabled", matchIfMissing = true)
	public static class SchedulerInitializer implements ApplicationListener<ContextRefreshedEvent> {

		@Autowired
		private Scheduler scheduler;
		@Autowired
		private SchedulerService schedulerService;

		@Override
		public void onApplicationEvent(ContextRefreshedEvent event) {
			try {
				scheduler.start();
				schedulerService.init();
			} catch (SchedulerException ex) {
				LOG.error(null, ex);
			}
		}
	}

}
