package org.simplifyworks.client.config.documentation;

import java.util.List;

import org.simplifyworks.core.model.domain.ExtendedArrayList;

import com.fasterxml.classmate.ResolvedType;
import com.fasterxml.classmate.TypeResolver;

import springfox.documentation.schema.AlternateTypeRule;

/**
 * Custom type rule for {@link ExtendedArrayList}.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 * @see ExtendedArrayList
 */
public class ExtendedArrayListAlternateTypeRule extends AlternateTypeRule {

	/**
	 * Creates a new instance.
	 */
	public ExtendedArrayListAlternateTypeRule() {
		super(null, null);
	}
	
	@Override
	public ResolvedType alternateFor(ResolvedType type) {
		return new TypeResolver().resolve(ExtendedArrayListWrapper.class, type.getTypeParameters().get(0));
	}
	
	@Override
	public boolean appliesTo(ResolvedType type) {
		return type.isInstanceOf(ExtendedArrayList.class);
	}
	
	/**
	 * Wrapper for documentation purposes of {@link ExtendedArrayList}.
	 * 
	 * @author Štěpán Osmík (osmik@ders.cz)
	 */
	class ExtendedArrayListWrapper<E> {
		private int recordsReturned;
		private int recordsFiltered;
		private int recordsTotal;
		
		private List<E> resources;

		public int getRecordsReturned() {
			return recordsReturned;
		}

		public void setRecordsReturned(int recordsReturned) {
			this.recordsReturned = recordsReturned;
		}

		public int getRecordsFiltered() {
			return recordsFiltered;
		}

		public void setRecordsFiltered(int recordsFiltered) {
			this.recordsFiltered = recordsFiltered;
		}

		public int getRecordsTotal() {
			return recordsTotal;
		}

		public void setRecordsTotal(int recordsTotal) {
			this.recordsTotal = recordsTotal;
		}

		public List<E> getResources() {
			return resources;
		}

		public void setResources(List<E> resources) {
			this.resources = resources;
		}
	}
}
