package org.simplifyworks.client.config.documentation;

import static springfox.documentation.builders.PathSelectors.regex;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@ConditionalOnWebApplication
public class SwaggerConfig {
	
	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.groupName("CORE")
				.alternateTypeRules(new ExtendedArrayListAlternateTypeRule())
				.consumes(Collections.singleton("application/json"))
				.produces(Collections.singleton("application/json"))
				.protocols(new HashSet<>(Arrays.asList("http", "https")))
				.select()
				.apis(RequestHandlerSelectors.basePackage("org.simplifyworks"))
				.paths(regex("(/auth/log-in|/api/build-descriptions|/api/(core|scheduler|workflow))(/.*)?")).build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfo(
		      "SimplifyWorks CORE REST API",
		      "Welcome to the documentation of SimplifyWorks REST API.",
		      "version unknown",
		      "terms of service unknown",
		      "info@simplifyworks.org",
		      "licence unknown",
		      "licence URL unknown");
	}

}
