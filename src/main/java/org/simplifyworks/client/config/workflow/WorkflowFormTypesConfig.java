package org.simplifyworks.client.config.workflow;

import java.sql.SQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.activiti.engine.impl.cfg.ProcessEngineConfigurationImpl;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.simplifyworks.workflow.model.domain.WorkflowFormTypes;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class WorkflowFormTypesConfig {

	private static final Logger LOG = LoggerFactory.getLogger(WorkflowFormTypesConfig.class);

	@Bean
	public BeanPostProcessor activitiConfigurer() {
		return new BeanPostProcessor() {

			@Override
			public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
				if (bean instanceof ProcessEngineFactoryBean) {
					ProcessEngineConfigurationImpl processEngineConfiguration = ((ProcessEngineFactoryBean) bean).getProcessEngineConfiguration(); //
					processEngineConfiguration.setFormTypes(new WorkflowFormTypes());
					try {
						//fixed Activiti bug: another Oracle schema contains "activiti" table
						if ("Oracle".equalsIgnoreCase(processEngineConfiguration.getDataSource().getConnection().getMetaData().getDatabaseProductName())
										&& (processEngineConfiguration.getDatabaseSchema() == null || processEngineConfiguration.getDatabaseSchema().isEmpty())) {
							String userName = processEngineConfiguration.getDataSource().getConnection().getMetaData().getUserName();
							processEngineConfiguration.setDatabaseSchema(userName);
						}
					} catch (SQLException ex) {
						LOG.error(null, ex);
					}
				}
				return bean;
			}

			@Override
			public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
				return bean;
			}
		};

	}
}
