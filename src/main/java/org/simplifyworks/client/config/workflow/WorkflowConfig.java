package org.simplifyworks.client.config.workflow;

import org.activiti.engine.ProcessEngineConfiguration;
import org.activiti.engine.impl.bpmn.parser.factory.ActivityBehaviorFactory;
import org.activiti.spring.ProcessEngineFactoryBean;
import org.activiti.spring.SpringProcessEngineConfiguration;
import org.activiti.spring.autodeployment.SingleResourceAutoDeploymentStrategy;
import org.simplifyworks.workflow.domain.CustomActivityBehaviorFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Workflow
 *
 * @todo: sloucit s WorkflowFormTypesConfig, DefaultWorkflowProcessDefinitionService ?
 */
@Configuration
public class WorkflowConfig {

	ProcessEngineConfiguration processEngineConfiguration;

	@Bean
	public ActivityBehaviorFactory activityBehaviorFactory() {
		CustomActivityBehaviorFactory customActivityBehaviorFactory = new CustomActivityBehaviorFactory();
		//We have to set expressionManager for evaluate expression in workflow
		customActivityBehaviorFactory.setExpressionManager(((SpringProcessEngineConfiguration) processEngineConfiguration).getExpressionManager());
		//Set custom activity behavior ... for catch email
		((SpringProcessEngineConfiguration) processEngineConfiguration).getBpmnParser().setActivityBehaviorFactory(customActivityBehaviorFactory);
		return customActivityBehaviorFactory;
	}

	/**
	 * configure deployment mode for automatic resource deployment
	 * (http://www.activiti.org/userguide/#_automatic_resource_deployment)
	 * "default" (group all resources into a single deployment) is incorrect in combination with UI workflow agenda (delete action)
	 * "single-resource" is OK
	 *
	 * @return
	 */
	@Bean
	public BeanPostProcessor activitiDeploymentModeConfigurer() {
		return new BeanPostProcessor() {

			@Override
			public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
				if (bean instanceof ProcessEngineFactoryBean) {
					processEngineConfiguration = ((ProcessEngineFactoryBean) bean).getProcessEngineConfiguration();
					((SpringProcessEngineConfiguration) processEngineConfiguration).setDeploymentMode(SingleResourceAutoDeploymentStrategy.DEPLOYMENT_MODE);
				}
				return bean;
			}

			@Override
			public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
				return bean;
			}
		};
	}

	/*@Bean
	public WorkflowEventListener workflowEventListener() {
		WorkflowEventListener listener = new WorkflowEventListener();
		processEngineConfiguration.getRuntimeService().addEventListener(listener);
		return listener;
	}*/
}
