/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.config.flyway;

import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * SIFY-260
 *
 * @author hanak@ders.cz
 */
public abstract class AbstractFlywayConfig {

	@Autowired
	FlywayBeanPostProcessor.FlywayConfigurationExt flywayCoreConfigExt;

	public Flyway createFlyway() {
		return flywayCoreConfigExt.createFlyway();
	}

}
