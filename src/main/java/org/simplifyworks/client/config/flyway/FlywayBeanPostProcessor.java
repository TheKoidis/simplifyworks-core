/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.config.flyway;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.internal.command.DbBaseline;
import org.flywaydb.core.internal.dbsupport.DbSupport;
import org.flywaydb.core.internal.dbsupport.DbSupportFactory;
import org.flywaydb.core.internal.dbsupport.Schema;
import org.flywaydb.core.internal.metadatatable.MetaDataTableImpl;
import org.flywaydb.core.internal.util.jdbc.JdbcUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * SIFY-132 / SIFY-260 multi-module db migrations
 * - based on org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration
 *
 * @author hanak@ders.cz
 */
@Configuration
@ConditionalOnClass(Flyway.class)
@ConditionalOnProperty(prefix = "flyway", name = "enabled", matchIfMissing = false)
@AutoConfigureAfter(DataSourceAutoConfiguration.class)
public class FlywayBeanPostProcessor extends FlywayAutoConfiguration {

	@Bean
	public BeanPostProcessor flywayMetaDataTablePreparer() {
		return new BeanPostProcessor() {

			@Override
			public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
				return bean;
			}

			@Override
			public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
				if (bean instanceof Flyway) {
					Flyway fw = (Flyway) bean;
					Connection connection = JdbcUtils.openConnection(fw.getDataSource());
					DbSupport dbSupport = DbSupportFactory.createDbSupport(connection, false);
					//replacing the symbolic name of the database
					String dbName = dbSupport.getDbName();
					String[] locations = fw.getLocations();
					for (int i = 0; i < locations.length; i++) {
						locations[i] = locations[i].replace("${dbName}", dbName);
					}
					fw.setLocations(locations);
					//
					if (!fw.isBaselineOnMigrate()) {
						String[] schemaNames = fw.getSchemas();
						if (schemaNames.length == 0) {
							//Flyway 3.x schemaNames = new String[]{dbSupport.getCurrentSchema().getName()};
							schemaNames = new String[]{dbSupport.getOriginalSchema().getName()};
						}
						Schema[] schemas = new Schema[schemaNames.length];
						for (int i = 0; i < schemaNames.length; i++) {
							schemas[i] = dbSupport.getSchema(schemaNames[i]);
						}
						List<Schema> nonEmptySchemas = new ArrayList<Schema>();
						for (Schema schema : schemas) {
							if (!schema.empty()) {
								nonEmptySchemas.add(schema);
							}
						}
						//Creating Metadata table with "baseline" marker
						if (!nonEmptySchemas.isEmpty() && !nonEmptySchemas.get(0).getTable(fw.getTable()).exists()) {
							MetaDataTableImpl metaDtTbl = new MetaDataTableImpl(dbSupport, schemas[0].getTable(fw.getTable()));
							//Flyway 3.x new DbBaseline(connection, metaDtTbl, fw.getBaselineVersion(), fw.getBaselineDescription(), fw.getCallbacks()).baseline();
							new DbBaseline(connection, dbSupport, metaDtTbl, schemas[0], fw.getBaselineVersion(), fw.getBaselineDescription(), fw.getCallbacks()).baseline();
						}
					}
					//call "initMethod"
					fw.migrate();
				}
				return bean;
			}
		};
	}

	@Configuration
	@Import(FlywayJpaDependencyConfiguration.class)
	public static class FlywayConfigurationExt extends FlywayAutoConfiguration.FlywayConfiguration {

		@PostConstruct
		@Override
		public void checkLocationExists() {
			// cannot be used - specific property prefix for modules "locations" property
		}

		@Override
		public Flyway flyway() {
			return null;
		}

		public Flyway createFlyway() {
			return super.flyway();
		}

	}
}
