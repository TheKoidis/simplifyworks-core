package org.simplifyworks.client.config.flyway;


import org.flywaydb.core.Flyway;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.flyway.FlywayProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.PropertySource;

/**
 * SIFY-132 / SIFY-260 multi-module db migrations
 * - based on org.springframework.boot.autoconfigure.flyway.FlywayAutoConfiguration
 *
 * @author hanak@ders.cz
 */
@Configuration
@ConditionalOnClass(Flyway.class)
@ConditionalOnProperty(prefix = "flyway", name = "enabled", matchIfMissing = false)
@AutoConfigureAfter(FlywayBeanPostProcessor.FlywayConfigurationExt.class)
@EnableConfigurationProperties(FlywayProperties.class)
@PropertySource("classpath:/flyway-core.properties")
public class FlywayConfigCore extends AbstractFlywayConfig {

		@DependsOn("flywayMetaDataTablePreparer")
		@ConditionalOnMissingBean(name = "flywayCore")
		@ConditionalOnExpression("${flyway.enabled:true} && '${flyway.core.locations}'!=''")
		@ConfigurationProperties(prefix = "flyway.core")
		@Bean
		public Flyway flywayCore() {
			return super.createFlyway();
		}

}
