package org.simplifyworks.client.config;

import java.util.HashMap;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Component;

@Component
@ConditionalOnProperty(prefix = "database.schema.validator", name = "enabled", matchIfMissing = false)
public class DatabaseSchemaValidator implements ApplicationListener<ContextRefreshedEvent> {

	@Autowired
	private DataSource dataSource;
		
	@Autowired
	private EntityManagerFactoryBuilder validationEntityManagerFactoryBuilder;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {		
		validateSchema();
	}
	
	public void validateSchema() {
		Map<String, Object> properties = new HashMap<>();
		properties.put("hibernate.hbm2ddl.auto", "validate");
		properties.put("hibernate.ejb.naming_strategy", SpringNamingStrategy.class);
		
		LocalContainerEntityManagerFactoryBean bean = validationEntityManagerFactoryBuilder
				.dataSource(dataSource)
				.packages("org.simplifyworks")
				.properties(properties)
				.persistenceUnit("validation-only")
				.build();
		
		bean.afterPropertiesSet();
		bean.destroy();		
	}
}
