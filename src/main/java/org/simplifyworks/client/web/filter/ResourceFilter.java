package org.simplifyworks.client.web.filter;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Component
public class ResourceFilter implements Filter {

	@Value("${application.url.hostName}")
	private String hostName;
	
	@Value("${application.url.contextName}")
	private String contextName;
	
	@Value("${application.sso.enabled:false}")
	private String ssoEnabled;
	
	@Value("${application.sso.logoutUrl:}")
	private String ssoLogoutUrl;

	/**
	 *
	 * @param request The servlet request we are processing
	 * @param response The servlet response we are creating
	 * @param chain The filter chain we are processing
	 *
	 * @exception IOException if an input/output error occurs
	 * @exception ServletException if a servlet error occurs
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain)
			throws IOException, ServletException {

		String path = ((HttpServletRequest) request).getRequestURI();
		if (path.matches("^(\\/[^/]*)?\\/(error)(\\/.*)?$")) {
			return;
		}
		if (path.matches("^(\\/[^/]*)?\\/(js|css|images|public|fonts|api|auth|monitoring|.*swagger.*|.*webjars.*|api[-]docs)(\\/.*)?$")) {
			chain.doFilter(request, response); // Just continue chain.
			return;
		}

		response.setContentType("text/html;charset=UTF-8");

		try (PrintWriter out = response.getWriter()) {
			InputStream resourceAsStream = ResourceFilter.class.getResourceAsStream("/index.html");
			StringWriter writer = new StringWriter();

			IOUtils.copy(resourceAsStream, writer);
			String content = writer.toString();
			content = content.replaceAll("\\{contextName\\}", contextName);
			content = content.replaceAll("\\{hostName\\}", hostName);
			content = content.replaceAll("\\{ssoEnabled\\}", ssoEnabled);
			content = content.replaceAll("\\{ssoLogoutUrl\\}", ssoLogoutUrl);
			out.write(content);
			out.close();
		}
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void destroy() {

	}
}
