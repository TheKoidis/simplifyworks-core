/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.client.web.filter;

import java.io.IOException;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * TODO: enable just specified origin: http://spring.io/guides/tutorials/bookmarks/
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
@Component
public class SimpleCORSFilter implements Filter {

	protected static final Logger LOG = LoggerFactory.getLogger(SimpleCORSFilter.class);

	@Value("#{'${access-control-allowed-origins:*}'.replaceAll(\"\\s*\",\"\").split(',')}")
	private List<String> accessControlAllowedOrigins;

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		HttpServletResponse response = (HttpServletResponse) res;

		String reqOrigin = ((HttpServletRequest) req).getHeader("Origin");
		if (accessControlAllowedOrigins.contains(reqOrigin)) {
			response.setHeader("Access-Control-Allow-Origin", reqOrigin);
			response.setHeader("Access-Control-Allow-Credentials", "true");
		} else {
			response.setHeader("Access-Control-Allow-Origin", "*");
			response.setHeader("Access-Control-Allow-Credentials", "false");
		}
		LOG.debug("Origin: " + reqOrigin +"; Access-Control-Allow-Origin: " + response.getHeader("Access-Control-Allow-Origin"));
		response.setHeader("Access-Control-Allow-Methods", "POST, GET, PUT, OPTIONS, DELETE");
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", "Origin, origin, Accept, accept, X-Requested-With, x-requested-with, Content-Type, content-type, Access-Control-Request-Method, access-control-request-method, Access-Control-Request-Headers, access-control-request-headers, Authorization, authorization, Cookie, cookie");
		response.setHeader("X-XSS-Protection", "0");
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig filterConfig) {}

	@Override
	public void destroy() {}

}
