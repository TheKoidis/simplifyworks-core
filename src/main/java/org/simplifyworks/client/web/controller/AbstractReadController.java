package org.simplifyworks.client.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public abstract class AbstractReadController<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> extends AbstractController<T, E, F> {

	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<T> read(@Valid @RequestBody F filter) {
		Long recordsCount = getManager().getRecordsCount(filter);

		List<E> entities;
		if (recordsCount > 0) {
			entities = getManager().read(filter);
		} else {
			entities = new ArrayList<>();
		}
		return wrap(entities, recordsCount, recordsCount);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public T read(@PathVariable("id") Long id) {
		T ent = getManager().load(id);
		return ent;
	}

}
