package org.simplifyworks.client.web.controller;

import java.util.Collection;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.simplifyworks.core.util.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public abstract class AbstractController<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> {

	@Autowired
	Wrapper wrapper;

	public abstract SearchLoadWriteManager<T, E, F> getManager();

	public T wrap(E entity) {
		return wrapper.wrap(entity, getManager());
	}

	public ExtendedArrayList<T> wrap(Collection<E> entities, Long recordsTotal, Long recordsFiltered) {
		return wrapper.wrap(entities, recordsTotal, recordsFiltered, getManager());
	}

}
