package org.simplifyworks.client.web.controller;

import javax.validation.Valid;

import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public abstract class AbstractReadWriteController<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto>
				extends AbstractReadController<T, E, F> {

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody T dto) {
		return getManager().create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		getManager().delete(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public Long write(@PathVariable("id") Long id, @Valid @RequestBody T dto) {
		return getManager().write(id, dto);
	}

}
