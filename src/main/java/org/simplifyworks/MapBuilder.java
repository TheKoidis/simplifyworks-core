package org.simplifyworks;

import java.util.LinkedHashMap;
import java.util.Map;

public class MapBuilder {

	private MapBuilder parent;
	private Map<String, Object> map;
	
	public MapBuilder() {
		this(null);
	}
	
	private MapBuilder(MapBuilder parent) {
		this.parent = parent;
		this.map = new LinkedHashMap<>();
	}
	
	public Map<String, Object> build() {		
		return map;
	}
		
	public MapBuilder put(String key, Object value) {
		map.put(key, value);
		
		return this;
	}
	
	public MapBuilder put(String key) {
		MapBuilder child = new MapBuilder(this);
		map.put(key, child.build());
		
		return child;
	}
	
	public MapBuilder close() {
		return parent;
	}
}
