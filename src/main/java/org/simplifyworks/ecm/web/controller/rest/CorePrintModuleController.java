/**
 * 
 */
package org.simplifyworks.ecm.web.controller.rest;

import javax.validation.Valid;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.ecm.model.dto.CorePrintModuleDto;
import org.simplifyworks.ecm.model.dto.CorePrintModuleFilterDto;
import org.simplifyworks.ecm.service.CorePrintModuleManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Controller for print modules
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@RestController
@RequestMapping(value = "/api/core/print-modules")
public class CorePrintModuleController {

	@Autowired
	private CorePrintModuleManager manager;
	
	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<CorePrintModuleDto> getIssues(@Valid @RequestBody CorePrintModuleFilterDto filter) {
		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody(required = true) CorePrintModuleDto dto) {
		return manager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		manager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public CorePrintModuleDto find(@PathVariable("id") Long id) {
		return manager.find(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void update(@PathVariable("id") Long id, @Valid @RequestBody CorePrintModuleDto dto) {
		manager.update(id, dto);
	}
}
