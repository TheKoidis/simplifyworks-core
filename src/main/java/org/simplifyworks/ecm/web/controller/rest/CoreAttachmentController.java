package org.simplifyworks.ecm.web.controller.rest;

import java.io.IOException;
import java.util.Collection;

import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.dto.CoreAttachmentFilterDto;
import org.simplifyworks.ecm.service.CoreAttachmentManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.InvalidMediaTypeException;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.util.InvalidMimeTypeException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Martin Široký <siroky@ders.cz>
 */
@RestController
@RequestMapping(value = "/api/core/attachments")
public class CoreAttachmentController {

	protected static final Logger LOG = LoggerFactory.getLogger(CoreAttachmentController.class);

	@Autowired
	private CoreAttachmentManager attachmentManager;

	@RequestMapping(value = "/download/{uid}", method = RequestMethod.GET)
	public ResponseEntity<InputStreamResource> download(@PathVariable String uid)
			throws IOException {
		Assert.notNull(uid, "Insert uid of attachment!");

		Collection<CoreAttachmentDto> resources = attachmentManager.getAllWrapped(new CoreAttachmentFilterDto(uid));
		if (resources.size() != 1) {
			return null;
		}
		CoreAttachmentDto attachment = resources.iterator().next();

		Assert.notNull(attachment, "Attachment not found!");

		MediaType mediaType = null;
		try {
			mediaType = MediaType.parseMediaType(attachment.getMimeType());
		} catch (InvalidMimeTypeException | InvalidMediaTypeException ex) {
			//
		}

		HttpHeaders respHeaders = new HttpHeaders();
		respHeaders.setContentType(mediaType == null ? MediaType.MULTIPART_FORM_DATA : mediaType);
		respHeaders.setContentLength(attachment.getSize());
		respHeaders.setContentDispositionFormData("attachment", attachment.getName());

		return new ResponseEntity<>(new InputStreamResource(attachmentManager.downloadAttachment(uid)), respHeaders, HttpStatus.OK);
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public CoreAttachmentDto upload(MultipartFile file) throws IOException {
		Assert.notNull(file, "Insert binary data!");

		return attachmentManager.saveAttachment(file);
	}
}
