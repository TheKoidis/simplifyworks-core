package org.simplifyworks.ecm.web.controller.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.annotations.ApiOperation;

/**
 * Base controller for report generating and downloading.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class AbstractReportController {

	/**
	 * Name parameter name
	 */
	public static final String NAME_PARAMETER = "name";
	
	/**
	 * Template parameter name
	 */
	public static final String TEMPLATE_PARAMETER = "template";
	
	/**
	 * Type parameter name
	 */
	public static final String TYPE_PARAMETER = "type";
	
	/**
	 * IDs parameter name
	 */
	public static final String IDS_PARAMETER = "ids";
	
	/**
	 * IDs parameter separator
	 */
	public static final String IDS_SEPARATOR = ",|;";
	
	/**
	 * Generates report using request parameters.
	 * 
	 * @param request request with parameters (must contains report name, type and may contains ids)
	 * @return generated report
	 * @throws IOException if something with IO is broken
	 */
	@RequestMapping(value = "/download", method = RequestMethod.GET)
	@ApiOperation(response = InputStream.class, value = "Returns generated document")
	public ResponseEntity<InputStreamResource> generate(@RequestParam(NAME_PARAMETER) String name, @RequestParam(TEMPLATE_PARAMETER) String template, @RequestParam(TYPE_PARAMETER) ReportType type, @RequestParam(IDS_PARAMETER) List<Long> ids) throws IOException {				
		Report report = generateReport(template, type, ids);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType(type.getMimeType()));
		headers.setContentLength(new File(report.getReportUri()).length());
		headers.setContentDispositionFormData(name, name + "." + type.getSuffix());

		return new ResponseEntity<InputStreamResource>(
				new InputStreamResource(new FileInputStream(new File(report.getReportUri()))), headers, HttpStatus.OK);
	}
	
	/**
	 * Generates report using template name, type and ids
	 * 
	 * @param template name of template
	 * @param type type of template
	 * @param ids ids of records
	 * @return generated report
	 */
	protected abstract Report generateReport(String template, ReportType type, List<Long> ids);
}