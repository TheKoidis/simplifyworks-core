package org.simplifyworks.ecm.model.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Entity
@Table(name = "CORE_ATTACHMENT")
@Inheritance(strategy = InheritanceType.JOINED)
public class CoreAttachment extends AbstractEntity {

	@NotNull
	@Size(max = 255)
	@Column(name = "NAME", nullable = false, length = 255)
	private String name;
	@NotNull
	@Size(min = 1, max = 36)
	@Column(name = "GUID", nullable = false, length = 36)
	private String guid;
	@NotNull
	@Max(999999999999999999l)
	@Column(name = "FILESIZE", nullable = false, precision = 18, scale = 0)
	private Long size;
	@Column(name = "UPLOADED")
	@Temporal(value = TemporalType.TIMESTAMP)
	private Date uploaded;
	@NotNull
	@Size(min = 1, max = 255)
	@Column(name = "MIMETYPE", nullable = false, length = 255)
	private String mimeType;

	public CoreAttachment() {
	}

	public CoreAttachment(Long id) {
		super(id);
	}

	public CoreAttachment(String name, String uid, Long size, Date uploaded, String mimeType) {
		this.name = name;
		this.guid = uid;
		this.size = size;
		this.uploaded = uploaded;
		this.mimeType = mimeType;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Date getUploaded() {
		return uploaded;
	}

	public void setUploaded(Date uploaded) {
		this.uploaded = uploaded;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}
