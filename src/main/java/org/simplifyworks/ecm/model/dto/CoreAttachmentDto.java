package org.simplifyworks.ecm.model.dto;

import java.util.Date;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreAttachmentDto extends AbstractDto {

	@NotNull
	@Size(max = 255)
	private String name;
	@NotNull
	@Size(min = 1, max = 36)
	private String guid;
	@NotNull
	@Max(999999999999999999l)
	private Long size;
	private Date uploaded;
	@NotNull
	@Size(min = 1, max = 255)
	private String mimeType;

	public CoreAttachmentDto(String name, String uid, Long size, Date uploaded, String mimeType) {
		this.name = name;
		this.guid = uid;
		this.size = size;
		this.uploaded = uploaded;
		this.mimeType = mimeType;
	}

	public CoreAttachmentDto() {
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public Long getSize() {
		return size;
	}

	public void setSize(Long size) {
		this.size = size;
	}

	public Date getUploaded() {
		return uploaded;
	}

	public void setUploaded(Date uploaded) {
		this.uploaded = uploaded;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}
}
