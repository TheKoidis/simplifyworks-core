package org.simplifyworks.ecm.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.dto.AbstractDto;
import org.simplifyworks.ecm.domain.ReportType;
import org.simplifyworks.ecm.model.entity.CorePrintModule;

/**
 * Data object for {@link CorePrintModule}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class CorePrintModuleDto extends AbstractDto {

	@NotNull
	@Size(min = 1, max = 255)
	private String name;
	@NotNull
	@Size(min = 1, max = 255)
	private String template;
	@NotNull
	private ReportType type;
	@NotNull
	@Size(min = 1, max = 128)
	private String objectType;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTemplate() {
		return template;
	}

	public void setTemplate(String template) {
		this.template = template;
	}

	public ReportType getType() {
		return type;
	}

	public void setType(ReportType type) {
		this.type = type;
	}

	public String getObjectType() {
		return objectType;
	}

	public void setObjectType(String objectType) {
		this.objectType = objectType;
	}
}
