package org.simplifyworks.ecm.model.dto;

import org.simplifyworks.core.model.dto.AbstractFilterDto;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class CoreAttachmentFilterDto extends AbstractFilterDto {

	private String guid;

	public CoreAttachmentFilterDto() {
	}

	public CoreAttachmentFilterDto(String guid) {
		this.guid = guid;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

}
