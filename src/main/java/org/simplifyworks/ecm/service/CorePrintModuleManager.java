package org.simplifyworks.ecm.service;

import org.simplifyworks.core.service.SearchFilterManager;
import org.simplifyworks.ecm.model.dto.CorePrintModuleDto;
import org.simplifyworks.ecm.model.dto.CorePrintModuleFilterDto;
import org.simplifyworks.ecm.model.entity.CorePrintModule;

/**
 * Interface for print module manager implementations
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface CorePrintModuleManager extends SearchFilterManager<CorePrintModuleDto, CorePrintModule, CorePrintModuleFilterDto> {

	public Long create(CorePrintModuleDto dto);

	public void delete(Long id);

	public CorePrintModuleDto find(Long id);

	public void update(Long id, CorePrintModuleDto dto);
}
