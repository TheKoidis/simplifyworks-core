/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.ecm.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import org.simplifyworks.core.service.SearchFilterManager;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.dto.CoreAttachmentFilterDto;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Zavrel
 */
public interface CoreAttachmentManager extends SearchFilterManager<CoreAttachmentDto, CoreAttachment, CoreAttachmentFilterDto> {

	public CoreAttachmentDto saveAttachment(MultipartFile file) throws IllegalStateException, IOException;

	public InputStream downloadAttachment(String uid) throws FileNotFoundException;
}
