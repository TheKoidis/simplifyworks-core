/**
 * 
 */
package org.simplifyworks.ecm.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.simplifyworks.ecm.service.ReportManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Embedded implementation of {@link ReportManager} which uses JasperReports.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class EmbeddedJasperReportManager extends AbstractJasperReportManager implements ReportManager {

	protected static final Logger LOG = LoggerFactory.getLogger(EmbeddedJasperReportManager.class);

	private String templateBaseDir;
	
	@Override
	public Report generateReport(String name, ReportType type, URI dataUri) {
		try {
			File reportFile = createFile(reportBaseDir, REPORT_FILE_PREFIX, name, type.getSuffix(), UUID.randomUUID());
			
			Map<String, Object> parameters = new HashMap<>();
			parameters.put(XML_DATA_PARAM_NAME, dataUri.toString());

			JasperReport jasperReport = JasperCompileManager.compileReport(findTemplateFile(name).getAbsolutePath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters);
			
			switch(type) {
				case PDF:
					JasperExportManager.exportReportToPdfFile(jasperPrint, reportFile.getAbsolutePath());
					break;
				case HTML:
					JasperExportManager.exportReportToHtmlFile(jasperPrint, reportFile.getAbsolutePath());
					break;
				case XML:
					JasperExportManager.exportReportToXmlFile(jasperPrint, reportFile.getAbsolutePath(), false);
					break;
			}
			
			return new Report(name, type, dataUri, reportFile.toURI(), true);
		} catch(JRException | IOException e) {
			throw new CoreException(e);
		}
	}
	
	/**
	 * Finds template file using specified name in template base directory
	 * 
	 * @param name name of template
	 * @return template file
	 */
	private File findTemplateFile(String name) {
		File templateFile = new File(templateBaseDir, name + ".jrxml");
		
		if(!templateFile.exists()) {
			throw new CoreException("Report template '" + templateFile + "' does not exist");
		}
		
		return templateFile;
	}
	
	// configuration properties setters
	
	public void setTemplateBaseDir(String templateBaseDir) {
		this.templateBaseDir = templateBaseDir;
	}
}
