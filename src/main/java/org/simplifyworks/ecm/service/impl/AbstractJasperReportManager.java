/**
 *
 */
package org.simplifyworks.ecm.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.MessageFormat;
import java.util.UUID;

import javax.annotation.PostConstruct;
import javax.xml.bind.JAXB;

import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.simplifyworks.ecm.service.ReportManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for JasperReport managers
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public abstract class AbstractJasperReportManager implements ReportManager {

	protected static final Logger LOG = LoggerFactory.getLogger(AbstractJasperReportManager.class);

	/**
	 * JasperServer reports XML data parameter name constant
	 */
	protected final String XML_DATA_PARAM_NAME = "net.sf.jasperreports.xml.source";

	/**
	 * Prefix for data files
	 */
	protected final String DATA_FILE_PREFIX = "data";

	/**
	 * Prefix for report files
	 */
	protected final String REPORT_FILE_PREFIX = "report";

	protected String dataBaseDir;
	protected String reportBaseDir;
	protected String dataBaseUrl;

	/**
	 * Clean directories (both sources and results). Also does check of
	 * existence.
	 */
	@PostConstruct
	protected void cleanDirs() {
		prepareDir(dataBaseDir);
		prepareDir(reportBaseDir);
	}

	/**
	 * Prepares directory (creates or cleans).
	 *
	 * @param dir directory path
	 */
	private void prepareDir(String dir) {
		try {
			if (LOG.isDebugEnabled()) {
				LOG.debug("Preparing directory '{}' for Jasper reports", dir);
			}

			File file = new File(dir);

			if (!file.exists() && !file.mkdir()) {
				throw new CoreException("Directory " + file + " does not exist and cannot be created");
			} else if (file.isDirectory()) {
				FileUtils.cleanDirectory(file);
			} else {
				throw new CoreException("File " + file + " is not directory");
			}
		} catch (IOException e) {
			throw new CoreException(e);
		}
	}

	@Override
	public Report generateReport(String name, ReportType type, Object data) {
		try {
			UUID randomUUID = UUID.randomUUID();
			File dataFile = createFile(dataBaseDir, DATA_FILE_PREFIX, name, "xml", randomUUID);
			URI uri = createUri(dataBaseUrl, DATA_FILE_PREFIX, name, "xml", randomUUID);

			JAXB.marshal(data, dataFile);
			
			if(LOG.isDebugEnabled()) {
				LOG.debug("Data file '{}'/{} bytes", dataFile, dataFile.length());
			}

			return generateReport(name, type, uri);
		} catch (Exception e) {
			throw new CoreException(e);
		}
	}

	/**
	 * Creates file for data or report.
	 *
	 * @param dir directory
	 * @param prefix file name prefix
	 * @param name name of report
	 * @param suffix file name suffix
	 * @return created file
	 * @throws IOException if something bad happened
	 */
	protected File createFile(String dir, String prefix, String name, String suffix, UUID randomUUID) throws IOException {
		return Files.createFile(Paths.get(dir, generateFileName(prefix, name, randomUUID, suffix))).toFile();
	}

	private static URI createUri(String dir, String prefix, String name, String suffix, UUID randomUUID) throws URISyntaxException {
		return new URI(dir + generateFileName(prefix, name, randomUUID, suffix));
	}

	private static String generateFileName(String prefix, String name, UUID randomUUID, String suffix) {
		return MessageFormat.format("{0}_{1}_{2}.{3}", prefix, name.replaceAll("/", "-"), randomUUID, suffix);
	}

	// configuration properties setters
	public void setDataBaseDir(String dataBaseDir) {
		this.dataBaseDir = dataBaseDir;
	}

	public void setReportBaseDir(String reportBaseDir) {
		this.reportBaseDir = reportBaseDir;
	}

	public void setDataBaseUrl(String dataBaseUrl) {
		this.dataBaseUrl = dataBaseUrl;
	}

}
