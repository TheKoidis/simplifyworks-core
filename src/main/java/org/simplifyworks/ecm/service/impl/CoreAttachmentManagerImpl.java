package org.simplifyworks.ecm.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.service.impl.AbstractBaseManager;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.ecm.model.dto.CoreAttachmentDto;
import org.simplifyworks.ecm.model.dto.CoreAttachmentFilterDto;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.ecm.model.entity.CoreAttachment_;
import org.simplifyworks.ecm.service.CoreAttachmentManager;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Service
@Transactional
public class CoreAttachmentManagerImpl extends AbstractBaseManager<CoreAttachmentDto, CoreAttachment> implements CoreAttachmentManager {

	@Value("${attachment.store.dir:}")
	private String attachmentStroreDir;

	@Override
	public CoreAttachmentDto saveAttachment(MultipartFile file) throws IllegalStateException, IOException {
		UUID randomUUID = UUID.randomUUID();

		file.transferTo(new File(getAttachmentStoreDir(), randomUUID.toString()));

		return new CoreAttachmentDto(file.getOriginalFilename(), randomUUID.toString(), file.getSize(), new Date(), file.getContentType());
	}

	@Override
	public InputStream downloadAttachment(String uid) throws FileNotFoundException {
		File file = new File(getAttachmentStoreDir(), uid);
		return new FileInputStream(file);
	}

	@Override
	public ExtendedArrayList<CoreAttachmentDto> getAllWrapped(CoreAttachmentFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[0];
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreAttachment> root, CoreAttachmentFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//guid
		if (!StringUtils.isEmpty(filter.getGuid())) {
			filterParameters.add(
					criteriaBuilder.equal(root.get(CoreAttachment_.guid),
							filter.getGuid()
					)
			);
		}
	}

	private String getAttachmentStoreDir() {
		return StringUtils.isEmpty(attachmentStroreDir) ? System.getProperty("java.io.tmpdir") : attachmentStroreDir;
	}

	@Override
	public List<CoreAttachment> read(CoreAttachmentFilterDto filter) {
		return searchManager.read(filter, this);
	}
}
