package org.simplifyworks.ecm.service.impl;

import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.service.impl.AbstractBaseManager;
import org.simplifyworks.ecm.model.dto.CorePrintModuleDto;
import org.simplifyworks.ecm.model.dto.CorePrintModuleFilterDto;
import org.simplifyworks.ecm.model.entity.CorePrintModule;
import org.simplifyworks.ecm.model.entity.CorePrintModule_;
import org.simplifyworks.ecm.service.CorePrintModuleManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Default implementation of {@link CorePrintModuleManager}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
@Transactional
public class CorePrintModuleManagerImpl extends AbstractBaseManager<CorePrintModuleDto, CorePrintModule> implements CorePrintModuleManager {

	@Override
	public ExtendedArrayList<CorePrintModuleDto> getAllWrapped(CorePrintModuleFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[]{"name", "template"};
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CorePrintModule> root, CorePrintModuleFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		//objectType
		if (!StringUtils.isEmpty(filter.getObjectType())) {
			filterParameters.add(
					criteriaBuilder.equal(root.get(CorePrintModule_.objectType),
							filter.getObjectType()
					)
			);
		}
	}

	@Override
	public Long create(CorePrintModuleDto dto) {
		CorePrintModule entity = toEntity(dto);

		dataRepository.persist(entity);

		return entity.getId();
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(getEntityClass(), id);
	}

	@Override
	public CorePrintModuleDto find(Long id) {
		return toDto(dataRepository.find(getEntityClass(), id));
	}

	@Override
	public void update(Long id, CorePrintModuleDto dto) {
		dataRepository.update(toEntity(dto));
	}

	@Override
	public List<CorePrintModule> read(CorePrintModuleFilterDto filter) {
		return searchManager.read(filter, this);
	}
}
