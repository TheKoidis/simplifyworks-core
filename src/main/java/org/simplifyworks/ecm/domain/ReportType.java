/**
 *
 */
package org.simplifyworks.ecm.domain;

/**
 * Report type (type of report output)
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public enum ReportType {

	PDF("pdf", "application/pdf"),
	HTML("html", "text/html"),
	XML("xml", "text/xml");

	private final String suffix;
	private final String mimeType;

	private ReportType(String suffix, String mimeType) {
		this.suffix = suffix;
		this.mimeType = mimeType;
	}

	public String getSuffix() {
		return suffix;
	}

	public String getMimeType() {
		return mimeType;
	}
}
