package org.simplifyworks.template.web.controller.rest;

import javax.validation.Valid;

import org.simplifyworks.core.model.dto.EmptyFilterDto;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.template.model.dto.CoreTemplateDto;
import org.simplifyworks.template.service.CoreTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
@RestController
@RequestMapping(value = "/api/core/templates")
public class CoreTemplateController {

	@Autowired
	private CoreTemplateManager manager;
	
	@RequestMapping(method = RequestMethod.POST, value = "/filter")
	public ExtendedArrayList<CoreTemplateDto> getIssues(@Valid @RequestBody EmptyFilterDto filter) {
		return manager.getAllWrapped(filter);
	}

	@RequestMapping(method = RequestMethod.POST)
	public Long create(@Valid @RequestBody(required = true) CoreTemplateDto dto) {
		return manager.create(dto);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	public void delete(@PathVariable("id") Long id) {
		manager.delete(id);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{id}")
	public CoreTemplateDto find(@PathVariable("id") Long id) {
		return manager.find(id);
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/{id}")
	public void update(@PathVariable("id") Long id, @Valid @RequestBody CoreTemplateDto dto) {
		manager.update(id, dto);
	}
}