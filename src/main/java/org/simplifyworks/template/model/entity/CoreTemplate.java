package org.simplifyworks.template.model.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import org.simplifyworks.core.model.entity.AbstractEntity;

/**
 * Created by Svanda on 12.8.2015.
 */
@Entity
@Table(name = "CORE_TEMPLATE")
public class CoreTemplate extends AbstractEntity {

	@Basic(optional = false)
	@NotNull
	@Size(min = 1, max = 50)
	@Column(name = "CODE", nullable = false, length = 50)
	private String code;
	@Size(max = 255)
	@Column(name = "SUBJECT", length = 255)
	private String subject;
	@Size(max = 2000)
	@Column(name = "DESCRIPTION", length = 2000)
	private String description;
	@Basic(optional = false)
	@NotNull
	@Lob
	@Column(name = "BODY", length = -1, nullable = false)
	private String body;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
}
