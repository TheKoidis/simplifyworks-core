package org.simplifyworks.template.model.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.simplifyworks.core.model.dto.AbstractDto;

/**
 * Created by Svanda on 12.8.2015.
 */
public class CoreTemplateDto extends AbstractDto {

	@NotNull
	@Size(min = 1, max = 50)
	private String code;
	@Size(max = 255)
	private String subject;
	@Size(max = 2000)
	private String description;
	@NotNull
	private String body;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

}
