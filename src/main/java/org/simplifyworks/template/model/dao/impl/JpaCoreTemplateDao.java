package org.simplifyworks.template.model.dao.impl;

import org.simplifyworks.core.model.dao.impl.JpaAnyTypeDao;
import org.simplifyworks.template.model.entity.CoreTemplate;
import org.simplifyworks.template.model.dao.CoreTemplateDao;
import org.springframework.stereotype.Repository;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
@Repository 
public class JpaCoreTemplateDao extends JpaAnyTypeDao<CoreTemplate> implements CoreTemplateDao {

}
