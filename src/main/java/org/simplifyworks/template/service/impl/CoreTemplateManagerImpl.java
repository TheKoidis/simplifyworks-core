package org.simplifyworks.template.service.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.velocity.VelocityContext;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.EmptyFilterDto;
import org.simplifyworks.core.service.VelocityService;
import org.simplifyworks.core.service.impl.AbstractBaseManager;
import org.simplifyworks.template.domain.ProcessedTemplate;
import org.simplifyworks.template.domain.TemplateVariablesWrapper;
import org.simplifyworks.template.model.dto.CoreTemplateDto;
import org.simplifyworks.template.model.entity.CoreTemplate;
import org.simplifyworks.template.model.entity.CoreTemplate_;
import org.simplifyworks.template.service.CoreTemplateManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
@Service("coreTemplateManager")
@Transactional
public class CoreTemplateManagerImpl extends AbstractBaseManager<CoreTemplateDto, CoreTemplate> implements CoreTemplateManager {

	@Autowired
	VelocityService velocityService;

	/**
	 * Usage from workflow:
	 * coreTemplateManager.createVariables().put("var2","Variable two (defined
	 * in workflow sify_1)").put("var1","Variable one (defined in workflow
	 * sify_1)")
	 */
	@Override
	public TemplateVariablesWrapper createVariables() {
		TemplateVariablesWrapper map = new TemplateVariablesWrapper();
		return map;
	}

	/**
	 * Generate html string from template and variables (by Velocity)
	 *
	 * @param templateCode Template code from CoreTemplate
	 * @param variables Variables wrapped in TemplateVariablesWrapper (need for
	 * workflow definition)
	 * @return
	 */
	@Override
	public ProcessedTemplate getHtmlByTemplate(String templateCode, TemplateVariablesWrapper variables) {
		Assert.notNull(templateCode, "Template code must be fill!");

		CriteriaBuilder builder = dataRepository.getCriteriaBuilder();
		CriteriaQuery<CoreTemplate> criteria = builder.createQuery(CoreTemplate.class);
		Root<CoreTemplate> root = criteria.from(CoreTemplate.class);

		criteria.where(builder.equal(root.get(CoreTemplate_.code), templateCode));

		CoreTemplate template = dataRepository.createQuery(criteria).getSingleResult();

		VelocityContext context = new VelocityContext();

		if (variables != null) {
			for (String key : variables.getMap().keySet()) {
				context.put(key, variables.getMap().get(key));
			}
		}

		//call velocity with template and variables
		String body = velocityService.evaluate(context, template.getBody());
		String subject = velocityService.evaluate(context, template.getSubject());

		return new ProcessedTemplate(subject, body);
	}

	@Override
	public ExtendedArrayList<CoreTemplateDto> getAllWrapped(EmptyFilterDto filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public String[] getQuickSearchProperties() {
		return new String[] {"code", "subject", "description"};
	}

	@Override
	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<CoreTemplate> root, EmptyFilterDto filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType) {
		// do nothing
	}

	@Override
	public Long create(CoreTemplateDto dto) {
		CoreTemplate entity = toEntity(dto);

		dataRepository.persist(entity);

		return entity.getId();
	}

	@Override
	public void delete(Long id) {
		dataRepository.delete(getEntityClass(), id);
	}

	@Override
	public CoreTemplateDto find(Long id) {
		return toDto(dataRepository.find(getEntityClass(), id));
	}

	@Override
	public void update(Long id, CoreTemplateDto dto) {
		dataRepository.update(toEntity(dto));
	}

	@Override
	public List<CoreTemplate> read(EmptyFilterDto filter) {
		return searchManager.read(filter, this);
	}
}
