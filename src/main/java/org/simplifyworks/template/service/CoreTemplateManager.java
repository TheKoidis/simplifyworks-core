package org.simplifyworks.template.service;

import org.simplifyworks.core.model.dto.EmptyFilterDto;
import org.simplifyworks.core.service.SearchFilterManager;

import org.simplifyworks.template.domain.ProcessedTemplate;
import org.simplifyworks.template.domain.TemplateVariablesWrapper;
import org.simplifyworks.template.model.dto.CoreTemplateDto;
import org.simplifyworks.template.model.entity.CoreTemplate;

/**
 * @author SimplifyWorks Generator
 * @since 2015-08-12
 */
public interface CoreTemplateManager extends SearchFilterManager<CoreTemplateDto, CoreTemplate, EmptyFilterDto> {

    TemplateVariablesWrapper createVariables();

	ProcessedTemplate getHtmlByTemplate(String templateCode, TemplateVariablesWrapper variables);

	Long create(CoreTemplateDto dto);

	void delete(Long id);

	CoreTemplateDto find(Long id);

	void update(Long id, CoreTemplateDto dto);
}