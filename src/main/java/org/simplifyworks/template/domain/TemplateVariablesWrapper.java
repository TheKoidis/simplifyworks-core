package org.simplifyworks.template.domain;

import java.util.HashMap;
import java.util.Map;

/**
 * Wrapper class for variables in workflow (we need builder pattern) Created by
 * Svanda on 13.8.2015.
 */
public class TemplateVariablesWrapper {

	private final Map<String, Object> map;

	public TemplateVariablesWrapper() {
		map = new HashMap<>();
	}

	public TemplateVariablesWrapper put(String key, Object value) {
		map.put(key, value);
		return this;
	}

	public Map<String, Object> getMap() {
		return map;
	}
}
