package org.simplifyworks.repository;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import org.springframework.stereotype.Repository;

@Repository
public class DbDataRepository {

	@PersistenceContext(unitName = "default")
	protected EntityManager entityManager;

	public <T> CriteriaBuilder getCriteriaBuilder() {
		return entityManager.getCriteriaBuilder();
	}

	public <T> TypedQuery<T> createQuery(CriteriaQuery<T> criteriaQuery) {
		return entityManager.createQuery(criteriaQuery);
	}

	public <T> EntityType<T> getType(Class<T> clazz) {
		return entityManager.getMetamodel().entity(clazz);
	}

	public <T> T find(Class<T> clazz, Long id) {
		return entityManager.find(clazz, id);
	}

	public <T> T update(T entity) {
		return entityManager.merge(entity);
	}

	public void persist(Object entity) {
		entityManager.persist(entity);
	}

	public void delete(Object entity) {
		entityManager.remove(entity);
	}

	public void delete(Class<? extends Object> clazz, Long id) {
		entityManager.remove(find(clazz, id));
	}

	public <T> List<T> getAll(Class<T> clazz) {
		CriteriaBuilder criteriaBuilder = getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
		Root<T> from = criteriaQuery.from(clazz);
		criteriaQuery = criteriaQuery.select(from);
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

	public Metamodel getMetamodel() {
		return entityManager.getMetamodel();
	}

}
