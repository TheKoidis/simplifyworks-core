package org.simplifyworks.core.util;

import java.util.ArrayList;
import java.util.Collection;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.BaseManager;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public class Wrapper {

	public <T extends BaseDto, E extends BaseEntity> T wrap(E entity, BaseManager<T, E> manager) {
		return manager.toDto(entity);
	}

	public <T extends BaseDto, E extends BaseEntity> ExtendedArrayList<T> wrap(Collection<E> entities, Long recordsTotal, Long recordsFiltered, BaseManager<T, E> manager) {
		Collection<T> resources = new ArrayList<>();
		entities.stream().forEach((entity) -> {
			resources.add(manager.toDto(entity));
		});
		return new ExtendedArrayList<>(recordsTotal, resources);
	}

}
