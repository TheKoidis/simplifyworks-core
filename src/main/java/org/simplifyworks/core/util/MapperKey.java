package org.simplifyworks.core.util;

import java.util.Objects;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class MapperKey {

	Object srcValue;
	Class<?> dstPropertyType;

	public MapperKey(Object srcValue, Class<?> dstPropertyType) {
		this.srcValue = srcValue;
		this.dstPropertyType = dstPropertyType;
	}

	public Object getSrcValue() {
		return srcValue;
	}

	public Class<?> getDstPropertyType() {
		return dstPropertyType;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 13 * hash + Objects.hashCode(this.srcValue);
		hash = 13 * hash + Objects.hashCode(this.dstPropertyType);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final MapperKey other = (MapperKey) obj;
		if (!Objects.equals(this.srcValue, other.srcValue)) {
			return false;
		}
		if (!Objects.equals(this.dstPropertyType, other.dstPropertyType)) {
			return false;
		}
		return true;
	}

}
