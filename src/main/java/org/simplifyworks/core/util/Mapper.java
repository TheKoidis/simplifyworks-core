package org.simplifyworks.core.util;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javassist.Modifier;
import org.apache.commons.beanutils.PropertyUtils;
import org.simplifyworks.core.exception.CoreException;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class Mapper {

	/**
	 * Maps bean to another bean
	 */
	public <T> T map(Object src, Class<T> dstClass) {
		try {
			return map(src, dstClass.newInstance());
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new CoreException("Cannot create instance of " + dstClass.getName(), ex);
		}
	}

	/**
	 * Maps bean to another bean
	 */
	public <T> T map(Object src, T dst) {
		return map(src, dst, new HashMap<>());
	}

	/**
	 * Maps bean to another bean
	 *
	 * @param src source object
	 * @param dst destination object
	 * @param created map of new created instances of destination subobjects (to
	 * use them in case of cycle in source object graph)
	 * @return destination object
	 */
	private <T> T map(Object src, T dst, Map<MapperKey, Object> created) {
		if (src == null) {
			return null;
		}

		if (dst == null) {
			throw new CoreException("Destination object cannot be null");
		}

		created.put(new MapperKey(src, dst.getClass()), dst);

		PropertyDescriptor[] srcDescriptors = PropertyUtils.getPropertyDescriptors(src.getClass());
		PropertyDescriptor[] dstDescriptors = PropertyUtils.getPropertyDescriptors(dst.getClass());

		for (PropertyDescriptor srcDescriptor : srcDescriptors) {
			PropertyDescriptor dstDescriptor = getDstDescriptor(dstDescriptors, srcDescriptor);

			try {
				//skip property that does not exist in destination class
				if (dstDescriptor == null || dstDescriptor.getWriteMethod() == null) {
					continue;
				}

				Object srcValue = srcDescriptor.getReadMethod().invoke(src);

				//primitive types or null - simple copies value
				if (srcDescriptor.getPropertyType().isPrimitive() && srcDescriptor.getPropertyType() == dstDescriptor.getPropertyType() || srcValue == null) {
					dstDescriptor.getWriteMethod().invoke(dst, srcValue);
					continue;
				}

				//collections
				if (Collection.class.isAssignableFrom(srcDescriptor.getPropertyType())) {
					mapCollection(srcValue, dstDescriptor, dst, created);
					continue;
				}

				//map
				if (Map.class.isAssignableFrom(dstDescriptor.getPropertyType())) {
					mapMap(srcValue, dstDescriptor, dst, created);
					continue;
				}

				//arrays
				if (srcDescriptor.getPropertyType().isArray()) {
					mapArray(dstDescriptor, srcValue, dst, created);
					continue;
				}

				//other types
				dstDescriptor.getWriteMethod().invoke(dst, getDstValue(srcValue, srcDescriptor.getPropertyType(), dstDescriptor.getPropertyType(), created));
			} catch (IllegalAccessException | InvocationTargetException | InstantiationException | IllegalArgumentException ex) {
				throw new CoreException("Cannot map property " + srcDescriptor.getName() + " of " + src.getClass() + " (instance=" + src + ")", ex);
			}
		}

		return dst;
	}

	private void mapArray(PropertyDescriptor dstDescriptor, Object srcArray, Object dst, Map<MapperKey, Object> created) throws InstantiationException, NegativeArraySizeException, InvocationTargetException, IllegalArgumentException, ArrayIndexOutOfBoundsException, IllegalAccessException {
		Class<?> componentType = dstDescriptor.getPropertyType().getComponentType();
		int length = Array.getLength(srcArray);

		boolean primitive = componentType.isPrimitive();
		Object dstArray = Array.newInstance(componentType, length);
		for (int i = 0; i < length; i++) {
			Object srcElement = Array.get(srcArray, i);

			Array.set(dstArray, i, primitive ? Array.get(srcArray, i)
					: srcElement != null ? getDstValue(srcElement, srcElement.getClass(), componentType, created) : null);
		}
		dstDescriptor.getWriteMethod().invoke(dst, dstArray);
	}

	private void mapMap(Object srcValue, PropertyDescriptor dstDescriptor, Object dst, Map<MapperKey, Object> created) throws IllegalAccessException, IllegalArgumentException, CoreException, InstantiationException, InvocationTargetException {
		Map dstValue = (Map) dstDescriptor.getReadMethod().invoke(dst);
		ParameterizedType genericSuperclass = (ParameterizedType) dstDescriptor.getReadMethod().getGenericReturnType();
		if (genericSuperclass == null || genericSuperclass.getActualTypeArguments().length == 0) {
			throw new CoreException("Cannot get generics type of collection for property " + dstDescriptor.getName() + " of class " + dst.getClass());
		}
		Class destClassKey = (Class) genericSuperclass.getActualTypeArguments()[0];
		Class destClassValue = (Class) genericSuperclass.getActualTypeArguments()[1];

		if (dstValue != null) {
			dstValue.clear();
		} else {
			dstValue = new HashMap();
		}

		Set<Entry> entrySet = ((Map) srcValue).entrySet();
		for (Entry entry : entrySet) {
			dstValue.put(
					entry.getKey() != null ? getDstValue(entry.getKey(), entry.getKey().getClass(), destClassKey, created) : null,
					entry.getValue() != null ? getDstValue(entry.getValue(), entry.getValue().getClass(), destClassValue, created) : null
			);
		}
		dstDescriptor.getWriteMethod().invoke(dst, dstValue);
	}

	private void mapCollection(Object srcValue, PropertyDescriptor dstDescriptor, Object dst, Map<MapperKey, Object> created) throws IllegalAccessException, IllegalArgumentException, CoreException, InstantiationException, InvocationTargetException {
		Collection dstValue = (Collection) dstDescriptor.getReadMethod().invoke(dst);
		ParameterizedType genericSuperclass = (ParameterizedType) dstDescriptor.getReadMethod().getGenericReturnType();
		if (genericSuperclass == null || genericSuperclass.getActualTypeArguments().length == 0) {
			throw new CoreException("Cannot get generics type of collection for property " + dstDescriptor.getName() + " of class " + dst.getClass());
		}
		Class destClass = (Class) genericSuperclass.getActualTypeArguments()[0];

		if (dstValue != null) {
			dstValue.clear();
		} else if (Set.class.isAssignableFrom(dstDescriptor.getPropertyType())) {
			dstValue = new HashSet();
		} else {
			dstValue = new ArrayList();
		}

		for (Object srcElement : (Collection) srcValue) {
			dstValue.add(srcElement != null ? getDstValue(srcElement, srcElement.getClass(), destClass, created) : null);
		}
		dstDescriptor.getWriteMethod().invoke(dst, dstValue);
	}

	private PropertyDescriptor getDstDescriptor(PropertyDescriptor[] dstDescriptors, PropertyDescriptor srcDescriptor) {
		PropertyDescriptor dstDescriptor = null;
		for (PropertyDescriptor descriptor : dstDescriptors) {
			if (descriptor.getName().equals(srcDescriptor.getName())) {
				dstDescriptor = descriptor;
			}
		}
		return dstDescriptor;
	}

	private Object getDstValue(Object srcValue, Class<?> srcPropertyType, Class<?> dstPropertyType, Map<MapperKey, Object> created) throws InvocationTargetException, InstantiationException, IllegalAccessException, IllegalArgumentException {
		//immutable types
		if (BigInteger.class == srcPropertyType
				|| BigDecimal.class == srcPropertyType
				|| Integer.class == srcPropertyType
				|| Long.class == srcPropertyType
				|| Short.class == srcPropertyType
				|| Double.class == srcPropertyType
				|| Float.class == srcPropertyType
				|| Boolean.class == srcPropertyType
				|| String.class == srcPropertyType
				|| srcPropertyType.isEnum()) {
			return srcValue;
		}

		//date
		if (Date.class == srcPropertyType) {
			return srcValue != null ? new Date(((Date) srcValue).getTime()) : null;
		}

		//complex types
		MapperKey mapperKey = new MapperKey(srcValue, dstPropertyType);
		for (Entry<MapperKey, Object> c : created.entrySet()) {
			if (c.getKey().equals(mapperKey)) {
				return c.getValue();
			}
		}

		Object dstValue;
		if (Modifier.isAbstract(dstPropertyType.getModifiers())) {
			if (dstPropertyType.isAssignableFrom(srcValue.getClass())) {
				dstValue = srcValue.getClass().newInstance();
			} else {
				throw new CoreException("Cannot instantiate abstract class " + dstPropertyType.getName());
			}
		} else {
			dstValue = dstPropertyType.newInstance();
		}
		created.put(mapperKey, dstValue);
		return map(srcValue, dstValue, created);
	}
}
