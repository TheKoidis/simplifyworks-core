package org.simplifyworks.core.model.dto;

import org.simplifyworks.security.model.domain.AccessType;

/**
 * Ancestor for DTOs which are protected by security, i.e. contain security access information.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class AbstractProtectedDto extends AbstractDto {

	private static final long serialVersionUID = 1968324258710379888L;
	
	private AccessType accessType;
	
	public AccessType getAccessType() {
		return accessType;
	}
	
	public void setAccessType(AccessType accessType) {
		this.accessType = accessType;
	}
}
