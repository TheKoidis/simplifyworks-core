package org.simplifyworks.core.model.dto;

import java.util.Date;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class Lock {

	private Date date;
	private String user;

	public Lock(Date date, String user) {
		this.date = date;
		this.user = user;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
