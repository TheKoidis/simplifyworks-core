package org.simplifyworks.core.model.dto;

/**
 * Dto for ordering
 *
 * @author kakrda
 */
public class SortDto {
    
    private String property;
    private String order;

    public String getProperty() {
        return property;
    }

    public void setProperty(String property) {
        this.property = property;
    }

    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }       

}