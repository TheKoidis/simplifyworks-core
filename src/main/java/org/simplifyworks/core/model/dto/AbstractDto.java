package org.simplifyworks.core.model.dto;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.Size;
import org.simplifyworks.security.model.domain.AccessType;

/**
 * Base dto mapping
 *
 * TODO: Serializable interface
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public abstract class AbstractDto implements Serializable, BaseDto {

	private static final long serialVersionUID = -5187898779275725184L;

	private Long id;
	private Date created;
	private Date modified;
	@Size(max = 50)
	private String creator;
	@Size(max = 50)
	private String modifier;
        private AccessType accessType;

	public AbstractDto() {
	}

	public AbstractDto(Long id) {
		this.id = id;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	public Date getModified() {
		return modified;
	}

	public void setModified(Date modified) {
		this.modified = modified;
	}

	@Override
	public String toString() {
		return getClass().getCanonicalName() + "[ id=" + getId() + " ]";
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (getId() != null ? getId().hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		if (object == null || !object.getClass().equals(getClass())) {
			return false;
		}

		AbstractDto other = (AbstractDto) object;
		return !((this.getId() == null && other.getId() != null)
				|| (this.getId() != null && !this.getId().equals(other.getId()))
				|| (this.getId() == null && other.getId() == null && this != other));
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

        public AccessType getAccessType() {
            return accessType;
        }

        public void setAccessType(AccessType accessType) {
            this.accessType = accessType;
        }
        
}
