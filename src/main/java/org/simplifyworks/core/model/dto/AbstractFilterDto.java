package org.simplifyworks.core.model.dto;

import java.util.List;

/**
 * Base filter dto
 *
 * @author kakrda
 */
public abstract class AbstractFilterDto implements BaseFilterDto {

	private List<SortDto> sorts;
	private Integer pageNumber = 1;
	private Integer pageSize = Integer.MAX_VALUE;
	private String[] quickSearchProperties;
	private List<String> quickSearchTokens;

	@Override
	public List<SortDto> getSorts() {
		return sorts;
	}

	@Override
	public void setSorts(List<SortDto> sorts) {
		this.sorts = sorts;
	}

	@Override
	public Integer getPageNumber() {
		return pageNumber;
	}

	@Override
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}

	@Override
	public Integer getPageSize() {
		return pageSize;
	}

	@Override
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}

	@Override
	public String[] getQuickSearchProperties() {
		return quickSearchProperties;
	}

	@Override
	public void setQuickSearchProperties(String[] quickSearchProperties) {
		this.quickSearchProperties = quickSearchProperties;
	}

	@Override
	public List<String> getQuickSearchTokens() {
		return quickSearchTokens;
	}

	@Override
	public void setQuickSearchTokens(List<String> quickSearchTokens) {
		this.quickSearchTokens = quickSearchTokens;
	}

}
