/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.dto;

import java.util.List;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public interface BaseFilterDto {

	Integer getPageNumber();

	Integer getPageSize();

	String[] getQuickSearchProperties();

	List<String> getQuickSearchTokens();

	List<SortDto> getSorts();

	void setPageNumber(Integer pageNumber);

	void setPageSize(Integer pageSize);

	void setQuickSearchProperties(String[] quickSearchProperties);

	void setQuickSearchTokens(List<String> quickSearchTokens);

	void setSorts(List<SortDto> sorts);

}
