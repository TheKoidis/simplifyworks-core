package org.simplifyworks.core.model.dto;

/**
 *
 * @author hanak
 */
public interface BaseDto {

	/**
	 * Returns indentifier
	 *
	 * @return
	 */
	Long getId();

	/**
	 * Set indentifier
	 *
	 * @param id
	 */
	void setId(Long id);
}
