/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.dao.impl;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.Bindable;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.Metamodel;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.hibernate.annotations.Formula;
import org.simplifyworks.core.model.dao.AnyTypeDao;
import org.simplifyworks.core.model.domain.FieldInfo;
import org.simplifyworks.core.model.domain.FilterGroup;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.SortOrder;
import org.simplifyworks.core.model.domain.SortValue;
import org.simplifyworks.core.model.domain.filteroperator.IsNullFilterOperator;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.model.entity.IConceptEntity;
import org.simplifyworks.core.model.entity.IValidableEntity;
import org.simplifyworks.core.util.EntityUtils;
import org.simplifyworks.core.util.JpaUtils;
import org.simplifyworks.security.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.google.common.collect.Lists;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 * @param <T>
 */
@Deprecated
public class JpaAnyTypeDao<T extends AbstractEntity> implements AnyTypeDao<T> {

	private static final Logger LOG = LoggerFactory.getLogger(JpaAnyTypeDao.class);

	protected Class<T> clazz;
	
	@PersistenceContext(unitName = "default")	
	protected EntityManager entityManager;
	
	@Autowired
	private SecurityService securityService;
	
	//
	private SingularAttribute<? super T, ?> idAttribute; // cache - id atribute
	private Boolean hasFormulaAnnotation; // cache - ma-li entita anotaci forlmule

	public JpaAnyTypeDao() {
		//gets declared type
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		clazz = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
	}

	public JpaAnyTypeDao(Class<T> clazz) {
		this.clazz = clazz;
	}

	@Override
	public Class<T> getEntityClass() {
		return clazz;
	}

	@Override
	public List<T> findAll() {
		CriteriaBuilder criteriaBuilder = this.getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
		Root<T> root = criteriaQuery.from(clazz);
		criteriaQuery.select(root);
		return getEntityManager().createQuery(criteriaQuery).getResultList();
	}

	@Override
	public T findById(Serializable id) {
		return this.getEntityManager().find(this.clazz, id);
	}

	public void save(T entity) {
		this.getEntityManager().persist(entity);
	}

	@Override
	public T saveOrUpdate(T entity) {
		try {
			//zjistime primanrni klic - nelze pres standardni descriptor, protoye prekrzvame datove typy a defaultne se hleda serializable ...
			PropertyDescriptor propertyDescriptor = EntityUtils.getPropertyDescriptor(clazz, getIdAttribute());
			Object pk = propertyDescriptor.getReadMethod().invoke(entity);
			//overime zda je jiz v DB
			try {
				T findEntity = null;
				if (pk != null) {
					findEntity = getEntityManager().find(clazz, pk);
				}
				if (findEntity == null) {
					throw new EntityNotFoundException();
				}
			} catch (EntityNotFoundException e) {
				//neni v DB -> pak insertneme
				try {
					getEntityManager().persist(entity);
					getEntityManager().flush();
					return entity;
				} catch (Throwable ex) {
					// musime zajistit vycisteni idcka
					propertyDescriptor.getWriteMethod().invoke(entity, (Object) null);
					// toho: problem je s podobjekty, ktery jsou taky detachovany
					throw ex;
				}
			}
			entity = getEntityManager().merge(entity);
			getEntityManager().flush(); // tohle je dosti potreba, napr v PRZ u zmeny planu
			return entity;
			//problem se zjistenim PK
		} catch (Throwable ex) {
			LOG.error("Chyba pri ukladani entity [" + entity + "]", ex);
			if (ex instanceof RuntimeException) {
				throw (RuntimeException) ex;
			}
			throw new RuntimeException(ex);
		}
	}

	@Override
	public T batch(T entity) {
		entity = saveOrUpdate(entity);
		getEntityManager().flush();
		getEntityManager().clear();
		return entity;
	}

	@Override
	public T update(T entity) {
		return getEntityManager().merge(entity);
	}

	@Override
	public void remove(Serializable id) {
		if (id == null) {
			return;
		}
		T entity = this.getEntityManager().find(this.clazz, id);
		if (entity != null) {
			this.getEntityManager().remove(getEntityManager().merge(entity));
			getEntityManager().flush();
			getEntityManager().clear();
		}
	}

	@Override
	public List<T> search(SearchParameters searchParameters) {
		CriteriaQuery<T> criteriaQuery = createSelectCriteriaQuery(searchParameters);
		TypedQuery<T> query = getEntityManager().createQuery(criteriaQuery);
		// TODO: Tenhle ifik by patril spis do metody 
		if (searchParameters.getRange() != null
				&& searchParameters.getRange().getFirstRow() >= 0
				&& searchParameters.getRange().getRows() > 0
				&& !getHasFormulaAnnotation()) {
			query.setFirstResult(searchParameters.getRange().getFirstRow());
			query.setMaxResults(searchParameters.getRange().getRows());
		}
		return query.getResultList();
	}

	/**
	 * Pocet zaznamu dle filtru
	 *
	 * @param searchParameters
	 * @return
	 */
	@Override
	public long count(SearchParameters searchParameters) {
		CriteriaQuery<Long> criteriaQuery = createCountCriteriaQuery(searchParameters);
		return getEntityManager().createQuery(criteriaQuery).getSingleResult();
	}

	/**
	 * Vrati PK od aktualni classy entity
	 *
	 * @return
	 */
	@Override
	public SingularAttribute<? super T, ?> getIdAttribute() {
		if (idAttribute == null) {
			Metamodel metamodel = getEntityManager().getMetamodel();
			EntityType<T> ent = metamodel.entity(clazz);
			idAttribute = ent.getId(ent.getIdType().getJavaType());
		}
		return idAttribute;
	}

	protected CriteriaQuery<Long> createCountCriteriaQuery(SearchParameters searchParameters) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<T> root = criteriaQuery.from(clazz);

		Expression<Boolean> filterCriteria = createFilterCriteria(searchParameters, criteriaBuilder, criteriaQuery, root);
		if (filterCriteria != null) {
			criteriaQuery.where(filterCriteria);
		}
		if (criteriaQuery.isDistinct() || searchParameters.isDistinct()) {
			criteriaQuery.select(criteriaBuilder.countDistinct(root));
		} else {
			criteriaQuery.select(criteriaBuilder.count(root));
		}

		return criteriaQuery;
	}

	protected CriteriaQuery<T> createSelectCriteriaQuery(SearchParameters searchParameters) {
		CriteriaBuilder criteriaBuilder = getEntityManager().getCriteriaBuilder();
		CriteriaQuery<T> criteriaQuery = criteriaBuilder.createQuery(clazz);
		Root<T> root = criteriaQuery.from(clazz);
		//
		List<Order> orders = createOrders(searchParameters, criteriaBuilder, root, true);
		//
		if (searchParameters.getRange() != null
				&& searchParameters.getRange().getFirstRow() >= 0
				&& searchParameters.getRange().getRows() > 0
				&& getHasFormulaAnnotation()) {
			//kvůli pomalému order by v případě použítí @Formula provedeme nejprve subselect, který vrací seznam id (seřazených dle order kritérií) s nastavenm maxResults
			//pro stránkování a pak teprve dotáhneme seznam "plných" objektů pro tyto id

			CriteriaQuery subCriteriaQuery = criteriaBuilder.createQuery();
			Root<T> idListRoot = subCriteriaQuery.from(clazz);

			Expression<Boolean> filterCriteria = createFilterCriteria(searchParameters, criteriaBuilder, subCriteriaQuery, idListRoot);
			if (filterCriteria != null) {
				subCriteriaQuery.where(filterCriteria);
			}

			List<Order> subOrders = createOrders(searchParameters, criteriaBuilder, idListRoot, false);
			if (!subOrders.isEmpty()) {
				subCriteriaQuery.orderBy(subOrders);
			}

			subCriteriaQuery.multiselect(getNeededColumns(idListRoot, subOrders));

			if (!subCriteriaQuery.isDistinct()) {
				subCriteriaQuery.distinct(searchParameters.isDistinct());
			}

			TypedQuery subQuery = getEntityManager().createQuery(subCriteriaQuery);

			if (searchParameters.getRange() != null && searchParameters.getRange().getFirstRow() >= 0 && searchParameters.getRange().getRows() > 0) {
				subQuery.setFirstResult(searchParameters.getRange().getFirstRow());
				subQuery.setMaxResults(searchParameters.getRange().getRows());
			}

			//získám id záznamů, které mám vrátit
			List rows = subQuery.getResultList();

			//pokud není žádné řazení, pak dostanu přímo list id, jinak list Object[], kde id je první záznam
			List ids = new ArrayList();
			for (Object row : rows) {
				if (row.getClass().isArray()) {
					ids.add(((Object[]) row)[0]);
				} else {
					ids.add(row);
				}
			}

			if (ids.isEmpty()) {
				//do "in" nelze poslat prázdný list, proto dávám nesmyslnou podmínku abych dostal prázdný výsledek
				criteriaQuery.where(idListRoot.get(getIdAttribute().getName()).isNull());
			} else {
				//pokud je použito stránkování, tak mám jistotu že idček nebude víc než 1000 (velikost stránky musí být menší než 1000, což bude)
				criteriaQuery.where(idListRoot.get(getIdAttribute().getName()).in(ids));
			}
		} else if (searchParameters.isDistinct() && !orders.isEmpty()) {
			//kvůli problému s distinct v kombinaci s orderBy dělám subquery a exists (problém pokud řadím přes sloupec, který není součástí selectovaného objektu)
			//nutno nějak upravit, sice generuje dotaz, který je validní, ale někdy extrémně pomalý
			CriteriaQuery<T> subCriteriaQuery = criteriaBuilder.createQuery(clazz);
			Subquery<T> subquery = subCriteriaQuery.subquery(clazz);
			Root<T> subRoot = subquery.from(clazz);
			subquery.select(subRoot);

			Expression<Boolean> filterCriteria = createFilterCriteria(searchParameters, criteriaBuilder, subCriteriaQuery, subRoot);
			Predicate subqueryCondition = criteriaBuilder.equal(subRoot.get(getIdAttribute().getName()), root.get(getIdAttribute().getName()));

			if (filterCriteria != null) {
				subquery.where(criteriaBuilder.and(
						filterCriteria, subqueryCondition));
			} else {
				subquery.where(subqueryCondition);
			}

			subCriteriaQuery.distinct(true);

			criteriaQuery.where(criteriaBuilder.exists(subquery));

			criteriaQuery.distinct(false);
		} else {
			// pokud nestrankujeme, ani neni distinct zároveň s order by, tak nemusime optimalizovat			
			Expression<Boolean> filterCriteria = createFilterCriteria(searchParameters, criteriaBuilder, criteriaQuery, root);
			if (filterCriteria != null) {
				criteriaQuery.where(filterCriteria);
			}

			if (!criteriaQuery.isDistinct()) {
				criteriaQuery.distinct(searchParameters.isDistinct());
			}
		}

		if (!orders.isEmpty()) {
			criteriaQuery.orderBy(orders);
		}
		
		if (StringUtils.isNotBlank(searchParameters.getFulltext())) {
			LOG.warn("Full text is not implemented for database search - use indexed manager instead.");
		}

		return criteriaQuery;
	}

	/**
	 * vytvori sortovaci kriteria
	 *
	 * @todo: asi by se dalo si to schovat do mapy (asi ne, je potreba generovat
	 * joiny)
	 *
	 * @param searchParameters
	 * @param criteriaBuilder
	 * @param root
	 * @param applyCustomFunction
	 * @return
	 */
	protected List<Order> createOrders(SearchParameters searchParameters, CriteriaBuilder criteriaBuilder, Root<T> root, boolean applyCustomFunction) {
		List<Order> orders = new ArrayList<>();
		if (searchParameters.getSorts().isEmpty()) {
			return orders;
		}
		for (SortValue sortValue : searchParameters.getSorts().values()) {
			if (sortValue.getSortOrder() == null) {
				continue;
			}
			Expression expression = createPropertyOrderPath(criteriaBuilder, root, sortValue.getPropertyName(), searchParameters.getLocale(), applyCustomFunction); // overeni supportovaneho sortu
			if (expression != null) {
				// case insensitive
				if (sortValue.getSortOrder() == SortOrder.ASC) {
					orders.add(criteriaBuilder.asc(expression));
				} else if (sortValue.getSortOrder() == SortOrder.DESC) {
					orders.add(criteriaBuilder.desc(expression));
				}
			}
		}
		// pridame 'nejemnejsi' trideni kvuli problemu se strankovanim - kdyz nemame zadne order, mohou se vracet nahodne vysledky
		Path<?> idPath = root.get(getIdAttribute());
		Order idOrder = criteriaBuilder.asc(idPath);
		orders.add(idOrder);
		//
		return orders;
	}

	protected List<Order> createOrders(SearchParameters searchParameters, CriteriaBuilder criteriaBuilder, Root<T> root) {
		return createOrders(searchParameters, criteriaBuilder, root, false);
	}

	/**
	 * path dle teckove notace + ev. join na root - normalne funguje, ale udela
	 * inner join :-( - rozsireno na left outer join, jinak se vyselektuji jen
	 * nektere zaznamy
	 *
	 * @param criteriaBuilder
	 * @param root
	 * @param propertyName
	 * @param locale
	 * @return
	 */
	public Expression createPropertyOrderPath(CriteriaBuilder criteriaBuilder, Root<T> root, String propertyName, Locale locale) {
		return createPropertyOrderPath(criteriaBuilder, root, propertyName, locale, false);
	}

	public Expression createPropertyOrderPath(CriteriaBuilder criteriaBuilder, Root<T> root, String propertyName, Locale locale, boolean applyCustomFunction) {
		Path expression = root;
		String[] props = propertyName.split("\\.");
		for (String prop : props) {
			Path expPath = expression.get(prop);
			//*** tanecky kolem left outer join ***//
			Bindable model = expPath.getModel();
			if (model instanceof SingularAttribute) {
				SingularAttribute saModel = (SingularAttribute) model;
				Attribute.PersistentAttributeType persistentAttributeType = saModel.getPersistentAttributeType();
				Type type = saModel.getType();
				//pokud to je entita a vazba many-to-one nebo one-to-one, pak udelame left outer join
				if (Type.PersistenceType.ENTITY.equals(type.getPersistenceType())
						&& (Attribute.PersistentAttributeType.MANY_TO_ONE.equals(persistentAttributeType) || Attribute.PersistentAttributeType.ONE_TO_ONE.equals(persistentAttributeType))) {
					if (LOG.isDebugEnabled()) {
						LOG.debug("pro property " + prop + " delam join " + ((EntityType) saModel.getType()).getName());
					}
					expression = ((From) expression).join(saModel, JoinType.LEFT);//pokud to je entita, tak nad ni musi byt taky entita, proto join (cast na From)
				} else { //normalni atribut a nebo neco co zatim jeste nevim
					expression = expPath;
				}
			} else { //zrejme nejaka Colection, takze nic
				return null;
			}
		}
		return expression;
	}

	protected Expression<Boolean> createFilterCriteriaForField(FilterValue filter, Root<T> anyPath, CriteriaBuilder criteriaBuilder, Locale locale) {
		if (filter == null || filter.isEmpty()) {
			return null;
		}
		Path<String> expression;
		if (!(filter.getOperator() instanceof IsNullFilterOperator) && filter.getFilterGroup().getGroupOperator().equals(FilterGroup.GroupOperatorType.AND)) {
			// path dle teckove notace, automaticky udela inner join, resp. propojeni ve where (pokud je potreba), coz je OK (na rozdil od sortu)
			expression = (Path<String>) createExpression(filter.getPropertyName(), anyPath);
		} else {
			// pokud ale hledame pomoci operatoru is null, tak protrebujeme spojit pomoci left joinu - 
			expression = (Path<String>) createPropertyOrderPath(criteriaBuilder, anyPath, filter.getPropertyName(), locale);
		}
		if (expression == null) { // nepodporovana property
			return null;
		}
		return createFilterOperatorCriteria(expression, filter, criteriaBuilder, locale);
	}

	protected Expression<Boolean> createFilterCriteria(SearchParameters searchParameters, CriteriaBuilder criteriaBuilder, CriteriaQuery criteriaQuery, Root<T> root) {
        Expression<Boolean> groupCriteria = null;

        if (IConceptEntity.class.isAssignableFrom(clazz)) {
            Predicate sameUser = criteriaBuilder.equal(root.get(IConceptEntity.CONCEPT_AUTHOR_FIELD_NAME), securityService.getUsername());
            Predicate notConcept = criteriaBuilder.equal(root.get(IConceptEntity.CONCEPT_FIELD_NAME), false);
            Predicate conceptPredicate = criteriaBuilder.or(sameUser, notConcept);
            if (groupCriteria == null) {
                groupCriteria = conceptPredicate.as(Boolean.class);
            } else {
                groupCriteria = criteriaBuilder.and(groupCriteria, conceptPredicate.as(Boolean.class));
            }
        }

		if (searchParameters.isEmptyFilters()) { // pri nenastavenem filtru ani sortu neni potreba dale zpracovavat
			return groupCriteria;
		}

		for (FilterGroup filterGroup : searchParameters.getFilterGroups().values()) {
			if (filterGroup.isEmpty()) {
				continue;
			}
			Expression<Boolean> filterCriteria = null;
			for (FilterValue filterValue : filterGroup.getFilters().values()) {
				// platnost je univerzal
				if (IValidableEntity.class.isAssignableFrom(clazz) && FILTER_VALID.equals(filterValue.getPropertyName())) {
					if (filterValue.isEmpty()) {
						continue;
					}
					Boolean valid = (Boolean) filterValue.getFirstValue();
					Path<?> validFrom = root.get("validFrom"); // TODO: kde vzit konstanty?
					Path<?> validTill = root.get("validTill");
					Predicate predicate = criteriaBuilder.and(
							criteriaBuilder.or(
									criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.function(JpaUtils.resolveDateFunction(getEntityManager()), Date.class, validFrom), DateUtils.truncate(new Date(), Calendar.DATE)),
									criteriaBuilder.isNull(validFrom)),
							criteriaBuilder.or(
									criteriaBuilder.greaterThanOrEqualTo(criteriaBuilder.function(JpaUtils.resolveDateFunction(getEntityManager()), Date.class, validTill), DateUtils.truncate(new Date(), Calendar.DATE)),
									criteriaBuilder.isNull(validTill)));
					if (!valid) {
						predicate = criteriaBuilder.not(predicate);
					}
					if (filterCriteria == null) {
						filterCriteria = predicate.as(Boolean.class);
					} else {
						// TODO: proc ten as? Bez nej to ale moc nefunguje ...
						filterCriteria = criteriaBuilder.and(filterCriteria, predicate.as(Boolean.class));
					}
					continue;
				}

//TODO				
//				// ekj je univerzal
//				if (IEkjEntity.class.isAssignableFrom(clazz) && FILTER_EKJ.equals(filterValue.getPropertyName())) {
//					if (filterValue.isEmpty()) {
//						continue;
//					}
//
//					Predicate ekjSame = criteriaBuilder.equal(root.get(IEkjEntity.EKJ_FIELD_NAME), filterValue.getFirstValue());
//					Predicate ekjNull = root.get(IEkjEntity.EKJ_FIELD_NAME).isNull();
//					Predicate ekjPredicate = criteriaBuilder.or(ekjSame, ekjNull);
//					if (filterCriteria == null) {
//						filterCriteria = ekjPredicate.as(Boolean.class);
//					} else {
//						filterCriteria = criteriaBuilder.and(filterCriteria, ekjPredicate.as(Boolean.class));
//					}
//				}
//				// kosik je universal
//				if (FILTER_CART.equals(filterValue.getPropertyName())) {
//					Collection cart = (Collection) filterValue.getFirstValue();
//					Predicate predicate;
//					if (cart == null || cart.isEmpty()) {
//						predicate = criteriaBuilder.disjunction();
//					} else {
//						predicate = criteriaBuilder.and(root.get(getIdAttribute().getName()).in(cart));
//					}
//					if (filterCriteria == null) {
//						filterCriteria = predicate.as(Boolean.class);
//					} else {
//						// TODO: proc ten as? Bez nej to ale moc nefunguje ...
//						filterCriteria = criteriaBuilder.and(filterCriteria, predicate.as(Boolean.class));
//					}
//					continue;
//				}
				if (filterValue.isManual()) { // rucne pridane filtrovaci kriteria nezpracovavame
					continue;
				}
				Expression<Boolean> predicate = createFilterCriteriaForField(filterValue, root, criteriaBuilder, searchParameters.getLocale());
				if (predicate == null) {
					continue;
				}
				if (filterCriteria == null) {
					filterCriteria = predicate.as(Boolean.class);
				} else {
					if (filterGroup.getGroupOperator().equals(FilterGroup.GroupOperatorType.AND)) { // TODO: Pro zpetnou kompatibilitu - defaultni skupina neumi or => pridat operator do skupiny
						// TODO: proc ten as?
						filterCriteria = criteriaBuilder.and(filterCriteria, predicate.as(Boolean.class));
					} else {
						// TODO: proc ten as?
						filterCriteria = criteriaBuilder.or(filterCriteria, predicate.as(Boolean.class));
					}
				}
			}
			if (filterCriteria != null) {
				if (groupCriteria == null) {
					groupCriteria = filterCriteria;
				} else {
					// TODO: skupina ve skupine - potom uz bude and i or bez omezeni 
					groupCriteria = criteriaBuilder.and(groupCriteria, filterCriteria);
				}
			}
		}

		return groupCriteria;
	}

	/**
	 * Vytvori expression pro konkretni property (propertyName) od konkretniho
	 * predka (anyPath)
	 *
	 * @param propertyName
	 * @param anyPath
	 * @return
	 */
	protected Path<?> createExpression(String propertyName, Path anyPath) {
		Path<?> expression = anyPath;
		String[] props = propertyName.split("\\.");
		for (String prop : props) {
			expression = expression.get(prop);
		}
		return expression;
	}

	/**
	 *
	 * @param expression
	 * @param filter
	 * @param criteriaBuilder
	 * @return
	 */
	private Expression<Boolean> createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale) {
		return filter.getOperator().createFilterOperatorCriteria(expression, filter, criteriaBuilder, locale, getEntityManager());
	}

	/**
	 * vrati primitivni persistovane atributy entitni tridy + primitivni
	 * persistovane atributy navazanych entit (vztah many-to-one nebo
	 * one-to-one) a subentit dalsich levelech.
	 *
	 * Hodi se predevsim pro validaci rucne pridanych entit.
	 *
	 * TODO: mohlo by byt v utils
	 *
	 * @return
	 */
	@Override
	public List<FieldInfo> getSingularAttributes(int level) {
		List<FieldInfo> attrs = new ArrayList<FieldInfo>();
		Metamodel metamodel = getEntityManager().getMetamodel();
		EntityType ent = metamodel.entity(clazz);
		//
		appendSingularAttributes(clazz, attrs, ent.getSingularAttributes(), null, null, level);
		//
		return attrs;
	}

	/**
	 * Zpracovani predanych singularnich atributu (singularAttributes) do
	 * globalnich atributu (attrs)
	 *
	 * @param entityClass
	 * @param resultAttrs
	 * @param singularAttributes
	 * @param parentAttributePath - pokud jsou atributy ze subentite,
	 * potrebujeme cestu k subentite id hlavni entity
	 * @param parentAttribute - rodicovsky field subentity
	 */
	private static void appendSingularAttributes(Class entityClass, List<FieldInfo> resultAttrs, Set<SingularAttribute> singularAttributes, String parentAttributePath, Field parentAttribute, int level) {
		if (StringUtils.isEmpty(parentAttributePath)) {
			parentAttributePath = "";
		} else {
			parentAttributePath = parentAttributePath + ".";
		}
		if (parentAttributePath.split("\\.").length > level) { // poduroven podle levelu, aby jsme necyklili a aby tam nebyla cela DB	
			return;
		}
		for (SingularAttribute attr : singularAttributes) {
			if (attr.getType() instanceof javax.persistence.metamodel.BasicType) {
				resultAttrs.add(new FieldInfo(entityClass, (Field) attr.getJavaMember(), parentAttributePath + attr.getName(), parentAttribute, attr.isId()));
			} else if (attr.getType() instanceof javax.persistence.metamodel.EntityType) {
				appendSingularAttributes(entityClass, resultAttrs, ((javax.persistence.metamodel.EntityType) attr.getType()).getSingularAttributes(), parentAttributePath + attr.getName(), (Field) attr.getJavaMember(), level);
			}
		}
	}

	protected Expression<Boolean> addPredicateToFilterCriteria(Expression<Boolean> filterCriteria, Predicate predicate, CriteriaBuilder criteriaBuilder) {
		if (filterCriteria == null) {
			filterCriteria = predicate;
		} else {
			filterCriteria = criteriaBuilder.and(filterCriteria, predicate);
		}
		return filterCriteria;
	}

	/**
	 * Přidám seznam sloupečků nutných pro subselect (všechny kromě formulí + ty
	 * přes které řadím)
	 */
	private List<Path<Object>> getNeededColumns(Root<T> idListRoot, List<Order> orders) {
		List<Path<Object>> columns = new ArrayList<Path<Object>>();
		Path<Object> idPath = idListRoot.get(getIdAttribute().getName());
		columns.add(idPath);
		for (Order order : orders) {
			columns.add((Path) order.getExpression());
		}
		for (Attribute attribute : idListRoot.getModel().getAttributes()) {
			if (!(attribute instanceof SingularAttribute) || !(((SingularAttribute) attribute).getType() instanceof javax.persistence.metamodel.BasicType)) {
				continue;
			}
			Field declaredField = null;
			Class c = clazz;
			while (declaredField == null && c != null) {
				try {
					declaredField = c.getDeclaredField(attribute.getName());
				} catch (NoSuchFieldException e) {
					c = c.getSuperclass();
				} catch (SecurityException ex) {
					java.util.logging.Logger.getLogger(JpaAnyTypeDao.class.getName()).log(Level.SEVERE, null, ex);
				}

			}
			if (declaredField != null && !declaredField.isAnnotationPresent(Formula.class)) {
				columns.add(idListRoot.get(attribute.getName()));
			}
		}
		return columns;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	protected EntityManager getEntityManager() {
		return entityManager;
	}

	/**
	 * Ma-li entita formule - drobna optimalizace, aby se nemusela furt
	 * dohledavat
	 *
	 * @return
	 */
	private Boolean getHasFormulaAnnotation() {
		if (hasFormulaAnnotation == null) {
			hasFormulaAnnotation = EntityUtils.hasAnnotatedField(clazz, Formula.class);
		}
		return hasFormulaAnnotation;
	}

}
