/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.entity;

import java.io.Serializable;

/**
 * Base entity
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public interface BaseEntity extends Serializable {

	/**
	 * Returns indentifier
	 *
	 * @return
	 */
	Serializable getId();
	
	/**
	 * Set indentifier
	 *
	 * @param id
	 */
	void setId(Serializable id);
}
