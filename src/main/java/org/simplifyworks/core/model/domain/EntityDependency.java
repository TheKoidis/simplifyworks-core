package org.simplifyworks.core.model.domain;

import java.text.MessageFormat;

import com.google.common.base.Objects;

/**
 * Class represents dependency on entity (for index purposes)
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class EntityDependency {

	private Class<?> entityClass;
	private String path;
	private String inversePath;
	
	public EntityDependency() {
	}
	
	public EntityDependency(Class<?> entityClass, String path, String inversePath) {
		this.entityClass = entityClass;
		this.path = path;
		this.inversePath = inversePath;
	}

	public Class<?> getEntityClass() {
		return entityClass;
	}
	
	public void setEntityClass(Class<?> entityClass) {
		this.entityClass = entityClass;
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getInversePath() {
		return inversePath;
	}
	
	public void setInversePath(String inversePath) {
		this.inversePath = inversePath;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof EntityDependency) {
			EntityDependency that = (EntityDependency) obj;
			
			return Objects.equal(this.getEntityClass(), that.getEntityClass())
					&& Objects.equal(this.getPath(), that.getPath());
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return Objects.hashCode(getEntityClass(), getPath());
	}
	
	@Override
	public String toString() {
		return MessageFormat.format("{0} ({1} / {2})", getEntityClass(), getPath(), getInversePath());
	}
}