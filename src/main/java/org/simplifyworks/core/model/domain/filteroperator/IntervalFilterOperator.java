package org.simplifyworks.core.model.domain.filteroperator;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.util.JpaUtils;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class IntervalFilterOperator extends FilterOperator {

	public static final String NAME = "INTERVAL";

	@Override
	public String getLabel() {
		return "⇔";
	}

	@Override
	public List<Object> getFilledValues(List<Object> values) {
		List<Object> filledValues = new ArrayList<>();
		for (Object value : values) {
			if (value != null) {
				// zajimaji nas prvni dve a prvni nemusi byt vyplnena - doplnit check
				if ((getFirstValue(values) != null && !StringUtils.isEmpty(getFirstValue(values).toString()))
						|| (getSecondValue(values) != null && !StringUtils.isEmpty(getSecondValue(values).toString()))) {
					filledValues.add(value);
				}
			}
		}

		return filledValues;
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;
		Comparable from = (Comparable) filter.getFirstValue();
		Comparable to = (Comparable) filter.getSecondValue();
		if (from != null && StringUtils.isNotEmpty(from.toString())) {
			if (String.class.isAssignableFrom(expression.getJavaType())) {
				predicate = criteriaBuilder.greaterThanOrEqualTo(criteriaBuilder.upper((Path<String>) expression), ((String) from).toUpperCase());
			} else if (Date.class.isAssignableFrom(expression.getJavaType())) {
				predicate = criteriaBuilder.greaterThanOrEqualTo(criteriaBuilder.function(JpaUtils.resolveDateFunction(entityManager), Date.class, expression), DateUtils.truncate(from, Calendar.DATE));
			} else {
				predicate = criteriaBuilder.greaterThanOrEqualTo((Path<Comparable>) expression, from);
			}
		}
		if (to != null && StringUtils.isNotEmpty(to.toString())) {
			Predicate subPredicate;
			if (String.class.isAssignableFrom(expression.getJavaType())) {
				subPredicate = criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.upper((Path<String>) expression), ((String) to).toUpperCase());
			} else if (Date.class.isAssignableFrom(expression.getJavaType())) {
				subPredicate = criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.function(JpaUtils.resolveDateFunction(entityManager), Date.class, expression), DateUtils.truncate(to, Calendar.DATE));
			} else {
				subPredicate = criteriaBuilder.lessThanOrEqualTo((Path<Comparable>) expression, to);
			}
			if (predicate == null) {
				predicate = subPredicate;
			} else {
				predicate = criteriaBuilder.and(predicate, subPredicate);
			}
		}

		return predicate;
	}

	public Object getFirstValue(List<Object> values) {
		if (values.isEmpty()) {
			values.add(null);
		}
		return values.get(0);
	}

	public Object getSecondValue(List<Object> values) {
		if (values.isEmpty()) {
			values.add(null);
		}
		if (values.size() < 2) {
			values.add(null);
		}
		return values.get(1);
	}
}
