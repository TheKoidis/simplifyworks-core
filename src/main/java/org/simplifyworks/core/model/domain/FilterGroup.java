/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.Maps;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

/**
 *
 * @author Radek Tomiška <tomiska@simplifyworks.org>
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FilterGroup implements Serializable {
	
	public static enum GroupOperatorType {
		AND,
		OR
	}
	
	private String name;
	private final Map<String, FilterValue> filters = Maps.newLinkedHashMap(); // propertyName - filter
	private GroupOperatorType groupOperator = GroupOperatorType.AND;

	public FilterGroup() {
	}

	public FilterGroup(String name) {
		this.name = name;
	}
	
	public FilterGroup(String name, GroupOperatorType groupOperator) {
		this.name = name;
		this.groupOperator = groupOperator;
	}

	@JsonIgnore
	public Map<String, FilterValue> getFilters() {
		return filters;
	}

	/**
	 * Only for JSON serialization
	 */
	@JsonProperty(value = "filters")
	public Collection<FilterValue> getFiltersList() {
		return getFilters().values();
	}

	/**
	 * Only for JSON serialization
	 */
	public void setFiltersList(Collection<FilterValue> collection) {
		getFilters().clear();
		for (FilterValue filter: collection) {
			getFilters().put(filter.getPropertyName(), filter);
		}
	}
	
	/**
	 * Nastavi filter pro property
	 * 
	 * @param filterValue 
	 * @return  
	 */
	public FilterValue addFilter(FilterValue filterValue) {
		if(filterValue == null || StringUtils.isBlank(filterValue.getPropertyName())) {
			return null;
		}
		filterValue.setFilterGroup(this);
		if(filters.containsKey(filterValue.getPropertyName())) {
			filters.get(filterValue.getPropertyName()).setOperator(filterValue.getOperator());
			filters.get(filterValue.getPropertyName()).setValues(filterValue.getValues());
			filters.get(filterValue.getPropertyName()).setInternal(filterValue.isInternal());
		} else {
			filters.put(filterValue.getPropertyName(), filterValue);
		}
		return filters.get(filterValue.getPropertyName());
	}
	
	public FilterValue addInternalFilter(FilterValue filterValue) {
		filterValue.setInternal(true);
		return addFilter(filterValue);
	}
	
	public FilterValue addManualFilter(FilterValue filterValue) {
		filterValue.setManual(true);
		return addFilter(filterValue);
	}
	
	public FilterValue initFilter(FilterValue filterValue) {
		if(filters.containsKey(filterValue.getPropertyName())) {
			return filters.get(filterValue.getPropertyName()); 
		}
		return addFilter(filterValue);
	}
	
	public FilterValue initManualFilter(FilterValue filterValue) {
		filterValue.setManual(true);
		return initFilter(filterValue);
	}
	
	public FilterValue initInternalFilter(FilterValue filterValue) {
		filterValue.setInternal(true);
		return initFilter(filterValue);
	}

	@JsonIgnore
	public boolean isEmpty() {
		return filters.isEmpty();
	}
	
        /*
         * @author Jirka Pech <pech@simplifyworks.org>
         */
	public void clearFilters() {
                getFilters().clear(); // this should be pretty enough
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public GroupOperatorType getGroupOperator() {
		return groupOperator;
	}

	public void setGroupOperator(GroupOperatorType groupOperator) {
		this.groupOperator = groupOperator;
	}
}
