package org.simplifyworks.core.model.domain;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import org.simplifyworks.client.config.documentation.ExtendedArrayListAlternateTypeRule;
import org.simplifyworks.core.model.domain.ExtendedArrayList.Deserializer;
import org.simplifyworks.core.model.domain.ExtendedArrayList.Serializer;
import org.springframework.util.Assert;

/**
 * Simple implementation of {@link ExtendedList} which uses custom serialization and no deserialization.
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 * @see ExtendedArrayListAlternateTypeRule
 */
@JsonSerialize(using = Serializer.class)
@JsonDeserialize(using = Deserializer.class)
public class ExtendedArrayList<E> extends ArrayList<E> implements ExtendedList<E> {

	private static final long serialVersionUID = 3768489893948726387L;

	private long total;

	/**
	 * Creates empty list.
	 */
	public ExtendedArrayList() {
	}

	/**
	 * Creates list using collection.
	 *
	 * Total is set to size of collection.
	 *
	 * @param collection collection to use
	 */
	public ExtendedArrayList(Collection<E> collection) {
		this(collection.size(), collection);
	}

	/**
	 * Creates list using total and collection.
	 *
	 * @param total total to use (must be greater than or equal to size of collection)
	 * @param collection collection to use
	 */
	public ExtendedArrayList(long total, Collection<E> collection) {
		super(collection);

		Assert.isTrue(total >= collection.size(), "Total must be greater than or equal to size of collection!");

		this.total = total;
	}

	@Override
	public long total() {
		return total;
	}

	/**
	 * Custom serializer for {@link ExtendedArrayList}
	 *
	 * @author Štěpán Osmík (osmik@ders.cz)
	 */
	public static class Serializer extends StdSerializer<ExtendedArrayList<?>> {

		public Serializer() {
			super(ExtendedArrayList.class, true);
		}

		@Override
		public void serialize(ExtendedArrayList<?> value, JsonGenerator jgen, SerializerProvider provider) throws IOException, JsonGenerationException {
			jgen.writeStartObject();

			// TODO refactor field names
			jgen.writeNumberField("recordsReturned", value.size());
			jgen.writeNumberField("recordsFiltered", value.total());
			jgen.writeNumberField("recordsTotal", value.total());

			jgen.writeFieldName("resources");
			// because of problem while serializing collections of elements with @JsonTypeInfo annotation
			jgen.writeStartArray();
			for (Object object : value) {
				jgen.writeObject(object);
			}
			jgen.writeEndArray();

			jgen.writeEndObject();
		}
	}

	/**
	 * Custom deserializer for {@link ExtendedArrayList}
	 *
	 * @author Štěpán Osmík (osmik@ders.cz)
	 */
	public static class Deserializer extends StdDeserializer<ExtendedArrayList<?>> {

		private static final long serialVersionUID = 8800377556546594712L;

		public Deserializer() {
			super(ExtendedArrayList.class);
		}

		@Override
		public ExtendedArrayList<?> deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
			throw new JsonMappingException(ExtendedArrayList.class.getName() + " is not deserializable!");
		}
	}
}
