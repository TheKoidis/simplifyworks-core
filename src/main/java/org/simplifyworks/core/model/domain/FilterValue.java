/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.model.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.simplifyworks.core.model.domain.filteroperator.EqualsFilterOperator;
import org.springframework.util.Assert;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Uchovava nastaveni a hodnotu filtru pro property
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public class FilterValue implements Serializable {

	/**
	 * Klic property
	 */
	private String propertyName;
	/**
	 * Operator
	 */
	private FilterOperator operator;
	/**
	 * Nastavene hodnoty
	 */
	private List<Object> values; // TODO: asi bude rozhrani a ruzne implementace pro ruzne datove typy?
	/**
	 * Interni filrovaci kriterium - nemuze nastavovat uzivatel
	 */
	private boolean internal = false;
	/**
	 * Rucne pridane filtrovaci kriterium
	 */
	private boolean manual = false;

	@JsonIgnore
	private FilterGroup filterGroup;

	public FilterValue() {
	}

	public FilterValue(String propertyName, FilterOperator operator, Object value) {
		this(propertyName, operator, Collections.singletonList(value));
	}
	
	@SuppressWarnings("unchecked")
	public FilterValue(String propertyName, FilterOperator operator, List<?> values) {
		Assert.notNull(propertyName, "Property je povinna");
		Assert.notNull(operator, "Operator je povinny");
		this.propertyName = propertyName;
		this.operator = operator;
		this.values = (List<Object>) values;
	}

	public static FilterValue single(String propertyName, FilterOperator operator, Object value) {
		FilterValue filter = new FilterValue(propertyName, operator, null);
		filter.getValues().add(value);
		return filter;
	}

	public static FilterValue single(String propertyName, Object value) {
		return single(propertyName, new EqualsFilterOperator(), value);
	}

	public String getPropertyName() {
		return propertyName;
	}

	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}

	public List<Object> getValues() {
		if (values == null) {
			values = new ArrayList<>();
		}
		return values;
	}

	/**
	 * Vrati jen vyplnene hodnoty ve filtru pro skladani filtru
	 *
	 * @return
	 */
	@JsonIgnore
	public List<Object> getFilledValues() {
		return getOperator().getFilledValues(getValues());
	}

	@SuppressWarnings("unchecked")
	public void setValues(List<?> values) {
		this.values = (List<Object>) values;
	}

	/**
	 * Single value
	 *
	 * @param value
	 */
	public void setValue(Object value) {
		getValues().clear();
		if (value != null) {
			getValues().add(value);
		}
	}

	/**
	 * Single value
	 *
	 * @return
	 */
	@JsonIgnore
	public Object getValue() {
		return getFirstValue();
	}

	/**
	 * Pomocnici pro intervaly ... TODO: jina implementace?
	 *
	 * @return
	 */
	@JsonIgnore
	public Object getFirstValue() {
		if (getValues().isEmpty()) {
			getValues().add(null);
		}
		return getValues().get(0);
	}

	public void setFirstValue(Object value) {
		if (getValues().isEmpty()) {
			getValues().add(value);
		} else {
			getValues().set(0, value);
		}
	}

	@JsonIgnore
	public Object getSecondValue() {
		if (getValues().isEmpty()) {
			getValues().add(null);
		}
		if (getValues().size() < 2) {
			getValues().add(null);
		}
		return getValues().get(1);
	}

	public void setSecondValue(Object value) {
		if (getValues().isEmpty()) {
			getValues().add(null);
		}
		if (getValues().size() < 2) {
			getValues().add(value);
		} else {
			getValues().set(1, value);
		}
	}

	@JsonIgnore
	public boolean isEmpty() {
		if (StringUtils.isBlank(propertyName) || operator == null) {
			return true;
		}

		return operator.isEmpty(getValues());
	}

	/**
	 * Vycisteni hodnot filtru
	 */
	public void clear() {
		getValues().clear();
	}

	/**
	 * Operator nikdy nemuze byti prazdny
	 *
	 * @return
	 */
	public FilterOperator getOperator() {
		if (operator == null) {
			operator = new EqualsFilterOperator();
		}
		return operator;
	}

	public void setOperator(FilterOperator operator) {
		this.operator = operator;
	}

	@JsonIgnore
	public boolean isInternal() {
		return internal;
	}

	public void setInternal(boolean internal) {
		this.internal = internal;
	}

	@JsonIgnore
	public boolean isManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}

	public FilterGroup getFilterGroup() {
		return filterGroup;
	}

	public void setFilterGroup(FilterGroup filterGroup) {
		this.filterGroup = filterGroup;
	}
}
