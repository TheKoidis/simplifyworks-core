package org.simplifyworks.core.model.domain.filteroperator;

import java.util.List;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class IsNotNullFilterOperator extends FilterOperator {

	public static final String NAME = "IS_NOT_NULL";

	@Override
	public String getLabel() {
		return "•";
	}

	@Override
	public boolean isEmpty(List<Object> values) {
		return false;
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		return criteriaBuilder.isNotNull(expression);
	}
}
