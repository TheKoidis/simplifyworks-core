package org.simplifyworks.core.model.domain;

import java.util.List;

/**
 * Ordered collection with additional information about total size.
 * 
 * Total size can be used for pagination support.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public interface ExtendedList<E> extends List<E> {

	/**
	 * Returns total size (can be different from size - greater than or equal to).
	 *  
	 * @return total size
	 */
	public long total();
}
