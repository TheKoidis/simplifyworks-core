package org.simplifyworks.core.model.domain.filteroperator;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.time.DateUtils;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.util.JpaUtils;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class LessEqualsFilterOperator extends FilterOperator {

	public static final String NAME = "LESS_EQUALS";

	@Override
	public String getLabel() {
		return "≤";
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;
		for (Object obj : filter.getFilledValues()) {
			Comparable comp = (Comparable) obj;
			Predicate subPredicate;
			if (String.class.isAssignableFrom(expression.getJavaType())) {
				subPredicate = criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.upper((Path<String>) expression), ((String) comp).toUpperCase());
			} else if (Date.class.isAssignableFrom(expression.getJavaType())) {
				subPredicate = criteriaBuilder.lessThanOrEqualTo(criteriaBuilder.function(JpaUtils.resolveDateFunction(entityManager), Date.class, expression), DateUtils.truncate(comp, Calendar.DATE));
			} else {
				subPredicate = criteriaBuilder.lessThanOrEqualTo((Path<Comparable>) expression, comp);
			}
			if (predicate == null) {
				predicate = subPredicate;
			} else {
				predicate = criteriaBuilder.or(predicate, subPredicate);
			}
		}

		return predicate;
	}
}
