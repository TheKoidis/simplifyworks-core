package org.simplifyworks.core.model.domain.filteroperator;

import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.simplifyworks.MapBuilder;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;

import com.google.common.base.Strings;

/**
 * SQL like vcetne wildcard
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class LikeMaskFilterOperator extends FilterOperator {

	public static final String NAME = "LIKE_MASK";

	@Override
	public String getLabel() {
		return "≈";
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;
		for (Object obj : filter.getFilledValues()) {
			if (!String.class.isAssignableFrom(expression.getJavaType())) {
				throw new CoreException("Like mask type error", "filter.operator.likeMask.error.type", new MapBuilder().put("propertyName", filter.getPropertyName()).put("type", expression.getJavaType().getSimpleName()).build());
			}
			String stringFilterValue = String.valueOf(obj);
			if (!Strings.isNullOrEmpty(stringFilterValue)) {
				Predicate subPredicate = criteriaBuilder.like(criteriaBuilder.upper((Path<String>) expression), stringFilterValue.toUpperCase(locale));
				if (predicate == null) {
					predicate = subPredicate;
				} else {
					predicate = criteriaBuilder.or(predicate, subPredicate);
				}
			}
		}

		return predicate;
	}
}
