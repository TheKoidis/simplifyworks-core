package org.simplifyworks.core.model.domain;

/**
 *
 * @author kakrda
 */
public enum ApplyFilterTypeEnum {
        RESULTS,
        COUNT    
}
