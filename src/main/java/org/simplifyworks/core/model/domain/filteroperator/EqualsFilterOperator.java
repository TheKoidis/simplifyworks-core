package org.simplifyworks.core.model.domain.filteroperator;

import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.Locale;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;

import org.apache.commons.lang3.time.DateUtils;
import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.util.JpaUtils;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class EqualsFilterOperator extends FilterOperator {

	public static final String NAME = "EQUALS";

	@Override
	public String getLabel() {
		return "=";
	}

	@Override
	public Predicate createFilterOperatorCriteria(Path<?> expression, FilterValue filter, CriteriaBuilder criteriaBuilder, Locale locale, EntityManager entityManager) {
		Predicate predicate = null;

		for (Object obj : filter.getFilledValues()) {
			Predicate subPredicate;
			if (Date.class.isAssignableFrom(expression.getJavaType())) {
				if (obj instanceof String) {
					obj = new Date(Long.valueOf((String) obj));
				}
				subPredicate = criteriaBuilder.equal(criteriaBuilder.function(JpaUtils.resolveDateFunction(entityManager), Date.class, expression), DateUtils.truncate(obj, Calendar.DATE));
			} else if (obj instanceof Collection) { // TODO: this is wrong - why we dont use multi value instead?
				subPredicate = expression.in(obj);
			} else if (expression.getJavaType().isEnum()) {
				subPredicate = criteriaBuilder.equal(expression, valueOf(expression.getJavaType(), obj));
			} else if (Boolean.class.isAssignableFrom(expression.getJavaType())) {
				if (obj instanceof String) {
					obj = Boolean.valueOf((String) obj);
				}
				subPredicate = criteriaBuilder.equal(expression, obj);
			} else {
				subPredicate = criteriaBuilder.equal(expression, obj);
			}
			if (predicate == null) {
				predicate = subPredicate;
			} else {
				predicate = criteriaBuilder.or(predicate, subPredicate);
			}
		}

		return predicate;
	}

	private Enum valueOf(Class clazz, Object value) {
		for (Object o : clazz.getEnumConstants()) {
			if (value.equals(((Enum) o).name())) {
				return ((Enum) o);
			}
		}
		return null;
	}
}
