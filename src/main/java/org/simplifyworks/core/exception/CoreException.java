package org.simplifyworks.core.exception;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Zakladni vyjimka pracujici s lokalizaci
 *
 * @author Radek Tomiška <tomiska@ders.cz>
 */
@JsonIgnoreProperties({"localizedMessage", "stackTrace", "suppressed", "cause"})
public class CoreException extends RuntimeException {

	private static final long serialVersionUID = -1728082458277701864L;
	
	private String messageKey;
	private Map<String, Object> parameters;
	
	public CoreException(String message) {
		this(message, null, null, null);
	}
	
	public CoreException(Throwable cause) {
		this("Unexpected error", null, null, cause);
	}

	public CoreException(String message, String messageKey) {
		this(message, messageKey, null, null);
	}
	
	public CoreException(String message, Throwable cause) {
		this(message, null, null, cause);
	}
	
	public CoreException(String message, String messageKey, Map<String, Object> parameters) {
		this(message, messageKey, parameters, null);
	}
	
	public CoreException(String message, String messageKey, Map<String, Object> parameters, Throwable cause) {
		super(message, cause);
		
		this.messageKey = messageKey;
		this.parameters = parameters;
	}

	public String getMessageKey() {
		return messageKey;
	}
	
	public Map<String, Object> getParameters() {
		return parameters;
	}
}
