package org.simplifyworks.core.exception;

import java.util.Collections;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class ValidationException extends CoreException {

	public ValidationException(String message, ValidationFail[] details) {
		super("Validation failed", message, Collections.singletonMap("fails", details));
	}
}
