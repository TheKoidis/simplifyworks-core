/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.exception;

import java.util.Map;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.VndErrors;

/**
 * Add parameters to standard VndError
 *
 * @author Radek Tomiška <radek.tomiska@gmail.com>
 */
public class ParametrizedVndError extends VndErrors.VndError {

	private String humanMessageKey;
	private Map<String, Object> params;
	private String type;

	public ParametrizedVndError(String logref, String message) {
		super(logref, message);
	}

	public ParametrizedVndError(String logref, String message, Link... links) {
		super(logref, message, links);
	}

	public ParametrizedVndError(String logref, String message, String humanMessageKey, Map<String, Object> params, String type, Link... links) {
		super(logref, message, links);
		this.humanMessageKey = humanMessageKey;
		this.params = params;
		this.type = type;
	}

	public String getHumanMessageKey() {
		return humanMessageKey;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public String getType() {
		return type;
	}

}
