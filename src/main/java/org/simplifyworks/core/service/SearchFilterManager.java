package org.simplifyworks.core.service;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;

/**
 * Search filter manager
 *
 * @author kakrda
 */
public interface SearchFilterManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> extends BaseManager<T, E> {

/**
 * @deprecated (pouzit SearchLoadWriteManager.read)
 * @param filter
 * @return
 */
	public ExtendedArrayList<T> getAllWrapped(F filter);

	/**
	 * @todo vyhodit, tady je asi k nicemu (pouzit SearchLoadWriteManager.read)
	 * @param filter
	 * @return
	 */
	public List<E> read(F filter);

	public abstract String[] getQuickSearchProperties();

	public void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<E> root, F filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType);

}
