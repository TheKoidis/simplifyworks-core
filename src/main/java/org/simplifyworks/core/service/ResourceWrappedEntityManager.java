package org.simplifyworks.core.service;

import java.util.Collection;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;

import org.simplifyworks.core.model.domain.ExtendedArrayList;

/**
 * @deprecated ? lepe asi pouzit wrapper az v controleru (viz AbstractController)
 * @author hanak petr [hanak@ders.cz]
 */
public interface ResourceWrappedEntityManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> extends SearchLoadWriteManager<T, E, F> {

	public T wrap(E entity);

	public ExtendedArrayList<T> wrap(Collection<E> entities, Long recordsTotal, Long recordsFiltered);

	public T readWrapped(Long id);

	public ExtendedArrayList<T> readWrapped(F filter);

}
