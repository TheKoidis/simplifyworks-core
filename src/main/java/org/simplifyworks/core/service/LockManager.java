package org.simplifyworks.core.service;

import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import org.simplifyworks.core.model.dto.Lock;
import org.simplifyworks.security.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LockManager {

	@Autowired
	private SecurityService securityService;

	private final long LOCK_EXPIRATION = 1000 * 60;

	private Map<String, Lock> locks = new ConcurrentHashMap<>();

	public void tryLock(String name) {
		clearExpiredLocks();

		Lock existingLock = locks.get(name);

		if (existingLock != null) {
			if (existingLock.getUser().equals(securityService.getUsername())) {
				existingLock.setDate(new Date());
			} else {
				throw new RuntimeException("Already locked");
			}
		} else {
			locks.put(name, new Lock(new Date(), securityService.getUsername()));
		}
	}

	public void holdLock(String name) {
		Lock existingLock = locks.get(name);

		if (existingLock == null || !existingLock.getUser().equals(securityService.getUsername())) {
			throw new RuntimeException("Already unlocked");
		}

		existingLock.setDate(new Date());
	}

	public void releaseLock(String name) {
		clearExpiredLocks();

		locks.remove(name);
	}

	public void clearExpiredLocks() {
		Date expirationTreshold = new Date(System.currentTimeMillis() - LOCK_EXPIRATION);

		Set<Map.Entry<String, Lock>> locks = this.locks.entrySet();
		for (Map.Entry<String, Lock> lock : locks) {
			if (lock.getValue().getDate().before(expirationTreshold)) {
				locks.remove(lock);
			}
		}
	}
}
