package org.simplifyworks.core.service.impl;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.SearchLoadWriteManager;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public abstract class AbstractSearchLoadWriteManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto>
				extends AbstractBaseManager<T, E>
				implements SearchLoadWriteManager<T, E, F> {

	/**
	 * @deprecated use readWrapped method
	 * @param filter
	 * @return
	 */
	@Override
	public ExtendedArrayList<T> getAllWrapped(F filter) {
		return searchManager.getAllWrapped(filter, this);
	}

	@Override
	public Long getRecordsCount(F filter) {
		return searchManager.getRecordsCount(filter, this);
	}

	@Override
	public E read(Long id) {
		E entity = dataRepository.find(entityClass, id);
		return entity;
	}

	@Override
	public List<E> read(F filter) {
		return searchManager.read(filter, this);
	}

	/**
	 * @deprecated use readWrapped method instead
	 * @param id
	 * @return
	 */
	@Override
	public T load(Long id) {
		E entity = dataRepository.find(entityClass, id);
		return toDto(entity);
	}

	@Override
	@Transactional
	public Long create(T dto) {
		return write(null, dto);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		dataRepository.delete(entityClass, id);
	}

	/**
	 * @deprecated use write method instead
	 * @param id
	 * @param dto
	 * @return
	 */
	@Override
	@Transactional
	public Long save(Long id, T dto) {
		return write(id, dto);
	}

	@Override
	@Transactional
	public Long write(Long id, T dto) {
		E entity;
		try {
			entity = (id == null ? entityClass.newInstance() : dataRepository.find(entityClass, id));
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new CoreException(ex);
		}
		entity = mapper.map(dto, entity);
		if (entity.getId() == null) {
			dataRepository.persist(entity);
		} else {
			entity = dataRepository.update(entity);
		}
		return (Long) entity.getId();
	}

	@Override
	public abstract String[] getQuickSearchProperties();

	@Override
	public abstract void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<E> root, F filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType);

}
