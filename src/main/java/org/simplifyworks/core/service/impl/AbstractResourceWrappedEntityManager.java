package org.simplifyworks.core.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.ResourceWrappedEntityManager;
import org.simplifyworks.core.util.Wrapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @deprecated ? lepe asi pouzit wrapper az v controleru (viz AbstractController)
 * @author hanak petr [hanak@ders.cz]
 */
public abstract class AbstractResourceWrappedEntityManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto>
				extends AbstractSearchLoadWriteManager<T, E, F>
				implements ResourceWrappedEntityManager<T, E, F> {

	@Autowired
	Wrapper wrapper;

	@Override
	public T wrap(E entity) {
		return wrapper.wrap(entity, this);
	}

	@Override
	public ExtendedArrayList<T> wrap(Collection<E> entities, Long recordsTotal, Long recordsFiltered) {
		return wrapper.wrap(entities, recordsTotal, recordsFiltered, this);
	}

	@Override
	public T readWrapped(Long id) {
		E entity = read(id);
		return wrap(entity);
	}

	@Override
	public ExtendedArrayList<T> readWrapped(F filter) {
		Long recordsCount = getRecordsCount(filter);

		List<E> entities;
		if (recordsCount > 0) {
			entities = read(filter);
		} else {
			entities = new ArrayList<>();
		}

		return wrap(entities, recordsCount, recordsCount);
	}

}
