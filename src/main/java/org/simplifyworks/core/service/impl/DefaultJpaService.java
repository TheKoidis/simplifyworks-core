package org.simplifyworks.core.service.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.simplifyworks.core.model.entity.AbstractEntity;
import org.simplifyworks.core.service.JpaService;
import org.springframework.stereotype.Service;

/**
 * Default implementation of {@link JpaService}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@Service
public class DefaultJpaService implements JpaService {

	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;

	@Override
	public <E extends AbstractEntity> E create(E entity) {
		try {
			entityManager.persist(entity);

			return entity;
		} finally {
			entityManager.flush();
		}
	}

	@Override
	public <E extends AbstractEntity> E update(E entity) {
		try {
			return entityManager.merge(entity);
		} finally {
			entityManager.flush();
		}
	}

	@Override
	public <E extends AbstractEntity> E remove(Class<E> type, long id) {
		try {
			E entity = entityManager.find(type, id);

			if (entity != null) {
				entityManager.remove(entity);
			}

			return entity;
		} finally {
			entityManager.flush();
		}
	}

	@Override
	public <E extends AbstractEntity> E refresh(E entity) {
		entityManager.refresh(entity);

		return entity;
	}

	@Override
	public <E extends AbstractEntity> List<E> findAll(Class<E> type) {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(type);
		criteriaQuery.from(type);

		return entityManager.createQuery(criteriaQuery).getResultList();
	}
}
