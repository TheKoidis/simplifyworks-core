package org.simplifyworks.core.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.From;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Order;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.Attribute;
import javax.persistence.metamodel.Bindable;
import javax.persistence.metamodel.EntityType;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.Type;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.dto.SortDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.SearchFilterManager;
import org.simplifyworks.core.service.SearchManager;
import org.simplifyworks.repository.DbDataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author kakrda
 */
@Service
@Transactional
public class SearchManagerImpl implements SearchManager {

	@Autowired
	protected DbDataRepository dataRepository;

	@Override
	public <T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> ExtendedArrayList<T> getAllWrapped(F filter, SearchFilterManager<T, E, F> manager) {
		List<E> entities = read(filter, manager);

		Collection<T> resources = new ArrayList<>();
		for (E entity : entities) {
			resources.add(manager.toDto(entity));
		}

		//records count
		Long recordsCount = getRecordsCount(filter, manager);

		return new ExtendedArrayList<>(recordsCount, resources);
	}

	@Override
	public <T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> List<E> read(F filter, SearchFilterManager<T, E, F> manager) {
		CriteriaBuilder criteriaBuilder = dataRepository.getCriteriaBuilder();
		Class<E> entityClass = manager.getEntityClass();
		CriteriaQuery<E> criteriaQuery = criteriaBuilder.createQuery(entityClass);
		Root<E> root = criteriaQuery.from(entityClass);
		criteriaQuery = criteriaQuery.select(root);

		criteriaQuery = setCriteriaQuery(root, criteriaQuery, filter, manager, ApplyFilterTypeEnum.RESULTS);

		//ordering
		if (filter.getSorts() != null) {
			criteriaQuery = createOrderParameters(root, criteriaQuery, filter.getSorts());
		}

		Query query = dataRepository.createQuery(criteriaQuery);

		//pagination
		query.setFirstResult((filter.getPageNumber() - 1) * filter.getPageSize());
		query.setMaxResults(filter.getPageSize());

		return query.getResultList();
	}

	@Override
	public <T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> Long getRecordsCount(F filter, SearchFilterManager<T, E, F> manager) {
		CriteriaBuilder criteriaBuilder = dataRepository.getCriteriaBuilder();
		CriteriaQuery<Long> criteriaQuery = criteriaBuilder.createQuery(Long.class);
		Root<E> root = criteriaQuery.from(manager.getEntityClass());
		criteriaQuery = criteriaQuery.select(criteriaBuilder.countDistinct(root));

		criteriaQuery = setCriteriaQuery(root, criteriaQuery, filter, manager, ApplyFilterTypeEnum.COUNT);

		Query query = dataRepository.createQuery(criteriaQuery);

		return (Long) query.getSingleResult();
	}

	<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto, Clazz> CriteriaQuery<Clazz> setCriteriaQuery(Root<E> root, CriteriaQuery<Clazz> criteriaQuery, F filter, SearchFilterManager<T, E, F> manager, ApplyFilterTypeEnum filterType) {
		CriteriaBuilder criteriaBuilder = dataRepository.getCriteriaBuilder();
		EntityType<E> type = dataRepository.getMetamodel().entity(manager.getEntityClass());
		List<Predicate> predicates = new ArrayList<>();

		//quick search
		filter.setQuickSearchProperties(manager.getQuickSearchProperties());
		List<Predicate> quickSearchParameters = createQuickSearchParameters(root, type, filter);
		if (quickSearchParameters.size() > 0) {
			predicates.add(criteriaBuilder.or(quickSearchParameters.toArray(new Predicate[]{})));
		}

		//filter
		List<Predicate> filterParameters = new ArrayList<>();
		manager.applyFilter(criteriaBuilder, criteriaQuery, root, filter, filterParameters, filterType);
		if (filterParameters.size() > 0) {
			predicates.add(criteriaBuilder.and(filterParameters.toArray(new Predicate[]{})));
		}

		//quick search + filter
		if (predicates.size() > 0) {
			criteriaQuery = criteriaQuery.where(predicates.toArray(new Predicate[]{}));
		}

		return criteriaQuery;
	}

	<E extends BaseEntity, F extends BaseFilterDto> List<Predicate> createQuickSearchParameters(Root<E> root, EntityType<E> type, F filter) {
		CriteriaBuilder criteriaBuilder = dataRepository.getCriteriaBuilder();
		List<Predicate> quickSearchParameters = new ArrayList<>();
		List<String> quickSearchTokens = filter.getQuickSearchTokens();

		if (quickSearchTokens == null) {
			return quickSearchParameters;
		}
		for (String token : quickSearchTokens) {
			for (String property : filter.getQuickSearchProperties()) {
				quickSearchParameters.add(
						criteriaBuilder.like(
								criteriaBuilder.lower(createPropertyExpression(root, property)),
								"%" + token.toLowerCase() + "%"
						)
				);
			}
		}
		return quickSearchParameters;
	}

	<E extends BaseEntity> CriteriaQuery<E> createOrderParameters(Root<E> root, CriteriaQuery<E> criteriaQuery, List<SortDto> sorts) {
		CriteriaBuilder criteriaBuilder = dataRepository.getCriteriaBuilder();
		List<Order> orders = new ArrayList<>();
		for (SortDto sort : sorts) {
			Expression expression = createPropertyExpression(root, sort.getProperty());
			if (sort.getOrder().equals("asc")) {
				orders.add(criteriaBuilder.asc(expression));
			}
			if (sort.getOrder().equals("desc")) {
				orders.add(criteriaBuilder.desc(expression));
			}
		}
		criteriaQuery = criteriaQuery.orderBy(
				orders.toArray(new Order[]{})
		);
		return criteriaQuery;
	}

	<E extends BaseEntity> Expression createPropertyExpression(Root<E> root, String propertyName) {
		Path expression = root;
		String[] props = propertyName.split("\\.");
		for (String prop : props) {
			Path expPath = expression.get(prop);
			Bindable model = expPath.getModel();
			if (model instanceof SingularAttribute) {
				SingularAttribute saModel = (SingularAttribute) model;
				Attribute.PersistentAttributeType persistentAttributeType = saModel.getPersistentAttributeType();
				Type type = saModel.getType();
				//if entity and many-to-one or one-to-one relation, then left outer join
				if (Type.PersistenceType.ENTITY.equals(type.getPersistenceType())
						&& (Attribute.PersistentAttributeType.MANY_TO_ONE.equals(persistentAttributeType) || Attribute.PersistentAttributeType.ONE_TO_ONE.equals(persistentAttributeType))) {
					expression = ((From) expression).join(saModel, JoinType.LEFT);
				} else { //common attribute
					expression = expPath;
				}
			} else {
				return null;
			}
		}
		return expression;
	}
}
