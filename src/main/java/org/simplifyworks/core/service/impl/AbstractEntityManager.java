package org.simplifyworks.core.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.EntityReadWriteManager;
import org.simplifyworks.core.service.SearchFilterManager;
import org.springframework.transaction.annotation.Transactional;

/**
 * @deprecated asi k nicemu, pouzij AbstractSearchLoadWriteManager
 * @author hanak petr [hanak@ders.cz]
 */
public abstract class AbstractEntityManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto>
				extends AbstractBaseManager<T, E>
				implements EntityReadWriteManager<T, E, F>, SearchFilterManager<T, E, F> {

	/**
	 * @deprecated use {@link #read(F filter) read} method instead
	 * @param filter
	 * @return
	 */
	@Override
	public ExtendedArrayList<T> getAllWrapped(F filter) {
		return searchManager.getAllWrapped(filter, this);
	}

//	@Override
	public Long getRecordsCount(F filter) {
		return searchManager.getRecordsCount(filter, this);
	}

	@Override
	public E read(Long id) {
		return dataRepository.find(entityClass, id);
	}

	@Override
	public List<E> read(F filter) {
		return searchManager.read(filter, this);
	}

	@Override
	@Transactional
	public Long create(T dto) {
		return write(null, dto);
	}

	@Override
	@Transactional
	public void delete(Long id) {
		dataRepository.delete(entityClass, id);
	}

	@Override
	@Transactional
	public Long write(Long id, T dto) {
		E entity;
		try {
			entity = (id == null ? entityClass.newInstance() : dataRepository.find(entityClass, id));
		} catch (InstantiationException | IllegalAccessException ex) {
			throw new CoreException(ex);
		}
		mapper.map(dto, entity);
		if (entity.getId() == null) {
			dataRepository.persist(entity);
		} else {
			entity = dataRepository.update(entity);
		}
		return (Long) entity.getId();
	}

//	@Override
	public T wrap(E entity) {
		return toDto(entity);
	}

//	@Override
	public ExtendedArrayList<T> wrap(Collection<E> entities, Long recordsTotal, Long recordsFiltered) {
		Collection<T> resources = new ArrayList<>();
		for (E entity : entities) {
			resources.add(toDto(entity));
		}
		return new ExtendedArrayList<>(recordsTotal, resources);
	}

//	@Override
	public T readWrapped(Long id) {
		E entity = read(id);
		return wrap(entity);
	}

//	@Override
	public ExtendedArrayList<T> readWrapped(F filter) {
		Long recordsCount = getRecordsCount(filter);

		List<E> entities;
		if (recordsCount > 0) {
			entities = read(filter);
		} else {
			entities = new ArrayList<>();
		}

		return wrap(entities, recordsCount, recordsCount);
	}
	@Override
	public abstract String[] getQuickSearchProperties();

	@Override
	public abstract void applyFilter(CriteriaBuilder criteriaBuilder, CriteriaQuery<?> criteriaQuery, Root<E> root, F filter, List<Predicate> filterParameters, ApplyFilterTypeEnum filterType);

}
