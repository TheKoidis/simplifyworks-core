package org.simplifyworks.core.service.impl;

import java.lang.reflect.ParameterizedType;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.BaseManager;
import org.simplifyworks.core.service.SearchManager;
import org.simplifyworks.core.util.Mapper;
import org.simplifyworks.repository.DbDataRepository;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author kakrda
 */
public abstract class AbstractBaseManager<T extends BaseDto, E extends BaseEntity> implements BaseManager<T, E> {

	protected final Class<T> dtoClass;

	protected final Class<E> entityClass;

	@Autowired
	protected DbDataRepository dataRepository;

	@Autowired
	protected SearchManager searchManager;

	@Autowired
	protected Mapper mapper;

	public AbstractBaseManager() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		dtoClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
		entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
	}

	@Override
	public Class<T> getDtoClass() {
		return dtoClass;
	}

	@Override
	public Class<E> getEntityClass() {
		return entityClass;
	}

	@Override
	public T toDto(E entity) {
		return entity == null ? null : mapper.map(entity, dtoClass);
	}

	@Override
	public E toEntity(T object) {
		return object == null ? null : mapper.map(object, entityClass);
	}
}
