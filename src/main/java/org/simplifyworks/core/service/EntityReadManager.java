package org.simplifyworks.core.service;

import java.util.List;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public interface EntityReadManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> extends BaseManager<T, E> {

	public E read(Long id);

	public List<E> read(F filter);

}
