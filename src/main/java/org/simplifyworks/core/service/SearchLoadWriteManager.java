package org.simplifyworks.core.service;

import java.util.List;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;


/**
 * @todo: merge with SearchFilterManager ?
 * @author hanak petr [hanak@ders.cz]
 */
public interface SearchLoadWriteManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> extends SearchFilterManager<T, E, F> {

	public Long create(T dto);

	public void delete(Long id);

	public Long write(Long id, T dto);

	/**
	 * @deprecated use read method instead
	 * @param id
	 * @return
	 */
	public T load(Long id);

	public E read(Long id);

	/**
	 * @deprecated use write method instead
	 * @param id
	 * @param dto
	 * @return
	 */
	public Long save(Long id, T dto);

	public List<E> read(F filter);

	public Long getRecordsCount(F filter);

}
