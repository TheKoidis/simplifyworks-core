package org.simplifyworks.core.service;

import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;

/**
 *
 * @author hanak petr [hanak@ders.cz]
 */
public interface EntityReadWriteManager<T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> extends EntityReadManager<T, E, F> {

	public Long create(T dto);

	public Long write(Long id, T dto);

	public void delete(Long id);

}
