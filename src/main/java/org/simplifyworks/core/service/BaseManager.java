package org.simplifyworks.core.service;

import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.entity.BaseEntity;

/**
 * Base manager
 *
 * @author kakrda
 */
public interface BaseManager<T extends BaseDto, E extends BaseEntity> {

	public Class<T> getDtoClass();

	public Class<E> getEntityClass();

	public T toDto(E entity);

	public E toEntity(T object);

}
