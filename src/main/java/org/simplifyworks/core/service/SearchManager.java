package org.simplifyworks.core.service;

import java.util.List;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;

/**
 * Search manager
 *
 * @author kakrda
 */
public interface SearchManager {

	/**
	 * @deprecated
	 */
	public <T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> ExtendedArrayList<T> getAllWrapped(F filter, SearchFilterManager<T, E, F> manager);

	public <T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> List<E> read(F filter, SearchFilterManager<T, E, F> manager);

	public <T extends BaseDto, E extends BaseEntity, F extends BaseFilterDto> Long getRecordsCount(F filter, SearchFilterManager<T, E, F> manager);
}
