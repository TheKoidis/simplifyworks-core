package org.simplifyworks.core.web.controller.rest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.simplifyworks.core.model.domain.FilterOperator;
import org.simplifyworks.core.model.domain.FilterValue;
import org.simplifyworks.core.model.domain.RecordRange;
import org.simplifyworks.core.model.domain.SearchParameters;
import org.simplifyworks.core.model.domain.SortOrder;
import org.simplifyworks.core.model.domain.SortValue;

import com.google.common.collect.Lists;

/**
 * Request parametr "filter" - comma separated string in form: "key:value" or
 * "key:value:operator" or "key:value1:value2:operator" (for interval)
 * <p>
 * Request parametr "simpleOrder" - comma separated string in form: "key" (for
 * ascending) or "key:value" (value = ACS or DESC)
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class RestSimpleFilterRequestParser {

    private final String simpleFilter;
    private final String order;
    private final String page;
    private final String pageSize;

    public RestSimpleFilterRequestParser(HttpServletRequest httpRequest) {
        simpleFilter = httpRequest.getParameter("filter");
        order = httpRequest.getParameter("simpleOrder");
        page = httpRequest.getParameter("page");
        pageSize = httpRequest.getParameter("pageSize");
    }

    public SearchParameters toSearchParameters() {
        SearchParameters searchParameters = new SearchParameters();

		if (simpleFilter != null && !simpleFilter.isEmpty()) {

			for (String single : simpleFilter.split(",")) {
                if (single.isEmpty()) {
                    continue;
                }
				String[] filter = single.split(":");

                FilterOperator filterOperator = FilterOperator.EQUALS;
                if (filter.length > 2) {
                    filterOperator = FilterOperator.valueOf(filter[filter.length - 1]);
                }

                if (filterOperator == FilterOperator.INTERVAL) {
					searchParameters.addFilter(new FilterValue(decode(filter[0]), filterOperator, Lists.newArrayList(decode(filter[1]), decode(filter[2]))));
                } else {
					String[] values = (filter.length > 1 ? filter[1] : "").split(";");
                    List<Object> filterValues = new ArrayList<>();
                    for (String value : values) {
						filterValues.add(decode(value));
                    }
					searchParameters.addFilter(new FilterValue(decode(filter[0]), filterOperator, filterValues));
                }
            }
        }

        if (order != null && !order.isEmpty()) {
            for (String single : order.split(",")) {
                if (single.isEmpty()) {
                    continue;
                }
                String[] sort = single.split(":");

                searchParameters.addSort(new SortValue(sort[0], (sort.length > 1 && "DESC".equalsIgnoreCase(sort[1]) ? SortOrder.DESC : SortOrder.ASC)));

            }
        }

        if (pageSize != null || page != null) {
            RecordRange range = new RecordRange();
            int size = pageSize != null ? Integer.valueOf(pageSize) : 100;
            range.setRows(size);
            range.setFirstRow(size * (page != null ? Integer.valueOf(page) : 0));
            searchParameters.setRange(range);
        }

        return searchParameters;
    }

	private static String decode(String text) {
		return text.replaceAll("_semicolon_", ";").replaceAll("_comma_", ",").replaceAll("_colon_", ":").replaceAll("_underscore_", "_");
    }
}
