package org.simplifyworks.core.web.controller.rest;

import org.simplifyworks.core.service.LockManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/core/locks")
public class LockController {
	
	@Autowired
	private LockManager lockManager;
	
	@RequestMapping(method = RequestMethod.POST)
	public void tryLock(@RequestParam("name") String name) {
		lockManager.tryLock(name);
	}

	@RequestMapping(method = RequestMethod.PUT)
	public void holdLock(@RequestParam("name") String name) {
		lockManager.holdLock(name);
	}
	
	@RequestMapping(method = RequestMethod.DELETE)
	public void releaseLock(@RequestParam("name") String name) {
		lockManager.releaseLock(name);
	}
}
