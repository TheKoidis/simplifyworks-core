/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.controller.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@RestController
@RequestMapping(value = "/api/utils")
public class UtilsController {

	private static final Logger LOG = LoggerFactory.getLogger(UtilsController.class);
}
