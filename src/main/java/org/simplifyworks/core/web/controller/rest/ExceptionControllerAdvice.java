/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.core.web.controller.rest;

import org.activiti.engine.ActivitiException;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.core.exception.ValidationException;
import org.simplifyworks.security.model.domain.exception.AuthenticationException;
import org.simplifyworks.workflow.exception.WorkflowException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Wraps application exceptions for rest controllers ("exception decorator")
 *
 * @author Radek Tomiška (radek.tomiska@gmail.com)
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@ControllerAdvice
public class ExceptionControllerAdvice {

	private static final Logger LOG = LoggerFactory.getLogger(ExceptionControllerAdvice.class);

	// we have to use '400 Bad Request' instead of '401 Unauthorized' because of IE ...
	@ResponseBody
	@ExceptionHandler(AuthenticationException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public AuthenticationException handleAuthenticationException(AuthenticationException cause) {
		LOG.warn(cause.getMessage(), cause);
		
		return cause;
	}
	
	@ResponseBody
	@ExceptionHandler(CoreException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public CoreException handleCoreException(CoreException cause) {
		LOG.error(cause.getMessage(), cause);
		
		return cause;
	}

	@ResponseBody
	@ExceptionHandler(ValidationException.class)
	@ResponseStatus(HttpStatus.CONFLICT)
	public ValidationException handleValidationException(ValidationException cause) {
		LOG.warn(cause.getMessage(), cause);
		
		return cause;
	}


	@ResponseBody
	@ExceptionHandler(value = ActivitiException.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public CoreException handleActivitException(ActivitiException cause) throws Throwable {
		LOG.error(cause.getMessage(), cause);
				
		return new WorkflowException(cause);
	}
	
	@ResponseBody
	@ExceptionHandler(value = Exception.class)
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public CoreException defaultErrorHandler(Throwable cause) throws Throwable {
		LOG.error(cause.getMessage(), cause);
		
		// If the exception is annotated with @ResponseStatus rethrow it and let
		// the framework handle it - like the OrderNotFoundException example
		// at the start of this post.
		if (AnnotationUtils.findAnnotation(cause.getClass(), ResponseStatus.class) != null) {
			throw cause;
		}
		
		return new CoreException("Unexpected error ocurred", "unexpected-error", null, cause);
	}
}
