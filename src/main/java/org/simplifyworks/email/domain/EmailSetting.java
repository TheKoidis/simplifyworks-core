/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.domain;

import com.google.common.collect.Lists;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Email setting
 * 
 * @author Svanda
 */
public class EmailSetting {

	private String from;  // Sender
	private List<String> recipients;  //Emails addresses of recipients
	private List<String> cc;
	private List<String> bcc;
	private List<EmailAttachment> attachments; 
	private String subject; //Subject of mail
	private String html; //Message in html
	private String text; //Message in plain text
	private String charset; //Message

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return html;
	}

	public void setBody(String body) {
		this.html = body;
	}

	public List<EmailAttachment> getAttachments() {
		if (attachments == null) {
			attachments = new ArrayList<>();
		}
		return attachments;
	}

	public void setAttachments(List<EmailAttachment> attachments) {
		this.attachments = attachments;
	}

	public List<String> getRecipients() {
		if (recipients == null) {
			recipients = new ArrayList<String>();
		}
		return recipients;
	}

	public void setRecipients(List<String> recipients) {
		this.recipients = recipients;
	}
	
	public void addRecipient(String recipient) {
		getRecipients().add(recipient);
	}
	
	public void addAttachment(EmailAttachment attachment) {
		getAttachments().add(attachment);
	}
	
	public void addAttachment(InputStream attachment) {
		getAttachments().add(new EmailAttachment(attachment, null, null));
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public List<String> getCc() {
		if (cc == null) {
			cc = new ArrayList<>();
		}
		return cc;
	}

	public void setCc(List<String> cc) {
		this.cc = cc;
	}

	public List<String> getBcc() {
		if (bcc == null) {
			bcc = new ArrayList<>();
		}
		return bcc;
	}

	public void setBcc(List<String> bcc) {
		this.bcc = bcc;
	}

	public String getHtml() {
		return html;
	}

	public void setHtml(String html) {
		this.html = html;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCharset() {
		return charset;
	}

	public void setCharset(String charset) {
		this.charset = charset;
	}
}
