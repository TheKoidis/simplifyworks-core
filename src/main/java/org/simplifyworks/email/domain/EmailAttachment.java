/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.domain;

import java.io.InputStream;

/**
 * Email attachment
 * @author Svanda
 */
public class EmailAttachment {
	
	public static final String DEFAULT_NAME = "Attachment";
	public static final String DEFAULT_MIMETYPE = "application/octet-stream";
	
	private InputStream inputStream;
	private String name;
	private String mimetype;

	public EmailAttachment() {
	}

	public EmailAttachment(InputStream inputStream, String name, String mimetype) {
		this.inputStream = inputStream;
		this.name = name;
		this.mimetype = mimetype;
	}

	public InputStream getInputStream() {
		return inputStream;
	}

	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}

	public String getName() {
		if(name == null) {
			name = DEFAULT_NAME;
		}
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMimetype() {
		if(mimetype == null) {
			mimetype = DEFAULT_MIMETYPE;
		}
		return mimetype;
	}

	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}
}
