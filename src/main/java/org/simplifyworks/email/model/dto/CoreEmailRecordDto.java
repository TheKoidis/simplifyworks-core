/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.model.dto;

import org.simplifyworks.core.model.dto.AbstractDto;
import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Dto for email records
 *
 * @author svanda
 */
public class CoreEmailRecordDto extends AbstractDto {

	@Size(max = 255)
	private String emailFrom;
	@Size(max = 512)
	private String recipients;
	@Size(max = 512)
	private String cc;
	@Size(max = 512)
	private String bcc;
	@Size(max = 255)
	private String subject;
	private String message;
	@NotNull
	@Size(min = 1, max = 25)
	private String encoding;
	private CoreEmailRecordDto parent;
	private CoreEmailRecordDto previousVersion;
	private Date sent;
	@Size(max = 2000)
	private String sentLog;

	public CoreEmailRecordDto() {
	}

	public CoreEmailRecordDto(Long id) {
		super(id);
	}

	public String getEmailFrom() {
		return emailFrom;
	}

	public void setEmailFrom(String emailFrom) {
		this.emailFrom = emailFrom;
	}

	public String getRecipients() {
		return recipients;
	}

	public void setRecipients(String recipients) {
		this.recipients = recipients;
	}

	public String getCc() {
		return cc;
	}

	public void setCc(String cc) {
		this.cc = cc;
	}

	public String getBcc() {
		return bcc;
	}

	public void setBcc(String bcc) {
		this.bcc = bcc;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public CoreEmailRecordDto getParent() {
		return parent;
	}

	public void setParent(CoreEmailRecordDto parent) {
		this.parent = parent;
	}

	public CoreEmailRecordDto getPreviousVersion() {
		return previousVersion;
	}

	public void setPreviousVersion(CoreEmailRecordDto previousVersion) {
		this.previousVersion = previousVersion;
	}

	public Date getSent() {
		return sent;
	}

	public void setSent(Date sent) {
		this.sent = sent;
	}

	public String getSentLog() {
		return sentLog;
	}

	public void setSentLog(String sentLog) {
		this.sentLog = sentLog;
	}
}
