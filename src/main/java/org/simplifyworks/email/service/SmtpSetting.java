package org.simplifyworks.email.service;

/**
 * @author Radek Tomiška <tomiska@ders.cz>
 */
public interface SmtpSetting {


    public String getMailhost();


    public String getMailport();


    public String getUsername();


    public String getPassword();


    public String getFrom();


    public String getReplyto();

    /**
     * Use ssl communications
     *
     * @return
     */
    public boolean getUseSSL();

    void setMailhost(String mailhostAppParamKey);

    void setMailport(String mailportAppParamKey);

    void setUsername(String usernameAppParamKey);

    void setPassword(String passwordAppParamKey);

    void setReplyto(String replytoAppParamKey);

    void setUseSSL(boolean useSSLParamKey);

    void setFrom(String fromParamKey);
}
