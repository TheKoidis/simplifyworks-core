/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.service.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.simplifyworks.core.util.Mapper;
import org.simplifyworks.email.model.dto.CoreEmailRecordDto;
import org.simplifyworks.email.model.entity.CoreEmailRecord;
import org.simplifyworks.email.service.EmailRecordManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Save email
 * <p>
 * Attention: Attachments are save only on create email
 *
 * @author snitila@ders.cz
 */
@Service("emailRecordManager")
@Transactional
public class EmailRecordManagerImpl implements EmailRecordManager {

	private static final transient Logger logger = LoggerFactory.getLogger(EmailRecordManagerImpl.class);
	
	@Autowired
	private Mapper mapper;
	
	@PersistenceContext(unitName = "default")
	private EntityManager entityManager;

	@Override
	public CoreEmailRecordDto toDto(CoreEmailRecord entity) {
		return entity == null ? null : mapper.map(entity, CoreEmailRecordDto.class);
	}

	@Override
	public CoreEmailRecord toEntity(CoreEmailRecordDto dto) {
		return dto == null ? null : mapper.map(dto, CoreEmailRecord.class);
	}

	@Override
	public CoreEmailRecordDto create(CoreEmailRecordDto dto) {
		CoreEmailRecord entity = toEntity(dto);
		
		entityManager.persist(entity);
		
		return toDto(entity);
	}

}
