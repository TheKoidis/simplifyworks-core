/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.service.impl;

import java.io.ByteArrayInputStream;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;
import org.simplifyworks.conf.service.CoreAppSettingManager;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.model.entity.CoreAttachment;
import org.simplifyworks.email.domain.EmailAttachment;
import org.simplifyworks.email.domain.EmailSetting;
import org.simplifyworks.email.model.entity.CoreEmailRecord;
import org.simplifyworks.email.service.EmailRecordManager;
import org.simplifyworks.email.service.Emailer;
import org.simplifyworks.email.service.SmtpSetting;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Send email (SMTP setting is in CoreAppSetting)
 *
 * @author VŠ
 */
@Service
public class DefaultEmailer implements Emailer {

	private static final transient Logger logger = LoggerFactory.getLogger(DefaultEmailer.class);
	private SmtpSetting smtpSetting;
	private String parameterTestEnabled;
	private String parameterTestCopyRecipients;
	private String parameterTestRecipientsAllowed;
	private String parameterTestRecipientsAllowedSuffix;

	/**
	 * Save sent email
	 */
	@Autowired(required = false)
	private EmailRecordManager emailRecordManager;

	@Autowired
	private CoreAppSettingManager coreAppSettingManager;

	public DefaultEmailer() {

	}

	public DefaultEmailer(SmtpSetting smtpSetting) {
		this.smtpSetting = smtpSetting;
	}

	@Override
	public void init() {
		logger.info("Start emailer (test= " + isTestEnabled() + ")");
		parameterTestEnabled = coreAppSettingManager.getSetting(PARAMETER_TEST_ENABLED);
		parameterTestCopyRecipients = coreAppSettingManager.getSetting(PARAMETER_TEST_COPY_RECIPIENTS);
		parameterTestRecipientsAllowed = coreAppSettingManager.getSetting(PARAMETER_TEST_RECIPIENTS_ALLOWED);
		parameterTestRecipientsAllowedSuffix = coreAppSettingManager.getSetting(PARAMETER_TEST_RECIPIENTS_ALLOWED_SUFFIX);

	}

	@Override
	public Boolean sendEmail(String subject, String body, List<String> recipients, List<EmailAttachment> attachments) {
		EmailSetting emailSetting = new EmailSetting();
		emailSetting.setRecipients(recipients);
		emailSetting.setAttachments(attachments);
		emailSetting.setSubject(subject);
		emailSetting.setBody(body);
		return sendEmail(emailSetting);
	}

	@Override
	public Boolean sendEmail(String subject, String body, String recipient) {
		EmailSetting emailSetting = new EmailSetting();
		emailSetting.getRecipients().add(recipient);
		emailSetting.setSubject(subject);
		emailSetting.setBody(body);
		return sendEmail(emailSetting);
	}

	@Override
	public Boolean sendTestEmail(String recipient) {
		EmailSetting emailSetting = new EmailSetting();
		emailSetting.getRecipients().add(recipient);
		emailSetting.setSubject("sify.cz");
		emailSetting.setHtml("attention czech chars: -;+ěščřžýáíé=´¨-");
//		emailSetting.setHtml("<!DOCTYPE html><html><head><title>Test mail</title><meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\"></head><body>Testovací email z aplikace založené na <strong>Jenero</strong> frameworku, <a href=\"http://www.jenero.cz\">jenero.ders.cz</a></body></html>");
		emailSetting.addAttachment(new EmailAttachment(new ByteArrayInputStream("test9k2345678šěčřžýá".getBytes()), "test.txt", "text/plain"));
		return sendEmail(emailSetting);
	}

	@Override
	public Boolean sendEmail(String subject, String body, List<String> recipients) {
		EmailSetting emailSetting = new EmailSetting();
		emailSetting.setRecipients(recipients);
		emailSetting.setSubject(subject);
		emailSetting.setBody(body);
		return sendEmail(emailSetting);
	}

	/**
	 * Send email
	 *
	 * @param emailSetting
	 * @return
	 */
	@Override
	public Boolean sendEmail(final EmailSetting emailSetting) {
		return sendEmail(CoreEmailRecord.getInstance(emailSetting));
	}

	@Override
	public Boolean sendEmail(CoreEmailRecord emailRecord) {
		if (StringUtils.isEmpty(emailRecord.getEncoding())) {
			emailRecord.setEncoding(DEFAULT_CHARSET);
		}
//    if (ApplicationStage.Test.equals(applicationStage)) {
//      emailRecord.setSubject("TEST-" + emailRecord.getSubject());
//      emailRecord.setMessage("<h3 style=\"color: red\"> Tento email byl odeslán z TESTovacího prostředí a slouží pro testovací účely. </h3>" + emailRecord.getMessage());
//    }
//    if (ApplicationStage.Development.equals(applicationStage)) {
//      emailRecord.setSubject("DEV-" + emailRecord.getSubject());
//      emailRecord.setMessage("<h3 style=\"color: red\"> Tento email byl odeslán z vývojového prostředí (DEV) a slouží pro vývojové a testovací účely. </h3>" + emailRecord.getMessage());
//    }
		HtmlEmail email = createHtmlEmail(emailRecord.getMessage(), checkDoctypeAndHead(emailRecord.getMessage(), emailRecord.getEncoding()));
		setMailServerProperties(email);
		setCharset(email, emailRecord.getEncoding());
		setSubject(email, emailRecord.getSubject());
		addTo(email, Arrays.asList(emailRecord.getRecipients().split(CoreEmailRecord.RECIPIENT_SEPARATOR)));
		if (emailRecord.getBcc() != null) {
			addBcc(email, Arrays.asList(emailRecord.getBcc().split(CoreEmailRecord.RECIPIENT_SEPARATOR)));
		}
		if (emailRecord.getCc() != null) {
			addCc(email, Arrays.asList(emailRecord.getCc().split(CoreEmailRecord.RECIPIENT_SEPARATOR)));
		}
		emailRecord.setEmailFrom(setFrom(email, emailRecord.getEmailFrom()));

		// send
		boolean result = false;
		String resultInfo = null;
		try {
			for (CoreAttachment attachment : emailRecord.getAttachments()) {
				// Copy of input stream - we need it next
				//TODO
//				DataSource source = new ByteArrayDataSource(attachment.getInputData(), attachment.getMimetype()); //  + "; charset=" + "UTF-8"
//				email.attach(source, attachment.getName(), "");
			}
			// We send copies
			String testRecipients = parameterTestCopyRecipients;
			try {
				if (StringUtils.isNotBlank(testRecipients)) {
					if (email.getToAddresses().isEmpty() && email.getCcAddresses().isEmpty() && email.getBccAddresses().isEmpty()) {
						// None email was sent -  we can change subject
						logger.debug("None emails addresses are allowed [test enabled=" + isTestEnabled() + "], we send only copies to [" + testRecipients + "].");
						email.setSubject(email.getSubject() + " (Originally to: " + emailRecord.getRecipients() + (StringUtils.isEmpty(emailRecord.getCc()) ? "" : ", Cc:" + emailRecord.getCc()) + (StringUtils.isEmpty(emailRecord.getBcc()) ? "" : ", Bcc:" + emailRecord.getBcc()) + ")");
					} else if (logger.isDebugEnabled()) {
						logger.debug("Send on addresses [" + email.getToAddresses() + "] [" + email.getCcAddresses() + "] [" + email.getBccAddresses() + "] and send copies to [" + testRecipients + "].");
					}
					try {
						for (String t : testRecipients.split(",")) {
							email.addBcc(t);
						}
					} catch (Exception ex) {
						logger.warn("Error in addresses configuration [" + PARAMETER_TEST_COPY_RECIPIENTS + "] - [" + testRecipients + "], split character is comma", ex);
					}
				}
			} catch (Exception ex) {
				logger.warn("Failed to send copies - [" + testRecipients + "], split character is comma", ex);
			}
			//
			if ((!StringUtils.isEmpty(emailRecord.getRecipients()) || !StringUtils.isEmpty(emailRecord.getCc()) || !StringUtils.isEmpty(emailRecord.getBcc()))
					&& email.getToAddresses().isEmpty() && email.getCcAddresses().isEmpty() && email.getBccAddresses().isEmpty()) {
				if (logger.isInfoEnabled()) {
					logger.info("Email [" + emailRecord.getSubject() + "] on addresses [" + emailRecord.getRecipients() + "] [" + emailRecord.getCc() + "] [" + emailRecord.getBcc() + "] can not be sent, none valid recipients [test enabled= " + isTestEnabled() + "]");
				}
				result = false;
				return isTestEnabled();
			}
			String emailId = email.send();
			resultInfo = "Email sent [" + emailRecord.getSubject() + "][" + emailId + "] on addresses [" + email.getToAddresses() + "] [" + email.getCcAddresses() + "] [" + email.getBccAddresses() + "]";
			logger.info(resultInfo);
			result = true;
			return result;
//		} catch (IOException ex) {
//			resultInfo = "Could not send e-mail: " + ex.getMessage();
//			throw new CoreException("Could not send e-mail", ex);
		} catch (EmailException ex) {
			resultInfo = "Could not send e-mail: " + ex.getMessage();
			throw new CoreException("Could not send e-mail", ex);
		} finally {
			createEmailRecord(emailRecord, result, resultInfo);
//			for (CoreAttachment attachment : emailRecord.getAttachments()) {
//				IOUtils.closeQuietly(attachment.getInputData());
//			}
		}
	}

	private CoreEmailRecord createEmailRecord(CoreEmailRecord emailRecord, boolean result, String resultInfo) {
		if (emailRecordManager != null) {
			try {
				if (result) {
					emailRecord.setSent(new Date());
				}
				emailRecord.setSentLog(resultInfo);
				emailRecord = emailRecordManager.toEntity(emailRecordManager.create(emailRecordManager.toDto(emailRecord)));
			} catch (Exception ex) {

				logger.error("Failed to save sent email [" + emailRecord + "] [result:" + result + "] [" + resultInfo + "]", ex);
			}
		}
		return emailRecord;
	}

	protected HtmlEmail createHtmlEmail(String text, String html) {
		HtmlEmail email = new HtmlEmail();
		try {
			email.setHtmlMsg(html);
			if (text != null) { // for email clients that don't support html
				email.setTextMsg(text);
			}
			return email;
		} catch (EmailException e) {
			throw new CoreException("Could not create HTML email", e);
		}
	}

	protected void setMailServerProperties(Email email) {
		String host = getSmtpSetting().getMailhost();
		if (host == null) {
			throw new CoreException("Could not send email: no SMTP host is configured");
		}
		email.setHostName(host);

		if (StringUtils.isNotEmpty(getSmtpSetting().getMailport())) {
			int port = Integer.valueOf(getSmtpSetting().getMailport());
			email.setSmtpPort(port);
		}
		email.setSSL(getSmtpSetting().getUseSSL());
		//
		String user = getSmtpSetting().getUsername();
		String password = getSmtpSetting().getPassword();
		if (StringUtils.isNotEmpty(user) && StringUtils.isNotEmpty(password)) {
			email.setAuthentication(user, password);
		}
		//
		try {
			if (StringUtils.isNotBlank(getSmtpSetting().getReplyto())) {
				email.addReplyTo(getSmtpSetting().getReplyto());
			}
		} catch (EmailException ex) {
			logger.error("Could not add " + getSmtpSetting().getReplyto() + " as replyTo", ex);
		}
	}

	protected void addTo(Email email, List<String> to) {
		for (String t : to) {
			if (skipEmailAddress(t)) {
				continue;
			}
			try {
				email.addTo(t);
			} catch (EmailException e) {
				throw new CoreException("Could not add " + t + " as recipient", e);
			}
		}
	}

	protected String setFrom(Email email, String from) {
		String fromAddres;
		if (StringUtils.isNotEmpty(from)) {
			fromAddres = from;
		} else if (StringUtils.isNotEmpty(getSmtpSetting().getFrom())) { // use default configured from address in process engine config
			fromAddres = getSmtpSetting().getFrom();
		} else { // use default configured from address in process engine config
			fromAddres = getSmtpSetting().getUsername();
		}
		try {
			email.setFrom(fromAddres);
			return fromAddres;
		} catch (EmailException e) {
			throw new CoreException("Could not set " + fromAddres + " as from address in email", e);
		}
	}

	protected void addCc(Email email, List<String> cc) {
		for (String c : cc) {
			if (skipEmailAddress(c)) {
				continue;
			}
			try {
				email.addCc(c);
			} catch (EmailException e) {
				throw new CoreException("Could not add " + c + " as cc recipient", e);
			}
		}
	}

	protected void addBcc(Email email, List<String> bcc) {
		for (String b : bcc) {
			if (skipEmailAddress(b)) {
				continue;
			}
			try {
				email.addBcc(b);
			} catch (EmailException e) {
				throw new CoreException("Could not add " + b + " as bcc recipient", e);
			}

		}
	}

	protected void setSubject(Email email, String subject) {
		email.setSubject(subject != null ? subject : "");
	}

	protected void setCharset(Email email, String charSetStr) {
		if (StringUtils.isNotEmpty(charSetStr)) {
			email.setCharset(charSetStr);
		}
	}

	/**
	 * Skip email addresses - "white list"
	 *
	 * @param emailAddress
	 * @return
	 */
	private boolean skipEmailAddress(String emailAddress) {
		if (StringUtils.isBlank(emailAddress)) {
			return true;
		}
		if (isTestEnabled() && !isTestAllowed(emailAddress)) {
			logger.info("Skip send to addresses [" + emailAddress + "] -  [tesnt enabled= " + isTestEnabled() + "].");
			return true;
		}
		return false;
	}

	/**
	 * Check header, doctype
	 *
	 * @param message
	 * @param encoding
	 * @return
	 */
	private String checkDoctypeAndHead(String message, String encoding) {
		if (message == null) {
			return null;
		}

		if (!message.matches(".*<body>.*")) {
			message = String.format("<body>%s</body>", message);
		}

		if (!message.matches(".*<head>.*")) {
			//add head
			int index = message.indexOf("<body>");
			String begin = message.substring(0, index);
			String end = message.substring(index);

			message = String.format("%s<head></head>%s", begin, end);
		}

		if (!message.matches(".*<meta http-equiv=.*")) {
			//add missing informations to head about content and coding
			int headIndex = message.indexOf("<head>") + 6;
			String begin = message.substring(0, headIndex);
			String end = message.substring(headIndex);
			message = String.format("%s%s%s", begin, "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=" + encoding + "\"/>", end);
		}

		if (!message.matches(".*<html>.*")) {
			message = String.format("<html>%s</html>", message);
		}

		if (!StringUtils.startsWith(message, "<!DOCTYPE")) {
			message = String.format("<!DOCTYPE html>%s", message);
		}

		if (!StringUtils.startsWith(message, "<?xml version=")) {
			//Search charset, if is no found, than is use from input parameter
			String xmlEncoding = encoding;
			Matcher m = Pattern.compile(".*charset=([^\"]*)\".*").matcher(message);
			if (m.matches()) {
				xmlEncoding = m.group(1);
			}
			message = String.format("<?xml version=\"1.0\" encoding=\"%s\"?>%s", xmlEncoding, message);
		}
		return message;
	}

	/**
	 * @param smtpSetting
	 */
	public void setSmtpSetting(SmtpSetting smtpSetting) {
		this.smtpSetting = smtpSetting;
	}

	//    "emailer.smtp.mailhost";
//    private String mailportAppParamKey = "emailer.smtp.mailport";
//    private String usernameAppParamKey = "emailer.smtp.username";
//    private String passwordAppParamKey = "emailer.smtp.password";
//    private String replytoAppParamKey = "emailer.smtp.replyto";
//    private String useSSLParamKey = "emailer.smtp.useSSL";
//    private String fromParamKey = "emailer.smtp.from";
	private SmtpSetting getSmtpSetting() {
		if (smtpSetting == null) {
			if (coreAppSettingManager == null) {
				throw new IllegalStateException("Emailer not have smtp configuration.");
			}
			smtpSetting = new SmtpSettingImpl();
			smtpSetting.setMailhost(coreAppSettingManager.getSetting("emailer.smtp.mailhost"));
			smtpSetting.setMailport(coreAppSettingManager.getSetting("emailer.smtp.mailport"));
			smtpSetting.setUsername(coreAppSettingManager.getSetting("emailer.smtp.username"));
			smtpSetting.setPassword(coreAppSettingManager.getSetting("emailer.smtp.password"));
			smtpSetting.setReplyto(coreAppSettingManager.getSetting("emailer.smtp.replyto"));
			smtpSetting.setUseSSL(coreAppSettingManager.getBooleanSetting("emailer.smtp.useSSL"));
			smtpSetting.setFrom(coreAppSettingManager.getSetting("emailer.smtp.from"));
		}
		return smtpSetting;
	}

	private boolean isTestEnabled() {
//		if (applicationSettingsManager != null) {
//			return applicationSettingsManager.getBooleanSetting(PARAMETER_TEST_ENABLED, !applicationStage.equals(ApplicationStage.Production));
//		}
//		return !applicationStage.equals(ApplicationStage.Production);
		return true;
	}

	private String getTestRecipientsAllowedSuffix() {
//		if (applicationSettingsManager != null) {
//			return applicationSettingsManager.getSetting(PARAMETER_TEST_RECIPIENTS_ALLOWED_SUFFIX, DEFAULT_TEST_RECIPIENTS_ALLOWED_SUFFIX);
//		}
		return DEFAULT_TEST_RECIPIENTS_ALLOWED_SUFFIX;
	}

	private boolean isTestAllowed(String emailAddress) {
		if (StringUtils.isNotBlank(getTestRecipientsAllowedSuffix()) && emailAddress.endsWith(getTestRecipientsAllowedSuffix())) {
			return true;
		}
		String allowedRecipients = parameterTestRecipientsAllowed;
		if (StringUtils.isNotEmpty(allowedRecipients)) {
			for (String t : allowedRecipients.split(",")) {
				if (emailAddress.equals(t)) {
					return true;
				}
			}
		}
		return false;
	}

	public void setEmailRecordManager(EmailRecordManager emailRecordManager) {
		this.emailRecordManager = emailRecordManager;
	}
}
