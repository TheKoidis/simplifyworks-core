/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.simplifyworks.email.service;

import org.simplifyworks.email.model.dto.CoreEmailRecordDto;
import org.simplifyworks.email.model.entity.CoreEmailRecord;

/**
 *
 * @author snitila@ders.cz
 */
public interface EmailRecordManager {
  
	public CoreEmailRecordDto toDto(CoreEmailRecord entity);
	
	public CoreEmailRecord toEntity(CoreEmailRecordDto dto);
	
	public CoreEmailRecordDto create(CoreEmailRecordDto dto);
	
}
