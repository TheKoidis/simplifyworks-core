package org.simplifyworks.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import org.junit.Assert;
import org.testng.annotations.Test;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
@Test
public class MapperTest {

	public MapperTest() {
	}

	@Test
	public void testMap() {
		MapperTestEntity entity = new MapperTestEntity();

		//simple values
		entity.setBigDecimalValue(BigDecimal.valueOf(3.1415926535));
		entity.setBigIntegerValue(BigInteger.valueOf(31415926535L));
		entity.setBooleanObjValue(Boolean.FALSE);
		entity.setBooleanValue(true);
		entity.setEnumValue(MapperTestEnum.VALUE2);
		entity.setDateArrayValue(new Date[]{new Date(12345678L), new Date(1234567890L)});
		entity.setDateValue(new Date(1234567890123456789L));
		entity.setDoubleObjValue(Double.valueOf("9.9989"));
		entity.setDoubleValue(9.87654);
		entity.setFloatObjValue(Float.valueOf("5.56789"));
		entity.setFloatValue(5.689899887F);
		entity.setIntArrayValue(new int[]{1, 2, 4, 5, 6, 7, 8});
		entity.setIntValue(125);
		entity.setIntegerValue(Integer.valueOf("465465465"));
		entity.setLongObjValue(Long.MIN_VALUE);
		entity.setLongValue(98989898989898L);
		entity.setStringArrayValue(new String[]{"one", "two", "three"});
		entity.setStringValue("test text");

		ArrayList<MapperTestEntity> collectionValue = new ArrayList<MapperTestEntity>();
		MapperTestEntity subEntity = new MapperTestEntity();
		subEntity.setStringValue("subentity test");
		collectionValue.add(subEntity);
		collectionValue.add(subEntity);
		entity.setCollectionValue(collectionValue);

		entity.setComplexArrayValue(new MapperTestEntity[]{subEntity, subEntity});

		ArrayList<String> listStringValue = new ArrayList<String>();
		listStringValue.add("five");
		listStringValue.add("six");
		listStringValue.add("seven");
		entity.setListStringValue(listStringValue);

		HashSet<MapperTestEntity> setComplexSameValue = new HashSet<MapperTestEntity>();
		setComplexSameValue.add(subEntity);
		entity.setSetComplexSameValue(setComplexSameValue);

		HashSet<MapperTestEnum> setEnumValue = new HashSet<MapperTestEnum>();
		setEnumValue.add(MapperTestEnum.VALUE1);
		setEnumValue.add(MapperTestEnum.VALUE3);
		entity.setSetEnumValue(setEnumValue);

		//the same instance in relation (cyclic relation)
		entity.setSameInstance(entity);

		Mapper mapper = new Mapper();
		MapperTestDto dto = new MapperTestDto();
		mapper.map(entity, dto);

		MapperTestEntity newEntity = new MapperTestEntity();
		mapper.map(dto, newEntity);

		check(entity, newEntity);
		Assert.assertSame(newEntity.getSameInstance(), newEntity);
	}

	@Test
	public void testMapNull() {
		MapperTestEntity entity = new MapperTestEntity();

		Mapper mapper = new Mapper();
		MapperTestDto dto = new MapperTestDto();
		mapper.map(entity, dto);

		MapperTestEntity newEntity = new MapperTestEntity();
		mapper.map(dto, newEntity);

		check(entity, newEntity);
	}

	private void check(MapperTestEntity entity, MapperTestEntity newEntity) {
		Assert.assertEquals(entity.getBigDecimalValue(), newEntity.getBigDecimalValue());
		Assert.assertEquals(entity.getBigIntegerValue(), newEntity.getBigIntegerValue());
		Assert.assertEquals(entity.getBooleanObjValue(), newEntity.getBooleanObjValue());
		Assert.assertEquals(entity.getDateValue(), newEntity.getDateValue());
		Assert.assertEquals(entity.getDoubleObjValue(), newEntity.getDoubleObjValue());
		Assert.assertEquals(entity.getDoubleValue(), newEntity.getDoubleValue(), 0.00000000000001);
		Assert.assertEquals(entity.getFloatObjValue(), newEntity.getFloatObjValue());
		Assert.assertEquals(entity.getFloatValue(), newEntity.getFloatValue(), 0.00000000000001);
		Assert.assertEquals(entity.getIntValue(), newEntity.getIntValue());
		Assert.assertEquals(entity.getIntegerValue(), newEntity.getIntegerValue());
		Assert.assertEquals(entity.getLongObjValue(), newEntity.getLongObjValue());
		Assert.assertEquals(entity.getLongValue(), newEntity.getLongValue());
		Assert.assertEquals(entity.getStringValue(), newEntity.getStringValue());
		Assert.assertEquals(entity.getEnumValue(), newEntity.getEnumValue());

		Assert.assertEquals(entity.getSetEnumValue(), newEntity.getSetEnumValue());
		Assert.assertEquals(entity.getSetComplexSameValue(), newEntity.getSetComplexSameValue());
		Assert.assertEquals(entity.getListStringValue(), newEntity.getListStringValue());
		Assert.assertEquals(entity.getCollectionValue(), newEntity.getCollectionValue());
		Assert.assertArrayEquals(entity.getStringArrayValue(), newEntity.getStringArrayValue());
		Assert.assertArrayEquals(entity.getDateArrayValue(), newEntity.getDateArrayValue());
		Assert.assertArrayEquals(entity.getComplexArrayValue(), newEntity.getComplexArrayValue());
		Assert.assertArrayEquals(entity.getIntArrayValue(), newEntity.getIntArrayValue());
	}


}
