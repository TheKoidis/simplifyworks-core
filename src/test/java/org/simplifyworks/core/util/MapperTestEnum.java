package org.simplifyworks.core.util;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public enum MapperTestEnum {
	VALUE1, VALUE2, VALUE3;
}
