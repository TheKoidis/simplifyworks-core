package org.simplifyworks.core.util;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Martin Široký <siroky@ders.cz>
 */
public class MapperTestEntity {

	private int intValue;
	private long longValue;
	private boolean booleanValue;
	private double doubleValue;
	private float floatValue;
	private Integer integerValue;
	private Long longObjValue;
	private Boolean booleanObjValue;
	private Double doubleObjValue;
	private Float floatObjValue;
	private Date dateValue;
	private String stringValue;
	private MapperTestEnum enumValue;
	private Set<MapperTestEnum> setEnumValue;
	private List<String> listStringValue;
	private Set<MapperTestEntity> setComplexSameValue;
	private Collection<MapperTestEntity> collectionValue;
	private String[] stringArrayValue;
	private int[] intArrayValue;
	private Date[] dateArrayValue;
	private MapperTestEntity[] complexArrayValue;
	private BigInteger bigIntegerValue;
	private BigDecimal bigDecimalValue;
	private MapperTestEntity sameInstance;

	public int getIntValue() {
		return intValue;
	}

	public void setIntValue(int intValue) {
		this.intValue = intValue;
	}

	public long getLongValue() {
		return longValue;
	}

	public void setLongValue(long longValue) {
		this.longValue = longValue;
	}

	public boolean isBooleanValue() {
		return booleanValue;
	}

	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}

	public double getDoubleValue() {
		return doubleValue;
	}

	public void setDoubleValue(double doubleValue) {
		this.doubleValue = doubleValue;
	}

	public float getFloatValue() {
		return floatValue;
	}

	public void setFloatValue(float floatValue) {
		this.floatValue = floatValue;
	}

	public Integer getIntegerValue() {
		return integerValue;
	}

	public void setIntegerValue(Integer integerValue) {
		this.integerValue = integerValue;
	}

	public Long getLongObjValue() {
		return longObjValue;
	}

	public void setLongObjValue(Long longObjValue) {
		this.longObjValue = longObjValue;
	}

	public Boolean getBooleanObjValue() {
		return booleanObjValue;
	}

	public void setBooleanObjValue(Boolean booleanObjValue) {
		this.booleanObjValue = booleanObjValue;
	}

	public Double getDoubleObjValue() {
		return doubleObjValue;
	}

	public void setDoubleObjValue(Double doubleObjValue) {
		this.doubleObjValue = doubleObjValue;
	}

	public Float getFloatObjValue() {
		return floatObjValue;
	}

	public void setFloatObjValue(Float floatObjValue) {
		this.floatObjValue = floatObjValue;
	}

	public Date getDateValue() {
		return dateValue;
	}

	public void setDateValue(Date dateValue) {
		this.dateValue = dateValue;
	}

	public String getStringValue() {
		return stringValue;
	}

	public void setStringValue(String stringValue) {
		this.stringValue = stringValue;
	}

	public List<String> getListStringValue() {
		return listStringValue;
	}

	public void setListStringValue(List<String> listStringValue) {
		this.listStringValue = listStringValue;
	}

	public Set<MapperTestEntity> getSetComplexSameValue() {
		return setComplexSameValue;
	}

	public void setSetComplexSameValue(Set<MapperTestEntity> setComplexSameValue) {
		this.setComplexSameValue = setComplexSameValue;
	}

	public Collection<MapperTestEntity> getCollectionValue() {
		return collectionValue;
	}

	public void setCollectionValue(Collection<MapperTestEntity> collectionValue) {
		this.collectionValue = collectionValue;
	}

	public String[] getStringArrayValue() {
		return stringArrayValue;
	}

	public void setStringArrayValue(String[] stringArrayValue) {
		this.stringArrayValue = stringArrayValue;
	}

	public int[] getIntArrayValue() {
		return intArrayValue;
	}

	public void setIntArrayValue(int[] intArrayValue) {
		this.intArrayValue = intArrayValue;
	}

	public Date[] getDateArrayValue() {
		return dateArrayValue;
	}

	public void setDateArrayValue(Date[] dateArrayValue) {
		this.dateArrayValue = dateArrayValue;
	}

	public MapperTestEntity[] getComplexArrayValue() {
		return complexArrayValue;
	}

	public void setComplexArrayValue(MapperTestEntity[] complexArrayValue) {
		this.complexArrayValue = complexArrayValue;
	}

	public BigInteger getBigIntegerValue() {
		return bigIntegerValue;
	}

	public void setBigIntegerValue(BigInteger bigIntegerValue) {
		this.bigIntegerValue = bigIntegerValue;
	}

	public BigDecimal getBigDecimalValue() {
		return bigDecimalValue;
	}

	public void setBigDecimalValue(BigDecimal bigDecimalValue) {
		this.bigDecimalValue = bigDecimalValue;
	}

	public MapperTestEnum getEnumValue() {
		return enumValue;
	}

	public void setEnumValue(MapperTestEnum enumValue) {
		this.enumValue = enumValue;
	}

	public Set<MapperTestEnum> getSetEnumValue() {
		return setEnumValue;
	}

	public void setSetEnumValue(Set<MapperTestEnum> setEnumValue) {
		this.setEnumValue = setEnumValue;
	}

	public MapperTestEntity getSameInstance() {
		return sameInstance;
	}

	public void setSameInstance(MapperTestEntity sameInstance) {
		this.sameInstance = sameInstance;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final MapperTestEntity other = (MapperTestEntity) obj;
		if (this.intValue != other.intValue) {
			return false;
		}
		if (this.longValue != other.longValue) {
			return false;
		}
		if (this.booleanValue != other.booleanValue) {
			return false;
		}
		if (Double.doubleToLongBits(this.doubleValue) != Double.doubleToLongBits(other.doubleValue)) {
			return false;
		}
		if (Float.floatToIntBits(this.floatValue) != Float.floatToIntBits(other.floatValue)) {
			return false;
		}
		if (!Objects.equals(this.stringValue, other.stringValue)) {
			return false;
		}
		if (!Objects.equals(this.integerValue, other.integerValue)) {
			return false;
		}
		if (!Objects.equals(this.longObjValue, other.longObjValue)) {
			return false;
		}
		if (!Objects.equals(this.booleanObjValue, other.booleanObjValue)) {
			return false;
		}
		if (!Objects.equals(this.doubleObjValue, other.doubleObjValue)) {
			return false;
		}
		if (!Objects.equals(this.floatObjValue, other.floatObjValue)) {
			return false;
		}
		if (!Objects.equals(this.dateValue, other.dateValue)) {
			return false;
		}
		if (this.enumValue != other.enumValue) {
			return false;
		}
		if (!Objects.equals(this.setEnumValue, other.setEnumValue)) {
			return false;
		}
		if (!Objects.equals(this.listStringValue, other.listStringValue)) {
			return false;
		}
		if (!Objects.equals(this.setComplexSameValue, other.setComplexSameValue)) {
			return false;
		}
		if (!Objects.equals(this.collectionValue, other.collectionValue)) {
			return false;
		}
		if (!Arrays.deepEquals(this.stringArrayValue, other.stringArrayValue)) {
			return false;
		}
		if (!Arrays.equals(this.intArrayValue, other.intArrayValue)) {
			return false;
		}
		if (!Arrays.deepEquals(this.dateArrayValue, other.dateArrayValue)) {
			return false;
		}
		if (!Arrays.deepEquals(this.complexArrayValue, other.complexArrayValue)) {
			return false;
		}
		if (!Objects.equals(this.bigIntegerValue, other.bigIntegerValue)) {
			return false;
		}
		if (!Objects.equals(this.bigDecimalValue, other.bigDecimalValue)) {
			return false;
		}
		if (!Objects.equals(this.sameInstance, other.sameInstance)) {
			return false;
		}
		return true;
	}

}
