package org.simplifyworks.core.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.core.model.domain.ApplyFilterTypeEnum;
import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.core.model.dto.BaseDto;
import org.simplifyworks.core.model.dto.BaseFilterDto;
import org.simplifyworks.core.model.entity.BaseEntity;
import org.simplifyworks.core.service.SearchFilterManager;
import org.simplifyworks.repository.DbDataRepository;

/**
 * Test for {@link SearchManagerImpl}
 *
 * @author kakrda
 */
public class SearchManagerTest extends AbstractTest {

	private static final int FIRST_RESULT = 0;
	private static final int PAGE_NUMBER = 1;
	private static final int PAGE_SIZE = 10;
	private static final int RESULTS_COUNT = 3;
	private static final Class<BaseEntity> entityClass = BaseEntity.class;
	private static final List<BaseEntity> entities = new ArrayList<>();

	@Mock
	private CriteriaBuilder criteriaBuilder;

	@Mock
	private CriteriaQuery<BaseEntity> criteriaQuery;

	@Mock
	private Root<BaseEntity> root;

	@Mock
	private TypedQuery query;

	@Mock
	protected DbDataRepository dataRepository;

//	@Mock
//	protected EntityManager entityManager;

	@Mock
	private BaseEntity entity;

	@Mock
	private BaseDto dto;

	@Mock
	private BaseFilterDto filter;

	@Mock
	private SearchFilterManager<BaseDto, BaseEntity, BaseFilterDto> manager;

	@InjectMocks
	private SearchManagerImpl searchManager = new SearchManagerImpl();

	@Before
	public void init() {
		entities.add(entity);
		entities.add(entity);
		entities.add(entity);
	}

	/**
	 * Happy day scenario (no filter)
	 */
	@Test
	public void testGetAllWrapped() {
		// mocks and data initialization
		when(dataRepository.getCriteriaBuilder()).thenReturn(criteriaBuilder);
		when(manager.getEntityClass()).thenReturn(entityClass);
		when(criteriaBuilder.createQuery(entityClass)).thenReturn(criteriaQuery);
		when(criteriaQuery.from(entityClass)).thenReturn(root);
		when(criteriaQuery.select(root)).thenReturn(criteriaQuery);

		when(dataRepository.createQuery(criteriaQuery)).thenReturn(query);
		when(filter.getSorts()).thenReturn(null);
		when(filter.getPageNumber()).thenReturn(PAGE_NUMBER);
		when(filter.getPageSize()).thenReturn(PAGE_SIZE);
		when(query.getResultList()).thenReturn(entities);
		when(manager.toDto(entity)).thenReturn(dto);

		SearchManagerImpl searchManagerSpy = Mockito.spy(searchManager);
		Mockito.doReturn(criteriaQuery).when(searchManagerSpy).setCriteriaQuery(root, criteriaQuery, filter, manager, ApplyFilterTypeEnum.RESULTS);
		Mockito.doReturn(Long.valueOf(RESULTS_COUNT)).when(searchManagerSpy).getRecordsCount(filter, manager);

		// execution of tested method
		ExtendedArrayList<BaseDto> result = searchManagerSpy.getAllWrapped(filter, manager);

		// result verification
		assertEquals(result.size(), RESULTS_COUNT);

		verify(dataRepository).getCriteriaBuilder();
		verify(manager).getEntityClass();
		verify(criteriaBuilder).createQuery(entityClass);
		verify(criteriaQuery).from(entityClass);
		verify(criteriaQuery).select(root);

		verify(dataRepository).createQuery(criteriaQuery);
		verify(filter).getSorts();
		verify(filter).getPageNumber();
		verify(filter, Mockito.times(2)).getPageSize();
		verify(query).setFirstResult(FIRST_RESULT);
		verify(query).setMaxResults(PAGE_SIZE);
		verify(query).getResultList();
		verify(manager, Mockito.times(3)).toDto(entity);

		verify(searchManagerSpy).setCriteriaQuery(root, criteriaQuery, filter, manager, ApplyFilterTypeEnum.RESULTS);
		verify(searchManagerSpy).getRecordsCount(filter, manager);
	}

}
