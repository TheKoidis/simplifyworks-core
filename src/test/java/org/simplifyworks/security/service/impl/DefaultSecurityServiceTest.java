package org.simplifyworks.security.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.security.model.domain.AbstractAuthentication;
import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.simplifyworks.uam.service.CoreFlatOrganizationManager;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * Test for {@link DefaultSecurityService}
 * 
 * @author kakrda
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultSecurityServiceTest extends AbstractTest {

	private static final String CURRENT_USERNAME = "current";
	private static final String ORIGINAL_USERNAME = "original";
	private static final CorePersonDto PERSON = new CorePersonDto();
	private static final String ROLE_NAME = "testAdmin";
	private static final String ORGANIZATION_NAME = "testOrganization";
	
	private static final List<String> ROLES = Arrays.asList(ROLE_NAME);
	private static final Collection<RoleOrganizationGrantedAuthority> AUTHORITIES = Arrays.asList(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));
	
	private static final JwtUserAuthentication AUTHENTICATION = new JwtUserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, PERSON, new Date(), ROLES, AUTHORITIES);

	@Mock
	private SecurityContext contextMock;

	@Mock
	private CoreFlatRoleManager flatRoleManagerMock;

	@Mock
	private CoreFlatOrganizationManager flatOrganizationManagerMock;

	@InjectMocks
	private DefaultSecurityService defaultSecurityService = new DefaultSecurityService();

	@Before
	public void init() {
		SecurityContextHolder.setContext(contextMock);
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testIsAuthenticated() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		boolean result = defaultSecurityService.isAuthenticated();

		// result verification
		assertTrue(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetAuthentication() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		AbstractAuthentication result = defaultSecurityService.getAuthentication();

		// result verification
		assertEquals(result.getCurrentUsername(), AUTHENTICATION.getCurrentUsername());
		assertEquals(result.getOriginalUsername(), AUTHENTICATION.getOriginalUsername());
		assertEquals(result.getAuthorities(), AUTHENTICATION.getAuthorities());
		assertEquals(result.getDetails(), AUTHENTICATION.getDetails());

		verify(contextMock).getAuthentication();
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetUsername1() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		String result = defaultSecurityService.getUsername();

		// result verification
		assertEquals(result, AUTHENTICATION.getCurrentUsername());

		verify(contextMock).getAuthentication();
	}

	/**
	 * No authentication
	 */
	@Test
	public void testGetUsername2() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(null);

		// execution of tested method
		String result = defaultSecurityService.getUsername();

		// result verification
		assertEquals(result, null);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetOriginalUsername1() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		String result = defaultSecurityService.getOriginalUsername();

		// result verification
		assertEquals(result, AUTHENTICATION.getOriginalUsername());

		verify(contextMock).getAuthentication();
	}

	/**
	 * No authentication
	 */
	@Test
	public void testGetOriginalUsername2() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(null);

		// execution of tested method
		String result = defaultSecurityService.getOriginalUsername();

		// result verification
		assertEquals(result, null);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testIsSwitchedUser() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		boolean result = defaultSecurityService.isSwitchedUser();

		// result verification
		assertTrue(result);

		verify(contextMock).getAuthentication();
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testIsSwitchedUser1() {
		// mocks and data initialization
		JwtUserAuthentication authentication = new JwtUserAuthentication(ORIGINAL_USERNAME, ORIGINAL_USERNAME, PERSON, new Date(), Collections.emptyList(), Collections.emptyList());
		
		when(contextMock.getAuthentication()).thenReturn(authentication);

		// execution of tested method
		boolean result = defaultSecurityService.isSwitchedUser();

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetAllRoleNames1() {
		// mocks and data initialization
		List<String> roleNames = new ArrayList<>();
		roleNames.add(ROLE_NAME);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllDescendantRoleNames(roleNames)).thenReturn(roleNames);

		// execution of tested method
		Set<String> result = defaultSecurityService.getAllRoleNames();

		// result verification
		assertTrue(!result.isEmpty());

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllDescendantRoleNames(roleNames);
	}

	/**
	 * No role names
	 */
	@Test
	public void testGetAllRoleNames2() {
		// mocks and data initialization
		List<String> roleNames = new ArrayList<>();
		roleNames.add(ROLE_NAME);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllDescendantRoleNames(roleNames)).thenReturn(Collections.emptyList());

		// execution of tested method
		Set<String> result = defaultSecurityService.getAllRoleNames();

		// result verification
		assertTrue(result.isEmpty());

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllDescendantRoleNames(roleNames);
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testHasAnyRole1() {
		// mocks and data initialization
		List<String> roleNames = new ArrayList<>();
		roleNames.add(ROLE_NAME);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllDescendantRoleNames(roleNames)).thenReturn(roleNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasAnyRole(ROLE_NAME);

		// result verification
		assertTrue(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllDescendantRoleNames(roleNames);
	}

	/**
	 * Empty role names
	 */
	@Test
	public void testHasAnyRole2() {
		try {
			// execution of tested method
			defaultSecurityService.hasAnyRole(new String[] {});
			fail();
		} catch (IllegalArgumentException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testHasAllRoles1() {
		// mocks and data initialization
		List<String> roleNames = new ArrayList<>();
		roleNames.add(ROLE_NAME);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllDescendantRoleNames(roleNames)).thenReturn(roleNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasAllRoles(ROLE_NAME);

		// result verification
		assertTrue(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllDescendantRoleNames(roleNames);
	}

	/**
	 * Empty role names
	 */
	@Test
	public void testHasAllRoles2() {
		try {
			// execution of tested method
			defaultSecurityService.hasAllRoles(new String[] {});
			fail();
		} catch (IllegalArgumentException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testHasExplicitRoleInOrganization1() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		boolean result = defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);

		// result verification
		assertTrue(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * No role name
	 */
	@Test
	public void testHasExplicitRoleInOrganization2() {
		try {
			// execution of tested method
			defaultSecurityService.hasExplicitRoleInOrganization(null, ORGANIZATION_NAME);
			fail();
		} catch (IllegalArgumentException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * No organization name
	 */
	@Test
	public void testHasExplicitRoleInOrganization3() {
		try {
			// execution of tested method
			defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, null);
			fail();
		} catch (IllegalArgumentException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * No authority
	 */
	@Test
	public void testHasExplicitRoleInOrganization4() {
		// mocks and data initialization
		JwtUserAuthentication authentication = new JwtUserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, PERSON, new Date(), Collections.emptyList(), Collections.emptyList());
		
		when(contextMock.getAuthentication()).thenReturn(authentication);

		// execution of tested method
		boolean result = defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Wrong role name
	 */
	@Test
	public void testHasExplicitRoleInOrganization5() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		boolean result = defaultSecurityService.hasExplicitRoleInOrganization("wrongName", ORGANIZATION_NAME);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Wrong organization name
	 */
	@Test
	public void testHasExplicitRoleInOrganization6() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		boolean result = defaultSecurityService.hasExplicitRoleInOrganization(ROLE_NAME, "wrongName");

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Wrong role name and organization name
	 */
	@Test
	public void testHasExplicitRoleInOrganization7() {
		// mocks and data initialization
		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);

		// execution of tested method
		boolean result = defaultSecurityService.hasExplicitRoleInOrganization("wrongName", "wrongName");

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Happy day scenario
	 */
	@Test
	public void testHasRoleInOrganization1() {
		// mocks and data initialization
		List<String> roleNames = Collections.singletonList(ROLE_NAME);
		List<String> organizationNames = Collections.singletonList(ORGANIZATION_NAME);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllAncestorRoleNames(ROLE_NAME)).thenReturn(roleNames);
		when(flatOrganizationManagerMock.findAllAncestorOrganizationCodes(ORGANIZATION_NAME)).thenReturn(organizationNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);

		// result verification
		assertTrue(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllAncestorRoleNames(ROLE_NAME);
		verify(flatOrganizationManagerMock).findAllAncestorOrganizationCodes(ORGANIZATION_NAME);
	}

	/**
	 * No role name
	 */
	@Test
	public void testHasRoleInOrganization2() {
		try {
			// execution of tested method
			defaultSecurityService.hasRoleInOrganization(null, ORGANIZATION_NAME);
			fail();
		} catch (IllegalArgumentException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * No organization name
	 */
	@Test
	public void testHasRoleInOrganization3() {
		try {
			// execution of tested method
			defaultSecurityService.hasRoleInOrganization(ROLE_NAME, null);
			fail();
		} catch (IllegalArgumentException e) {
			// e.printStackTrace();
		}
	}

	/**
	 * No authority
	 */
	@Test
	public void testHasRoleInOrganization4() {
		// mocks and data initialization
		JwtUserAuthentication authentication = new JwtUserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, PERSON, new Date(), Collections.emptyList(), Collections.emptyList());

		when(contextMock.getAuthentication()).thenReturn(authentication);

		// execution of tested method
		boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
	}

	/**
	 * Wrong role name
	 */
	@Test
	public void testHasRoleInOrganization5() {
		// mocks and data initialization
		String wrongRoleName = "wrongRoleName";
		List<String> roleNames = Collections.singletonList(wrongRoleName);
		List<String> organizationNames = Collections.singletonList(ORGANIZATION_NAME);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllAncestorRoleNames(wrongRoleName)).thenReturn(roleNames);
		when(flatOrganizationManagerMock.findAllAncestorOrganizationCodes(ORGANIZATION_NAME)).thenReturn(organizationNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasRoleInOrganization(wrongRoleName, ORGANIZATION_NAME);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllAncestorRoleNames(wrongRoleName);
		verify(flatOrganizationManagerMock).findAllAncestorOrganizationCodes(ORGANIZATION_NAME);
	}

	/**
	 * Wrong organization name
	 */
	@Test
	public void testHasRoleInOrganization6() {
		// mocks and data initialization
		String wrongOrganizationName = "wrongOrganizationName";
		List<String> roleNames = Collections.singletonList(ROLE_NAME);
		List<String> organizationNames = Collections.singletonList(wrongOrganizationName);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllAncestorRoleNames(ROLE_NAME)).thenReturn(roleNames);
		when(flatOrganizationManagerMock.findAllAncestorOrganizationCodes(wrongOrganizationName)).thenReturn(organizationNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, wrongOrganizationName);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllAncestorRoleNames(ROLE_NAME);
		verify(flatOrganizationManagerMock).findAllAncestorOrganizationCodes(wrongOrganizationName);
	}

	/**
	 * Wrong role name and organization name
	 */
	@Test
	public void testHasRoleInOrganization7() {
		// mocks and data initialization
		String wrongRoleName = "wrongRoleName";
		String wrongOrganizationName = "wrongOrganizationName";
		List<String> roleNames = Collections.singletonList(wrongRoleName);
		List<String> organizationNames = Collections.singletonList(wrongOrganizationName);

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllAncestorRoleNames(wrongRoleName)).thenReturn(roleNames);
		when(flatOrganizationManagerMock.findAllAncestorOrganizationCodes(wrongOrganizationName)).thenReturn(organizationNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasRoleInOrganization(wrongRoleName, wrongOrganizationName);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllAncestorRoleNames(wrongRoleName);
		verify(flatOrganizationManagerMock).findAllAncestorOrganizationCodes(wrongOrganizationName);
	}

	/**
	 * No role names
	 */
	@Test
	public void testHasRoleInOrganization8() {
		// mocks and data initialization
		List<String> roleNames = new ArrayList<>();

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllAncestorRoleNames(ROLE_NAME)).thenReturn(roleNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllAncestorRoleNames(ROLE_NAME);
	}

	/**
	 * No organization names
	 */
	@Test
	public void testHasRoleInOrganization9() {
		// mocks and data initialization
		List<String> roleNames = Collections.singletonList(ROLE_NAME);
		List<String> organizationNames = new ArrayList<>();

		when(contextMock.getAuthentication()).thenReturn(AUTHENTICATION);
		when(flatRoleManagerMock.findAllAncestorRoleNames(ROLE_NAME)).thenReturn(roleNames);
		when(flatOrganizationManagerMock.findAllAncestorOrganizationCodes(ORGANIZATION_NAME)).thenReturn(organizationNames);

		// execution of tested method
		boolean result = defaultSecurityService.hasRoleInOrganization(ROLE_NAME, ORGANIZATION_NAME);

		// result verification
		assertFalse(result);

		verify(contextMock).getAuthentication();
		verify(flatRoleManagerMock).findAllAncestorRoleNames(ROLE_NAME);
		verify(flatOrganizationManagerMock).findAllAncestorOrganizationCodes(ORGANIZATION_NAME);
	}

}
