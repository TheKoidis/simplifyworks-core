package org.simplifyworks.security.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.hibernate.jpa.internal.metamodel.SingularAttributeImpl;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;

import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.uam.model.dto.CoreOrganizationDto;
import org.simplifyworks.uam.model.dto.CoreRoleDto;
import org.simplifyworks.uam.model.dto.CoreUserRoleOrganizationDto;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization_;
import org.simplifyworks.uam.model.entity.CoreUser_;
import org.simplifyworks.uam.service.CoreUserRoleOrganizationManager;
import org.springframework.security.core.GrantedAuthority;

/**
 * Test for {@link DefaultGrantedAuthoritiesFactory}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class DefaultGrantedAuthoritiesFactoryTest extends AbstractTest {

	@Mock
	private CoreUserRoleOrganizationManager coreUserRoleOrganizationManager;

	@InjectMocks
	private DefaultGrantedAuthoritiesFactory defaultGrantedAuthoritiesFactory = new DefaultGrantedAuthoritiesFactory();
	
	/**
	 * Initializes static meta model (normally is initialized by Hibernate at start)
	 */
	@Before
	public void initializeMetaModel() {
		CoreUser_.username = new SingularAttributeImpl<CoreUser, String>("username", String.class,
				null, null, false, false, false, null, null);
		CoreUserRoleOrganization_.user = new SingularAttributeImpl<CoreUserRoleOrganization, CoreUser>("user", CoreUser.class,
				null, null, false, false, false, null, null);
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testGetGrantedAuthorities1() {
		// mocks and data initialization
		String username = "test-user";

		String firstRoleName = "first-role";
		CoreRoleDto firstRole = new CoreRoleDto();
		firstRole.setName(firstRoleName);

		String secondRoleName = "second-role";
		CoreRoleDto secondRole = new CoreRoleDto();
		secondRole.setName(secondRoleName);

		String organizationCode = "organization";
		CoreOrganizationDto organization = new CoreOrganizationDto();
		organization.setCode(organizationCode);

		CoreUserRoleOrganizationDto firstUserRole = new CoreUserRoleOrganizationDto();
		firstUserRole.setRole(firstRole);

		CoreUserRoleOrganizationDto secondUserRole = new CoreUserRoleOrganizationDto();
		secondUserRole.setRole(secondRole);
		secondUserRole.setOrganization(organization);

		List<CoreUserRoleOrganizationDto> roles = new ArrayList<>();
		roles.add(firstUserRole);
		roles.add(secondUserRole);

		when(coreUserRoleOrganizationManager.findByUsername(username)).thenReturn(roles);

		// execution of tested method
		List<RoleOrganizationGrantedAuthority> result = defaultGrantedAuthoritiesFactory.getGrantedAuthorities(username);

		// result verification
		assertEquals(2, result.size());

		Iterator<? extends GrantedAuthority> authorityIterator = result.iterator();

		RoleOrganizationGrantedAuthority firstAuthority = (RoleOrganizationGrantedAuthority) authorityIterator.next();
		assertEquals(firstRoleName, firstAuthority.getRoleName());
		assertNull(firstAuthority.getOrganizationCode());

		RoleOrganizationGrantedAuthority secondAuthority = (RoleOrganizationGrantedAuthority) authorityIterator.next();
		assertEquals(secondRoleName, secondAuthority.getRoleName());
		assertEquals(organizationCode, secondAuthority.getOrganizationCode());

		verify(coreUserRoleOrganizationManager).findByUsername(username);
	}
	
	/**
	 * No data scenario
	 */
	@Test
	public void testGetGrantedAuthorities2() {
		// mocks and data initialization
		String username = "test-user";

		when(coreUserRoleOrganizationManager.findByUsername(username)).thenReturn(Collections.emptyList());

		// execution of tested method
		List<RoleOrganizationGrantedAuthority> result = defaultGrantedAuthoritiesFactory.getGrantedAuthorities(username);

		// result verification
		assertTrue(result.isEmpty());

		verify(coreUserRoleOrganizationManager).findByUsername(username);
	}
}
