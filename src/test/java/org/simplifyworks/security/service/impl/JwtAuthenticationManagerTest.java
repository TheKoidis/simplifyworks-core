package org.simplifyworks.security.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.model.domain.exception.ExpiredJwtUserAuthenticationException;
import org.simplifyworks.security.model.domain.exception.NoJwtUserAuthenticationException;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.springframework.security.core.Authentication;

/**
 * Test for {@link JwtAuthenticationManager}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class JwtAuthenticationManagerTest extends AbstractTest {

    private static final String CURRENT_USERNAME = "current";
    private static final String ORIGINAL_USERNAME = "original";
    private static final CorePersonDto PERSON = new CorePersonDto();
    private static final String ROLE_NAME = "testAdmin";
    private static final String ORGANIZATION_NAME = "testOrganization";

    private static final List<String> ROLES = Arrays.asList(ROLE_NAME);
    private static final Collection<RoleOrganizationGrantedAuthority> AUTHORITIES = Arrays.asList(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));

    private static final JwtUserAuthentication AUTHENTICATION = new JwtUserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, PERSON, new Date(System.currentTimeMillis() + 10000L), ROLES, AUTHORITIES);

    @InjectMocks
    private JwtAuthenticationManager manager = new JwtAuthenticationManager();

    /**
     * Happy day scenario
     */
    @Test
    @Ignore // FIXME
    public void testAuthenticate1() {    	
        // execution of tested method
        Authentication auth = manager.authenticate(AUTHENTICATION);
        // result verification
        assertEquals(auth, AUTHENTICATION);
    }

    /**
     * Null authentication
     */
    @Test(expected = NoJwtUserAuthenticationException.class)
    public void testAuthenticate2() {
        // execution of tested method
        manager.authenticate(null);
    }
    
    /**
     * Expired authentication
     */
    @Test(expected = ExpiredJwtUserAuthenticationException.class)
    public void testAuthenticate3() {
        // execution of tested method
        manager.authenticate(new JwtUserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, PERSON, new Date(System.currentTimeMillis() - 10000L), ROLES, AUTHORITIES));
    }
}
