package org.simplifyworks.security.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.security.service.SecurityService;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.service.CoreUserManager;
import org.springframework.security.crypto.password.PasswordEncoder;

public class ChangePasswordServiceImplTest extends AbstractTest{

	@Mock
	private SecurityService securityService;
	
	@Mock
	private CoreUserManager coreUserManager;
	
	@Mock
	private PasswordEncoder passwordEncoder;
	
	@InjectMocks
	private ChangePasswordServiceImpl changePasswordService = new ChangePasswordServiceImpl();
	
	/**
     * Happy day scenario
     */
    @Test
    public void testChangePassword1() {
    	// mocks and data initialization
    	String newPassword = "java";
    	String originalUsername = "duke";
    	CoreUser user = new CoreUser();
    	String encodedPassword = "CAFE BABE";
    	
    	when(securityService.getOriginalUsername()).thenReturn(originalUsername);
    	when(coreUserManager.readUser(originalUsername)).thenReturn(user);
    	when(passwordEncoder.encode(newPassword)).thenReturn(encodedPassword);
    	
        // execution of tested method
    	changePasswordService.changePassword(newPassword);
        
        // result verification
    	assertEquals(encodedPassword, new String(user.getPassword()));
    	verify(securityService).getOriginalUsername();
        verify(coreUserManager).readUser(originalUsername);
        verify(passwordEncoder).encode(newPassword);
    }
    
    /**
     * Empty password
     */
    @Test(expected = IllegalArgumentException.class)
    public void testChangePassword2() {    	
        // execution of tested method
    	changePasswordService.changePassword("");
    }
}
