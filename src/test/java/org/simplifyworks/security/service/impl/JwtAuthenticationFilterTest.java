package org.simplifyworks.security.service.impl;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.RoleOrganizationGrantedAuthority;
import org.simplifyworks.security.model.domain.exception.ExpiredJwtUserAuthenticationException;
import org.simplifyworks.security.model.domain.exception.NoJwtUserAuthenticationException;
import org.simplifyworks.uam.model.dto.CorePersonDto;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.jwt.crypto.sign.MacSigner;
import org.springframework.security.jwt.crypto.sign.SignerVerifier;
import org.springframework.test.util.ReflectionTestUtils;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Test for {@link JwtAuthenticationFilter}
 *
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class JwtAuthenticationFilterTest extends AbstractTest {

    public static final String AUTHORIZATION_HEADER = "Authorization";
    public static final String AUTHORIZATION_HEADER_VALUE_PREFIX = "Bearer ";
    public static final String AUTH_TOKEN_PARAMETER = "auth-token";

    public static final SignerVerifier verifier = new MacSigner("simplifyworks");
    public static final String token = AUTHORIZATION_HEADER_VALUE_PREFIX + "eyJhbGciOiJIUzI1NiJ9.eyJjdXJyZW50VXNlcm5hbWUiOiJhZG1pbiIsIm9yaWdpbmFsVXNlcm5hbWUiOiJhZG1pbiIsImV4cGlyYXRpb24iOiIyMDE2LTA2LTA3VDE3OjA3OjI2WiIsInJvbGVzIjpbImFkbWluIiwiYWRtaW5fc2NoZWR1bGVyIiwiY29yZV9vcmdhbml6YXRpb25fd3JpdGUiLCJjb3JlX29yZ2FuaXphdGlvbl9yZWFkIiwiY29yZV9vcmdhbml6YXRpb25fZGVsZXRlIiwiY29yZV9wZXJzb25fd3JpdGUiLCJjb3JlX3BlcnNvbl9yZWFkIiwiY29yZV9wZXJzb25fb3JnYW5pemF0aW9uX3dyaXRlIiwiY29yZV9wZXJzb25fb3JnYW5pemF0aW9uX3JlYWQiLCJjb3JlX3BlcnNvbl9vcmdhbml6YXRpb25fZGVsZXRlIiwiY29yZV9yb2xlX3dyaXRlIiwiY29yZV9yb2xlX3JlYWQiLCJjb3JlX3JvbGVfZGVsZXRlIiwiY29yZV9yb2xlX2NvbXBvc2l0aW9uX3dyaXRlIiwiY29yZV9yb2xlX2NvbXBvc2l0aW9uX3JlYWQiLCJjb3JlX3JvbGVfY29tcG9zaXRpb25fZGVsZXRlIiwiY29yZV91c2VyX3dyaXRlIiwiY29yZV91c2VyX3JlYWQiLCJjb3JlX3VzZXJfZGVsZXRlIiwiY29yZV91c2VyX3JvbGVfd3JpdGUiLCJjb3JlX3VzZXJfcm9sZV9yZWFkIiwiY29yZV91c2VyX3JvbGVfZGVsZXRlIiwid29ya2Zsb3dfYWRtaW4iLCJjb3JlX3VzZXIiLCJtxaEtcm9sZSJdLCJhdXRob3JpdGllcyI6W3sicm9sZU5hbWUiOiJhZG1pbiIsIm9yZ2FuaXphdGlvbkNvZGUiOiJERVJTIn0seyJyb2xlTmFtZSI6ImFkbWluX3NjaGVkdWxlciIsIm9yZ2FuaXphdGlvbkNvZGUiOiJERVJTIn1dfQ.1KZMwq1vx66pcGGp2TKN8N3SoFSaNzGmhpYKRj_fvVo";
    public static final String invalidToken = token + "a";

    private static final String CURRENT_USERNAME = "current";
    private static final String ORIGINAL_USERNAME = "original";
    private static final CorePersonDto PERSON = new CorePersonDto();
    private static final String ROLE_NAME = "testAdmin";
    private static final String ORGANIZATION_NAME = "testOrganization";

    private static final List<String> ROLES = Arrays.asList(ROLE_NAME);
    private static final Collection<RoleOrganizationGrantedAuthority> AUTHORITIES = Arrays.asList(new RoleOrganizationGrantedAuthority(ROLE_NAME, ORGANIZATION_NAME));

    private static final JwtUserAuthentication AUTHENTICATION = new JwtUserAuthentication(CURRENT_USERNAME, ORIGINAL_USERNAME, PERSON, new Date(), ROLES, AUTHORITIES);

    @Mock
    private SecurityContext contextMock;

    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Mock
    private FilterChain chain;

    @Mock
    private JwtAuthenticationManager authenticationManagerMock;

    @InjectMocks
    private JwtAuthenticationFilter filter = new JwtAuthenticationFilter();

    @Before
    public void init() {
        ReflectionTestUtils.setField(filter, "secret", "simplifyworks");
        ReflectionTestUtils.setField(filter, "jsonMapper", new ObjectMapper());
        SecurityContextHolder.setContext(contextMock);
    }

    /**
     * Happy day scenario (token in header)
     */
    @Test
    public void testDoFilter1() throws IOException, ServletException {
        // mocks and data initialization
        when(request.getHeader(AUTHORIZATION_HEADER)).thenReturn(token);
        when(request.getParameter(AUTH_TOKEN_PARAMETER)).thenReturn(null);
        when(authenticationManagerMock.authenticate(any(Authentication.class))).thenReturn(AUTHENTICATION);
        // execution of tested method
        filter.doFilter(request, response, chain);
        // result verification
        verify(request).getHeader(AUTHORIZATION_HEADER);
        verify(request).getParameter(AUTH_TOKEN_PARAMETER);
        verify(chain).doFilter(request, response);
        verify(contextMock).setAuthentication(AUTHENTICATION);
        verify(authenticationManagerMock).authenticate(any(Authentication.class));
    }

    /**
     * Happy day scenario (token in parameter)
     */
    @Test
    public void testDoFilter2() throws IOException, ServletException {
        // mocks and data initialization
        when(request.getHeader(AUTHORIZATION_HEADER)).thenReturn(null);
        when(request.getParameter(AUTH_TOKEN_PARAMETER)).thenReturn("eyJhbGciOiJIUzI1NiJ9.eyJjdXJyZW50VXNlcm5hbWUiOiJhZG1pbiIsIm9yaWdpbmFsVXNlcm5hbWUiOiJhZG1pbiIsImV4cGlyYXRpb24iOiIyMDE2LTA2LTA3VDE3OjA3OjI2WiIsInJvbGVzIjpbImFkbWluIiwiYWRtaW5fc2NoZWR1bGVyIiwiY29yZV9vcmdhbml6YXRpb25fd3JpdGUiLCJjb3JlX29yZ2FuaXphdGlvbl9yZWFkIiwiY29yZV9vcmdhbml6YXRpb25fZGVsZXRlIiwiY29yZV9wZXJzb25fd3JpdGUiLCJjb3JlX3BlcnNvbl9yZWFkIiwiY29yZV9wZXJzb25fb3JnYW5pemF0aW9uX3dyaXRlIiwiY29yZV9wZXJzb25fb3JnYW5pemF0aW9uX3JlYWQiLCJjb3JlX3BlcnNvbl9vcmdhbml6YXRpb25fZGVsZXRlIiwiY29yZV9yb2xlX3dyaXRlIiwiY29yZV9yb2xlX3JlYWQiLCJjb3JlX3JvbGVfZGVsZXRlIiwiY29yZV9yb2xlX2NvbXBvc2l0aW9uX3dyaXRlIiwiY29yZV9yb2xlX2NvbXBvc2l0aW9uX3JlYWQiLCJjb3JlX3JvbGVfY29tcG9zaXRpb25fZGVsZXRlIiwiY29yZV91c2VyX3dyaXRlIiwiY29yZV91c2VyX3JlYWQiLCJjb3JlX3VzZXJfZGVsZXRlIiwiY29yZV91c2VyX3JvbGVfd3JpdGUiLCJjb3JlX3VzZXJfcm9sZV9yZWFkIiwiY29yZV91c2VyX3JvbGVfZGVsZXRlIiwid29ya2Zsb3dfYWRtaW4iLCJjb3JlX3VzZXIiLCJtxaEtcm9sZSJdLCJhdXRob3JpdGllcyI6W3sicm9sZU5hbWUiOiJhZG1pbiIsIm9yZ2FuaXphdGlvbkNvZGUiOiJERVJTIn0seyJyb2xlTmFtZSI6ImFkbWluX3NjaGVkdWxlciIsIm9yZ2FuaXphdGlvbkNvZGUiOiJERVJTIn1dfQ.1KZMwq1vx66pcGGp2TKN8N3SoFSaNzGmhpYKRj_fvVo");
        when(authenticationManagerMock.authenticate(any(Authentication.class))).thenReturn(AUTHENTICATION);
        // execution of tested method
        filter.doFilter(request, response, chain);
        // result verification
        verify(request).getHeader(AUTHORIZATION_HEADER);
        verify(request).getParameter(AUTH_TOKEN_PARAMETER);
        verify(chain).doFilter(request, response);
        verify(contextMock).setAuthentication(AUTHENTICATION);
        verify(authenticationManagerMock).authenticate(any(Authentication.class));
    }

    /**
     * Invalid token in header
     */
    @Test
    public void testDoFilter3() throws IOException, ServletException {
        // mocks and data initialization
        when(request.getHeader(AUTHORIZATION_HEADER)).thenReturn(invalidToken);
        when(request.getParameter(AUTH_TOKEN_PARAMETER)).thenReturn(null);
        // execution of tested method
        filter.doFilter(request, response, chain);
        // result verification
        verify(request).getHeader(AUTHORIZATION_HEADER);
        verify(request).getParameter(AUTH_TOKEN_PARAMETER);

        verify(response).sendError(HttpServletResponse.SC_BAD_REQUEST);
    }

    /**
     * Missing token
     */
    @Test
    public void testDoFilter4() throws IOException, ServletException {
        // mocks and data initialization
        when(request.getHeader(AUTHORIZATION_HEADER)).thenReturn(null);
        when(request.getParameter(AUTH_TOKEN_PARAMETER)).thenReturn(null);
        // execution of tested method
        filter.doFilter(request, response, chain);
        // result verification
        verify(request).getHeader(AUTHORIZATION_HEADER);
        verify(request).getParameter(AUTH_TOKEN_PARAMETER);
        verify(chain).doFilter(request, response);
    }
    
    /**
     * No JwtUserAuthentication token
     */
    @Test
    public void testDoFilter5() throws IOException, ServletException {
    	 // mocks and data initialization
        when(request.getHeader(AUTHORIZATION_HEADER)).thenReturn(token);
        when(request.getParameter(AUTH_TOKEN_PARAMETER)).thenReturn(null);
        when(authenticationManagerMock.authenticate(any(Authentication.class))).thenThrow(new NoJwtUserAuthenticationException("Error occured"));
        
        // execution of tested method
        filter.doFilter(request, response, chain);
        
        // result verification
        verify(request).getHeader(AUTHORIZATION_HEADER);
        verify(request).getParameter(AUTH_TOKEN_PARAMETER);
        verify(authenticationManagerMock).authenticate(any(Authentication.class));
        verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
    
    /**
     * Expired JwtUserAuthentication token
     */
    @Test
    public void testDoFilter6() throws IOException, ServletException {
    	 // mocks and data initialization
        when(request.getHeader(AUTHORIZATION_HEADER)).thenReturn(token);
        when(request.getParameter(AUTH_TOKEN_PARAMETER)).thenReturn(null);
        when(authenticationManagerMock.authenticate(any(Authentication.class))).thenThrow(new ExpiredJwtUserAuthenticationException("Error occured"));
        
        // execution of tested method
        filter.doFilter(request, response, chain);
        
        // result verification
        verify(request).getHeader(AUTHORIZATION_HEADER);
        verify(request).getParameter(AUTH_TOKEN_PARAMETER);
        verify(authenticationManagerMock).authenticate(any(Authentication.class));
        verify(response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }
}
