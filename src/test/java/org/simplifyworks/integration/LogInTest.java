package org.simplifyworks.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import org.simplifyworks.security.model.domain.JwtUserAuthentication;
import org.simplifyworks.security.model.domain.exception.BadCredentialsException;
import org.simplifyworks.security.web.controller.rest.LogInController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.jwt.JwtHelper;
import org.testng.annotations.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

@Test
public class LogInTest extends AbstractIntegrationTest {
	
	@Autowired
	private LogInController logInController;
	
	@Test
	public void testSuccesfulLogIn() throws Exception {
		String token = JwtHelper.decode(logInController.logInUsingUsernameAndPassword(TEST_USERNAME, TEST_PASSWORD)).getClaims();
		ObjectMapper jsonMapper = new ObjectMapper();
		JwtUserAuthentication authentication = jsonMapper.readValue(token, JwtUserAuthentication.class);
		
		assertNotNull(authentication);
		
		assertEquals(TEST_USERNAME, authentication.getCurrentUsername());
		assertEquals(TEST_USERNAME, authentication.getOriginalUsername());
		
		// we do NOT want these values in JWT token
		assertNull(authentication.getRoles());
		assertNull(authentication.getRoleOrganizationAuthorities());
	}
	
	@Test(expectedExceptions = BadCredentialsException.class)
	public void testBadCredentialsLogIn() {
		logInController.logInUsingUsernameAndPassword(TEST_USERNAME, TEST_PASSWORD + "_IS_BAD");
	}
}
