package org.simplifyworks.integration;

import javax.persistence.EntityManager;

import org.simplifyworks.Application;
import org.simplifyworks.conf.model.entity.CoreAppSetting;
import org.simplifyworks.uam.model.entity.CoreOrganization;
import org.simplifyworks.uam.model.entity.CoreRole;
import org.simplifyworks.uam.model.entity.CoreUser;
import org.simplifyworks.uam.model.entity.CoreUserRoleOrganization;
import org.simplifyworks.uam.service.CoreFlatOrganizationManager;
import org.simplifyworks.uam.service.CoreFlatRoleManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * Base class for integration tests.
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
@SpringApplicationConfiguration(classes = { Application.class })
@IntegrationTest("server.port:0")
@ActiveProfiles("test")
@TestExecutionListeners(inheritListeners = false, listeners = {
		TransactionalTestExecutionListener.class,
		DependencyInjectionTestExecutionListener.class,
		DirtiesContextTestExecutionListener.class })
@Transactional
@TransactionConfiguration(defaultRollback = false)
@Test(enabled = false)
public abstract class AbstractIntegrationTest extends AbstractTestNGSpringContextTests {

	private static final Logger LOG = LoggerFactory.getLogger(AbstractIntegrationTest.class);
	
	public static final String TEST_APP_SETTING_KEY = "java";
	public static final String TEST_APP_SETTING_VALUE = "CAFEBABE";
	
	public static final String TEST_USERNAME = "test";
	public static final String TEST_PASSWORD = "pass";
	
	public static final String TEST_ROLE_NAME = "test-role";
	public static final String TEST_ROLE_DESCRIPTION = "Role for testing";
	
	public static final String TEST_ORGANIZATION_CODE = "test-org";
	public static final String TEST_ORGANIZATION_NAME = "Organization for testing";
	public static final String TEST_ORGANIZATION_ABBREVIATION = "test-org";
	
	@Autowired
	protected EntityManager em;
	
	@Autowired
	private CoreFlatRoleManager flatRoleManager;
	
	@Autowired
	private CoreFlatOrganizationManager flatOrganizationManager;

	@BeforeMethod
	public void prepareDatabase() throws Exception {
		if (em.createQuery("FROM CoreAppSetting").getResultList().isEmpty()) {
			LOG.info("Initializing database data ...");
			
			CoreAppSetting appSetting = new CoreAppSetting(TEST_APP_SETTING_KEY, TEST_APP_SETTING_VALUE);
			em.persist(appSetting);

			CoreUser user = new CoreUser(TEST_USERNAME, new BCryptPasswordEncoder().encode(TEST_PASSWORD).toCharArray());
			em.persist(user);

			CoreRole role = new CoreRole();
			role.setName(TEST_ROLE_NAME);
			role.setDescription(TEST_ROLE_DESCRIPTION);

			em.persist(role);

			CoreOrganization organization = new CoreOrganization();
			organization.setCode(TEST_ORGANIZATION_CODE);
			organization.setName(TEST_ORGANIZATION_NAME);
			organization.setAbbreviation(TEST_ORGANIZATION_ABBREVIATION);

			em.persist(organization);

			CoreUserRoleOrganization userRoleOrganization = new CoreUserRoleOrganization();
			userRoleOrganization.setUser(user);
			userRoleOrganization.setRole(role);
			userRoleOrganization.setOrganization(organization);

			em.persist(userRoleOrganization);
			
			// we have to rebuild role/organization flat structures
			flatRoleManager.rebuild();
			flatOrganizationManager.rebuild();
			
			LOG.info("Database data initialized");
		}
	}
}
