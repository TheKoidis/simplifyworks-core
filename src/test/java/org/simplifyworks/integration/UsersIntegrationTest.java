package org.simplifyworks.integration;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import org.simplifyworks.core.model.domain.ExtendedArrayList;
import org.simplifyworks.uam.model.dto.CoreUserDto;
import org.simplifyworks.uam.model.dto.CoreUserFilterDto;
import org.simplifyworks.uam.web.controller.rest.CoreUserController;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

@Test
public class UsersIntegrationTest extends AbstractIntegrationTest {

	@Autowired
	private CoreUserController userController;
	
	@Test
	public void testSearching() {
		CoreUserFilterDto filter = new CoreUserFilterDto();
		
		ExtendedArrayList<CoreUserDto> users = userController.getPersons(filter);
		
		assertFalse(users.isEmpty());
		
		filter.setUsername(TEST_USERNAME + "_IS_BAD");
		
		users = userController.getPersons(filter);
		
		assertEquals(0, users.size());
		assertEquals(0, users.total());
		
		filter.setUsername(TEST_USERNAME);
		
		users = userController.getPersons(filter);
		
		assertEquals(1, users.size());
		assertEquals(1, users.total());
	}
}
