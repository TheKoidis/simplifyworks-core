/**
 * 
 */
package org.simplifyworks.ecm.service.config;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.service.ReportManager;
import org.simplifyworks.ecm.service.impl.EmbeddedJasperReportManager;
import org.simplifyworks.ecm.service.impl.StandaloneJasperReportManager;
import org.springframework.test.util.ReflectionTestUtils;

/**
 * Test for {@link JasperReportConfig}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class JasperReportConfigTest extends AbstractTest {

	private JasperReportConfig jasperReportConfig = new JasperReportConfig();

	/**
	 * Standalone scenario
	 */
	@Test
	public void testPrepareManager1() {
		// mocks and data initialization
		ReflectionTestUtils.setField(jasperReportConfig, "type", JasperReportConfig.STANDALONE);

		String url = "url";
		ReflectionTestUtils.setField(jasperReportConfig, "url", url);

		String username = "username";
		ReflectionTestUtils.setField(jasperReportConfig, "username", username);

		String password = "password";
		ReflectionTestUtils.setField(jasperReportConfig, "password", password);

		String dataBaseDir = "data";
		ReflectionTestUtils.setField(jasperReportConfig, "dataBaseDir", dataBaseDir);

		String reportBaseDir = "report";
		ReflectionTestUtils.setField(jasperReportConfig, "reportBaseDir", reportBaseDir);

		// execution of tested method
		ReportManager reportManager = jasperReportConfig.prepareManager();

		// result verification
		assertTrue(reportManager instanceof StandaloneJasperReportManager);

		assertEquals(ReflectionTestUtils.getField(reportManager, "url"), url);
		assertEquals(ReflectionTestUtils.getField(reportManager, "username"), username);
		assertEquals(ReflectionTestUtils.getField(reportManager, "password"), password);
		assertEquals(ReflectionTestUtils.getField(reportManager, "dataBaseDir"), dataBaseDir);
		assertEquals(ReflectionTestUtils.getField(reportManager, "reportBaseDir"), reportBaseDir);
	}

	/**
	 * Embedded scenario
	 */
	@Test
	public void testPrepareManager2() {
		// mocks and data initialization
		ReflectionTestUtils.setField(jasperReportConfig, "type", JasperReportConfig.EMBEDDED);

		String dataBaseDir = "data";
		ReflectionTestUtils.setField(jasperReportConfig, "dataBaseDir", dataBaseDir);

		String reportBaseDir = "report";
		ReflectionTestUtils.setField(jasperReportConfig, "reportBaseDir", reportBaseDir);

		// execution of tested method
		ReportManager reportManager = jasperReportConfig.prepareManager();

		// result verification
		assertTrue(reportManager instanceof EmbeddedJasperReportManager);

		assertEquals(ReflectionTestUtils.getField(reportManager, "dataBaseDir"), dataBaseDir);
		assertEquals(ReflectionTestUtils.getField(reportManager, "reportBaseDir"), reportBaseDir);
	}

	/**
	 * Configuration mismatch scenario
	 */
	@Test
	public void testPrepareManager3() {
		// mocks and data initialization
		ReflectionTestUtils.setField(jasperReportConfig, "type", "foo");

		try {
			// execution of tested method
			jasperReportConfig.prepareManager();

			fail();
		} catch (CoreException e) {
			// result verification
			assertNotNull(e);
		}
	}
}
