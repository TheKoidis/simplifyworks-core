/**
 * 
 */
package org.simplifyworks.ecm.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.File;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.simplifyworks.AbstractTest;
import org.simplifyworks.core.exception.CoreException;
import org.simplifyworks.ecm.domain.Report;
import org.simplifyworks.ecm.domain.ReportType;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestOperations;

/**
 * Test for {@link StandaloneJasperReportManager}
 * 
 * @author Štěpán Osmík (osmik@ders.cz)
 */
public class StandaloneJasperReportManagerTest extends AbstractTest {

	private static final String url = "http://localhost:8081/jasperserver";
	
	private static final String username = "jasperuser";
	private static final String password = "jasperpassword";
	
	private static final String dataBaseDir = System.getProperty("java.io.tmpdir") + File.separator + "data";
	private static final String reportBaseDir = System.getProperty("java.io.tmpdir") + File.separator + "report";
	
	@Mock
	private RestOperations restMock;
	
	@InjectMocks
	private StandaloneJasperReportManager jasperReportManager;
	
	@Before
	public void initProperties() {
		jasperReportManager.setUrl(url);
		
		jasperReportManager.setUsername(username);
		jasperReportManager.setPassword(password);
		
		jasperReportManager.setDataBaseDir(dataBaseDir);
		jasperReportManager.setReportBaseDir(reportBaseDir);
		
		jasperReportManager.cleanDirs();
	}
	
	/**
	 * Happy day scenario
	 */
	@Test
	public void testGenerateReport1() {
		// mocks and data initialization
		String name = "Test";
		ReportType type = ReportType.PDF;
		Object object = createDataObject();
		byte[] report = new byte[10];
		
		when(restMock.exchange(any(String.class), eq(HttpMethod.GET), any(HttpEntity.class), eq(byte[].class))).thenReturn(new ResponseEntity<byte[]>(report, HttpStatus.OK));
		
		// execution of tested method
		Report result = jasperReportManager.generateReport(name, type, object);
					
		// result verification
		assertNotNull(result);
		assertEquals(name, result.getName());
		assertEquals(type, result.getType());
		assertNotNull(result.getDataUri());
		assertNotNull(result.getReportUri());
		
		ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
		verify(restMock).exchange(urlCaptor.capture(), eq(HttpMethod.GET), any(HttpEntity.class), eq(byte[].class));
		
		assertTrue(urlCaptor.getValue().startsWith(url));
	}
	
	/**
	 * Error in REST communication scenario
	 */
	@Test
	public void testGenerateReport2() {
		// mocks and data initialization
		String name = "Test";
		ReportType type = ReportType.HTML;
		Object object = createDataObject();
		
		when(restMock.exchange(any(String.class), eq(HttpMethod.GET), any(HttpEntity.class), eq(byte[].class))).thenThrow(new RestClientException("Something is wrong"));
		
		try {
			// execution of tested method
			jasperReportManager.generateReport(name, type, object);
			
			fail();
		} catch(CoreException e) {
			// result verification			
			ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
			verify(restMock).exchange(urlCaptor.capture(), eq(HttpMethod.GET), any(HttpEntity.class), eq(byte[].class));
			
			assertTrue(urlCaptor.getValue().startsWith(url));
		}
	}
	
	private Data createDataObject() {
		Data data = new Data();
		
		data.persons.add(new Person("John", "England"));
		data.persons.add(new Person("Jan", "Czech Republic"));
		
		return data;
	}
}
